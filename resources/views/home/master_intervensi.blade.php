<div class="row g-0 light mb-2">
    <div class="col-sm-12 col-lg-4">
        <a href="#detailIntervensiSwk" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/swk.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">SWK</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Sentra Wisata Kuliner</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_swk }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailSkgModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/skg.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">SKG</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Surabaya Kriya Galery</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_skg }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailPasarModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/pasar.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pasar</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_pasar }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row g-0 light mb-2">
    <div class="col-sm-12 col-lg-4">
        <a href="#detailTokelModel" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/tokel.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Peken</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Pemberdayaan Ekonomi lan
                                Ketahanan
                                Ekonomi Nang
                                Suroboyo</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_tokel }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailPelatihan" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/pelatihan.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pelatihan</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_pelatihan }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailPameranModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/pameran.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pameran</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_pameran }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row g-0 light mb-2">
    <div class="col-sm-12 col-lg-4">
        <a href="#detailBPUMModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/industri-rumah.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">BPUM</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Bantuan Produktif Usaha Mikro
                            </h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_bpum }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailRumahKreatif" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/kreatif.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Rumah Kreatif</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_rk }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailPadatKaryaModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/padat-karya.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Padat Karya</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_padatkarya }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>
<div class="row g-0 light mb-2">
    <div class="col-sm-12 col-lg-4">
        <a href="#detailKurModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/kur.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Kur</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Kredit Usaha Rakyat</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_kur }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailCsrModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/csr.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Csr</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Corporate Social Responsibility
                            </h3>

                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_csr }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-12 col-lg-4 ps-lg-2">
        <a href="#detailPuspitaModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/puspita.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Puspita</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Pinjaman Usaha Mikro Kecil
                                Menengah
                                (UMKM) Surabaya
                                Pasti Tangguh</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_puspita }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

<div class="row g-0 light mb-4">
    <div class="col-sm-6 col-lg-6">
        <a href="#detailBelumIntervensiModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                {{-- <div class="card-body d-flex justify-content-center text-center">
                    <!-- <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                <img src="{{ asset('dist/assets/img/icons/kur.png') }}" alt="" width="70">
                                                                                                                                                                                                                                                                                                                                                                                                                                                                            </div> -->
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2 text-uppercase fs-3">Belum Intervensi</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-2">
                                {{ $data['total_intervensi'][0]->jumlah_belum_taging }}
                            </h3>
                        </div>
                    </div>
                </div> --}}
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/belum_intervensi.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Belum Intervensi</h3>
                            {{-- <h3 class="fw-medium" style="font-size: 10px;">Online Single Submission</h3> --}}
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_belum_taging }}

                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 ps-lg-2">
        <a href="#detail_oss_modal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
            <div class="card border h-100 border-success border-5">
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <img src="{{ asset('dist/assets/img/icons/oss.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">OSS</h3>
                            <h3 class="fw-medium" style="font-size: 10px;">Online Single Submission</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                {{ $data['total_intervensi'][0]->jumlah_oss }}
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>