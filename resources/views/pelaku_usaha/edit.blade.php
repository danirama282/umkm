<x-templates.default>
    <x-slot name="title">Entri Data Pelaku Usaha</x-slot>

    <!-- Konten baris keempat -->
    {{-- {{ dd($data['pelaku_usaha']->peken_tokel) }} --}}
    @hasanyrole('superadmin|admin')
    @include('pelaku_usaha.edit_admin')
    @else
    @include('pelaku_usaha.edit_user')
    @endrole


    <meta name="csrf-token" content="{{ csrf_token() }}">

    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(".js-select2").select2({
                theme: "bootstrap-5",
                width: "100%",
            });

            $('#submit').attr('disabled', true); //disable input

            $('#chck_submit').click(function () {
                //check if checkbox is checked
                if ($(this).is(':checked')) {

                    $('#submit').removeAttr('disabled'); //enable input

                } else {
                    $('#submit').attr('disabled', true); //disable input
                }
            });
            /**/
            $('#nik').on('input', function () {
                this.value = this.value.replace(/[^0-9]/g, '');
            });

            $('#nik').attr('readonly', true);


            getKelurahanDomisili();
            getKelurahanUsaha();
            getDataKelurahan();
            submit();
            submitKecKel();
            checkLengthNIK();
            filterpadatKaryaPeken();
            domisiliKtp();
            getDataKabupaten();
            getDataKecamatan();
            renderKabupaten();
            renderKelurahan();
            cekNik();

            var is_surabaya = "{{$data['pelaku_usaha']->is_surabaya}}"
            if (is_surabaya == 1) {
                $('#kabupaten_kota').attr('readonly', true);
                $('#provinsi').attr('readonly', true);
            } else {
                $('#provinsi').removeAttr('readonly', true);
                $('#kabupaten_kota').removeAttr('readonly', true);
            }

        });

        function isSby() {
            // var prov = document.getElementById('provinsi');
            // prov.value = 35;
            $('#provinsi').val({{$data['provinsi'] -> id_m_setup_prop}});
            // $('#kabupaten_kota').val({{ $data['kabupaten'][0]->id_m_setup_kab}});
            // $('#provinsi').trigger('change');
            // $('#kabupaten_kota').trigger('change');
            document.getElementById("provinsi").value = {{$data['provinsi'] -> id_m_setup_prop}};
            // document.getElementById("kabupaten_kota").value = {{ $data['kabupaten'][0]->id_m_setup_kab}};
            // document.getElementById("kecamatan").value = 78;
            var id_kab = {{$data['kabupaten'][0] -> id_m_setup_kab}};
            var id_prov = $('#provinsi').val();

            renderKabupaten(id_prov, id_kab, null);
            renderProvinsi(id_prov, id_kab);
            $('#kabupaten_kota').attr('readonly', true);
            $('#provinsi').attr('readonly', true);
        }

        function isNonSby() {
            $('#provinsi').removeAttr('readonly', true);
            $('#kabupaten_kota').removeAttr('readonly', true);
        }

        function renderProvinsi(id_prov, id_kab) {

            var provinsi = <?php echo json_encode($data['provinsi_non_sby']); ?>;
            let render = '';
            let renderNama = '';

            provinsi.forEach(function (item) {
                if (item.id_m_setup_prop == id_prov) {

                    render +=`<option value="${item.id_m_setup_prop}" value-name="${item.nm_prop}" selected>${item.nm_prop}</option>`;
                } else {
                    render +=`<option value="${item.id_m_setup_prop}" value-name="${item.nm_prop}">${item.nm_prop}</option>`;
                }
            });
            $('#provinsi').html(render);
            // $('#provinsi').val(renderNama);
            // var id_kec_new = $('#kecamatan').val();
            renderKabupaten(id_prov, id_kab, params = null);
        }

        function renderKabupaten(id_prov, id_kab, params) {
            var kabupaten_pemilik = "{{$data['kabupaten_pemilik'][0]->id_m_setup_kab ?? null}}"
            if (params) {
                $.ajax({
                        type: "POST",
                        url: "{{ route('cari_kabupaten') }}",
                        dataType: "json",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            id_m_setup_prop: $('[name="provinsi"]').val(),
                        }
                    })
                    .done(function (response) {
                        let render = '';
                        // $('#kabupaten_kota').empty();
                        response.data.forEach(function (item) {
                            if (item.id_m_setup_kab == params) {
                                // var id_kab = item.id_m_setup_kab;
                                // var id_prov = item.id_m_setup_prop;
                                render +=`<option value="${item.id_m_setup_kab}" selected>${item.nm_kab}</option>`;
                                // renderKecamatan(id_kab, id_prov);
                            } else {
                                render +=`<option value="${item.id_m_setup_kab}">${item.nm_kab}</option>`;
                            }
                        });

                        $('#kabupaten_kota').html(render);

                    })
                    .fail(function (xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Eror, Mohon Hubungi Admin',
                            text: response.message,
                            footer: 'Error code: ' + xhr.status
                        });
                    });
            } else if (id_prov, id_kab) {
                $.ajax({
                    url: "{{ route('cari_kabupaten') }}",
                    type: "POST",
                    dataType: "json",
                    data: {
                        id_m_setup_prop: $('[name="provinsi"]').val(),
                        _token: "{{ csrf_token() }}",
                    },
                    success: function (data) {
                        var render = '<option value="" disabled>Pilih :</option>';
                        /* for (var i = 0; i < data.data.length; i++) {
                            if (data.data[i].id_m_setup_kab == id_kab) {
                                console.log(data.data[i],'masuk if');
                                render += '<option value="' + data.data[i].id_m_setup_kab +'" selected>' + data.data[i].nm_kab + '</option>';
                            } else {
                                console.log('masuk else');
                                render += '<option value="' + data.data[i].id_m_setup_kab +'">' + data.data[i].nm_kab + '</option>';
                            }
                        } */
                        data.data.forEach(function (data) {
                            if (data.id_m_setup_kab == id_kab) {
                                render +=`<option value="${data.id_m_setup_kab}" data-name="${data.nm_kab}" selected>${data.nm_kab}</option>`;
                                renderNama = data.nm_kab;
                            } else {
                                render +=`<option value="${data.id_m_setup_kab}" data-name="${data.nm_kab}">${data.nm_kab}</option>`;
                            }
                        });
                        $('[name="kabupaten_kota"]').html(render);

                        // Menghilangkan atribut disabled pada select2 kelurahan
                        // $('select[name="kabupaten_kota"]').prop('disabled', false);
                    }
                });
                renderKecamatan(id_prov, id_kab, null);
            } else {
                $.ajax({
                        type: "POST",
                        url: "{{ route('cari_kabupaten') }}",
                        dataType: "json",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            id_m_setup_prop: $('[name="provinsi"]').val(),
                        }
                    })
                    .done(function (response) {
                        var render = '<option value="" disabled>Pilih :</option>';
                        response.data.forEach(function (item) {
                            if (item.id_m_setup_kab == kabupaten_pemilik) {
                                // var id_kab = item.id_m_setup_kab;
                                // var id_prov = item.id_m_setup_prop;
                                render +=`<option value="${item.id_m_setup_kab}" selected>${item.nm_kab}</option>`;
                                // renderKecamatan(id_kab, id_prov);
                            } else {
                                render +=`<option value="${item.id_m_setup_kab}">${item.nm_kab}</option>`;
                            }
                        });
                        $('#kabupaten_kota').html(render);
                    })
                    .fail(function (xhr, status, error) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Eror, Mohon Hubungi Admin',
                            text: response.message,
                            footer: 'Error code: ' + xhr.status
                        });
                    });
            }

        }

        function renderKecamatan(id_prov, id_kab, params) {
            var kecamatan = <?php echo json_encode($data['kecamatan']); ?>;
            let render = '';
            let renderNama = '';


            if (id_kab) {
                kecamatan.forEach(function (item) {
                    // render += `<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                    var id_kec = item.id_m_setup_kec;
                    if (item.id_m_setup_kec == 28) {
                        render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}" selected>${item.nm_kec}</option>`;
                        // renderNama = `"${item.nm_kec}"`;
                        renderNama = item.nm_kec;
                    } else {
                        render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                    }
                });
            } else {
                kecamatan.forEach(function (item) {
                    // render += `<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                    var id_kec = item.id_m_setup_kec;

                    if (item.id_m_setup_kec == params) {
                        render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}" selected>${item.nm_kec}</option>`;
                        // renderNama = `"${item.nm_kec}"`;
                        renderNama = item.nm_kec;
                    } else {
                        render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                    }
                });
            }
            $('#kecamatan').html(render);
            $('#kecamatan_string').val(renderNama);
            var id_kec_new = $('#kecamatan').val();
            renderKelurahan(id_prov, id_kab, id_kec_new)
        }

        function renderKelurahan(id_prov, id_kab, id_kec, params = null) {
            // $('#kelurahan').empty();
            var kelurahan_pemilik = "{{$data['kelurahan_pemilik'][0]->id_m_setup_kel ?? null}}"
            // console.log(kelurahan_pemilik);
            var id_m_setup_prop = $('#provinsi').val();
            var id_m_setup_kab = $('#kabupaten_kota').val();
            var id_m_setup_kec = $('#kecamatan').val();

            if (id_kab, id_prov, id_kec) {
                $.ajax({
                        type: "POST",
                        url: "{{ route('cari_kelurahan') }}",
                        dataType: "json",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            id_m_setup_kec: id_kec,
                            id_m_setup_prop: id_prov,
                            id_m_setup_kab: id_kab
                        }
                    })
                    .done(function (response) {
                        if (response.status == 'success') {
                            var render = '<option value="" disabled>Pilih :</option>';
                            let renderNama = '';
                            response.data.forEach(function (item) {
                                if (item.id_m_setup_kel == params) {
                                    render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}" selected>${item.nm_kel}</option>`;
                                    renderNama = item.nm_kel;
                                } else {
                                    render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}">${item.nm_kel}</option>`;
                                }
                            });
                            $('#kelurahan').html(render);
                            $('#kelurahan_string').val(renderNama);
                            $('select[name="kelurahan"]').prop('disabled', false);

                        } else {
                            /* Swal fire */
                            Swal.fire({
                                icon: 'error',
                                title: 'Eror, Mohon Hubungi Admin',
                                text: response.message,
                            });
                        }
                    })
            } else {
                $.ajax({
                        type: "POST",
                        url: "{{ route('cari_kelurahan') }}",
                        dataType: "json",
                        data: {
                            '_token': "{{ csrf_token() }}",
                            id_m_setup_kec: id_m_setup_kec,
                            id_m_setup_prop: id_m_setup_prop,
                            id_m_setup_kab: id_m_setup_kab
                        }
                    })
                    .done(function (response) {
                        if (response.status == 'success') {
                            var render = '<option value="" disabled>Pilih :</option>';
                            let renderNama = '';
                            response.data.forEach(function (item) {
                                if (item.id_m_setup_kel == kelurahan_pemilik) {
                                    render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}" selected>${item.nm_kel}</option>`;
                                    renderNama = item.nm_kel;
                                } else {
                                    render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}">${item.nm_kel}</option>`;
                                }
                            });

                            $('#kelurahan').html(render);
                            $('#kelurahan_string').val(renderNama);
                            $('select[name="kelurahan"]').prop('disabled', false);

                        } else {
                            /* Swal fire */
                            Swal.fire({
                                icon: 'error',
                                title: 'Eror, Mohon Hubungi Admin',
                                text: response.message,
                            });
                        }
                    })
            }
        }

        function cekNik() {
            $('#cek_nik').click(function (e) {
                e.preventDefault();
                // var NIK_PEMOHON = $(this).val();
                var NIK_PEMOHON = $('#nik').val();

                if (NIK_PEMOHON.length == '16') {
                    var input = $("input[name=nik]").val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                .attr('content')
                        }
                    });
                    $.ajax({
                        /* Memanggil route pelaku_usaha.cek_nik */
                        url: "{{ route('pelaku_usaha.cek_nik') }}",
                        type: 'POST',
                        data: {
                            nik: input,
                            status: 'update'
                        },
                        success: function (response) {
                            if (response.message == "OK") {
                                $('#is_surabaya').val('1');
                                $('#nik').val(response.nik);
                                $('#nama_pelaku_usaha').val(response
                                    .nama_lengkap);
                                $('#jenis_kelamin').val(response.jenis_kelamin);
                                $('#tempat_lahir').val(response.tempat_lahir);
                                $('#alamat_ktp').val(response.alamat);
                                $('#rt_ktp').val(response.rt);
                                $('#rw_ktp').val(response.rw);
                                $('#tanggal_lahir').val(response.tgl_lahir);
                                $('#provinsi').val(35);
                                renderKabupaten(78);
                                renderKecamatan(response.id_kecamatan);
                                // renderKelurahan(response.id_kelurahan);
                                domisiliKtp(response.id_kecamatan, response.id_kelurahan);
                                $(':input[type="submit"]').removeAttr(
                                    'readonly');

                            } else {
                                swal.fire('Peringatan!',
                                    response.error,
                                    "error");
                                $('#is_surabaya').val('1');
                                $('#nik').val('');
                                $('#nama_pelaku_usaha').val('');
                                $('#alamat_ktp').val('');
                                $('#kecamatan').val('');
                                $('#kelurahan').val('');
                                $('#rt_ktp').val('');
                                $('#rw_ktp').val('');
                                $('#tanggal_lahir').val('');
                                $('#nama_pelaku_usaha').removeAttr('readonly');
                                $('#alamat_ktp').removeAttr('readonly');
                                $('#kecamatan').removeAttr('readonly');
                                $('#kelurahan').removeAttr('readonly');
                                $('#provinsi_pemilik').val('');
                                $('#kabupaten_pemilik').val('');
                                $('#kecamatan_pemilik').val('');
                                $('#kelurahan_pemilik').val('');
                                $('#rt_ktp').removeAttr('readonly');
                                $('#rw_ktp').removeAttr('readonly');
                                $('#tanggal_lahir').removeAttr('readonly');
                                $(':input[type="submit"]').prop('disabled',
                                    true);
                            }
                        },
                        error: function () {
                            swal.fire("Telah terjadi kesalahan pada sistem",
                                "Mohon refresh halaman browser Anda",
                                "error");
                        }
                    })
                }
            });
        }

        function getKelurahanDomisili() {
            $('[name="kecamatan_domisili"]').on('change', function () {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {


                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel +'">' + data.data[i].nm_kel + '</option>';
                            }

                            $('[name="kelurahan_domisili"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan_domisili"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan_domisili"]').empty();
                }
            });


            var kelurahan = <?php echo json_encode($data['kelurahan_domisili'][0]); ?>;
            var id_m_setup_kec = $('[name="kecamatan_domisili"]').val();
            $.ajax({
                url: "{{ route('getKelurahan') }}",
                type: "POST",
                dataType: "json",
                data: {
                    id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                    id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                    id_m_setup_kec: id_m_setup_kec,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {

                    // melakukan foreach data dari kelurahan ke tampilan option
                    var render = '<option value="" disabled>Pilih :</option>';

                    for (var i = 0; i < data.data.length; i++) {


                        if (data.data[i].nm_kel == kelurahan.nm_kel) {
                            render += '<option value="' + data.data[i].id_m_setup_kel + '" selected>' +data.data[i].nm_kel + '</option>';
                        } else {
                            render += '<option value="' + data.data[i].id_m_setup_kel +'">' + data.data[i].nm_kel + '</option>';
                        }

                    }

                    $('[name="kelurahan_domisili"]').html(render);

                    // Menghilangkan atribut disabled pada select2 kelurahan
                    // $('select[name="kelurahan_usaha"]').prop('disabled', false);
                }
            });

        }

        function getKelurahanUsaha() {
            var kelurahan = <?php echo json_encode($data['kelurahan_usaha'][0]); ?>;
            var id_m_setup_kec = $('[name="kecamatan_usaha"]').val();
            $.ajax({
                url: "{{ route('getKelurahan') }}",
                type: "POST",
                dataType: "json",
                data: {
                    id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                    id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                    id_m_setup_kec: id_m_setup_kec,
                    _token: "{{ csrf_token() }}",
                },
                success: function (data) {

                    // melakukan foreach data dari kelurahan ke tampilan option
                    var render = '<option value="" disabled>Pilih :</option>';

                    for (var i = 0; i < data.data.length; i++) {


                        if (data.data[i].nm_kel == kelurahan.nm_kel) {
                            render += '<option value="' + data.data[i].id_m_setup_kel + '" selected>' +data.data[i].nm_kel + '</option>';
                        } else {render += '<option value="' + data.data[i].id_m_setup_kel +'">' + data.data[i].nm_kel + '</option>';
                        }

                    }

                    $('[name="kelurahan_usaha"]').html(render);

                    // Menghilangkan atribut disabled pada select2 kelurahan
                    // $('select[name="kelurahan_usaha"]').prop('disabled', false);
                }
            });


            // $('select[name="kelurahan_usaha"]').prop('disabled', true);
            $('[name="kecamatan_usaha"]').on('change', function () {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {


                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';
                            // var render = '';
                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel +'">' + data.data[i].nm_kel + '</option>';
                            }

                            $('[name="kelurahan_usaha"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan_usaha"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan_usaha"]').empty();
                }
            });

        }

        function getDataKelurahan() {
            // $('select[name="kelurahan"]').prop('disabled', true);

            $('[name="kecamatan"]').on('change', function () {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('cari_kelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="provinsi"]').val(),
                            id_m_setup_kab: $('[name="kabupaten_kota"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {


                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {render += '<option value="' + data.data[i].id_m_setup_kel + '">' +data.data[i].nm_kel + '</option>';
                            }

                            $('[name="kelurahan"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan"]').empty();
                }
            });
        }

        function getDataKabupaten() {
            // $('select[name="kabupaten_kota"]').prop('disabled', true);

            $('[name="provinsi"]').on('change', function () {
                var id_m_setup_prop = $('#provinsi').val();
                if (id_m_setup_prop) {
                    $.ajax({
                        url: "{{ route('cari_kabupaten') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="provinsi"]').val(),
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {render += '<option value="' + data.data[i].id_m_setup_kab + '">' +data.data[i].nm_kab + '</option>';
                            }

                            $('[name="kabupaten_kota"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kabupaten_kota"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kabupaten_kota"]').empty();
                }
            });
        }

        function getDataKecamatan() {
            $('[name="kabupaten_kota"]').on('change', function () {
                var id_m_setup_kab = $('#kabupaten_kota').val();
                var id_m_setup_prop = $('#provinsi').val();
                if (id_m_setup_kab) {
                    $.ajax({
                        url: "{{ route('cari_kecamatan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_kab: id_m_setup_kab,
                            id_m_setup_prop: id_m_setup_prop,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {
                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kec +'">' + data.data[i].nm_kec + '</option>';
                            }

                            $('[name="kecamatan"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kecamatan"]').prop('disabled', false);
                        }
                    })
                } else {
                    $('select[name="kecamatan"]').empty();
                }
            })
        }

        function submit() {
            $('#formTambahPelakuUsaha').submit(function (e) {
                e.preventDefault();
                Swal.fire({
                    icon: 'warning',
                    title: 'Periksa Kembali Inputan Anda',
                    text: 'Apakah Anda yakin data yang Anda masukkan sudah benar?',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Lanjutkan!',
                    cancelButtonText: 'Tidak, Periksa Kembali'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Jika dikonfirmasi, lanjutkan ke langkah selanjutnya
                        $.ajax({
                            url: "{{ route('pelaku_usaha.update', ['id' => Crypt::encryptString($data['pelaku_usaha']->id_t_pelaku_usaha_disdag_181022)]) }}",
                            type: "POST",
                            dataType: "json",
                            data: $(this).serialize(),
                            success: function (data) {
                                if (data.status == true) {
                                    Swal.fire({
                                        title: 'Data Berhasil Dirubah',
                                        html: data.message,
                                        icon: 'success',
                                        // confirmButtonText: 'OK',
                                        showConfirmButton: false,
                                        timer: 1500,
                                        timerProgressBar: true,
                                    }).then((result) => {
                                        if (result.dismiss === Swal.DismissReason
                                            .timer) {
                                            location.reload();
                                        }
                                    })
                                } else {
                                    Swal.fire({
                                        title: 'Gagal',
                                        html: data.message,
                                        icon: 'error',
                                        confirmButtonText: 'OK'
                                    })
                                }
                            },
                            error: function () {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan!',
                                    html: '<h6 class="font-sans-serif fs--1">Terjadi Kesalahan Teknis, Silahkan Refresh Kembali Browser Anda!</h6>',
                                    confirmButtonText: 'OK',
                                    // showConfirmButton: false,
                                    // timer: 1500,
                                    // timerProgressBar: true,
                                })
                                // .then((result) => {
                                //     console.log(result);
                                //     if (result.dismiss === Swal.DismissReason.timer) {
                                //         console.log('I was closed by the timer')
                                //         location.reload();
                                //     }
                                // })
                            }
                        });
                    }
                });
            });
        }

        function submitKecKel() {
            $('#formEditKecKel').submit(function (e) {
                e.preventDefault();
                Swal.fire({
                    icon: 'warning',
                    title: 'Periksa Kembali Inputan Anda',
                    text: 'Apakah Anda yakin data yang Anda masukkan sudah benar?',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Lanjutkan!',
                    cancelButtonText: 'Tidak, Periksa Kembali'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // submitData(); // If confirmed, submit the data
                        $.ajax({
                            url: "{{ route('pelaku_usaha.update_keckel', ['id' => Crypt::encryptString($data['pelaku_usaha']->id_t_pelaku_usaha_disdag_181022)]) }}",
                            type: "POST",
                            dataType: "json",
                            data: $(this).serialize(),
                            success: function (data) {
                                if (data.status == true) {
                                    Swal.fire({
                                        title: 'Berhasil',
                                        html: data.message,
                                        icon: 'success',
                                        // confirmButtonText: 'OK',
                                        showConfirmButton: false,
                                        timer: 1500,
                                        timerProgressBar: true,
                                    }).then((result) => {
                                        if (result.dismiss === Swal.DismissReason
                                            .timer) {
                                            location.reload();
                                        }
                                    })
                                } else {
                                    Swal.fire({
                                        title: 'Gagal',
                                        html: data.message,
                                        icon: 'error',
                                        confirmButtonText: 'OK'
                                    })
                                }
                            }
                        });
                    }
                });
            });
        }

        function checkLengthNIK() {
            var nik = $('#nik').val();

            if (nik.length > 16) {
                $('#nik').val(nik.substring(0, 16));
            }
        }

        function filterpadatKaryaPeken() {
            var id_pelaku_usaha = $('#id_pelaku_usaha').val();
            var alamat = $('#alamat_ktp').val();
            var kec = $('#kecamatan').val();
            var kel = $('#kelurahan').val();
            // $('#nik').attr('readonly', true);
            // $('#nama_pelaku_usaha').attr('readonly', true);
            // $('#alamat_ktp').attr('readonly', true);
            // $('#kecamatan').attr('readonly', true);
            // $('#kelurahan').attr('readonly', true);
            // $('#rt_ktp').attr('readonly', true);
            // $('#rw_ktp').attr('readonly', true);
            // $('#tanggal_lahir').attr('readonly', true);
            // $('#kecamatan_domisili_ktp').attr('readonly', true);
            // $('#kelurahan_domisili_ktp').attr('readonly', true);

            // document.getElementById("surabaya").disabled = true;
            // document.getElementById("nonSurabaya").disabled = true;

            var padat_karya = $('#status_padat_karya').val();
            var peken = $('#status_peken').val();
            /*  document.addEventListener("DOMContentLoaded", function(event) {

            }); */
            /* if (padat_karya == 1 || peken == 1 || peken == 2) {
                $('#nik').attr('readonly', true);
                $('#nama_pelaku_usaha').attr('readonly', true);
                $('#alamat_ktp').attr('readonly', true);
                // $('#kecamatan').attr('readonly', true);
                // $('#kelurahan').attr('readonly', true);
                // $('#rt_ktp').attr('readonly', true);
                // $('#rw_ktp').attr('readonly', true);
                $('#tanggal_lahir').attr('readonly', true);
                $('#no_telp_usaha').attr('readonly', true);
                $('#nama_usaha').attr('readonly', true);
                $('#kategori_jenis_produk').attr('disabled', true);
                $('#jenis_produk').attr('readonly', true);
                $('#skala_pemasaran').attr('disabled', true);
                $('#status_bangunan').attr('disabled', true);
                $('#tahun_binaan').attr('readonly', true);
                $('#sumber_permodalan').attr('disabled', true);
                $('#jumlah_permodalan').attr('readonly', true);
                $('#omset_awal').attr('readonly', true);
                $('#kapasitas_produksi').attr('readonly', true);
                $('#biaya_produksi').attr('readonly', true);
                $('#jumlah_tenaga_kerja').attr('readonly', true);
                $('#keanggotaan_koperasi').attr('disabled', true);
                $('#status_keaktifan').attr('disabled', true);


                document.getElementById("surabaya").disabled = true;
                document.getElementById("nonSurabaya").disabled = true;
                document.getElementById("ayam_bebek").disabled = true;
                document.getElementById("beras").disabled = true;
                document.getElementById("buah").disabled = true;
                document.getElementById("cabai").disabled = true;
                document.getElementById("daging").disabled = true;
                document.getElementById("gula").disabled = true;
                document.getElementById("kain").disabled = true;
                document.getElementById("kayu").disabled = true;
                document.getElementById("kedelai").disabled = true;
                document.getElementById("minyak").disabled = true;
                document.getElementById("sayur").disabled = true;
                document.getElementById("tepung").disabled = true;
                document.getElementById("telur").disabled = true;

                document.getElementById("peken_ukm").disabled = true;
                document.getElementById("peken_tokel").disabled = true;
                document.getElementById("padat_karya").disabled = true;


                $('#npwp').attr('readonly', true);
                $('#nib').attr('readonly', true);
                $('#iumk').attr('readonly', true);
                $('#pirt').attr('readonly', true);
                $('#bpom').attr('readonly', true);
                $('#sertifikat_merk').attr('readonly', true);
                $('#sertifikat_halal').attr('readonly', true);
                $('#haki').attr('readonly', true);
            } */
        }

        function domisiliKtp() {
            var alamat = <?php echo json_encode($data['pelaku_usaha'] -> alamat_ktp); ?>;
            var dropdownKecamatanDom = document.getElementById("kecamatan");
            var selectedValueKecamatanDom = dropdownKecamatanDom.options[dropdownKecamatanDom.selectedIndex].value;

            var dropdownKelurahanDom = document.getElementById("kelurahan");
            var selectedValueKelurahanDom = dropdownKelurahanDom.options[dropdownKelurahanDom.selectedIndex].value;

            $('#check_alamat_domisili').on('click', function () {
                if ($(this).is(':checked')) {
                    // alert('tes');
                    /* $('#alamat_domisili').attr('readonly', true);
                    $('#kecamatan_domisili').attr('disabled',true);
                    $('#kelurahan_domisili').attr('disabled',true); */
                    $('#alamat_domisili').val(alamat);
                    renderKecamatanDomisili(selectedValueKecamatanDom);
                    renderKelurahanDomisili(selectedValueKelurahanDom);

                } else {
                    $('#alamat_domisili').removeAttr(
                        'readonly');
                    $('#dom_ktp').hide();
                    $('#dom_non_ktp').show();

                    $('#alamat_domisili').val('');
                    $('#kecamatan_domisili').val('');
                    $('#kecamatan_domisili').trigger('change');
                    // getKelurahanDomisili();
                    // renderKecamatanDomisili();
                    $('#kelurahan_domisili').val('');
                    $('#kelurahan_domisili').trigger('change');
                    $('#kelurahan_domisili').prop('disabled', true);
                    $('#kelurahan_domisili').html(
                    '<option value="" selected disabled>Pilih Kelurahan</option>');
                    // $('#kecamatan_domisili').attr('disabled',false);
                    // $('#kelurahan_domisili').attr('disabled',false);
                    // $('#kecamatan_domisili').append('<option value="" disabled>Pilih :</option>');
                    // $('#kelurahan_domisili').append('<option value="" disabled>Pilih :</option>');
                    // getKelurahanDomisili();

                }
            });
        }

        function renderKecamatanDomisili(params) {
            /* get {{ $data['kecamatan'] }} to array variable */
            var kecamatan = <?php echo json_encode($data['kecamatan']); ?> ;
            /* foreach kecamatan, when same echo attr selected */
            var render = '<option value="" disabled>Pilih Kecamatan</option>';
            let renderNama = '';
            kecamatan.forEach(function (item) {
                if (item.id_m_setup_kec == params) {
                    render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}" selected>${item.nm_kec}</option>`;
                    // renderNama = `"${item.nm_kec}"`;
                    renderNama = item.nm_kec;
                } else {
                    render +=`<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                }
            });
            $('#kecamatan_domisili').html(render);
        }

        function renderKelurahanDomisili(params) {

            $.ajax({
                    type: "POST",
                    url: "{{ route('getKelurahan') }}",
                    dataType: "json",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        id_m_setup_kec: $('[name="kecamatan"]').val(),
                    }
                })
                .done(function (response) {
                    if (response.status == 'success') {
                        let render = '';
                        let renderNama = '';
                        response.data.forEach(function (item) {
                            if (item.id_m_setup_kel == params) {
                                render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}" selected>${item.nm_kel}</option>`;
                                renderNama = item.nm_kel;
                            } else {
                                render +=`<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}">${item.nm_kel}</option>`;
                            }
                        });

                        $('#kelurahan_domisili').html(render);
                    } else {
                        /* Swal fire */
                        Swal.fire({
                            icon: 'error',
                            title: 'Eror, Mohon Hubungi Admin',
                            text: response.message,
                        });
                    }
                })
        }

    </script>
    @endpush

</x-templates.default>
