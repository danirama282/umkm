<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TBantuanUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table ='t_bantuan_usaha';
    protected $fillable = [
        't_pelaku_usaha',
        'm_jenis_bantuan_id',
        'm_jenis_binaan_id',
        'm_jenis_permodalan_id',
        'm_perusahaan_id',
        'tanggal_menerima_bantuan',
        'status_kerjasama',
        'nama_binaan',
        'nominal_binaan',
        'nama_barang',
        'nama_barang',
        'jumlah_barang',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    public function pelaku_usaha()
    {
        return $this->belongsTo(TPelakuUsahaModel::class, 't_pelaku_usaha_id', 'id');
    }
    public function jenis_bantuan()
    {
        return $this->belongsTo(MJenisBantuanModel::class, 'm_jenis_usaha_id', 'id');
    }
    public function jenis_binaan()
    {
        return $this->belongsTo(MJenisBinaanModel::class, 'm_jenis_usaha_id', 'id');
    }
    public function jenis_permodalan()
    {
        return $this->belongsTo(MJenisPermodalanModel::class, 'm_jenis_usaha_id', 'id');
    }
    public function perusahaan()
    {
        return $this->belongsTo(MPerusahaanModel::class, 'm_jenis_usaha_id', 'id');
    }
}
