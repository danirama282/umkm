<x-templates.default>
    <x-slot name="title">Daftar Bantuan</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <input type="hidden" name="pelaku_usaha_id" id="pelaku_usaha_id" value="{{ Crypt::encryptString($pelaku_usaha->id) }}">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="text-bold mt-2">DETAIL DATA PELAKU USAHA</h5>
                    </div>
                </div>
                <div class="card-body position-relative">
                    <input type="hidden" name="pelaku_usaha_id" id="pelaku_usaha_id" value="{{Crypt::encryptString ($pelaku_usaha->id) }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Nama Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Telepon Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->telp_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Alamat Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->alamat_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Status MBR</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->status_mbr_pemilik }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Kategori Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_kategori_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Jenis Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_sub_kategori_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Jenis Tempat
                                        Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_jenis_tempat_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Tempat Usaha</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_tempat_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Nama Pemilik</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->nama_pemilik }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-2">
                                    <label class="col-form-label" for="staticEmail">Alamat Pemilik</label>
                                </div>
                                <div class="col-sm-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-sm-9"><input class="form-control-plaintext outline-none"
                                        id="staticEmail" type="text" readonly=""
                                        value="{{ $pelaku_usaha->alamat_pemilik }} {{ ', RT ' . $pelaku_usaha->rt_pemilik }} {{ ', RW ' . $pelaku_usaha->rw_pemilik }} {{ ', Kelurahan ' . $pelaku_usaha->nm_kel }} {{ ', Kecamatan ' . $pelaku_usaha->nm_kec }}" />
                                </div>
                            </div>
                        </div>

                        {{-- <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Telepon Usaha
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly="" value="{{ $pelaku_usaha->telp_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Alamat Usaha
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly="" value="{{ $pelaku_usaha->alamat_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Status MBR
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly="" value="{{ $pelaku_usaha->status_mbr_pemilik }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Kategori Usaha
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly="" value="{{ $pelaku_usaha->nama_kategori_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Jenis Usaha
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly=""
                                    value="{{ $pelaku_usaha->nama_sub_kategori_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Jenis Tempat
                                Usaha :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly=""
                                    value="{{ $pelaku_usaha->nama_jenis_tempat_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Tempat Usaha
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                                    type="text" readonly="" value="{{ $pelaku_usaha->nama_tempat_usaha }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Nama Pemilik
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none"
                                    id="staticEmail" type="text" readonly=""
                                    value="{{ $pelaku_usaha->nama_pemilik }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div>
                        <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Alamat Pemilik
                                :</label>
                            <div class="col-sm-10"><input class="form-control-plaintext outline-none"
                                    id="staticEmail" type="text" readonly=""
                                    value="{{ $pelaku_usaha->alamat_pemilik }} {{ ', RT: ' . $pelaku_usaha->rt_pemilik }} {{ ', RW: ' . $pelaku_usaha->nm_kab }} {{ ', Kecamatan: ' . $pelaku_usaha->nm_kec }} {{ ', Kabupaten: ' . $pelaku_usaha->nm_kel }}" />
                                <div class="mb-3 row"></div>
                            </div>
                        </div> --}}
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                            <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- <div class="card mb-3">
        <div class="bg-holder d-none d-lg-block bg-card"
            style="background-image:url(../../assets/img/icons/spot-illustrations/corner-4.png);"></div>
        <!--/.bg-holder-->
        <div class="card-body position-relative">
            <input type="hidden" name="pelaku_usaha_id" id="pelaku_usaha_id" value="{{ $pelaku_usaha->id }}">
            <div class="row">
                <div class="col-lg-8">
                    <h3>Detail Data Pelaku Usaha</h3>
                </div>
                <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Nama Usaha</label>
                    <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                            type="text" readonly="" value="{{ $pelaku_usaha->nama_usaha }}" />
                        <div class="mb-3 row"></div>
                    </div>
                </div>
                <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Alamat Usaha</label>
                    <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                            type="text" readonly="" value="{{ $pelaku_usaha->alamat_usaha }}" />
                        <div class="mb-3 row"></div>
                    </div>
                </div>
                <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Nama Pemilik</label>
                    <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                            type="text" readonly="" value="{{ $pelaku_usaha->nama_pemilik }}" />
                        <div class="mb-3 row"></div>
                    </div>
                </div>
                <div class="mb-3 row"><label class="col-sm-2 col-form-label" for="staticEmail">Alamat Pemilik</label>
                    <div class="col-sm-10"><input class="form-control-plaintext outline-none" id="staticEmail"
                            type="text" readonly="" value="{{ $pelaku_usaha->alamat_pemilik }}" />
                        <div class="mb-3 row"></div>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="text-bold mt-2">DAFTAR DATA BANTUAN</h5>
                    </div>
                    <div>
                        <a href="{{ route('bantuan.tambah_bantuan', Crypt::encryptString($pelaku_usaha->id)) }}"
                            class="btn btn-primary">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Tambah Data Bantuan
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Tanggal Menerima Bantuan</th>
                                    <th>Jenis Bantuan</th>
                                    <th>Nama Perusahaan</th>
                                    <th>Data Bantuan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('styles')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>

        <script>
            $(function() {

                var id = $('#pelaku_usaha_id').val();

                // console.log(id);

                var url = "{{ url('bantuan') }}/" + id;
                $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'tgl_menerima_bantuan',
                            name: 'tgl_menerima_bantuan'
                        },
                        {
                            data: 'jenis_bantuan',
                            name: 'jenis_bantuan'
                        },
                        {
                            data: 'nama_perusahaan',
                            name: 'nama_perusahaan'
                        },
                        {
                            data: 'data_bantuan',
                            render: function(data, type, row, meta) {
                                li = '';
                                // console.log(data);
                                if (data != null) {
                                    for (let index = 0; index < data.length; index++) {
                                        li += '<li>' + data[index] + '</li>';

                                    }
                                } else {

                                }

                                return '<ul>' + li + '</ul>';
                            }
                        },
                        {
                            data: 'aksi',
                            name: 'aksi',
                            orderable: false,
                            searchable: false
                        },
                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            delete datatable
            $('#dataTable').on('click', '#delete_data', function() {
                var id = $(this).data('id');
                // console.log(id);
                // var nama_instansi = $(this).data('nama_instansi');

                swal.fire({
                        title: 'Apakah Anda Yakin ?',
                        text: 'Menghapus data ini ?',
                        // text: 'Menghapus '+nama_instansi+' dari data instansi',
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Ya',
                        cancelButtonText: 'Tidak'
                    })
                    .then(function(e) {
                        if (e.value) {
                            $.ajax({
                                url: '{{ route('bantuan.hapus_bantuan') }}',
                                method: 'delete',
                                data: {
                                    'id': id,
                                    '_token': '{{ csrf_token() }}',
                                },
                                // data: {id: id},
                                // headers: { 'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content') },
                                success: function(data) {
                                    if ($.isEmptyObject(data.error)) {
                                        swal.fire('Data Terhapus!', 'berhasil menghapus data !',
                                                'success')
                                            .then(function(e) {
                                                // table.ajax.reload();
                                                location.reload();
                                            });
                                    }
                                },
                                error: function() {
                                    swal.fire("Telah terjadi kesalahan pada sistem",
                                        "Mohon refresh halaman browser Anda", "error");
                                }
                            });
                        } else {
                            return false;
                        }
                    })
            });
        </script>
    @endpush
</x-templates.default>
