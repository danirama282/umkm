<footer class="footer">
    <div class="row g-0 justify-content-between fs--1 mt-4 mb-3">
        <div class="col-12 col-sm-auto text-center">
            <p class="mb-0 text-600">UMKM Surabaya <span class="d-none d-sm-inline-block">| </span><br
                    class="d-sm-none" /><span id="current-year"></span></span> &copy; Dinas Koperasi UKM dan Perdagangan
            </p>
        </div>
        <div class="col-12 col-sm-auto text-center">
            <p class="mb-0 text-600">v3.4.0</p>
        </div>
    </div>
</footer>

@push('scripts')
<script>
    document.getElementById("current-year").innerHTML = new Date().getFullYear();
</script>
@endpush