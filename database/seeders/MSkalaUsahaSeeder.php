<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MSkalaUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create data di MSkalaUsahaModel
        \App\Models\MSkalaUsahaModel::create([
            'nama_skala_usaha' => 'MAKRO',
        ]);

        \App\Models\MSkalaUsahaModel::create([
            'nama_skala_usaha' => 'MIKRO',
        ]);
    }
}
