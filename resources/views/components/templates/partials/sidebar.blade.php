<nav class="navbar navbar-light navbar-vertical navbar-expand-xl">

    <!-- Script untuk mengubah tampilan navbar -->
    <script>
        var navbarStyle = localStorage.getItem("navbarStyle");
        if (navbarStyle && navbarStyle !== 'transparent') {
            document.querySelector('.navbar-vertical').classList.add(`navbar-${navbarStyle}`);
        }
    </script>

    <div class="d-flex align-items-center">
        <div class="toggle-icon-wrapper">

            <button class="btn navbar-toggler-humburger-icon navbar-vertical-toggle" data-bs-toggle="tooltip"
                data-bs-placement="left" title="Toggle Navigation"><span class="navbar-toggle-icon"><span
                        class="toggle-line"></span></span></button>

        </div>
        <!-- <a class="navbar-brand" href="index.html">
            <div class="d-flex align-items-center py-3"><img class="me-2" src="{{ asset('dist/assets/img/icons/spot-illustrations/falcon.png') }}" alt="" width="40" /><span class="font-sans-serif">falcon</span>
        </div>
        </a> -->
        <div class="navbar-brand">
            <div class="d-flex align-items-center py-3 fs-1 text-danger text-uppercase">
                Umkm Surabaya
            </div>
        </div>
    </div>

    <div class="collapse navbar-collapse" id="navbarVerticalCollapse">
        <div class="navbar-vertical-content scrollbar">
            <ul class="navbar-nav flex-column mb-3" id="navbarVerticalNav">
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('welcome*') ? 'active' : '' }}" href="{{ route('welcome') }}"
                        aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-home"></span></span><span class="nav-link-text ps-1">Beranda</span>
                        </div>
                    </a>
                </li>
                @role('superadmin')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('home*') ? 'active' : '' }}" href="{{ route('home') }}"
                        aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-chart-pie"></span></span><span
                                class="nav-link-text ps-1">Dashboard</span>
                        </div>
                    </a>
                </li>

                <li class="nav-item">
                    <!-- label-->
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Monitoring
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <!-- Membuat div coming soon -->
                    <!-- <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span></div> -->
                    <a class="nav-link {{ request()->is('monitor_pu*') ? 'active' : '' }}"
                        href="{{ route('monitor_pu.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-users"></span></span><span class="nav-link-text ps-1">Monitor
                                Pelaku Usaha</span>
                        </div>
                    </a>
                    <!-- parent pages-->
                    <!-- <a class="nav-link" href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                                                                                                                            <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-calendar-alt"></span></span><span class="nav-link-text ps-1">Pelaku Usaha</span>
                                                                                                                            </div>
                                                                                                                        </a> -->
                    <a class="nav-link {{ request()->is('monitor_omset*') ? 'active' : '' }}"
                        href="{{ route('monitor_omset.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-money-check-alt"></span></span><span
                                class="nav-link-text ps-1">Monitor
                                Omset Pelaku Usaha</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Transaksi
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <!-- <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span></div> -->

                    @if (request()->is('pelaku_usaha*'))
                    <a class="nav-link {{ request()->is('pelaku_usaha*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @elseif(request()->is('bantuan*'))
                    <a class="nav-link {{ request()->is('bantuan*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @else
                    <a class="nav-link {{ request()->is('omset_usaha*') || request()->is('klasifikasi_kbli*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @endif
                </li>
                <!-- <li class="nav-item">
                            <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                                <div class="col-auto navbar-vertical-label">Master
                                </div>
                                <div class="col ps-0">
                                    <hr class="mb-0 navbar-vertical-divider" />
                                </div>
                            </div>

                            <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span></div>


                            <a class="nav-link" href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                                <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-calendar-alt"></span></span><span class="nav-link-text ps-1">Pelaku Usaha</span>
                                </div>
                            </a>
                        </li> -->
                @elseif ('admin')
                <li class="nav-item">
                    <a class="nav-link {{ request()->is('home*') ? 'active' : '' }}" href="{{ route('home') }}"
                        aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-chart-pie"></span></span><span
                                class="nav-link-text ps-1">Dashboard</span>
                        </div>
                    </a>
                </li>
                <li class="nav-item">
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Transaksi
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <!-- <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span></div> -->

                    @if (request()->is('pelaku_usaha*'))
                    <a class="nav-link {{ request()->is('pelaku_usaha*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @elseif(request()->is('bantuan*'))
                    <a class="nav-link {{ request()->is('bantuan*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @else
                    <a class="nav-link {{ request()->is('omset_usaha*') || request()->is('klasifikasi_kbli*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @endif
                </li>
                @else
                <li class="nav-item">
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Transaksi
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <!-- <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span></div> -->

                    @if (request()->is('pelaku_usaha*'))
                    <a class="nav-link {{ request()->is('pelaku_usaha*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @elseif(request()->is('bantuan*'))
                    <a class="nav-link {{ request()->is('bantuan*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @else
                    <a class="nav-link {{ request()->is('omset_usaha*') ? 'active' : '' }}"
                        href="{{ route('pelaku_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-user-edit"></span></span><span class="nav-link-text ps-1">Pelaku
                                Usaha</span>
                        </div>
                    </a>
                    @endif

                </li>
                @endrole
                <li class="nav-item">
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Buku Panduan
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>

                    <a class="nav-link {{ request()->is('buku_panduan*') ? 'active' : '' }}"
                        href="{{ route('buku_panduan.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span
                                    class="fas fa-book"></span></span><span class="nav-link-text ps-1">Buku
                                Panduan</span>
                        </div>
                    </a>
                </li>
                <!--   <li class="nav-item">
                    <div class="row navbar-vertical-label-wrapper mt-3 mb-2">
                        <div class="col-auto navbar-vertical-label">Master
                        </div>
                        <div class="col ps-0">
                            <hr class="mb-0 navbar-vertical-divider" />
                        </div>
                    </div>
                    <a class="nav-link" href="{{ route('jenis_usaha.index') }}" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-calendar-alt"></span></span><span class="nav-link-text ps-1">Jenis Usaha</span>
                        </div>
                    </a>
                    <a class="nav-link" href="{{ route('jenis_usaha.index') }}l" role="button" aria-expanded="false">
                        <div class="d-flex align-items-center"><span class="nav-link-icon"><span class="fas fa-comments"></span></span><span class="nav-link-text ps-1">Status Bangunan</span>
                        </div>
                    </a>
                </li> -->

            </ul>
            <!-- <div class="settings mb-3">
                <div class="card alert p-0 shadow-none" role="alert">
                    <div class="btn-close-falcon-container">
                        <div class="btn-close-falcon" aria-label="Close" data-bs-dismiss="alert"></div>
                    </div>
                    <div class="card-body text-center"><img src="assets/img/icons/spot-illustrations/navbar-vertical.png" alt="" width="80" />
                        <p class="fs--2 mt-2">Loving what you see? <br />Get your copy of <a href="#!">Falcon</a></p>
                        <div class="d-grid"><a class="btn btn-sm btn-purchase" href="https://themes.getbootstrap.com/product/falcon-admin-dashboard-webapp-template/" target="_blank">Purchase</a></div>
                    </div>
                </div>
            </div> -->
        </div>
    </div>

</nav>