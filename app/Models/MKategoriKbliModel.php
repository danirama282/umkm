<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class MKategoriKbliModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_kategori_kbli';
    /* Inisiasi protected untuk id */
    protected $primaryKey = 'id_m_kategori_kbli';
    protected $fillable = [
        'kode_kategori_kbli',
        'nama_kategori_kbli'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function relasi_klasifikasi_kbli()
    {
        return $this->hasMany(MKlasifikasiKbliModel::class, 'id_m_kategori_kbli');
    }
}
