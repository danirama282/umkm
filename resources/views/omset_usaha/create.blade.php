<x-templates.default>
    <x-slot name="title">Entri Omset Usaha</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="fw-semi-bold">Entri Omset Usaha</h3>
                </div>
                <div class="card-body">
                    <form class="row g-3 needs-validation" id="frm-filter">
                        @csrf
                       {{-- {{ dd($pelaku_usaha->id_t_pelaku_usaha_disdag_181022 ) }} --}}
                        <input type="hidden" name="id_pelaku_usaha" id="id_pelaku_usaha" value="{{ $pelaku_usaha->id_t_pelaku_usaha_disdag_181022 }}">
                        {{-- Input Bulan Omset --}}
                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Bulan Laporan Omset</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <select class="form-select js-select2" name="bulan_laporan_omset" id="bulan_laporan_omset" data-control="select2" data-placeholder="Pilih Bulan" required="">
                                    <option></option>
                                    <option value="01">Januari</option>
                                    <option value="02">Februari</option>
                                    <option value="03">Maret</option>
                                    <option value="04">April</option>
                                    <option value="05">Mei</option>
                                    <option value="06">Juni</option>
                                    <option value="07">Juli</option>
                                    <option value="08">Agustus</option>
                                    <option value="09">September</option>
                                    <option value="10">Oktober</option>
                                    <option value="11">November</option>
                                    <option value="12">Desember</option>
                                </select>
                              <div class="invalid-feedback">Pilih bulan</div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Tahun</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <select class="form-select js-select2" name="tahun_laporan" id="tahun_laporan" data-control="select2" data-placeholder="Pilih Tahun">
                                    <option></option>
                                    @php
                                        $tahun_now = date('Y');
                                    @endphp
                                    {{-- <option selected disabled>Pilih Tahun</option> --}}
                                    @for ($i = 2022; $i<=$tahun_now ; $i++)
                                    <option  value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                              <div class="invalid-feedback">Pilih Tahun</div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Masukkan jumlah omset</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <div class="input-group has-validation"><span class="input-group-text" id="input_jumlah_omset">Rp</span>
                                    <input class="form-control" name="jumlah_omset" id="jumlah_omset" type="text"
                                    onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="form-control"
                                    data-inputmask="'alias': 'numeric','digits': 2,'groupSeparator': ',','removeMaskOnSubmit': true,'autoUnmask':true"
                                    required />
                                    <div class="invalid-feedback">Masukkan jumlah omset</div>
                                  </div>
                              <div class="invalid-feedback">Masukkan jumlah omset</div>
                            </div>
                        </div>

                        {{-- Simpan Omset --}}
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('omset_usaha.index', Crypt::encryptString($pelaku_usaha->id_t_pelaku_usaha_disdag_181022)) }}" class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                            <button class="btn btn-primary me-1 mb-1 btn_filter" type="button" ><span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan</button>

                            {{-- <button class="btn btn-secondary ms-2">Kembali</button> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

<meta name="csrf-token" content="{{ csrf_token() }}">

<script>
    $(document).ready(function(){
        $('#jumlah_omset').inputmask();
    })
</script>

@push('scripts')
<script src="assets/plugins/global/plugins.bundle.js"></script>
@endpush
@push('styles')
<link href="assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
@endpush

{{-- validasi field tapi durung kenek --}}
{{-- <script>
    const form = document.getElementById('frm-filter');
    var validator = FormValidation.FormValidation(form,
        {
            fields:{
                bulan_laporan_omset: {
                    validators: {
                        notEmpty: {
                            message: "Bulan Laporan Harus Di Isi"
                        },
                    },
                },
                tahun_laporan: {
                    validators: {
                        notEmpty: {
                            message: "Tahun Laporan Harus Di Isi"
                        },
                    },
                },
                jumlah_omset: {
                    validators: {
                        notEmpty: {
                            message: "Jumlah Omset Harus Di Isi"
                        },
                    },
                },
            },
            plugins: {
                trigger: new FormValidation.plugins.Trigger,
                bootstrap: new FormValidation.plugins.Bootstrap5({
                    rowSelector: ".fv-row",
                    eleInvalidClass: '',
                    eleValidClass: ''
                })
            }
        }
    );
</script> --}}

<script>
    /* Select Pelaku Usaha */
    $(document).ready(function(){
        $('.js-select2').select2({
            theme: "bootstrap-5",
        });
    });

    $( document ).ready(function() {
        var list_cmb_bulan = get_list_laporan_bulan({});
        $('.btn_filter').on('click', function(e) {
            let formData = new FormData(document.getElementById("frm-filter"));
            var pelaku_usaha = $('#id_pelaku_usaha').val();
            var bulan_laporan = $('#bulan_laporan_omset').val();
            var tahun = $('#tahun_laporan').val();
            var jumlah_omset = $('#jumlah_omset').val();
            $.ajax({
                url: "{{ route('omset_usaha.store') }}",
                type: "POST",
                headers: {
                    "X-CSRF-TOKEN": $(`meta[name="csrf-token"]`).attr("content")
                },
                data : {
                    pelaku_usaha : pelaku_usaha,
                    bulan_laporan : bulan_laporan,
                    tahun : tahun,
                    jumlah_omset : jumlah_omset
                },
                typeData : "json",
                success: function(data) {
                    Swal.fire({
                    icon: 'success',
                    title: 'Data berhasil disimpan',
                    showConfirmButton: false,
                    timer: 1500
                    })

                    // window.location = "{{ route('omset_usaha.index',  Crypt::encryptString($pelaku_usaha->id_t_pelaku_usaha_disdag_181022)) }}";
                },
                error: function(error) {
                    // console.log(error);
                    Swal.fire({
                    icon: 'error',
                    title: 'Terjadi kesalahan',
                    text: 'Harap isi data dengan benar!'
                    })
                }
            });

        });
    });

    function get_list_laporan_bulan({inputDate = undefined, range = 12, list_disabled = []}) {
        let date = inputDate == undefined ? new Date() : new Date(inputDate);
        let list = [{
            id: "",
            text: ""
        }];

        let year, month, month_format, value, disabled;

        for (let i = 1; i <= range; i++) {
            date.setMonth(date.getMonth() - 1);

            year = Intl.DateTimeFormat("id", { year: "numeric" }).format(date);
            month = Intl.DateTimeFormat("id", { month: "long" }).format(date);

            month_format = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
            // console.log(date.getFullYear() + "-" + month_format);
            value = date.getFullYear() + "-" + month_format;
            disabled = list_disabled.includes(value);
            // console.log(value, disabled);
            list.push({
                id: value,
                text: year + " " + month,
                disabled: disabled
            });
        }

        return list;
    }
</script>



</x-templates.default>
