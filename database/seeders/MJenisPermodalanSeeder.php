<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MJenisPermodalanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat data untuk MJenisPermodalanModel
        \DB::table('m_jenis_permodalan')->insert([
            'nama_jenis_permodalan' => 'UANG',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_jenis_permodalan')->insert([
            'nama_jenis_permodalan' => 'BARANG',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
