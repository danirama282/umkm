<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MSubKategoriUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_sub_kategori_usaha';
    protected $fillable = [
        'm_kategori_usaha_id',
        'nama_sub_kategori_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function kategori_usaha()
    {
        return $this->belongsTo(MKategoriUsahaModel::class, 'm_kategori_usaha_id', 'id');
    }
}
