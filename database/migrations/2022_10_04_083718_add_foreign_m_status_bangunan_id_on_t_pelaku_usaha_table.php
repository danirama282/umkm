<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddForeignMStatusBangunanIdOnTPelakuUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->unsignedBigInteger('m_status_bangunan_id')->nullable()->after('m_skala_pemasaran_id');
            $table->foreign('m_status_bangunan_id')->references('id')->on('m_status_bangunan');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->dropForeign(['m_status_bangunan_id']);
            $table->dropColumn('m_status_bangunan_id');
        });
    }
}
