<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MJenisBinaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('m_jenis_binaan')->insert([
            'nama_jenis_binaan' => 'PEMBINAAN 1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_jenis_binaan')->insert([
            'nama_jenis_binaan' => 'PEMBINAAN 2',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
