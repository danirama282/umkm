<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MPanduan extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'm_panduan';
    protected $primaryKey = 'id_panduan';
    protected $fillable = [
        'nama_panduan', 'kategori_panduan', 'file_panduan'
    ];
    protected $hidden = [
        'created_at',
        'deleted_at',
    ];
}
