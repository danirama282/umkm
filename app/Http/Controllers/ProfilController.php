<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfilController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        /* Fungsi untuk update atau ubah password dari user yang login */
        $validated = Validator::make($request->all(), [
            'current-password' => 'required',
            'new-password' => 'required',
            'confirm-password' => 'required',
        ]);

        $attrNames = [
            'current-password' => 'Password Lama',
            'new-password' => 'Password Baru',
            'confirm-password' => 'Konfirmasi Password Baru',
        ];

        $validated->setAttributeNames($attrNames);

        if($validated->passes()) {
            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Password lama tidak sesuai dengan password yang anda masukkan. Silahkan coba lagi!'
                ]);
            }

            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                return response()->json([
                    'status' => 'error',
                    'message' => 'Password baru tidak boleh sama dengan password lama. Silahkan masukkan password yang berbeda!'
                ]);
            }

            $user = Auth::user();
            $user->password = bcrypt($request->get('new-password'));
            $user->save();

            return response()->json([
                'status' => true,
                'message' => 'Password berhasil diubah!'
            ]);
        } else {
            return response()->json([
                'status' => false,
                'message' => $validated->errors()->all()
            ]);
        }
    }
}
