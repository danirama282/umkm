<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WelcomeController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\BantuanController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\MJenisUsahaController;
use App\Http\Controllers\MonitorPUController;
use App\Http\Controllers\MonitorOmsetController;
use App\Http\Controllers\TPelakuUsahaController;
use App\Http\Controllers\MWilayahController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\TOmsetUsahaController;
use App\Http\Controllers\MKlasifikasiKbliController;
use App\Http\Controllers\MPanduanController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Auth::routes([
    'register' => false,
]);

Route::middleware('auth')->group(function () {

    /* Tampilan awal ketika halaman ini diakses ada di Controller tampilan */
    Route::get('/welcome', WelcomeController::class)->name('welcome');

    /* Dibawah ini merupakan kumpulan route untuk fungsi Dashboard */
    Route::get('/home', [HomeController::class, 'index'])->name('home');
    /* Route::get('/dataTablesWilayah', [HomeController::class, 'dataTablesWilayah'])->name('dataTablesWilayah'); */
    Route::get('/dataTablesWilayahDisdag', [HomeController::class, 'dataTablesWilayahDisdag'])->name('dataTablesWilayahDisdag');
    Route::get('/dataTableSebaranKategoriUsaha', [HomeController::class, 'dataTableSebaranKategoriUsaha'])->name('dataTableSebaranKategoriUsaha');
    Route::get('/dataTablesPenerimaCsr', [HomeController::class, 'dataTablesPenerimaCsr'])->name('dataTablesPenerimaCsr');
    Route::get('/dataTablesWilayahSebaranUkm', [HomeController::class, 'dataTablesWilayahSebaranUkm'])->name('dataTablesWilayahSebaranUkm');
    Route::post('/grafikStatusMBR', [HomeController::class, 'grafikStatusMBR'])->name('grafikStatusMBR');
    Route::post('/grafikJenisUsaha', [HomeController::class, 'grafikJenisUsaha'])->name('grafikJenisUsaha');
    Route::post('/grafikInterverensi', [HomeController::class, 'grafikInterverensi'])->name('grafikInterverensi');
    Route::post('/grafikSebaranKategoriUsaha', [HomeController::class, 'grafikSebaranKategoriUsaha'])->name('grafikSebaranKategoriUsaha');
    Route::post('/dataTotalOmset', [HomeController::class, 'tableOmset'])->name('dataTotalOmset');
    Route::get('/dataTablePelakuUsahaIndustriPengolahan', [HomeController::class, 'dataTablePelakuUsahaIndustriPengolahan'])->name('dataTablePelakuUsahaIndustriPengolahan');
    Route::get('/detailKategoriUsahaFashion/{id}', [HomeController::class, 'detailKategoriUsahaFashion'])->name('detailKategoriUsahaFashion');
    Route::get('/detailKategoriUsahaToko/{id}', [HomeController::class, 'detailKategoriUsahaToko'])->name('detailKategoriUsahaToko');
    Route::get('/detailKategoriUsahaMamin/{id}', [HomeController::class, 'detailKategoriUsahaMamin'])->name('detailKategoriUsahaMamin');
    Route::get('/detailKategoriUsahaKerajinan/{id}', [HomeController::class, 'detailKategoriUsahaKerajinan'])->name('detailKategoriUsahaKerajinan');
    Route::get('/detailKategoriUsahaPertanian/{id}', [HomeController::class, 'detailKategoriUsahaPertanian'])->name('detailKategoriUsahaPertanian');
    Route::get('/detailKategoriUsahaJasa/{id}', [HomeController::class, 'detailKategoriUsahaJasa'])->name('detailKategoriUsahaJasa');
    Route::post('/detailIntervensi', [HomeController::class, 'detailIntervensi'])->name('detailIntervensi');

    // Define route untuk fungsi datatables Kategori Usaha
    Route::get('/dataTableFashion', [HomeController::class, 'dataTableFashion'])->name('dataTableFashion');
    Route::get('/dataTableMamin', [HomeController::class, 'dataTableMamin'])->name('dataTableMamin');
    Route::get('/dataTableKerajinan', [HomeController::class, 'dataTableKerajinan'])->name('dataTableKerajinan');
    Route::get('/dataTableFashionToko', [HomeController::class, 'dataTableFashionToko'])->name('dataTableFashionToko');
    Route::get('/dataTableJasa', [HomeController::class, 'dataTableJasa'])->name('dataTableJasa');
    Route::get('/dataTableBudidaya', [HomeController::class, 'dataTableBudidaya'])->name('dataTableBudidaya');

    /* Define route untuk fungsi datatables KBLI */
    Route::get('/dataTablesIndustriPengolahan', [HomeController::class, 'dataTablesIndustriPengolahan'])->name('dataTablesIndustriPengolahan');
    Route::get('/dataTablesPerdagangan', [HomeController::class, 'dataTablesPerdagangan'])->name('dataTablesPerdagangan');
    Route::get('/dataTablesAkomodasiUsaha', [HomeController::class, 'dataTablesAkomodasiUsaha'])->name('dataTablesAkomodasiUsaha');
    Route::get('/dataTablesAktivitasJasa', [HomeController::class, 'dataTablesAktivitasJasa'])->name('dataTablesAktivitasJasa');
    Route::get('/datatablePertanianKehutananPerikanan', [HomeController::class, 'datatablePertanianKehutananPerikanan'])->name('datatablePertanianKehutananPerikanan');

    // Define route untuk fungsi datatables Intervensi
    Route::get('/dataTableSwk', [HomeController::class, 'dataTableSwk'])->name('dataTableSwk');
    Route::get('/dataTableSkg', [HomeController::class, 'dataTableSkg'])->name('dataTableSkg');
    Route::get('/dataTablePasar', [HomeController::class, 'dataTablePasar'])->name('dataTablePasar');

    Route::get('/dataTableTokel', [HomeController::class, 'dataTableTokel'])->name('dataTableTokel');
    Route::get('/dataTablePelatihan', [HomeController::class, 'dataTablePelatihan'])->name('dataTablePelatihan');
    Route::get('/dataTablePameran', [HomeController::class, 'dataTablePameran'])->name('dataTablePameran');

    Route::get('/dataTableIndustriRumahan', [HomeController::class, 'dataTableIndustriRumahan'])->name('dataTableIndustriRumahan');
    Route::get('/dataTableRumahKreatif', [HomeController::class, 'dataTableRumahKreatif'])->name('dataTableRumahKreatif');
    Route::get('/dataTablePadatKarya', [HomeController::class, 'dataTablePadatKarya'])->name('dataTablePadatKarya');

    Route::get('/dataTableKur', [HomeController::class, 'dataTableKur'])->name('dataTableKur');
    Route::get('/dataTableCsr', [HomeController::class, 'dataTableCsr'])->name('dataTableCsr');
    Route::get('/dataTablePuspita', [HomeController::class, 'dataTablePuspita'])->name('dataTablePuspita');

    Route::get('/dataTableMbr', [HomeController::class, 'dataTableMbr'])->name('dataTableMbr');
    Route::get('/dataTableNonMbr', [HomeController::class, 'dataTableNonMbr'])->name('dataTableNonMbr');
    Route::get('/dataTableBPUM', [HomeController::class, 'dataTableBPUM'])->name('dataTableBPUM');

    Route::get('/dataTableBelumIntervensi', [HomeController::class, 'dataTableBelumIntervensi'])->name('dataTableBelumIntervensi');
    Route::get('/dataTableOss', [HomeController::class, 'dataTableOss'])->name('dataTableOss');

    Route::get('/getDataPadatKarya', [HomeController::class, 'getDataPadatKarya'])->name('getDataPadatKarya');
    Route::get('/getDataPeken', [HomeController::class, 'getDataPeken'])->name('getDataPeken');

    Route::get('/get_data_warga_miskin', [HomeController::class, 'get_data_warga_miskin'])->name('get_data_warga_miskin');

    // Route::get('/filterDataPadatKarya', [HomeController::class, 'filterDataPadatKarya'])->name('filterDataPadatKarya');
    Route::get('/filterDataPadatKaryaDua', [HomeController::class, 'filterDataPadatKaryaDua'])->name('filterDataPadatKaryaDua');
    Route::get('/filter_peken', [HomeController::class, 'filter_peken'])->name('filter_peken');
    Route::post('/detailGamis', [HomeController::class, 'detailGamis'])->name('detailGamis');
    Route::post('/detailKategori', [HomeController::class, 'detailKategori'])->name('detailKategori');
    Route::post('/detailGamisKbli', [HomeController::class, 'detailGamisKbli'])->name('detailGamisKbli');
    Route::post('/intervensiGamis', [HomeController::class, 'intervensiGamis'])->name('intervensiGamis');

    // detail omset
    Route::post('/detailOmset', [HomeController::class, 'detailOmset'])->name('detailOmset');
    Route::post('/detailOmsetGrafik', [HomeController::class, 'detailOmsetGrafik'])->name('detailOmsetGrafik');






    // Group BantuanController
    Route::group(['prefix' => 'bantuan'], function () {
        Route::get('/{id}', [BantuanController::class, 'index'])->name('bantuan.index');
        Route::post('/simpan_bantuan', [BantuanController::class, 'simpan_bantuan'])->name('bantuan.simpan_bantuan');
        Route::post('/update_bantuan/{id}', [BantuanController::class, 'update_bantuan'])->name('bantuan.update_bantuan');
        Route::delete('/hapus_bantuan', [BantuanController::class, 'hapus_bantuan'])->name('bantuan.hapus_bantuan');
        Route::get('/tambah_bantuan/{id}', [BantuanController::class, 'tambah_bantuan'])->name('bantuan.tambah_bantuan');
        Route::get('/edit_bantuan/{id}', [BantuanController::class, 'edit_bantuan'])->name('bantuan.edit_bantuan');
        Route::get('/data', [BantuanController::class, 'data'])->name('bantuan.data');
    });

    /* Group MJenisUsahaController */
    Route::group(['prefix' => 'jenis_usaha'], function () {
        Route::get('/', [MJenisUsahaController::class, 'index'])->name('jenis_usaha.index');
        Route::get('/create', [MJenisUsahaController::class, 'create'])->name('jenis_usaha.create');
        Route::post('/store', [MJenisUsahaController::class, 'store'])->name('jenis_usaha.store');
        Route::get('/edit/{id}', [MJenisUsahaController::class, 'edit'])->name('jenis_usaha.edit');
        Route::put('/update/{id}', [MJenisUsahaController::class, 'update'])->name('jenis_usaha.update');
        Route::delete('/destroy/{id}', [MJenisUsahaController::class, 'destroy'])->name('jenis_usaha.destroy');
    });

    /* Group TPelakuUsahaController */
    Route::group(['prefix' => 'pelaku_usaha'], function () {
        Route::post('/datatable', [TPelakuUsahaController::class, 'datatable'])->name('pelaku_usaha.datatable');
        Route::get('/', [TPelakuUsahaController::class, 'index'])->name('pelaku_usaha.index');
        Route::get('/test', [TPelakuUsahaController::class, 'test'])->name('pelaku_usaha.test');
        Route::get('/create', [TPelakuUsahaController::class, 'create'])->name('pelaku_usaha.create');
        Route::post('/store', [TPelakuUsahaController::class, 'store'])->name('pelaku_usaha.store');
        Route::get('/edit/{id}', [TPelakuUsahaController::class, 'edit'])->name('pelaku_usaha.edit');
        Route::post('/update/{id}', [TPelakuUsahaController::class, 'update'])->name('pelaku_usaha.update');
        Route::post('/update_keckel/{id}', [TPelakuUsahaController::class, 'update_keckel'])->name('pelaku_usaha.update_keckel');
        Route::delete('/destroy', [TPelakuUsahaController::class, 'destroy'])->name('pelaku_usaha.destroy');
        Route::post('/getSubKategoriUsaha', [TPelakuUsahaController::class, 'getSubKategoriUsaha'])->name('pelaku_usaha.getSubKategoriUsaha');
        Route::post('/getJenisTempatUsaha', [TPelakuUsahaController::class, 'getJenisTempatUsaha'])->name('pelaku_usaha.getJenisTempatUsaha');
        Route::post('/cek_nik', [TPelakuUsahaController::class, 'cek_nik'])->name('pelaku_usaha.cek_nik');
        Route::post('/cek_nik_update', [TPelakuUsahaController::class, 'cek_nik_update'])->name('pelaku_usaha.cek_nik_update');
        Route::get('/excel_omset', [TPelakuUsahaController::class, 'excel_omset'])->name('pelaku_usaha.excel_omset');
    });

    /* Define route post getKelurahan pada controller MWilayahController */
    Route::post('/getKelurahan', [MWilayahController::class, 'getKelurahan'])->name('getKelurahan');
    Route::post('/cari_kabupaten', [MWilayahController::class, 'cari_kabupaten'])->name('cari_kabupaten');
    Route::post('/cari_kecamatan', [MWilayahController::class, 'cari_kecamatan'])->name('cari_kecamatan');
    Route::post('/cari_kelurahan', [MWilayahController::class, 'cari_kelurahan'])->name('cari_kelurahan');

    /* Group TOmsetUsahaController */
    Route::group(['prefix' => 'omset_usaha'], function () {
        Route::get('/{id}', [TOmsetUsahaController::class, 'index'])->name('omset_usaha.index');
        Route::get('/create/{id}', [TOmsetUsahaController::class, 'create'])->name('omset_usaha.create');
        Route::post('/store', [TOmsetUsahaController::class, 'store'])->name('omset_usaha.store');
        Route::get('/edit/{id}', [TOmsetUsahaController::class, 'edit'])->name('omset_usaha.edit');
        Route::post('/update/{id}', [TOmsetUsahaController::class, 'update'])->name('omset_usaha.update');
        // Route::get('/destroy/{id}', [TOmsetUsahaController::class, 'destroy'])->name('omset_usaha.destroy');
        Route::delete('/destroy', [TOmsetUsahaController::class, 'destroy'])->name('omset_usaha.destroy');
        Route::get('/excel/{id}', [TOmsetUsahaController::class, 'excel'])->name('omset_usaha.excel');
    });

    /* Group MKlasifikasiKbliController */
    Route::group(['prefix' => 'klasifikasi_kbli'], function () {
        Route::get('/{id}', [MKlasifikasiKbliController::class, 'index'])->name('klasifikasi_kbli.index');
        Route::get('/edit/{id}', [MKlasifikasiKbliController::class, 'edit'])->name('klasifikasi_kbli.edit');
        Route::put('/update/{id}', [MKlasifikasiKbliController::class, 'update'])->name('klasifikasi_kbli.update');
        Route::post('/get-data-klasifikasi-kbli', [MKlasifikasiKbliController::class, 'getDataKlasifikasiKbli'])->name('klasifikasi_kbli.getDataKlasifikasiKbli');
        Route::put('/destroy-kode-kbli', [MKlasifikasiKbliController::class, 'destroyKodeKbli'])->name('klasifikasi_kbli.destroyKodeKbli');
    });

    Route::group(['prefix' => 'monitor_pu'], function () {
        Route::get('/', [MonitorPUController::class, 'index'])->name('monitor_pu.index');
        Route::post('/datatable', [MonitorPUController::class, 'datatable'])->name('monitor_pu.datatable');
        // Route::get('/test', [TPelakuUsahaController::class, 'test'])->name('pelaku_usaha.test');
        // Route::get('/create', [TPelakuUsahaController::class, 'create'])->name('pelaku_usaha.create');
        // Route::post('/store', [TPelakuUsahaController::class, 'store'])->name('pelaku_usaha.store');
        // Route::get('/edit/{id}', [TPelakuUsahaController::class, 'edit'])->name('pelaku_usaha.edit');
        // Route::post('/update/{id}', [TPelakuUsahaController::class, 'update'])->name('pelaku_usaha.update');
        // Route::delete('/destroy/{id}', [TPelakuUsahaController::class, 'destroy'])->name('pelaku_usaha.destroy');
        // Route::post('/getSubKategoriUsaha', [TPelakuUsahaController::class, 'getSubKategoriUsaha'])->name('pelaku_usaha.getSubKategoriUsaha');
        // Route::post('/getJenisTempatUsaha', [TPelakuUsahaController::class, 'getJenisTempatUsaha'])->name('pelaku_usaha.getJenisTempatUsaha');
    });

    Route::group(['prefix' => 'monitor_omset'], function () {
        Route::get('/', [MonitorOmsetController::class, 'index'])->name('monitor_omset.index');
        Route::post('/datatable', [MonitorOmsetController::class, 'datatable'])->name('monitor_omset.datatable');
    });

    Route::group(['prefix' => 'buku_panduan'], function () {
        Route::get('/', [MPanduanController::class, 'index'])->name('buku_panduan.index');
        Route::post('/store', [MPanduanController::class, 'store'])->name('buku_panduan.store');
        Route::delete('/destroy', [MPanduanController::class, 'destroy'])->name('buku_panduan.destroy');
    });

    /* define route change_password */
    // Route::post('/ubah_password', [ProfilController::class, '__invoke'])->name('ubah_password');

    /* Define route post logout */
    Route::post('/logout', [LoginController::class, 'logout'])->name('logout');
});
