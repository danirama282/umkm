<nav class="navbar navbar-light navbar-glass navbar-top navbar-expand">
    <button class="btn navbar-toggler-humburger-icon navbar-toggler me-1 me-sm-3" type="button" data-bs-toggle="collapse" data-bs-target="#navbarVerticalCollapse" aria-controls="navbarVerticalCollapse" aria-expanded="false" aria-label="Toggle Navigation"><span class="navbar-toggle-icon"><span class="toggle-line"></span></span></button>
    <div class="navbar-brand me-1 me-sm-3">
        <!-- <div class="d-flex align-items-center"><img class="me-2" src="assets/img/icons/spot-illustrations/falcon.png" alt="" width="40" /><span class="font-sans-serif">falcon</span> -->
        <div class="d-flex align-items-center">
            <div class="d-flex align-items-center py-3 fs-1 text-danger text-uppercase">
                Umkm Surabaya
            </div>
        </div>
    </div>
    <!-- <ul class="navbar-nav align-items-center d-none d-lg-block">
        <li class="nav-item">
            <div class="search-box" data-list='{"valueNames":["title"]}'>
                <form class="position-relative" data-bs-toggle="search" data-bs-display="static">
                    <input class="form-control search-input fuzzy-search" type="search" placeholder="Search..." aria-label="Search" />
                    <span class="fas fa-search search-box-icon"></span>

                </form>
                <div class="btn-close-falcon-container position-absolute end-0 top-50 translate-middle shadow-none" data-bs-dismiss="search">
                    <div class="btn-close-falcon" aria-label="Close"></div>
                </div>
                <div class="dropdown-menu border font-base start-0 mt-2 py-0 overflow-hidden w-100">
                    <div class="scrollbar list py-3" style="max-height: 24rem;">
                        <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Recently Browsed</h6><a class="dropdown-item fs--1 px-card py-1 hover-primary" href="app/events/event-detail.html">
                            <div class="d-flex align-items-center">
                                <span class="fas fa-circle me-2 text-300 fs--2"></span>

                                <div class="fw-normal title">Pages <span class="fas fa-chevron-right mx-1 text-500 fs--2" data-fa-transform="shrink-2"></span> Events</div>
                            </div>
                        </a>
                        <a class="dropdown-item fs--1 px-card py-1 hover-primary" href="app/e-commerce/customers.html">
                            <div class="d-flex align-items-center">
                                <span class="fas fa-circle me-2 text-300 fs--2"></span>

                                <div class="fw-normal title">E-commerce <span class="fas fa-chevron-right mx-1 text-500 fs--2" data-fa-transform="shrink-2"></span> Customers</div>
                            </div>
                        </a>

                        <hr class="bg-200 dark__bg-900" />
                        <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Suggested Filter</h6><a class="dropdown-item px-card py-1 fs-0" href="app/e-commerce/customers.html">
                            <div class="d-flex align-items-center"><span class="badge fw-medium text-decoration-none me-2 badge-soft-warning">customers:</span>
                                <div class="flex-1 fs--1 title">All customers list</div>
                            </div>
                        </a>
                        <a class="dropdown-item px-card py-1 fs-0" href="app/events/event-detail.html">
                            <div class="d-flex align-items-center"><span class="badge fw-medium text-decoration-none me-2 badge-soft-success">events:</span>
                                <div class="flex-1 fs--1 title">Latest events in current month</div>
                            </div>
                        </a>
                        <a class="dropdown-item px-card py-1 fs-0" href="app/e-commerce/product/product-grid.html">
                            <div class="d-flex align-items-center"><span class="badge fw-medium text-decoration-none me-2 badge-soft-info">products:</span>
                                <div class="flex-1 fs--1 title">Most popular products</div>
                            </div>
                        </a>

                        <hr class="bg-200 dark__bg-900" />
                        <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Files</h6><a class="dropdown-item px-card py-2" href="#!">
                            <div class="d-flex align-items-center">
                                <div class="file-thumbnail me-2"><img class="border h-100 w-100 fit-cover rounded-3" src="assets/img/products/3-thumb.png" alt="" /></div>
                                <div class="flex-1">
                                    <h6 class="mb-0 title">iPhone</h6>
                                    <p class="fs--2 mb-0 d-flex"><span class="fw-semi-bold">Antony</span><span class="fw-medium text-600 ms-2">27 Sep at 10:30 AM</span></p>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item px-card py-2" href="#!">
                            <div class="d-flex align-items-center">
                                <div class="file-thumbnail me-2"><img class="img-fluid" src="assets/img/icons/zip.png" alt="" /></div>
                                <div class="flex-1">
                                    <h6 class="mb-0 title">Falcon v1.8.2</h6>
                                    <p class="fs--2 mb-0 d-flex"><span class="fw-semi-bold">John</span><span class="fw-medium text-600 ms-2">30 Sep at 12:30 PM</span></p>
                                </div>
                            </div>
                        </a>

                        <hr class="bg-200 dark__bg-900" />
                        <h6 class="dropdown-header fw-medium text-uppercase px-card fs--2 pt-0 pb-2">Members</h6><a class="dropdown-item px-card py-2" href="pages/user/profile.html">
                            <div class="d-flex align-items-center">
                                <div class="avatar avatar-l status-online me-2">
                                    <img class="rounded-circle" src="assets/img/team/1.jpg" alt="" />

                                </div>
                                <div class="flex-1">
                                    <h6 class="mb-0 title">Anna Karinina</h6>
                                    <p class="fs--2 mb-0 d-flex">Technext Limited</p>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item px-card py-2" href="pages/user/profile.html">
                            <div class="d-flex align-items-center">
                                <div class="avatar avatar-l me-2">
                                    <img class="rounded-circle" src="assets/img/team/2.jpg" alt="" />

                                </div>
                                <div class="flex-1">
                                    <h6 class="mb-0 title">Antony Hopkins</h6>
                                    <p class="fs--2 mb-0 d-flex">Brain Trust</p>
                                </div>
                            </div>
                        </a>
                        <a class="dropdown-item px-card py-2" href="pages/user/profile.html">
                            <div class="d-flex align-items-center">
                                <div class="avatar avatar-l me-2">
                                    <img class="rounded-circle" src="assets/img/team/3.jpg" alt="" />

                                </div>
                                <div class="flex-1">
                                    <h6 class="mb-0 title">Emma Watson</h6>
                                    <p class="fs--2 mb-0 d-flex">Google</p>
                                </div>
                            </div>
                        </a>

                    </div>
                    <div class="text-center mt-n3">
                        <p class="fallback fw-bold fs-1 d-none">No Result Found.</p>
                    </div>
                </div>
            </div>
        </li>
    </ul> -->
    <ul class="navbar-nav navbar-nav-icons ms-auto flex-row align-items-center">

        <!-- <li>
            <div class="marquee" style="width: 75%; float: right;">
                <marquee behavior="scroll" direction="left" scrollamount="5" onmouseover="this.stop();" onmouseout="this.start();">
                    <span class="text-white fs-1 fw-bold bg-warning px-2 py-4">Perhatian! Sehubungan dengan jadwal maintenance rutin Server Pusat di Dinas Komunikasi dan Informatika, bersama ini dengan hormat disampaikan bahwa pada hari Selasa 15 November 2022 mulai pukul 19.00 WIB s.d Rabu 16  November 2022 layanan aplikasi akan dinonaktifkan sementara. Demikian mohon maklum. Terima kasih</span>
                </marquee>
            </div>
        </li> -->

        <!-- <li class="nav-item">
            <div class="theme-control-toggle fa-icon-wait px-2">
                <input class="form-check-input ms-0 theme-control-toggle-input" id="themeControlToggle" type="checkbox" data-theme-control="theme" value="dark" />
                <label class="mb-0 theme-control-toggle-label theme-control-toggle-light" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to light theme"><span class="fas fa-sun fs-0"></span></label>
                <label class="mb-0 theme-control-toggle-label theme-control-toggle-dark" for="themeControlToggle" data-bs-toggle="tooltip" data-bs-placement="left" title="Switch to dark theme"><span class="fas fa-moon fs-0"></span></label>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link px-0 notification-indicator notification-indicator-warning notification-indicator-fill fa-icon-wait" href="app/e-commerce/shopping-cart.html"><span class="fas fa-shopping-cart" data-fa-transform="shrink-7" style="font-size: 33px;"></span><span class="notification-indicator-number">1</span></a>

        </li>
        <li class="nav-item dropdown">
            <a class="nav-link notification-indicator notification-indicator-primary px-0 fa-icon-wait" id="navbarDropdownNotification" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="fas fa-bell" data-fa-transform="shrink-6" style="font-size: 33px;"></span></a>
            <div class="dropdown-menu dropdown-menu-end dropdown-menu-card dropdown-menu-notification" aria-labelledby="navbarDropdownNotification">
                <div class="card card-notification shadow-none">
                    <div class="card-header">
                        <div class="row justify-content-between align-items-center">
                            <div class="col-auto">
                                <h6 class="card-header-title mb-0">Notifications</h6>
                            </div>
                            <div class="col-auto ps-0 ps-sm-3"><a class="card-link fw-normal" href="#">Mark all as read</a></div>
                        </div>
                    </div>
                    <div class="scrollbar-overlay" style="max-height:19rem">
                        <div class="list-group list-group-flush fw-normal fs--1">
                            <div class="list-group-title border-bottom">NEW</div>
                            <div class="list-group-item">
                                <a class="notification notification-flush notification-unread" href="#!">
                                    <div class="notification-avatar">
                                        <div class="avatar avatar-2xl me-3">
                                            <img class="rounded-circle" src="assets/img/team/1-thumb.png" alt="" />

                                        </div>
                                    </div>
                                    <div class="notification-body">
                                        <p class="mb-1"><strong>Emma Watson</strong> replied to your comment : "Hello world 😍"</p>
                                        <span class="notification-time"><span class="me-2" role="img" aria-label="Emoji">💬</span>Just now</span>

                                    </div>
                                </a>

                            </div>
                            <div class="list-group-item">
                                <a class="notification notification-flush notification-unread" href="#!">
                                    <div class="notification-avatar">
                                        <div class="avatar avatar-2xl me-3">
                                            <div class="avatar-name rounded-circle"><span>AB</span></div>
                                        </div>
                                    </div>
                                    <div class="notification-body">
                                        <p class="mb-1"><strong>Albert Brooks</strong> reacted to <strong>Mia Khalifa's</strong> status</p>
                                        <span class="notification-time"><span class="me-2 fab fa-gratipay text-danger"></span>9hr</span>

                                    </div>
                                </a>

                            </div>
                            <div class="list-group-title border-bottom">EARLIER</div>
                            <div class="list-group-item">
                                <a class="notification notification-flush" href="#!">
                                    <div class="notification-avatar">
                                        <div class="avatar avatar-2xl me-3">
                                            <img class="rounded-circle" src="assets/img/icons/weather-sm.jpg" alt="" />

                                        </div>
                                    </div>
                                    <div class="notification-body">
                                        <p class="mb-1">The forecast today shows a low of 20&#8451; in California. See today's weather.</p>
                                        <span class="notification-time"><span class="me-2" role="img" aria-label="Emoji">🌤️</span>1d</span>

                                    </div>
                                </a>

                            </div>
                            <div class="list-group-item">
                                <a class="border-bottom-0 notification-unread  notification notification-flush" href="#!">
                                    <div class="notification-avatar">
                                        <div class="avatar avatar-xl me-3">
                                            <img class="rounded-circle" src="assets/img/logos/oxford.png" alt="" />

                                        </div>
                                    </div>
                                    <div class="notification-body">
                                        <p class="mb-1"><strong>University of Oxford</strong> created an event : "Causal Inference Hilary 2019"</p>
                                        <span class="notification-time"><span class="me-2" role="img" aria-label="Emoji">✌️</span>1w</span>

                                    </div>
                                </a>

                            </div>
                            <div class="list-group-item">
                                <a class="border-bottom-0 notification notification-flush" href="#!">
                                    <div class="notification-avatar">
                                        <div class="avatar avatar-xl me-3">
                                            <img class="rounded-circle" src="assets/img/team/10.jpg" alt="" />

                                        </div>
                                    </div>
                                    <div class="notification-body">
                                        <p class="mb-1"><strong>James Cameron</strong> invited to join the group: United Nations International Children's Fund</p>
                                        <span class="notification-time"><span class="me-2" role="img" aria-label="Emoji">🙋‍</span>2d</span>

                                    </div>
                                </a>

                            </div>
                        </div>
                    </div>
                    <div class="card-footer text-center border-top"><a class="card-link d-block" href="app/social/notifications.html">View all</a></div>
                </div>
            </div>

        </li> -->
        <li class="nav-item mb-1">
            <a class="js-acc-btn text-primary text-decoration-none fs--1 font-sans-serif fw-bolder text-uppercase">{{ Auth::user()->nama_user }}</a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link pe-0" id="navbarDropdownUser" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <div class="avatar avatar-xl">
                    <img class="rounded-circle" src="{{ asset('dist/assets/img/team/avatar.png') }}" alt="" />

                </div>
                <!-- <div class="content">
                    <a class="js-acc-btn" href="#">{{ Auth::user()->nama_user }}</a>
                </div> -->
            </a>
            <div class="dropdown-menu dropdown-menu-end py-0" aria-labelledby="navbarDropdownUser">
                <div class="bg-white dark__bg-1000 rounded-2 py-2">
                    <!-- <a class="dropdown-item fw-bold text-warning" href="#!"><span class="fas fa-crown me-1"></span><span>Go Pro</span></a> -->

                    <!-- <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#!">Set status</a>
                    <a class="dropdown-item" href="pages/user/profile.html">Profile &amp; account</a>
                    <a class="dropdown-item" href="#!">Feedback</a> -->

                    <!-- <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="pages/user/settings.html">Settings</a> -->

                    <!-- Membuat button untuk memanggil modal #ubahPasswordModal -->
                    <!-- <a class="dropdown-item" href="#" data-bs-toggle="modal" data-bs-target="#ubahPasswordModal">Ubah Password</a> -->

                    <!-- Membuat elemen a:href untuk mentrigger modal logut -->
                    <button class="dropdown-item" id="logout">Logout</button>
                </div>
            </div>
        </li>
    </ul>
</nav>

@section('modal')
<!-- Modal ubah password -->
<div class="modal fade" id="ubahPasswordModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body p-0">

                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Form Ubah Password</h5>
                </div>
                <form id="formUbahPassword">
                    <div class="p-4 pb-2">
                        @csrf
                        <!-- Membuat input hidden berisikan value auth user id yang sedang login sekarang -->
                        <input type="hidden" name="logged_user_id" value="{{ Auth::user()->id }}">
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label col-form-label-sm" for="current-password">Password
                                Lama</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="current-password" name="current-password" type="password" />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label col-form-label-sm" for="new-password">Password
                                Baru</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="new-password" name="new-password" type="password" />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-3 col-form-label col-form-label-sm" for="confirm-password">Konfirmasi
                                Password</label>
                            <div class="col-sm-9">
                                <input class="form-control" id="confirm-password" name="confirm-password" type="password" />
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button class="btn btn-primary me-1 mb-1" type="submit">
                                <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
    <script>
        $('#logout').click(function() {
            /* Menambahkan swal2 */
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Anda akan keluar dari sistem",
                icon: 'warning',
                showCancelButton: true,
                /* Costum button color */
                cancelButtonColor: '#d33',
                cancelButtonText: 'Batal',
                confirmButtonColor: '#3085d6',
                confirmButtonText: 'Ya, Keluar!'
            }).then((result) => {
                if (result.isConfirmed) {
                    /* Membuat ajax untuk logout */
                    $.ajax({
                        url: "{{ route('logout') }}",
                        method: "POST",
                        data: {
                            _token: "{{ csrf_token() }}"
                        },
                        success: function(response) {
                            /* Jika berhasil, maka akan redirect ke halaman login */
                            window.location.href = "{{ route('login') }}";
                        }
                    });
                }
            });
        });
    </script>

    <!-- <script>
        $('#formUbahPassword').submit(function(e) {
            e.preventDefault();
            $.ajax({
                method: "POST",
                data: $(this).serialize(),
                done: function(response) {
                    if(response.status == true) {
                        Swal.fire({
                            icon: 'success',
                            title: 'Berhasil',
                            text: response.message,
                        });
                    } if (response.status == 'error') {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                        });
                    } if (response.status == false) {
                        Swal.fire({
                            icon: 'error',
                            title: 'Gagal',
                            text: response.message,
                        });
                    } 
                },
            });
        });
    </script> -->
@endpush