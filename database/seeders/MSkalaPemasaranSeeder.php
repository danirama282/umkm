<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MSkalaPemasaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create data di MSkalaPemasaranModel 
        \App\Models\MSkalaPemasaranModel::create([
            'nama_skala_pemasaran' => 'LOKAL',
        ]);
    }
}
