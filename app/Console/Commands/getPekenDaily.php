<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class getPekenDaily extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'daily:peken';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'GET API from PEKEN take daily data';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /* init controller and method */
        $controller = new \App\Http\Controllers\HomeController();
        $method_get_peken = $controller->getDataPeken();

        /* call method */
        if ($method_get_peken) {
            /* log info */
            \Log::info('Success get data from PEKEN');
        } else {

            /* log error */
            \Log::error('Failed get data from PEKEN');
        }
    }
}
