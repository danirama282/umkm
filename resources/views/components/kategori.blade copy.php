<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-2 text-center">Fashion</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-2 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_all'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_all'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_miskinEkstrim'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_miskinEkstrim'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{ $data['fashion_wargaMiskin'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{ $data['fashion_wargaMiskin'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_praMiskinAll'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_praMiskinAll'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_praMiskin_ekstrim'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_praMiskin_ekstrim'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Pra
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_praMiskin'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_praMiskin'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_lainnyaAll'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['fashion_lainnyaAll'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_miskinAll'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['fashion_miskinAll'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">Toko</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_all'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_all'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_miskin_ekstrim'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_miskin_ekstrim'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_miskin_miskin'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_miskin_miskin'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_pramiskin_all'][0]->jiwa}} Jiwa </h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_pramiskin_all'][0]->kk}} KK </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_pramiskin_ektrim'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_pramiskin_ektrim'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Pra
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_pramiskin_pramiskin'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_pramiskin_pramiskin'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_wargalainnya_all'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                            {{$data['toko_wargalainnya_all'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_wargalainnya_all'][0]->jiwa}} Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    {{$data['toko_wargalainnya_all'][0]->kk}} KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">Makanan Dan Minuman</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            {{$data['mamin_all'][0]->jiwa}} Jiwa</h3>
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            {{$data['mamin_all'][0]->kk}} KK</h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">Budidaya Pertanian</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">Kerajinan</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="card-body bg-custom1 rounded mb-3">
    <div class="row g-0 light">
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">Jasa</h3>
        </div>
        <div class="col-xl-12">
            <h3 class="mt-2 text-white text-uppercase fs-3 text-center">
        </div>
        <div class="col-sm-6 col-lg-12 mb-2 d-flex">
            <div class="row g-0 light flex-fill">
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-5 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Pra
                                            Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between" style="height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                    Ekstrim
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center" style="width: 300px;">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-sm-6 col-lg-2 mb-4 ps-lg-2">
                    <a {{-- href="#" --}} class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                        <div class="card text-black bg-light">
                            <div class="card-body text-center" style="height: 300px;">
                                <div style="height: 80px;">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-black text-uppercase fs-2">Warga Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs-4"></h3> -->
                                        <h3 class="fw-medium mt-2 text-black text-uppercase fs-2">
                                            XX </h3>
                                    </div>
                                </div>
                                <div class="d-flex justify-content-between py-1 " style="
                                height: 155px;">
                                    <div class="card text-black bg-secondary">
                                        <div class="card-body text-center">
                                            <div class="card-title mb-0">
                                                <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga
                                                    Miskin
                                                </h3>
                                            </div>
                                            <div class="card-title mb-0">
                                                <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX Jiwa</h3>
                                                <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                    XX KK</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>