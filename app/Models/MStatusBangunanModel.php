<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MStatusBangunanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_status_bangunan';
    protected $fillable = [
        'nama_status_bangunan',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bangunan()
    {
        return $this->hasMany(BangunanModel::class, 'status_bangunan_id', 'id');
    }
}
