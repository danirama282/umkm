<?php

namespace App\Http\Controllers;

use App\Models\MSetupKabModel;
use App\Models\MSetupKecModel;
use App\Models\MSetupPropModel;
use App\Models\TPelakuUsahaDisdag131022Model;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Str;


class MonitorOmsetController extends Controller
{
    public function index()
    {
        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')
            ->where('nm_prop', 'JAWA TIMUR')
            ->first();

        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')
            ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
            ->where('nm_kab', 'KOTA SURABAYA')
            ->get();

        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')
            ->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)
            ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
            ->orderBy('nm_kec', 'ASC')
            ->get();

        // dd('ok');
        return view('monitor_omset.index', compact('data'));
    }

    public function datatable(Request $request, TPelakuUsahaDisdag131022Model $pelakuUsaha)
    {
        if ($request->ajax()) {
            $columns = [
                'nama_pelaku_usaha',
                'nama_usaha',
                'kecamatan',
                'kelurahan',
                'kategori_usaha',
                'intervensi',
                'omset_bulanan',
                'jumlah_omset',
                'jumlah_omset',
            ];
            $params['limit'] = $request->input('length');
            $params['start'] = $request->input('start');
            $params['order'] = $columns[$request->input('order.0.column')];
            $params['dir'] = $request->input('order.0.dir');
            $params['search'] = $request->input('search.value');

            $data = $pelakuUsaha->getDataMonitorOmset("", $params, $request->all());
            // dd($data);
            $json_data = array(
                "draw"              => intval($request->input('draw')),
                "recordsTotal"      => intval($data['total']),
                "recordsFiltered"   => intval($data['filtered']),
                "data"              => $data['data']
            );
            return response()->json($json_data);
        }
    }
}
