<x-templates.default>
    <x-slot name="title">Tambah Jenis Usaha</x-slot>

    <!-- Konten baris keempat -->
    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header">
                    <h5 class="fw-semi-bold">Form Tambah Jenis Usaha</h5>
                </div>

                <div class="card-body ">
                    <form id="formTambahJenisUsaha">
                        @csrf
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_jenis_usaha">Nama Jenis Usaha</label>
                            <div class="col-sm-10">
                                <input class="form-control" id="nama_jenis_usaha" name="nama_jenis_usaha" type="text" />
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <button class="btn btn-primary me-1 mb-1" type="submit">
                                <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <!-- Membuat ajax ketika submit formTambahJenisUsaha -->
    <script>
        $(document).ready(function() {
            $('#formTambahJenisUsaha').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "{{ route('jenis_usaha.store') }}",
                    type: "POST",
                    data: $('#formTambahJenisUsaha').serialize(),
                    /* done function */
                    success: function(response) {
                        console.log(response);
                        if (response.status == 'success') {
                            Swal.fire({
                                icon: 'success',
                                title: 'Berhasil',
                                text: response.message,
                            }).then((result) => {
                                if (result.isConfirmed) {
                                    window.location.href = "{{ route('jenis_usaha.index') }}";
                                }
                            });
                        } else if (response.status == 'error') {
                            Swal.fire({
                                icon: 'error',
                                title: 'Gagal',
                                text: response.message,
                            });
                        }
                    },
                    /* Jika response.status == check */
                    error: function(response) {
                        console.log(response);
                        if (response.status == 422) {
                            Swal.fire({
                                icon: 'warning',
                                title: 'Peringatan',
                                text: response.responseJSON.message,
                            });
                        }
                    }
                });
            });
        });
    </script>
    @endpush

</x-templates.default>