<div class="row g-0">
    <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
        <div class="card">
            <div class="card-header">
                <h5 class="fw-bold fs-1 mt-2">Entri Data Pelaku Usaha</h5>
            </div>
            <div class="card-body ">
                <form id="formTambahPelakuUsaha" multipart="enctype/form-data">
                    @csrf
                    <input type="hidden" name="is_surabaya">
                    <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                    <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">
                    <div class="row mb-3">
                        <div class="col-sm-12">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Pelaku Usaha
                            </h6>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="status">Status
                            Kependudukan</label>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" id="surabaya" type="radio"
                                    name="status" value="1" onclick="showButton()" /><label class="form-check-label"
                                    for="surabaya">Surabaya</label>
                            </div>
                            <div class="form-check"><input class="form-check-input" id="nonSurabaya" type="radio"
                                    name="status" value="0" onclick="hideButton()" /><label class="form-check-label"
                                    for="nonSurabaya">Non
                                    Surabaya</label></div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nik">NIK Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input class="form-control" id="nik" name="nik" type="number" />
                                <button class="btn btn-sm btn-primary" type="button" id="cek_nik"
                                    style="display:none;"><i class="fas fa-search"></i> Cari NIK </button>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_pelaku_usaha">Nama
                            Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="nama_pelaku_usaha" name="nama_pelaku_usaha" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_ktp">Alamat
                            Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="alamat_ktp" name="alamat_ktp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3 provinsi">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="provinsi_pelaku">Provinsi
                            Pelaku Usaha</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="provinsi" name="provinsi"
                                data-control="select2" data-placeholder="- Pilih Provinsi -">
                                <option value="" selected disabled>Pilih :</option>
                                @foreach ($data['provinsi_non_sby'] as $prov)
                                <option value="{{ $prov->id_m_setup_prop }}">
                                    {{ $prov->nm_prop }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3 kabupaten_kota">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kabupaten_kota">Kabupaten
                            Pelaku Usaha</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kabupaten_kota"
                                name="kabupaten_kota" data-control="select2"
                                data-placeholder="- Pilih Kabupaten/Kota -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan">Kecamatan
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kecamatan" name="kecamatan"
                                data-control="select2" data-placeholder="- Pilih Kecamatan -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="kecamatan_string" id="kecamatan_string">
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan">Kelurahan
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kelurahan" name="kelurahan"
                                data-control="select2" data-placeholder="- Pilih Kelurahan -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="kelurahan_string" id="kelurahan_string">
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="rt_ktp">RT Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="rt_ktp" name="rt_ktp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="rw_ktp">RW Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="rw_ktp" name="rw_ktp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="tanggal_lahir">Tanggal
                            Lahir Pelaku Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" id="tanggal_lahir" name="tanggal_lahir">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_domisili">Alamat
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <input type="checkbox" name="check_alamat_domisili" id="check_alamat_domisili">
                            <label class="form-check-label">Sesuai Dengan KTP</label>
                            <input class="form-control" id="alamat_domisili" name="alamat_domisili" type="text" />
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_domisili">Kecamatan
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Menjalankan foreach dari $data['kecamatan'] -->
                                @foreach ($data['kecamatan'] as $kecamatan)
                                <option value="{{ $kecamatan->id_m_setup_kec }}">{{ $kecamatan->nm_kec }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_domisili">Kelurahan
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="no_telp">No. Telepon
                            Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="no_telp" name="no_telp" type="number" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-12 mt-3">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Usaha</h6>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="status_keaktifan">Status
                            Keaktifan</label>
                        <div class="col-sm-10">
                            <select name="status_keaktifan" id="status_keaktifan" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Option yang berisi nilai boolean -->
                                <option value="AKTIF">AKTIF</option>
                                <option value="TIDAK_AKTIF">TIDAK AKTIF</option>
                                <option value="LIBUR">LIBUR</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_usaha">Nama
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="nama_usaha" name="nama_usaha" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_usaha">Alamat
                            Usaha</label>
                        <div class="col-sm-10">
                            <!-- Text Area bootstrap 5 -->
                            <textarea class="form-control" name="alamat_usaha" id=alamat_usaha"></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_usaha">Kecamatan
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kecamatan_usaha" id="kecamatan_usaha" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Menjalankan foreach dari $data['kecamatan'] -->
                                @foreach ($data['kecamatan'] as $kecamatan)
                                <option value="{{ $kecamatan->id_m_setup_kec }}">{{ $kecamatan->nm_kec }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_usaha">Kelurahan
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kelurahan_usaha" id="kelurahan_usaha" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kategori_jenis_produk">Kategori
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kategori_jenis_produk" id="kategori_jenis_produk"
                                class="form-select js-select2" aria-label="Default select example">
                                <option value="" selected disabled>Pilih Kategori Usaha</option>
                                <option value="FASHION">Fashion</option>
                                <option value="JASA">Jasa</option>
                                <option value="KERAJINAN">Kerajinan</option>
                                <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                                <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian</option>
                                <option value="TOKO">Toko</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="jenis_produk">Produk
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="jenis_produk" name="jenis_produk" type="text" />
                        </div>
                    </div>
                    <div class="alert alert-dark">
                        <div class="form-check">
                            <input class="form-check-input" id="chck_submit" type="checkbox" value="" /><label
                                class="form-check-label" for="flexCheckDefault">Dengan
                                Ini saya
                                bertanggung jawab atas kebenaran data dan informasi pelaku usaha yang telah kami
                                isikan
                                dan
                                validasi, apabila terdapat data dan informasi pelaku usaha yang keliru akibat
                                ketidaktelitian dan ketidakcermatan, saya bersedia untuk melakukan verifikasi
                                ulang</label>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                            <span class="fas fas fa-backward me-2" data-fa-transform="shrink-3"></span> Kembali
                        </a>
                        <button class="btn btn-primary me-1 mb-1" type="submit" id="submit_keckel" name="submit">
                            <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
