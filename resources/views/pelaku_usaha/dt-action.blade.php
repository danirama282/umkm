<!-- Tombol -->
<div class="d-flex flex-column bd-highlight my-2">
    <!-- <button class="btn btn-sm btn-secondary mb-2" type="button" disabled="disabled">
        <span class="nav-link-icon"><span class="fas fas-tools"></span></span><span class="nav-link-text ps-1">Dalam Perbaikan</span>
    </button> -->

    @hasanyrole('superadmin|admin')
    <a href="{{ route('pelaku_usaha.edit', Crypt::encryptString($data->id)) }}" class="btn btn-sm btn-warning mb-2">
        <span class="nav-link-icon"><span class="fas fa-edit"></span></span><span class="nav-link-text ps-1">Ubah Data
            Pelaku Usaha</span>
    </a>
    <a href="{{ route('omset_usaha.index', Crypt::encryptString($data->id)) }}" class="btn btn-sm btn-info mb-2">
        <span class="fas fa-edit" data-fa-transform="shrink-3"></span> Entri Omset Bulanan
    </a>
    <a href="{{ route('klasifikasi_kbli.index', Crypt::encryptString($data->id)) }}"
        class="btn btn-sm btn-primary mb-2">
        <span class="fas fa-edit" data-fa-transform="shrink-3"></span> Entri Data Klasifikasi KBLI
    </a>
        @if (empty($data->omset))    
            <button class="btn btn-sm btn-danger" id="delete_data" data-id="{{Crypt::encryptString($data->id)}}" data-nik="{{Crypt::encryptString($data->nik)}}"
                title="Hapus"><span class="fas fa-trash-alt" data-fa-transform="shrink-3"></span> Hapus Data Pelaku
                Usaha</button>
        @endif
    @else
    <a href="{{ route('pelaku_usaha.edit', Crypt::encryptString($data->id)) }}" class="btn btn-sm btn-warning mb-2">
        <span class="nav-link-icon"><span class="fas fa-edit"></span></span><span class="nav-link-text ps-1">Verifikasi
            Data
            Pelaku Usaha</span>
    </a>
    <a href="{{ route('omset_usaha.index', Crypt::encryptString($data->id)) }}" class="btn btn-sm btn-info mb-2">
        <span class="fas fa-edit" data-fa-transform="shrink-3"></span> Entri Omset Bulanan
    </a>
    @endrole
</div>
