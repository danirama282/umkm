<x-templates.default>
    <x-slot name="title">Dashboard</x-slot>

    {{-- @hasanyrole('superadmin|admin|user')
        <div class="row g-0">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card h-lg-100">
                    <div class="card-body position-relative">
                        <h6 class="text-dark">Selamat Datang, <span class="fw-bold">{{ $data['user']->name }}</span> dengan
                            role <span class="fw-bold">{{ $data['role'] }}!</h6>
                    </div>
                </div>
            </div>
        </div>
    @endrole  --}}

    <!-- Jika $data['user']->name == "walikotasby" maka akan menampilkan element card yang berisi ucapan selamat datang -->
    @if ($data['user']->name == 'walikotasby')
        <div class="row g-0 pt-3">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card h-lg-100">
                    <div class="card-body position-relative">
                        <h5 class="text-dark">Selamat Datang, <span class="fw-bold">Yth. Bapak Wali Kota
                                Surabaya</span>!</h5>
                    </div>
                </div>
            </div>
        </div>
    @endif

    @hasanyrole('superadmin|admin|user')
        <!-- BARIS KE 1 -->
        <div class="row g-0 light">
            <div class="col-sm-6 col-lg-4 mb-4 ps-lg-2 d-flex justify-content-center">
                <img src="{{ asset('dist/assets/img/illustrations/dash-ilus.png') }}" alt="" width="300">
            </div>
            <div class="col-sm-6 col-lg-8 mb-0 d-flex">
                <div class="row g-0 light flex-fill">
                    <div class="col-sm-6 col-lg-12 ps-lg-2">
                        <div class="card text-white bg-primary py-3">
                            <div class="card-body d-flex justify-content-between pt-3">
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-white text-uppercase fs-5">Total</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-4">
                                        {{ $data['total_pelaku_usaha'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
                        <a href="#detailMbrModal" class="text-decoration-none" data-bs-toggle="modal"
                            data-bs-target="#detailMbrModal">
                            <div class="card text-white bg-secondary">
                                <div class="card-body text-center d-flex justify-content-between">
                                    <div class="card-title mb-0">
                                        <h3 class="mt-2 text-white text-uppercase fs--1">Warga Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--2">
                                            {{ $data['total_pelaku_usaha_mbr'][0]->count }}</h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
                        <a href="#detailNonMbrModal" class="text-decoration-none" data-bs-toggle="modal"
                            data-bs-target="#detailNonMbrModal">
                            <div class="card text-white bg-secondary">
                                <div class="card-body text-center d-flex justify-content-between">
                                    <div class="card-title mb-0">
                                        <h3 class="mt-2 text-white text-uppercase fs--1">Non Warga Miskin</h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--2">
                                            {{ $data['total_pelaku_usaha_non_mbr'][0]->count }}
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <!-- BARIS KE 2 -->
        {{-- <div class="row g-0 light">
		<div class="col-sm-6 col-lg-6 mb-4 ">
            <div class="card border h-100 border-danger">
				<div class="text-white text-center bg-danger justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="mt-2 text-white text-uppercase fs-4">C</h3>
                    </div>
                </div>
                <div class="card-body d-flex">
                    <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                        <!-- <i class="fas fa-industry"></i> -->
                        <img src="{{ asset('dist/assets/img/icons/industri.png') }}" alt="" width="70">
                    </div>
                    <div>
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2 text-uppercase fs-2">Industri Pengolahan</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-uppercase fs-1">{{ $data['total_per_kategori']['Industri Pengolahan'] }}</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
        <div class="card border h-100 border-primary">
            <div class="text-white text-center bg-primary justify-content-center">
                <div class="card-title mb-0">
                    <h3 class="mt-2 text-white text-uppercase fs-4">G</h3>
                </div>
            </div>
            <div class="card-body d-flex">
                <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                    <!-- <i class="fas fa-warehouse"></i> -->
                    <img src="{{ asset('dist/assets/img/icons/dagang.png') }}" alt="" width="70">
                </div>
                <div>
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-uppercase fs-2">Perdagangan</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-uppercase fs-1">{{ $data['total_per_kategori']['Perdagangan'] }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-6 mb-4">
        <div class="card border h-100 border-warning">
            <div class="text-white text-center bg-warning justify-content-center">
                <div class="card-title mb-0">
                    <h3 class="mt-2 text-white text-uppercase fs-4">I</h3>
                </div>
            </div>
            <div class="card-body d-flex">
                <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                    <!-- <i class="fas fa-store"></i> -->
                    <img src="{{ asset('dist/assets/img/icons/mamin.png') }}" alt="" width="70">
                </div>
                <div>
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-uppercase fs-2">Akomodasi Mamin</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-uppercase fs-1">{{ $data['total_per_kategori']['Akomodasi Mamin'] }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
        <div class="card border h-100 border-success">
            <div class="text-white text-center bg-success justify-content-center">
                <div class="card-title mb-0">
                    <h3 class="mt-2 text-white text-uppercase fs-4">S</h3>
                </div>
            </div>
            <div class="card-body d-flex">
                <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                    <img src="{{ asset('dist/assets/img/icons/jasa.png') }}" alt="" width="70">
                </div>
                <div>
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-uppercase fs-2">Aktivitas Jasa Lainnya</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-uppercase fs-1">{{ $data['total_per_kategori']['Aktivitas Jasa'] }}</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div> --}}

        <!-- Katergori Usaha -->
        <div class="row g-0 light flex-fill">
            <div class="col-sm-6 col-lg-12 mb-2">
                <div class="card text-white bg-danger">
                    <div class="card-body d-flex justify-content-center py-1">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-3">KATEGORI USAHA</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Child Kategori Usaha -->
        <div class="row g-0 light">
            <div class="col-sm-6 col-lg-4 mb-2 ">
                <a href="#detailFashionModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailFashionModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/fashion-2.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Fashion</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_fashion }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4 mb-2 ps-lg-2">
                <a href="#detailTokoModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailTokoModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/toko.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Toko</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_toko }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4 mb-2 ps-lg-2">
                <a href="#detailKategoriMaminModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailKategoriMaminModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/mamin.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Makanan dan Minuman</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_mamin }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4 mb-4">
                <a href="#detailKerajinanModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailKerajinanModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/kerajinan.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Kerajinan</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_kerajinan }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4 mb-4 ps-lg-2">
                <a href="#detailBudidayaModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailBudidayaModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/sprout.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Budidaya Pertanian</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_budidaya }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-4 mb-4 ps-lg-2">
                <a href="#detailJasaModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailJasaModal">
                    <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/jasa.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Jasa</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kategori_usaha'][0]->jumlah_jasa }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- Kategori KBLI  -->
        <div class="row g-0 light flex-fill">
            <div class="col-sm-6 col-lg-12 mb-2">
                <div class="card text-white bg-warning">
                    <div class="card-body d-flex justify-content-center py-1">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-3">KBLI</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Child Kategori KBLI -->
        <div class="row g-0 light">
            <div class="col-sm-6 col-lg-6 mb-2 ">
                <a href="#" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailIndustriPengolahanModal">
                    <div class="card border h-100 border-warning">
                        <div class="text-white text-center bg-warning justify-content-center">
                            <div class="card-title mb-0">
                                <h3 class="mt-2 text-white text-uppercase fs-2">C</h3>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <!-- <i class="fas fa-industry"></i> -->
                                <img src="{{ asset('dist/assets/img/icons/industri.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Industri Pengolahan</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kbli']['Industri Pengolahan'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-6 mb-2 ps-lg-2">
                <a href="#detailPerdaganganModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailPerdaganganModal">
                    <div class="card border h-100 border-warning">
                        <div class="text-white text-center bg-warning justify-content-center">
                            <div class="card-title mb-0">
                                <h3 class="mt-2 text-white text-uppercase fs-2">G</h3>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <!-- <i class="fas fa-warehouse"></i> -->
                                <img src="{{ asset('dist/assets/img/icons/dagang.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Perdagangan</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kbli']['Perdagangan'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-6 mb-4">
                <a href="#detailAkomodasiMaminModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailAkomodasiMaminModal">
                    <div class="card border h-100 border-warning">
                        <div class="text-white text-center bg-warning justify-content-center">
                            <div class="card-title mb-0">
                                <h3 class="mt-2 text-white text-uppercase fs-2">I</h3>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <!-- <i class="fas fa-store"></i> -->
                                <img src="{{ asset('dist/assets/img/icons/mamin.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Akomodasi Mamin</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kbli']['Akomodasi Mamin'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
                <a href="#detailAktivitasJasaLainnya" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#detailAktivitasJasaLainnya">
                    <div class="card border h-100 border-warning">
                        <div class="text-white text-center bg-warning justify-content-center">
                            <div class="card-title mb-0">
                                <h3 class="mt-2 text-white text-uppercase fs-2">S</h3>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                {{-- <i class="fas fa-store"></i> --}}
                                <img src="{{ asset('dist/assets/img/icons/jasa.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Aktivitas Jasa Lainnya</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kbli']['Aktivitas Jasa'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-12 mb-4 ps-lg-2">
                <a href="#pertanianKehutananPerikanan" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#pertanianKehutananPerikanan">
                    <div class="card border h-100 border-warning">
                        <div class="text-white text-center bg-warning justify-content-center">
                            <div class="card-title mb-0">
                                <h3 class="mt-2 text-white text-uppercase fs-2">A</h3>
                            </div>
                        </div>
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right:20px;">
                                <img src="{{ asset('dist/assets/img/icons/pertanian.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-2">Pertanian Kehutanan Perikanan</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_kbli']['Pertanian, Kehutanan dan Perikanan'] }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- Intervensi -->
        <div class="row g-0 light flex-fill">
            <div class="col-sm-6 col-lg-12 mb-2">
                <div class="card text-white bg-success">
                    <div class="card-body d-flex justify-content-center py-1">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-3">Intervensi</h3>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Child Intervensi-->
        <div class="row g-0 light mb-2">
            <div class="col-sm-12 col-lg-4">
                <a href="#detailIntervensiSwk" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/swk.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">SWK</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Sentra Wisata Kuliner</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_swk }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailSkgModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/skg.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">SKG</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Surabaya Kriya Galery</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_skg }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailPasarModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/pasar.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pasar</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_pasar }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row g-0 light mb-2">
            <div class="col-sm-12 col-lg-4">
                <a href="#detailTokelModel" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/tokel.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Peken</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Pemberdayaan Ekonomi lan Ketahanan
                                        Ekonomi Nang
                                        Suroboyo</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_tokel }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailPelatihan" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/pelatihan.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pelatihan</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_pelatihan }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailPameranModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/pameran.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Pameran</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_pameran }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row g-0 light mb-2">
            <!-- <div class="col-sm-12 col-lg-4">
                                                                                                                                            <a href="#detailIndustriRumahanModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                                                                                                                                                <div class="card border h-100 border-success border-5">
                                                                                                                                                    <div class="card-body d-flex">
                                                                                                                                                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                                                                                                                                            <img src="{{ asset('dist/assets/img/icons/industri-rumah.png') }}" alt="" width="70">
                                                                                                                                                        </div>
                                                                                                                                                        <div>
                                                                                                                                                            <div class="card-title mb-0">
                                                                                                                                                                <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Industri Rumahan</h3>
                                                                                                                                                            </div>
                                                                                                                                                            <div class="card-title mb-0">
                                                                                                                                                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                                                                                                                                                    jumlah_ir
                                                                                                                                                                    {{ $data['total_intervensi'][0]->jumlah_bpum }}
                                                                                                                                                                </h3>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </a>
                                                                                                                                        </div> -->
            <div class="col-sm-12 col-lg-4">
                <a href="#detailBPUMModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/industri-rumah.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">BPUM</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Bantuan Produktif Usaha Mikro</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_bpum }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailRumahKreatif" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/kreatif.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Rumah Kreatif</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_rk }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailPadatKaryaModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/padat-karya.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Padat Karya</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_padatkarya }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
        <div class="row g-0 light mb-2">
            <div class="col-sm-12 col-lg-4">
                <a href="#detailKurModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/kur.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Kur</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Kredit Usaha Rakyat</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_kur }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailCsrModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/csr.png') }}" alt="" width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Csr</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Corporate Social Responsibility</h3>

                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_csr }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-12 col-lg-4 ps-lg-2">
                <a href="#detailPuspitaModal" class="text-decoration-none" data-bs-toggle="modal" data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex">
                            <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                <img src="{{ asset('dist/assets/img/icons/puspita.png') }}" alt=""
                                    width="70">
                            </div>
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mb-1 mt-2 text-uppercase fs-2">Puspita</h3>
                                    <h3 class="fw-medium" style="font-size: 10px;">Pinjaman Usaha Mikro Kecil Menengah
                                        (UMKM) Surabaya
                                        Pasti Tangguh</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                        {{ $data['total_intervensi'][0]->jumlah_puspita }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <div class="row g-0 light mb-4">
            <div class="col-sm-12 col-lg-12">
                <a href="#detailBelumIntervensiModal" class="text-decoration-none" data-bs-toggle="modal"
                    data-bs-target="#">
                    <div class="card border h-100 border-success border-5">
                        <div class="card-body d-flex justify-content-center text-center">
                            <!-- <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                                                                                                                                                            <img src="{{ asset('dist/assets/img/icons/kur.png') }}" alt="" width="70">
                                                                                                                                                        </div> -->
                            <div>
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-uppercase fs-3">Belum Intervensi</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-uppercase fs-2">
                                        {{ $data['total_intervensi'][0]->jumlah_belum_taging }}
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- BARIS KE 3 - Perdagangan -->
        {{-- <div class="row g-0 light">
        <div class="col-sm-6 col-lg-12 mb-4">
            <div class="card border h-100 border-primary">
                <div class="text-white text-center bg-primary justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Tokel</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Perdagangan )</h3>
                    </div>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_tokel'] }}</h3>
                     </div>
                </div>
            <div class="d-flex justify-content-between px-2">
                <div class="d-flex card-title mb-0">
                    <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                </div>
                <div class="card-title mb-0">
                    <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_tokel_mbr'] }}</h3>
                </div>
            </div>
            <div class="d-flex justify-content-between px-2">
                <div class="card-title mb-0">
                    <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                </div>
                <div class="card-title mb-0">
                    <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_tokel_non_mbr'] }}</h3>
                </div>
            </div>
         </div>
        </div>
    </div>

    <!-- BARIS KE 4 - JAsa -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="card border h-100 border-success">
                <div class="text-white text-center bg-success justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Jahit</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Jasa )</h3>
                    </div>
                </div>
                <!-- <div class="text-white text-center bg-success justify-content-center">
                        <div class="card-title mb-0 fs-2">JAHIT</div>
                        <div class="card-title mb-0 fs-1">( Jasa )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_jahit'] }}</h3>
                    </div>
                </div>
                <!-- <div class="card-body text-center justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_jahit'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jahit_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jahit_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jahit_non_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jahit_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-success">
                <div class="text-white text-center bg-success justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Cuci Sepatu</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Jasa )</h3>
                    </div>
                </div>
                <!--  <div class="text-white text-center bg-success justify-content-center">
                        <div class="card-title mb-0 fs-2">CUCI SEPATU</div>
                        <div class="card-title mb-0 fs-1">( Jasa )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_cuci_sepatu'] }}</h3>
                    </div>
                </div>
                <!-- <div class="card-body d-flex justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_cuci_sepatu'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_cuci_sepatu_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_cuci_sepatu_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_cuci_sepatu_non_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_cuci_sepatu_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-success">
                <div class="text-white text-center bg-success justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Cuci Helm</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Jasa )</h3>
                    </div>
                </div>
                <!--  <div class="text-white text-center bg-success justify-content-center">
                        <div class="card-title mb-0 fs-2">CUCI HELM</div>
                        <div class="card-title mb-0 fs-1">( Jasa )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_cuci_helm'] }}</h3>
                    </div>
                </div>
                <!-- <div class="card-body d-flex justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_cuci_helm'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_cuci_helm_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_cuci_helm_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_cuci_helm_non_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_cuci_helm_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-success">
                <div class="text-white text-center bg-success justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Laundry</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Jasa )</h3>
                    </div>
                </div>
                <!-- <div class="text-white text-center bg-success justify-content-center">
                        <div class="card-title mb-0 fs-2">LAUNDRY</div>
                        <div class="card-title mb-0 fs-1">( Jasa )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_jumlah_laundry'] }}</h3>
                    </div>
                </div>
                <!-- <div class="card-body d-flex justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_jumlah_laundry'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_laundry_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_laundry_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_laundry_non_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_laundry_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
    </div>

    <!-- BARIS KE 5 - TATAP -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-6 mb-4">
            <div class="card border h-100 border-warning">
                <div class="text-white text-center bg-warning justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Swk</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Industri )</h3>
                    </div>
                </div>
                <!-- <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0 fs-2">SWK</div>
                        <div class="card-title mb-0 fs-1">( Industri )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_jumlah_swk'] }}</h3>
                    </div>
                </div>
                <!--  <div class="card-body d-flex justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_jumlah_swk'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_swk_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_swk_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_swk_non_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_swk_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
        <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="text-white text-center bg-warning justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 mt-2 text-white text-uppercase fs-2">UMKM</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-medium mt-2 text-white text-uppercase fs--1">( Industri )</h3>
                    </div>
                </div>
                <!-- <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0 fs-2">UMKM</div>
                        <div class="card-title mb-0 fs-1">( Industri )</div>
                    </div> -->
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0">
                        <h3 class="display-6 text-uppercase fs-2">{{ $data['total_jumlah_umkm'] }}</h3>
                    </div>
                </div>
                <!--  <div class="card-body d-flex justify-content-center">
                        <div class="card-title mb-0 fs-2">{{ $data['total_jumlah_umkm'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_umkm_mbr'] }}</h3>
                    </div>
                </div>
                <!--  <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_umkm_mbr'] }}</div>
                    </div> -->
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">Non Mbr</h3>
                    </div>
                    <div class="card-title mb-0">
                        <h3 class="fw-semi-bold text-uppercase fs--1">{{ $data['total_jumlah_umkm_non_mbr'] }}</h3>
                    </div>
                </div>
                <!-- <div class="d-flex justify-content-between px-2">
                        <div class="card-title mb-0 fs-1">NON MBR</div>
                        <div class="card-title mb-0 fs-1">{{ $data['total_jumlah_umkm_non_mbr'] }}</div>
                    </div> -->
            </div>
        </div>
    </div> --}}
    @endrole

    <!-- BARIS KE 3 -->
    <!-- <div class="row g-0 light">
        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">SWK</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">SKG</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">RUMAH KREATIF</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">INDUSTRI RUMAHAN</div>
                </div>
            </div>
        </div>
    </div> -->

    <!-- BARIS KE 4 -->
    <!-- <div class="row g-0 light">
        <div class="col-sm-6 col-lg-3 mb-4">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">UMKM (PEKEN)</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">TOKEL (PEKEN</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">PASAR</div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 mb-4 ps-lg-2">
            <div class="card border h-100 border-warning">
                <div class="card-body d-flex justify-content-center">
                    <div class="card-title mb-0 fs-2">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="d-flex justify-content-between px-2">
                    <div class="card-title mb-0 fs-1">NON MBR</div>
                    <div class="card-title mb-0 fs-1">X.XXX</div>
                </div>
                <div class="text-white d-flex bg-warning justify-content-center">
                    <div class="card-title mb-0 fs-2">UMKM JAHIT</div>
                </div>
            </div>
        </div>
    </div> -->

    @role('superadmin')
        <!-- BARIS KE 5 -->
        {{-- <div class="row g-0 light">
            <div class="col-sm-6 col-lg-12 mb-4">
                <div class="card border h-100 border-info">
                    <div class="text-white d-flex bg-info justify-content-center">
                        <div class="card-title mb-0 fs-2">
                            <h3 class="display-6 mt-2 text-white text-uppercase fs-2">Wilayah Sebaran UKM</h3>
                        </div>
                    </div>
                    <!-- <div class="text-white d-flex bg-info justify-content-center">
                            <div class="display-6 mt-2 text-white text-uppercase fs-2">Wilayah Sebaran UKM</div>
                        </div> -->
                    <div class="card-body d-flex justify-content-center">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                id="wilayah_sebaran_ukm">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th rowspan="2" class="sort text-center align-middle" data-sort="name">No.
                                        </th>
                                        <th rowspan="2" class="sort text-center align-middle" data-sort="email">
                                            Kecamatan</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#64b5f6">Tokel</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">Jahit</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">Cuci Sepatu</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">Cuci Helm</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">Laundry</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">SWK</th>
                                        <th colspan="2" class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">UMKM
                                        </th>
                                    </tr>
                                    <tr>
                                        <!-- SWK -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#64b5f6">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#64b5f6 ">NON MBR</th>
                                        <!-- SKG -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">NON MBR</th>
                                        <!-- RUMAH KREATIF -->
                                        <th class="sort text-center align-middle"
                                            data-sort="age"style="background-color:#81c784">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">NON MBR</th>
                                        <!-- INDUSTRI RUMAHAN -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">NON MBR</th>
                                        <!-- UMKM (PEKEN) -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#81c784">NON MBR</th>
                                        <!-- PASAR -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">NON MBR</th>
                                        <!-- UMKM JAHIT -->
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">MBR</th>
                                        <th class="sort text-center align-middle" data-sort="age"
                                            style="background-color:#ffb74d">NON MBR</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    <!-- ISI DATA -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-lg-12 mb-4">
                            <div class="d-flex justify-content-center px-2">
                                <div class="card-title mb-0 fs-1">Grafik Pelaku Usaha (Status MBR)</div>
                            </div>
                            <div class="pelaku-usaha-mbr p-2" style="height: 950px;" data-echart-responsive="true"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div> --}}

        {{-- <div class="row g-0 light">
		<div class="col-sm-6 col-lg-12 mb-4">
			<div class="card border h-100 border-info">
				<div class="text-white d-flex bg-info justify-content-center">
                    <div class="card-title mb-0 fs-2">
                        <h3 class="display-6 mt-2 text-white text-center text-uppercase fs-2">Grafik Pelaku Usaha (Status MBR)</h3>
                    </div>
                </div>
				<div class="pelaku-usaha-mbr p-2" style="min-height: 350px;" data-echart-responsive="true"></div>
			</div>
		</div>
	</div> --}}

        {{-- <div class="col-lg-6">
		<div class="d-flex justify-content-center px-2">
			<div class="card-title mb-0 fs-1">Grafik Pelaku Usaha (Jenis Usaha)</div>
		</div>
		<div class="pelaku-usaha-jenis p-2" style="min-height: 350px;" data-echart-responsive="true"></div>
	</div> --}}

        {{-- <div class="row g-0 light">
		<div class="col-sm-6 col-lg-12 mb-4">
			<div class="card border h-100 border-info">
				<div class="text-white d-flex bg-info justify-content-center">
                    <div class="card-title mb-0 fs-2">
                        <h3 class="display-6 mt-2 text-white text-center text-uppercase fs-2">Grafik Pelaku Usaha (Jenis Usaha)</h3>
                    </div>
                </div>
				<div class="pelaku-usaha-jenis p-2" style="min-height: 350px;" data-echart-responsive="true"></div>
			</div>
		</div>
	</div> --}}

        <!-- BARIS KE 5 -->
        <div class="row g-0 light">
            <div class="col-sm-6 col-lg-12 mb-4">
                <div class="card border h-100 border-info">
                    <div class="text-white d-flex bg-info justify-content-center">
                        <div class="card-title mb-0 fs-2">
                            <h3 class="display-6 mt-2 text-white text-center text-uppercase fs-2">Wilayah Sebaran
                                Intervensi UM</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex justify-content-center">
                        <div class="table table-responsive">
                            <table class="table table-bordered table-bordered font-sans-serif mb-0"
                                id="wilayah_sebaran_umkm">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr class="fs--2">
                                        <th class="text-center">NO.</th>
                                        <th class="text-center">KECAMATAN</th>
                                        <th class="text-center">SWK</th>
                                        <th class="text-center">SKG</th>
                                        <th class="text-center">PASAR</th>
                                        <th class="text-center">PEKEN</th>
                                        <th class="text-center">PELATIHAN</th>
                                        <th class="text-center">PAMERAN</th>
                                        <th class="text-center">BPUM</th>
                                        <!-- <th class="text-center">INDUSTRI RUMAHAN</th> -->
                                        <th class="text-center">RUMAH KREATIF</th>
                                        <th class="text-center">PADAT KARYA</th>
                                        <th class="text-center">KUR</th>
                                        <th class="text-center">CSR</th>
                                        <th class="text-center">PUSPITA</th>
                                        <th class="text-center">BELUM INTERVENSI</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center fs--1">
                                    <!-- ISI DATA -->
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6 col-lg-12 mb-4">
                            <div class="d-flex justify-content-center px-2">
                                <div class="card-title mb-0 fs-2 font-sans-serif">Grafik Wilayah Sebaran Intervensi UM
                                </div>
                            </div>
                            <div class="intervensi-um p-2" style="height: 950px;" data-echart-responsive="true">
                            </div>
                        </div>
                    </div>
                    <!-- <div class="row">
                                                                                <div class="col-lg-6">
                                                                                    <div class="d-flex justify-content-center px-2">
                                                                                        <div class="card-title mb-0 fs-1">Grafik Pelaku Usaha (Status MBR)</div>
                                                                                    </div>
                                                                                    <div class="pelaku-usaha-mbr p-2" style="min-height: 350px;" data-echart-responsive="true"></div>
                                                                                </div>
                                                                                <div class="col-lg-6">
                                                                                    <div class="d-flex justify-content-center px-2">
                                                                                        <div class="card-title mb-0 fs-1">Grafik Pelaku Usaha (Jenis Usaha)</div>
                                                                                    </div>
                                                                                    <div class="pelaku-usaha-jenis p-2" style="min-height: 350px;" data-echart-responsive="true"></div>
                                                                                </div>
                                                                            </div> -->
                </div>
            </div>
        </div>
    @endrole

    @section('modal')
        {{-- MODAL KATEGORI USAHA --}}
        <div class="modal fade" id="detailFashionModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Usaha Fashion</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_fashion" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailKategoriMaminModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Usaha Makanan Dan Minuman</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_mamin" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailKerajinanModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Usaha Kerajinan</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_kerajinan" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailTokoModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Usaha Toko</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_toko" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailJasaModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Usaha Jasa</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_jasa" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>




        {{-- MODAL KBLI --}}
        <!-- Modal #detailIndustriPengolahanModal -->
        <div class="modal fade" id="detailIndustriPengolahanModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Industri Pengolahan</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0 w-100"
                                    id="tableDetailIndustriPengolahan">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama Pelaku Usaha</th>
                                            <th class="text-center">Nama Usaha</th>
                                            <th class="text-center">Alamat</th>
                                            <th class="text-center">Kecamatan</th>
                                            <th class="text-center">Kelurahan</th>
                                            <th class="text-center">RT</th>
                                            <th class="text-center">RW</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPerdaganganModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <h5 class="fw-bold fs-1">Detail Perdagangan</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0 w-100"
                                    id="tableDetailPerdagangan">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama Pelaku Usaha</th>
                                            <th class="text-center">Nama Usaha</th>
                                            <th class="text-center">Alamat</th>
                                            <th class="text-center">Kecamatan</th>
                                            <th class="text-center">Kelurahan</th>
                                            <th class="text-center">RT</th>
                                            <th class="text-center">RW</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailAkomodasiMaminModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Akomodasi Mamin</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0 w-100"
                                    id="tableDetailAkomodasiMamin">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama Pelaku Usaha</th>
                                            <th class="text-center">Nama Usaha</th>
                                            <th class="text-center">Alamat</th>
                                            <th class="text-center">Kecamatan</th>
                                            <th class="text-center">Kelurahan</th>
                                            <th class="text-center">RT</th>
                                            <th class="text-center">RW</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailAktivitasJasaLainnya" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <h5 class="fw-bold fs-1">Detail Industri Aktivitas Jasa Lainnya</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0 w-100"
                                    id="tableDetailAktivitasJasa">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama Pelaku Usaha</th>
                                            <th class="text-center">Nama Usaha</th>
                                            <th class="text-center">Alamat</th>
                                            <th class="text-center">Kecamatan</th>
                                            <th class="text-center">Kelurahan</th>
                                            <th class="text-center">RT</th>
                                            <th class="text-center">RW</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="pertanianKehutananPerikanan" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounted-top-lg py-3 ps-4 pe-6 bg-light">
                            <h5 class="fw-bold fs-1">Detail Pertanian, Kehutanan Dan Perikanan</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0 w-100"
                                    id="tableDetailPertanianKehutananPerikanan">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th class="text-center">No.</th>
                                            <th class="text-center">Nama Pelaku Usaha</th>
                                            <th class="text-center">Nama Usaha</th>
                                            <th class="text-center">Alamat</th>
                                            <th class="text-center">Kecamatan</th>
                                            <th class="text-center">Kelurahan</th>
                                            <th class="text-center">RT</th>
                                            <th class="text-center">RW</th>
                                        </tr>
                                    </thead>
                                    <tbody class="text-center">

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                    </div>
                </div>
            </div>
        </div>


        {{-- MODAL INTERVENSI --}}
        <div class="modal fade" id="detailIntervensiSwk" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi SWK</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_swk" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailSkgModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi SKG</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_skg" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPasarModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Pasar</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_pasar" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailTokelModel" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Tokel</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_tokel" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPelatihan" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Pelatihan</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_pelatihan" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPameranModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Pameran</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_pameran" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <!-- <div class="modal fade" id="detailIndustriRumahanModal" tabindex="-1" role="dialog" aria-hidden="true">
                                                                        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                                                            <div class="modal-content position-relative">
                                                                                <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                                                                                    <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                                </div>
                                                                                <div class="modal-body p-0">
                                                                                    <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                                                                                        <h5 class="fw-bold fs-1">Detail Intervensi Industri Rumahan</h5>
                                                                                    </div>
                                                                                    <div class="p-4 pb-2">
                                                                                        <div class="table table-responsive">
                                                                                            <div class="table table-responsive">
                                                                                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0" id="datatable_industri_rumahan" style="width: 100%">
                                                                                                    <thead class="bg-200 text-900 border-tabel">
                                                                                                        <tr>
                                                                                                            <th>No.</th>
                                                                                                            <th>Nama Pelaku Usaha</th>
                                                                                                            <th>Nama Usaha</th>
                                                                                                            <th>Alamat</th>
                                                                                                            <th>Kecamatan</th>
                                                                                                            <th>Kelurahan</th>
                                                                                                            <th>RT</th>
                                                                                                            <th>RW</th>
                                                                                                        </tr>
                                                                                                    </thead>
                                                                                                    <tbody class="text-center">

                                                                                                    </tbody>
                                                                                                </table>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                                                                                        <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
        <div class="modal fade" id="detailBPUMModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <h5 class="fw-bold fs-1">Detail BPUM</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_BPUM" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel text-center">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailRumahKreatif" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Rumah Kreatif</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_rumah_kreatif" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPadatKaryaModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Padat Karya</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_padat_karya" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailKurModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Kur</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_kur" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailCsrModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi CSR</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_csr" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailPuspitaModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Intervensi Puspita</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_puspita" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>

        <!-- <div class="modal fade" id="detailSWKmodal" tabindex="-1" role="dialog" aria-hidden="true">
                                                                                                                                        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                                                                                                                                            <div class="modal-content position-relative">
                                                                                                                                                <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                                                                                                                                                    <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
                                                                                                                                                </div>
                                                                                                                                                <div class="modal-body p-0">
                                                                                                                                                        <h5 class="fw-bold fs-1">Detail Intervensi SWK</h5>
                                                                                                                                                    </div>
                                                                                                                                                    <div class="p-4 pb-2">
                                                                                                                                                        <div class="table table-responsive">
                                                                                                                                                            <div class="table table-responsive">
                                                                                                                                                                <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0" id="tableDetailIndustriPengolahan">
                                                                                                                                                                    <thead class="bg-200 text-900 border-tabel">
                                                                                                                                                                        <tr>
                                                                                                                                                                            <th>No.</th>
                                                                                                                                                                            <th>Nama Pelaku Usaha</th>
                                                                                                                                                                            <th>Nama Usaha</th>
                                                                                                                                                                            <th>Alamat</th>
                                                                                                                                                                            <th>Kecamatan</th>
                                                                                                                                                                            <th>Kelurahan</th>
                                                                                                                                                                            <th>RT</th>
                                                                                                                                                                            <th>RW</th>
                                                                                                                                                                        </tr>
                                                                                                                                                                    </thead>
                                                                                                                                                                    <tbody class="text-center">

                                                                                                                                                                    </tbody>
                                                                                                                                                                </table>
                                                                                                                                                            </div>
                                                                                                                                                        </div>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                                <div class="modal-footer">
                                                                                                                                                    <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                                                                                                                                                        <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                                                                                                                                                    </button>
                                                                                                                                                    </div>
                                                                                                                                                </div>
                                                                                                                                            </div>
                                                                                                                                        </div>
                                                                                                                                    </div> -->

        <div class="modal fade" id="detailMbrModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Data Warga Miskin</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_mbr" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailNonMbrModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Data Non Warga Miskin</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_non_mbr" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailBudidayaModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Kategori Budidaya Pertanian</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_budidaya" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="detailBelumIntervensiModal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
                <div class="modal-content position-relative">
                    <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                        <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                            data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-0">
                        <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                            <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                            <h5 class="fw-bold fs-1">Detail Belum Intervensi</h5>
                        </div>
                        <div class="p-4 pb-2">
                            <div class="table table-responsive">
                                <div class="table table-responsive">
                                    <table class="table table-bordered font-sans-serif table-bordered fs--1 mb-0"
                                        id="datatable_belum_intervensi" style="width: 100%">
                                        <thead class="bg-200 text-900 border-tabel">
                                            <tr>
                                                <th>No.</th>
                                                <th>Nama Pelaku Usaha</th>
                                                <th>Nama Usaha</th>
                                                <th>Alamat</th>
                                                <th>Kecamatan</th>
                                                <th>Kelurahan</th>
                                                <th>RT</th>
                                                <th>RW</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-center">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                        <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                            <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                        </button>
                        <!-- <button class="btn btn-primary" type="button"> </button> -->

                    </div>
                </div>
            </div>
        </div>
    @endsection

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @push('styles')
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
        <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>


        {{-- SCRIPT TABEL KATEGORI USAHA --}}
        <script>
            $(function() {
                var url = "{!! route('dataTableBudidaya') !!}";
                $('#datatable_budidaya').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableFashion') !!}";
                $('#datatable_fashion').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableMamin') !!}";
                $('#datatable_mamin').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableKerajinan') !!}";
                $('#datatable_kerajinan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableFashionToko') !!}";
                $('#datatable_toko').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableJasa') !!}";
                $('#datatable_jasa').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        {{-- END --}}

        <!-- Script Datatable KBLI -->
        <script>
            $(function() {
                var url = "{!! route('dataTablesIndustriPengolahan') !!}";
                $('#tableDetailIndustriPengolahan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablesPerdagangan') !!}";
                $('#tableDetailPerdagangan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablesAkomodasiUsaha') !!}";
                $('#tableDetailAkomodasiMamin').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablesAktivitasJasa') !!}";
                $('#tableDetailAktivitasJasa').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>

        <script>
            $(function() {
                var url = "{!! route('datatablePertanianKehutananPerikanan') !!}";
                $('#tableDetailPertanianKehutananPerikanan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>

        {{-- SCRIPT TABEL INTERVENSI --}}
        <script>
            $(function() {
                var url = "{!! route('dataTableSwk') !!}";
                $('#datatable_swk').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableSkg') !!}";
                $('#datatable_skg').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablePasar') !!}";
                $('#datatable_pasar').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableTokel') !!}";
                $('#datatable_tokel').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablePelatihan') !!}";
                $('#datatable_pelatihan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablePameran') !!}";
                $('#datatable_pameran').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data pameran",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableBPUM') !!}";
                $('#datatable_BPUM').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableBelumIntervensi') !!}";
                $('#datatable_belum_intervensi').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pemilik',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat_pemilik',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        {{-- <script>
            $(function() {
                var url = "{!! route('dataTableIndustriRumahan') !!}";
                $('#datatable_industri_rumahan').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script> --}}
        <script>
            $(function() {
                var url = "{!! route('dataTableRumahKreatif') !!}";
                $('#datatable_rumah_kreatif').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablePadatKarya') !!}";
                $('#datatable_padat_karya').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableKur') !!}";
                $('#datatable_kur').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableCsr') !!}";
                $('#datatable_csr').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTablePuspita') !!}";
                $('#datatable_puspita').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        {{-- END --}}

        {{-- SCRIPT TABEL MBR & NON MBR --}}
        <script>
            $(function() {
                var url = "{!! route('dataTableMbr') !!}";
                $('#datatable_mbr').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
        <script>
            $(function() {
                var url = "{!! route('dataTableNonMbr') !!}";
                $('#datatable_non_mbr').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'nama_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'alamat',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kecamatan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'kelurahan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rw',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },
                        {
                            data: 'rt',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return "Tidak Ada";
                                } else {
                                    return data;
                                }
                            }
                        },

                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>

        {{-- END --}}
        <script>
            $(function() {
                /* grafikstatusMBR(); */
                grafikInterverensi();
                var url = "{!! route('dataTablesWilayahDisdag') !!}";
                $('#wilayah_sebaran_umkm').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nm_kec',
                            name: 'nm_kec'
                        },
                        {
                            data: 'jumlah_swk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_skg_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pasar_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_tp_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pelatihan_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pameran_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_bpum_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        /*  {
                             data: 'jumlah_ir_non_mbr',
                             render: function(data, type, row) {
                                 if (data == null) {
                                     return 0;
                                 } else {
                                     return data;
                                 }

                             }
                         }, */
                        {
                            data: 'jumlah_rk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_padat_karya_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_kur_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_csr_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_puspita_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_belum_intervensi',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },

                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>

        {{-- <script>
        $(function() {
            /* grafikstatusMBR(); */
            grafikJenisUsaha();
            var url = "{!! route('dataTablesWilayah') !!}";
            $('#wilayah_sebaran_umkm').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nm_kec',
                        name: 'nm_kec'
                    },
                    {
                        data: 'swk_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_swk',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'skg_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_skg',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'rk_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_pasar',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'ir_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_rk',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'peken_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_ir',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'pasar_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_peken',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'umkm_jahit_mbr',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },
                    {
                        data: 'jumlah_tp',
                        render: function(data, type, row) {
                            if (data == null) {
                                return 0;
                            } else {
                                return data;
                            }

                        }
                    },

                ],
                "pageLength": 10,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
    </script> --}}

        <script>
            $(function() {
                $('#resetUMKM').on('click', function() {
                    // console.log("masuk");
                    $('#jenis_usaha').prop('selectedIndex', 0);
                    $('#tahun_laporan').prop('selectedIndex', 0);
                });

                $('#searchUMKM').on('click', function() {
                    // console.log("masuk");

                    var jenis_usaha = $('#jenis_usaha').val();
                    var tahun_laporan = $('#tahun_laporan').val();

                    // console.log(tahun_laporan);

                    if (jenis_usaha != "" && tahun_laporan != "") {
                        var url = "{!! route('dataTotalOmset') !!}";

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $('#omset_umkm').DataTable({
                            processing: true,
                            serverSide: true,
                            bDestroy: true,
                            ajax: {
                                url: url,
                                type: "POST",
                                data: {
                                    jenis_umkm: jenis_usaha,
                                    tahun_laporan: tahun_laporan
                                },
                            },
                            columns: [{
                                    data: 'DT_RowIndex',
                                    name: 'DT_RowIndex',
                                    orderable: false,
                                    searchable: false
                                },
                                {
                                    data: 'nama_sub_kategori_usaha',
                                    name: 'nama_sub_kategori_usaha'
                                },
                                {
                                    data: 'jum_omset_januari',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')

                                },
                                {
                                    data: 'jum_omset_februari',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_maret',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_april',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_mei',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_juni',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_juli',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_agustus',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_september',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_oktober',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_november',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_desember',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },

                            ],
                            "pageLength": 10,
                            "lengthMenu": [
                                [5, 10, 25, 50, -1],
                                [5, 10, 25, 50, "All"]
                            ],
                            "language": {
                                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                                "infoFiltered": "(disaring dari _MAX_ total data)",
                                "lengthMenu": "Menampilkan _MENU_ data",
                                "search": "Cari:",
                                "zeroRecords": "Tidak ada data yang sesuai",
                                /* Kostum pagination dengan element baru */
                                "paginate": {
                                    "previous": "<i class='fas fa-angle-left'></i>",
                                    "next": "<i class='fas fa-angle-right'></i>"
                                }
                            },
                        });

                    }

                });


            });
        </script>

        <script>
            $(function() {
                var url = "{!! route('dataTablesPenerimaCsr') !!}";
                $('#penerima_csr').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_perusahaan',
                            name: 'nama_perusahaan'
                        },
                        {
                            data: 'jum_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Menghilangkan penomoran dan hanya menampilkan tombol sebelumn sesudah */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }

                    },
                });
            });
        </script>

        <script>
            // Grafik Status MBR
            /* function grafikstatusMBR() {

                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });

                    $.ajax({
                        type: 'post',
                        url: "{{ route('grafikStatusMBR') }}",
                        dataType: 'json',
                        success: function(data) {

                            // console.log(data);

                            // Swal.close();

                            // jum_data_sw();
                            // data_valid();

                            data_kec = [];
                            data_mbr = [];
                            data_non_mbr = [];

                            data.forEach(element => {
                                data_kec.push(element.nm_kec);
                                data_mbr.push(element.jum_pemilik_mbr);
                                data_non_mbr.push(element.jum_pemilik_non_mbr);
                            });

                            // console.log(data_non_mbr);

                            var $horizontalStackChartEl = document.querySelector('.pelaku-usaha-mbr');
                            if ($horizontalStackChartEl) {
                                // Get options from data attribute
                                var userOptions = utils.getData($horizontalStackChartEl, 'options');
                                var chart = window.echarts.init($horizontalStackChartEl);
                                var kec = data_kec;

                                var getDefaultOptions = function getDefaultOptions() {
                                    return {
                                        color: ["#7b1fa2", "#e1bee7"],
                                        tooltip: {
                                            trigger: 'axis',
                                            axisPointer: {
                                                type: 'shadow'
                                            },
                                            padding: [7, 10],
                                            backgroundColor: utils.getGrays()['100'],
                                            borderColor: utils.getGrays()['300'],
                                            textStyle: {
                                                color: utils.getColors().dark
                                            },
                                            borderWidth: 10,
                                            transitionDuration: 0.5,
                                            formatter: tooltipFormatter
                                        },
                                        toolbox: {
                                            feature: {
                                                magicType: {
                                                    type: ['tiled']
                                                }
                                            },
                                            right: 0
                                        },
                                        legend: {
                                            data: ['MBR', 'NON MBR'],
                                            textStyle: {
                                                color: utils.getGrays()['600']
                                            },
                                            // left: 0
                                        },
                                        xAxis: {
                                            type: 'value',
                                            axisLine: {
                                                show: true,
                                                lineStyle: {
                                                    color: utils.getGrays()['300']
                                                }
                                            },
                                            axisTick: {
                                                show: false
                                            },
                                            axisLabel: {
                                                color: utils.getGrays()['500']
                                            },
                                            splitLine: {
                                                lineStyle: {
                                                    show: true,
                                                    color: utils.getGrays()['200']
                                                }
                                            }
                                        },
                                        yAxis: {

                                            data: kec,
                                            axisLine: {
                                                lineStyle: {
                                                    show: true,
                                                    color: utils.getGrays()['300']
                                                }
                                            },
                                            axisTick: {
                                                show: false
                                            },
                                            axisLabel: {
                                                color: utils.getGrays()['500'],
                                                // formatter: function formatter(value) {
                                                // return value.substring(0, 3);
                                                // }
                                            }
                                        },
                                        series: [{
                                            name: 'MBR',
                                            type: 'bar',
                                            stack: 'total',
                                            label: {
                                                show: true,
                                                textStyle: {
                                                    color: '#000'
                                                }
                                            },
                                            emphasis: {
                                                focus: 'series'
                                            },
                                            data: data_mbr
                                        }, {
                                            name: 'NON MBR',
                                            type: 'bar',
                                            stack: 'total',
                                            label: {
                                                show: true,

                                            },
                                            emphasis: {
                                                focus: 'series'
                                            },
                                            data: data_non_mbr
                                        }],
                                        grid: {
                                            right: 5,
                                            left: 5,
                                            // bottom: 20,
                                            // top: '15%',
                                            containLabel: true
                                        }
                                        // grid: {
                                        //     left: "3%",
                                        //     right: "4%",
                                        //     bottom: "0%",
                                        //     containLabel: true
                                        // },
                                    };
                                };

                                echartSetOption(chart, userOptions, getDefaultOptions);
                            };
                            // END GRAFIK STATUS MBR
                        },
                        error: function(data) {
                            // Swal.close();

                        console.log(data);
                    }
                });
            } */

            function grafikInterverensi() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: "{{ route('grafikInterverensi') }}",
                    dataType: 'json',
                    success: function(data) {
                        // console.log(data);
                        // Swal.close();
                        // jum_data_sw();
                        // data_valid();

                        data_kec = [];
                        data_swk = [];
                        data_skg = [];
                        data_pasar = [];
                        data_tp = [];
                        data_pelatihan = [];
                        data_pameran = [];
                        data_bpum = [];
                        /* data_ir = []; */
                        data_rk = [];
                        data_padat_karya = [];
                        data_kur = [];
                        data_csr = [];
                        data_puspita = [];
                        data_belum_intervensi = [];

                        data.forEach(element => {
                            data_kec.push(element.nm_kec);
                            data_swk.push(element.jumlah_swk_non_mbr);
                            data_skg.push(element.jumlah_skg_non_mbr);
                            data_pasar.push(element.jumlah_pasar_non_mbr);
                            data_tp.push(element.jumlah_tp_non_mbr);
                            data_pelatihan.push(element.jumlah_pelatihan_non_mbr);
                            data_pameran.push(element.jumlah_pameran_non_mbr);
                            data_bpum.push(element.jumlah_pameran_non_mbr);
                            /* data_ir.push(element.jumlah_ir_non_mbr); */
                            data_rk.push(element.jumlah_rk_non_mbr);
                            data_padat_karya.push(element.jumlah_padat_karya_non_mbr);
                            data_kur.push(element.jumlah_kur_non_mbr);
                            data_csr.push(element.jumlah_csr_non_mbr);
                            data_puspita.push(element.jumlah_puspita_non_mbr);
                            data_belum_intervensi.push(element.jumlah_belum_intervensi);
                        });
                        // console.log(data_kec);

                        // Grafik Status Jenis Usaha
                        var $horizontalStackChartEl = document.querySelector('.intervensi-um');
                        if ($horizontalStackChartEl) {
                            // Get options from data attribute
                            var userOptions = utils.getData($horizontalStackChartEl, 'options');
                            var chart = window.echarts.init($horizontalStackChartEl);
                            var kec = data_kec;

                            var getDefaultOptions = function getDefaultOptions() {
                                return {
                                    color: ['#1e88e5', '#b71c1c', '#fdd835', '#4caf50', '#f57c00', '#00bcd4',
                                        '#b388ff', '#795548', '#9e9d24', '#00796b', '#546e7a'
                                    ],
                                    tooltip: {
                                        trigger: 'axis',
                                        axisPointer: {
                                            type: 'shadow'
                                        },
                                        padding: [7, 10],
                                        backgroundColor: utils.getGrays()['100'],
                                        borderColor: utils.getGrays()['300'],
                                        textStyle: {
                                            color: utils.getColors().dark
                                        },
                                        borderWidth: 1,
                                        transitionDuration: 0,
                                        formatter: tooltipFormatter
                                    },
                                    toolbox: {
                                        feature: {
                                            magicType: {
                                                type: ['tiled']
                                            }
                                        },
                                        right: 0
                                    },
                                    legend: {
                                        data: ['SWK', 'SKG', 'PASAR', 'PEKEN', 'PELATIHAN', 'PAMERAN', 'BPUM',
                                            'RUMAH KREATIF', 'PADAT_KARYA', 'KUR', 'CSR', 'PUSPITA',
                                            'BELUM INTERVENSI'
                                        ],
                                        textStyle: {
                                            color: utils.getGrays()['600']
                                        },
                                        // left: 0
                                    },
                                    xAxis: {
                                        type: 'value',
                                        axisLine: {
                                            show: true,
                                            lineStyle: {
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500']
                                        },
                                        splitLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['200']
                                            }
                                        }
                                    },
                                    yAxis: {
                                        type: 'category',
                                        data: data_kec,
                                        axisLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500'],
                                            // formatter: function formatter(value) {
                                            // return value.substring(0, 3);
                                            // }
                                        }
                                    },
                                    series: [{
                                        name: 'SWK',
                                        type: 'bar',
                                        barMinWidth: '100%',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_swk
                                    }, {
                                        name: 'SKG',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_skg
                                    }, {
                                        name: 'PASAR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pasar
                                    }, {
                                        name: 'PEKEN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_tp
                                    }, {
                                        name: 'PELATIHAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pelatihan
                                    }, {
                                        name: 'PAMERAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pameran
                                    }, {
                                        name: 'BPUM',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_bpum
                                    }, {
                                        name: 'RUMAH KREATIF',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_rk
                                    }, {
                                        name: 'PADAT_KARYA',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_padat_karya
                                    }, {
                                        name: 'KUR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_kur
                                    }, {
                                        name: 'CSR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_csr
                                    }, {
                                        name: 'PUSPITA',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_puspita
                                    }, {
                                        name: 'BELUM INTERVENSI',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_belum_intervensi
                                    }, ],
                                    grid: {
                                        right: 7,
                                        left: 7,
                                        // bottom: 5,
                                        // top: '15%',
                                        containLabel: true
                                    }
                                };
                            };

                            echartSetOption(chart, userOptions, getDefaultOptions);
                        };

                    },
                    error: function(data) {
                        // Swal.close();

                        console.log(data);
                    }
                });
            }

            // function grafikJenisUsaha() {
            //     $.ajaxSetup({
            //         headers: {
            //             'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //         }
            // });

            //     $.ajax({
            //         type: 'post',
            //         url: "{{ route('grafikJenisUsaha') }}",
            //         dataType: 'json',
            //         success: function(data) {
            //             // console.log(data);
            //             // Swal.close();
            //             // jum_data_sw();
            //             // data_valid();
            //             data_kec = [];
            //             data_swk = [];
            //             data_tokel = [];
            //             data_umkm = [];
            //             data_laundry = [];
            //             data_jahit = [];
            //             data_cuci_sepatu = [];
            //             data_cuci_helm = [];
            //             data.forEach(element => {
            //                 data_kec.push(element.nm_kec);
            //                 data_swk.push(element.jum_pemilik_swk);
            //                 data_tokel.push(element.jum_pemilik_tokel);
            //                 data_umkm.push(element.jum_pemilik_umkm);
            //                 data_laundry.push(element.jum_pemilik_laundry);
            //                 data_jahit.push(element.jum_pemilik_jahit);
            //                 data_cuci_sepatu.push(element.jum_pemilik_cuci_sepatu);
            //                 data_cuci_helm.push(element.jum_pemilik_cuci_helm);
            //             });
            //             // console.log(data_kec);

            //             // Grafik Status Jenis Usaha
            //             var $horizontalStackChartEl = document.querySelector('.pelaku-usaha-jenis');
            //             if ($horizontalStackChartEl) {
            //                 // Get options from data attribute
            //                 var userOptions = utils.getData($horizontalStackChartEl, 'options');
            //                 var chart = window.echarts.init($horizontalStackChartEl);
            //                 var kec = data_kec;

            //                 var getDefaultOptions = function getDefaultOptions() {
            //                     return {
            //                         color: ['#1e88e5', '#b71c1c', '#fdd835', '#4caf50', '#f57c00', '#00bcd4',
            //                             '#b388ff'
            //                         ],
            //                         tooltip: {
            //                             trigger: 'axis',
            //                             axisPointer: {
            //                                 type: 'shadow'
            //                             },
            //                             padding: [7, 10],
            //                             backgroundColor: utils.getGrays()['100'],
            //                             borderColor: utils.getGrays()['300'],
            //                             textStyle: {
            //                                 color: utils.getColors().dark
            //                             },
            //                             borderWidth: 1,
            //                             transitionDuration: 0,
            //                             formatter: tooltipFormatter
            //                         },
            //                         toolbox: {
            //                             feature: {
            //                                 magicType: {
            //                                     type: ['tiled']
            //                                 }
            //                             },
            //                             right: 0
            //                         },
            //                         legend: {
            //                             data: ['SWK', 'TOKEL', 'UMKM', 'Laundry', 'Jahit', 'Cuci Sepatu',
            //                                 'Cuci Helm'
            //                             ],
            //                             textStyle: {
            //                                 color: utils.getGrays()['600']
            //                             },
            //                             // left: 0
            //                         },
            //                         xAxis: {
            //                             type: 'value',
            //                             axisLine: {
            //                                 show: true,
            //                                 lineStyle: {
            //                                     color: utils.getGrays()['300']
            //                                 }
            //                             },
            //                             axisTick: {
            //                                 show: false
            //                             },
            //                             axisLabel: {
            //                                 color: utils.getGrays()['500']
            //                             },
            //                             splitLine: {
            //                                 lineStyle: {
            //                                     show: true,
            //                                     color: utils.getGrays()['200']
            //                                 }
            //                             }
            //                         },
            //                         yAxis: {
            //                             type: 'category',
            //                             data: kec,
            //                             axisLine: {
            //                                 lineStyle: {
            //                                     show: true,
            //                                     color: utils.getGrays()['300']
            //                                 }
            //                             },
            //                             axisTick: {
            //                                 show: false
            //                             },
            //                             axisLabel: {
            //                                 color: utils.getGrays()['500'],
            //                                 // formatter: function formatter(value) {
            //                                 // return value.substring(0, 3);
            //                                 // }
            //                             }
            //                         },
            //                         series: [{
            //                             name: 'SWK',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true,
            //                                 // textStyle: {
            //                                 // 	color: '#fff'
            //                                 // }
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_swk
            //                         }, {
            //                             name: 'TOKEL',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_tokel
            //                         }, {
            //                             name: 'UMKM',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true,
            //                                 // textStyle: {
            //                                 // 	color: '#fff'
            //                                 // }
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_umkm
            //                         }, {
            //                             name: 'Laundry',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true,
            //                                 // textStyle: {
            //                                 // 	color: '#fff'
            //                                 // }
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_laundry
            //                         }, {
            //                             name: 'Jahit',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_jahit
            //                         }, {
            //                             name: 'Cuci Sepatu',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_cuci_sepatu
            //                         }, {
            //                             name: 'Cuci Helm',
            //                             type: 'bar',
            //                             stack: 'total',
            //                             label: {
            //                                 show: true
            //                             },
            //                             emphasis: {
            //                                 focus: 'series'
            //                             },
            //                             data: data_cuci_helm
            //                         }, ],
            //                         grid: {
            //                             right: 5,
            //                             left: 5,
            //                             // bottom: 5,
            //                             // top: '15%',
            //                             containLabel: true
            //                         }
            //                     };
            //                 };
            //                 echartSetOption(chart, userOptions, getDefaultOptions);
            //             };
            //             // END GRAFIK STATUS Jenis Usaha
            //         },
            //         error: function(data) {
            //             // Swal.close();
            //             console.log(data);
            //         }
            //     });
            // }

            // Grafik Jumlah Omset
            var $lineAreaChartEl = document.querySelector('.jumlah-omset');
            if ($lineAreaChartEl) {
                // Get options from data attribute
                var userOptions = utils.getData($lineAreaChartEl, 'options');
                var chart = window.echarts.init($lineAreaChartEl);
                var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'
                ];
                var data = [1142, 1160, 1179, 946, 1420, 1434, 986, 1247, 1051, 1297, 927, 1282];

                var _tooltipFormatter7 = function _tooltipFormatter7(params) {
                    return "\n      <div>\n          <h6 class=\"fs--1 text-700 mb-0\">\n            <span class=\"fas fa-circle me-1\" style='color:"
                        .concat(params[0].borderColor, "'></span>\n            ").concat(params[0].name, " : ").concat(
                            params[0].value, "\n          </h6>\n      </div>\n      ");
                };

                var getDefaultOptions = function getDefaultOptions() {
                    return {
                        tooltip: {
                            trigger: 'axis',
                            padding: [7, 10],
                            backgroundColor: utils.getGrays()['100'],
                            borderColor: utils.getGrays()['300'],
                            textStyle: {
                                color: utils.getColors().dark
                            },
                            borderWidth: 1,
                            formatter: _tooltipFormatter7,
                            transitionDuration: 0,
                            axisPointer: {
                                type: 'none'
                            }
                        },
                        xAxis: {
                            type: 'category',
                            data: months,
                            boundaryGap: false,
                            axisLine: {
                                lineStyle: {
                                    color: utils.getGrays()['300'],
                                    type: 'solid'
                                }
                            },
                            axisTick: {
                                show: false
                            },
                            axisLabel: {
                                color: utils.getGrays()['400'],
                                formatter: function formatter(value) {
                                    return value.substring(0, 3);
                                },
                                margin: 15
                            },
                            splitLine: {
                                show: false
                            }
                        },
                        yAxis: {
                            type: 'value',
                            splitLine: {
                                lineStyle: {
                                    color: utils.getGrays()['200']
                                }
                            },
                            boundaryGap: false,
                            axisLabel: {
                                show: true,
                                color: utils.getGrays()['400'],
                                margin: 15
                            },
                            axisTick: {
                                show: false
                            },
                            axisLine: {
                                show: false
                            },
                            min: 600
                        },
                        series: [{
                            type: 'line',
                            data: data,
                            itemStyle: {
                                color: utils.getGrays().white,
                                borderColor: utils.getColor('primary'),
                                borderWidth: 2
                            },
                            lineStyle: {
                                color: utils.getColor('primary')
                            },
                            showSymbol: false,
                            symbolSize: 10,
                            symbol: 'circle',
                            smooth: false,
                            hoverAnimation: true,
                            areaStyle: {
                                color: {
                                    type: 'linear',
                                    x: 0,
                                    y: 0,
                                    x2: 0,
                                    y2: 1,
                                    colorStops: [{
                                        offset: 0,
                                        color: utils.rgbaColor(utils.getColors().primary, 0.5)
                                    }, {
                                        offset: 1,
                                        color: utils.rgbaColor(utils.getColors().primary, 0)
                                    }]
                                }
                            }
                        }],
                        grid: {
                            right: '3%',
                            left: '3%',
                            bottom: '10%',
                            top: '5%'
                        }
                    };
                };

                echartSetOption(chart, userOptions, getDefaultOptions);
            }
            // END GRAFIK JUMLAH Omset
        </script>

        <script>
            $(function() {
                var url = "{!! route('dataTablesWilayahSebaranUkm') !!}";
                $('#wilayah_sebaran_ukm').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nm_kec',
                            name: 'nm_kec'
                        },
                        {
                            data: 'tokel_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }
                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);

                                $(td).css('background-color', '#bbdefb');
                            }
                        },
                        {
                            data: 'tokel_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }
                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#bbdefb');
                            }
                        },
                        {
                            data: 'jahit_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }

                        },
                        {
                            data: 'jahit_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_sepatu_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_sepatu_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_helm_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_helm_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'laundry_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'laundry_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'swk_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'swk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'umkm_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'umkm_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },

                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
        </script>
    @endpush

</x-templates.default>
