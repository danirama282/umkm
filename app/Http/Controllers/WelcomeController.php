<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    /* Fungsi invoke */
    public function __invoke()
    {
        $data_user['user'] = auth()->user()->load('roles');

        /* Mengambil data role yang dipunya user */
        $data_user['role'] = $data_user['user']->roles->first()->name;
        return view('welcome', compact('data_user'));
    }
}
