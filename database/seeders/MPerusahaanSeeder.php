<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MPerusahaanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat data untuk MPerusahaanModel
        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BANK INDONESIA',
            'alamat_perusahaan' => 'Jl. Bumi No. 1',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BANK MANDIRI',
            'alamat_perusahaan' => 'Jl. Bumi No. 2',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BANK JATIM',
            'alamat_perusahaan' => 'Jl. Bumi No. 3',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BANK SAU',
            'alamat_perusahaan' => 'Jl. Bumi No. 4',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'UNIVERSITAS CIPUTRA',
            'alamat_perusahaan' => 'Jl. Bumi No. 5',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'UNIVERSITAS HAYAM WURUK',
            'alamat_perusahaan' => 'Jl. Bumi No. 6',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BOGASARI',
            'alamat_perusahaan' => 'Jl. Bumi No. 7',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'TOKOPEDIA',
            'alamat_perusahaan' => 'Jl. Bumi No. 8',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'GRAB',
            'alamat_perusahaan' => 'Jl. Bumi No. 9',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'BPJS KETENAGAKERJAAN',
            'alamat_perusahaan' => 'Jl. Bumi No. 10',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_perusahaan')->insert([
            'nama_perusahaan' => 'SAMPOERNA RETAIL COMMUNITY',
            'alamat_perusahaan' => 'Jl. Bumi No. 11',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
