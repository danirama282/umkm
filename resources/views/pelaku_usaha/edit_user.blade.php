<div class="row g-0">
    <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2"></div>
    <div class="card">
        <div class="card-header">
            <h5 class="fw-bold fs-1 mt-2">Ubah Data Pelaku Usaha</h5>
        </div>
        <div class="card-body ">
            <form id="formEditKecKel">
                @csrf
                <input type="hidden" name="id_pelaku_usaha" id="id_pelaku_usaha"
                    value="{{ Crypt::encryptString($data['pelaku_usaha']->id_t_pelaku_usaha_disdag_181022) }}">
                <input type="hidden" name="status_peken" id="status_peken"
                    value="{{ $data['pelaku_usaha']->peken_tokel }}">
                <input type="hidden" name="status_padat_karya" id="status_padat_karya"
                    value="{{ $data['pelaku_usaha']->padat_karya }}">

                <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">
                <!-- Sub judul dengan garis -->
                <div class="row mb-3">
                    <div class="col-sm-12">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Pelaku Usaha</h6>
                    </div>
                </div>
                {{-- {{ dd($data['pelaku_usaha']->is_surabaya) }} --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="status_kependudukan">Status
                        Kependudukan</label>
                    <div class="col-sm-3">
                        <div class="form-check"><input class="form-check-input" id="surabaya" type="radio" name="status"
                                value=1 onclick="showButton()" {{ $data['pelaku_usaha']->is_surabaya == 1 ? 'checked' :
                            '' }} /><label class="form-check-label" for="surabaya">Surabaya</label>
                        </div>
                        <div class="form-check"><input class="form-check-input" id="nonSurabaya" type="radio"
                                name="status" onclick="hideButton()" value=0 {{ $data['pelaku_usaha']->is_surabaya == 0
                            ? 'checked' : '' }} /><label class="form-check-label" for="nonSurabaya">Non Surabaya</label>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nik">NIK Pelaku
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nik" value="{{ $data['pelaku_usaha']->nik }}" name="nik"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_pelaku_usaha">Nama Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nama_pelaku_usaha"
                            value="{{ $data['pelaku_usaha']->nama_pelaku_usaha }}" name="nama_pelaku_usaha"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_ktp">Alamat
                        Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <!-- Text Area bootstrap 5 -->
                        <textarea class="form-control" value="{{ $data['pelaku_usaha']->alamat_ktp }}" name="alamat_ktp"
                            id="alamat_ktp">{{ $data['pelaku_usaha']->alamat_ktp }}</textarea>
                    </div>
                </div>
                {{-- <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan">Kecamatan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="kecamatan" value="{{ $data['pelaku_usaha']->kecamatan }}"
                            name="kecamatan" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan">Kelurahan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="kelurahan" value="{{ $data['pelaku_usaha']->kelurahan }}"
                            name="kelurahan" type="text" />
                    </div>
                </div> --}}
                <div class="row mb-3 provinsi">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="provinsi_pelaku">Provinsi
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select class="form-select js-select2" id="provinsi" name="provinsi" data-control="select2"
                            {{-- data-placeholder="- Pilih Provinsi -" --}}>
                            <option value="" selected disabled>Pilih :</option>
                            @foreach ($data['provinsi_non_sby'] as $item)
                            <option value="{{ $item->id_m_setup_prop }}" {{ $data['pelaku_usaha']->provinsi_pemilik ==
                                $item->id_m_setup_prop ? 'selected' : '' }}>{{ $item->nm_prop }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3 kabupaten_kota">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kabupaten_kota">Kabupaten
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select class="form-select js-select2" id="kabupaten_kota" name="kabupaten_kota"
                            data-control="select2" {{-- data-placeholder="- Pilih Kabupaten/Kota -" --}}>
                            <option value="" selected disabled>Pilih :</option>
                            @if ($data['kabupaten_pemilik'] == 'kosong')
                            <option value="" selected disabled>Pilih :</option>
                            @else
                            <option value="{{ $data['kabupaten_pemilik'][0]->id_m_setup_kab }}" selected>
                                {{ $data['kabupaten_pemilik'][0]->nm_kab }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm my-lg-1" for="kecamatan">Kecamatan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select name="kecamatan" data-control="select2" id="kecamatan" class="form-select js-select2"
                            aria-label="Default select example" {{-- {{ $data['kelurahan_pemilik']=='kosong' ? ''
                            : 'readonly' }} --}}>
                            <!-- Menjalankan foreach dari $data['kecamatan'] -->
                            <option value="" selected disabled>Pilih :</option>
                            @foreach ($data['kecamatan'] as $item)
                            <option value="{{ $item->id_m_setup_kec }}" {{ $data['pelaku_usaha']->kecamatan_pemilik ==
                                $item->id_m_setup_kec ? 'selected' : '' }}>
                                {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm my-lg-1" for="kelurahan">Kelurahan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select name="kelurahan" data-control="select2" id="kelurahan" class="form-select js-select2"
                            aria-label="Default select example" {{-- {{ $data['kelurahan_pemilik']=='kosong' ? ''
                            : 'readonly' }} --}}>
                            <option value="" selected disabled>Pilih :</option>

                            <!-- Jika $data['kelurahan_usaha'][0]->id_m_setup_kel == null maka akan menampilkan option kosong -->
                            @if ($data['kelurahan_pemilik'] == 'kosong')
                            <option value="" selected disabled>Pilih :</option>
                            @else
                            <option value="{{ $data['kelurahan_pemilik'][0]->id_m_setup_kel }}" selected>
                                {{ $data['kelurahan_pemilik'][0]->nm_kel }}</option>
                            <!-- Jika $data['kelurahan_pemilik'][0]->id_m_setup_kel != 'kosong' maka akan menampilkan option sesuai dengan data yang dipilih -->
                            @endif
                        </select>
                    </div>
                </div>
                <input type="hidden" name="kelurahan" value="{{ $data['kelurahan_pemilik'][0]->id_m_setup_kel }}">
                {{-- <input type="hidden" name="kelurahan" value=""> --}}
                {{-- <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="rt_ktp">RT Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="rt_ktp" value="{{ $data['pelaku_usaha']->rt_ktp }}"
                            name="rt_ktp" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="rw_ktp">RW Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="rw_ktp" value="{{ $data['pelaku_usaha']->rw_ktp }}"
                            name="rw_ktp" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="tanggal_lahir">Tanggal
                        Lahir Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <!-- Input date -->
                        <input class="form-control" type="date" id="tanggal_lahir"
                            value="{{ $data['pelaku_usaha']->tanggal_lahir }}" name="tanggal_lahir">
                    </div>
                </div> --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="no_telp">No. Telepon
                        Pelaku
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="no_telp" value="{{ $data['pelaku_usaha']->no_telp }}"
                            name="no_telp" type="number" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_domisili">Alamat
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="check_alamat_domisili" id="check_alamat_domisili"
                            {{strtolower($data['pelaku_usaha']->alamat_domisili) ==
                        strtolower($data['pelaku_usaha']->alamat_ktp) ? 'checked' :
                        ''}}>
                        <label class="form-check-label">Sesuai Dengan KTP</label>
                        <input class="form-control" id="alamat_domisili" name="alamat_domisili" type="text"
                            value="{{ $data['pelaku_usaha']->alamat_domisili }}" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_domisili">Kecamatan
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                            <!-- Menjalankan foreach dari $data['kecamatan'] -->
                            @foreach ($data['kecamatan'] as $item)
                            <option value="{{ $item->id_m_setup_kec }}" {{ $data['pelaku_usaha']->kecamatan_domisili ==
                                $item->id_m_setup_kec ? 'selected' : '' }}>
                                {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_domisili">Kelurahan
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                        </select>
                    </div>
                </div>

                <div class="row mb-3">
                    <div class="col-sm-12 mt-3">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Usaha</h6>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="status_keaktifan">Status
                        Keaktifan</label>
                    <div class="col-sm-10">
                        <select value="{{ $data['pelaku_usaha']->status_keaktifan }}" name="status_keaktifan"
                            id="status_keaktifan" class="form-select js-select2" aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['status_keaktifan'] as $index => $value)
                            <option value="{{ $index }}" {{ $data['pelaku_usaha']->status_keaktifan ==
                                $index ? 'selected' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_usaha">Nama
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nama_usaha" value="{{ $data['pelaku_usaha']->nama_usaha }}"
                            name="nama_usaha" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_usaha">Alamat
                        Usaha</label>
                    <div class="col-sm-10">
                        <!-- Text Area bootstrap 5 -->
                        <textarea class="form-control" name="alamat_usaha" id="alamat_usaha"
                            value="{{ $data['pelaku_usaha']->alamat_usaha }}">{{ $data['pelaku_usaha']->alamat_usaha }}</textarea>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_usaha">Kecamatan
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_usaha" id="kecamatan_usaha" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['kecamatan'] as $item)
                            <option value="{{ $item->id_m_setup_kec }}" {{ $data['pelaku_usaha']->kecamatan_usaha ==
                                $item->id_m_setup_kec ? 'selected' : '' }}>
                                {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_usaha">Kelurahan
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kelurahan_usaha" id="kelurahan_usaha" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                            {{-- @if ($data['kelurahan_usaha'] == null)
                            <option value="" selected disabled>Pilih :</option>
                            @elseif ($data['kelurahan_usaha'][0]->id_m_setup_kel != null)
                            <!-- Jika $data['kelurahan_usaha'][0]->id_m_setup_kel != null maka akan menampilkan option sesuai dengan data yang dipilih -->
                            <option value="{{ $data['kelurahan_usaha'][0]->id_m_setup_kel }}" selected>
                                {{ $data['kelurahan_usaha'][0]->nm_kel }}</option>
                            @endif --}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kategori_jenis_produk">Kategori
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kategori_jenis_produk" id="kategori_jenis_produk" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['kategori_usaha'] as $index => $value)
                            <option value="{{ $index }}" {{ $data['pelaku_usaha']->kategori_jenis_produk ==
                                $index ? 'selected' : '' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="jenis_produk">Produk
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="jenis_produk" value="{{ $data['pelaku_usaha']->jenis_produk }}"
                            name="jenis_produk" type="text" />
                    </div>
                </div>
                <div class="alert alert-dark">
                    <div class="form-check">
                        <input class="form-check-input" id="chck_submit" type="checkbox" value="" /><label
                            class="form-check-label" for="flexCheckDefault">Dengan Ini saya
                            bertanggung jawab atas kebenaran data dan informasi pelaku usaha yang telah kami isikan
                            dan
                            validasi, apabila terdapat data dan informasi pelaku usaha yang keliru akibat
                            ketidaktelitian dan ketidakcermatan, saya bersedia untuk melakukan verifikasi
                            ulang</label>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <!-- Button kembali untuk back ke route pelaku_usaha.index -->
                    <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                        <span class="fas fas fa-backward me-2" data-fa-transform="shrink-3"></span> Kembali
                    </a>
                    <button class="btn btn-primary me-1 mb-1" id="submit" type="submit">
                        <span class="fas fa-edit me-2" data-fa-transform="shrink-3"></span> Edit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
