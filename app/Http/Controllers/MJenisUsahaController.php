<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\MJenisUsahaModel;

class MJenisUsahaController extends Controller
{
    //
    public function index()
    {
        return view('jenis_usaha.index');
    }

    public function create()
    {
        return view('jenis_usaha.create');
    }

    public function store(Request $request, MJenisUsahaModel $jenis_usaha)
    {
        /* Membuat validasi response json */
        $valid = $request->validate([
            'nama_jenis_usaha' => 'required',
        ]);

        /* Jika data $valid == true */
        if ($valid) {
            try {
              /* Proses menyimpan kedalam model dengan variabel $jenis_usaha */
                $jenis_usaha->create([
                    'nama_jenis_usaha' => $request->nama_jenis_usaha,
                ]);

                /* Mengembalikan response json */
                return response()->json([
                    'status' => 'success',
                    'message' => 'Data berhasil disimpan!',
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Data gagal disimpan!',
                ]);
            }
        } else {
           /* return json dengan status check */
            return response()->json([
                'status' => 'check',
                'message' => 'Data tidak boleh kosong!',
            ]);
        }
    }

    public function edit($id)
    {
        return view('jenis_usaha.edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
