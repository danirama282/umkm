<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJenisUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_jenis_usaha';
    protected $fillable = [
        'nama_jenis_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function jenis_usaha_det()
    {
        return $this->hasMany(MJenisUsahaDetailModel::class, 'm_jenis_usaha_id', 'id');
    }
}
