<x-templates.default>
    <x-slot name="title">Rubah Data Bantuan</x-slot>


    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="fw-semi-bold">Edit Data Bantuan</h3>
                </div>

                <div class="card-body ">
                    <form class="form fv-plugins-bootstrap5 fv-plugins-framework" id="form_submit_bantuan"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="pelaku_usaha_id" name="pelaku_usaha_id"
                            value="{{ Crypt::encryptString($bantuan->t_pelaku_usaha_id) }}">
                        {{-- <input type="hidden" id="bantuan_id" name="bantuan_id"
                            value="{{ Crypt::encryptString($bantuan->id) }}"> --}}
                        {{-- {{dd(Crypt::encryptString($bantuan->t_pelaku_usaha_id))}} --}}
                        {{-- {{dd($bantuan->toArray())}} --}}
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="recipient-name">Tanggal
                                Menerima Bantuan</label>
                            <div class="col-sm-10">
                                <input class="form-select datetimepicker" id="tanggal_bantuan" name="tanggal_bantuan"
                                    type="date" value="{{ $bantuan->tanggal_menerima_bantuan }}" />
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                Bantuan</label>
                            <div class="col-sm-10">
                                <select class="form-select js-select2" id="jenis_bantuan" name="jenis_bantuan"
                                    data-control="select2" data-placeholder="- Pilih Jenis Bantuan -">
                                    <option></option>
                                    @foreach ($jenisbantuan as $div)
                                        <option value="{{ $div->id }}"
                                            {{ $div->id == $bantuan->m_jenis_bantuan_id ? 'selected' : '' }}>
                                            {{ $div->nama_jenis_bantuan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Apakah dengan
                                pihak Swasta</label>
                            <div class="col-sm-10">
                                <select class="form-select js-select2" id="pihak_swasta" name="pihak_swasta"
                                    data-control="select2" data-placeholder="- Pilih Jawaban -">
                                    <option></option>
                                    <option value="ya" {{ $bantuan->status_kerjasama == 'ya' ? 'selected' : '' }}>Ya
                                    </option>
                                    <option value="tidak"
                                        {{ $bantuan->status_kerjasama == 'tidak' ? 'selected' : '' }}>Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div id="swasta">
                            {{-- form nama perusahaan, akan muncul apabila pihak swasta = Ya --}}
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama
                                    Perusahaan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="nama_perusahaan" name="nama_perusahaan"
                                        data-control="select2" data-placeholder="- Pilih Nama Perusahaan -">
                                        <option></option>
                                        @foreach ($perusahaan as $div)
                                            <option value="{{ $div->id }}"
                                                {{ $div->id == $bantuan->m_perusahaan_id ? 'selected' : '' }}>
                                                {{ $div->nama_perusahaan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div id="binaan">
                            {{-- form binaan, akan muncul ketika jenis bantuan = PEMBINAAN --}}

                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                    Binaan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="jenis_binaan" name="jenis_binaan"
                                        data-control="select2" data-placeholder="- Pilih Jenis Binaan -">
                                        <option></option>
                                        @foreach ($binaan as $div)
                                            <option value="{{ $div->id }}"
                                                {{ $div->id == $bantuan->m_jenis_binaan_id ? 'selected' : '' }}>
                                                {{ $div->nama_jenis_binaan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama
                                    Binaan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Nama Binaan" name="nama_binaan" id="nama_binaan"
                                        value="{{ $bantuan->nama_binaan }}">
                                </div>
                            </div>
                        </div>

                        <div id="permodal">
                            {{-- form permodalan, akan muncul ketika jenis bantuan = PERMODALAN --}}
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                    Permodalan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="jenis_permodalan" name="jenis_permodalan"
                                        data-control="select2" data-placeholder="- Pilih Jenis Permodalan -">
                                        <option></option>
                                        @foreach ($permodalan as $div)
                                            <option value="{{ $div->id }}"
                                                {{ $div->id == $bantuan->m_jenis_permodalan_id ? 'selected' : '' }}>
                                                {{ $div->nama_jenis_permodalan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama Barang
                                    /
                                    Alat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Nama Barang" name="nama_barang" id="nama_barang"
                                        value="{{ $bantuan->nama_barang }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jumlah
                                    Barang /
                                    Alat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Jumlah Barang" name="jumlah_barang" id="jumlah_barang"
                                        value="{{ $bantuan->jumlah_barang }}">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm"
                                    for="message-text">Nominal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Nominal" name="nominal" id="nominal"
                                        onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
                                        data-inputmask="'alias': 'numeric', 'allowMinus': 'false','digits': 0,'groupSeparator': ',','removeMaskOnSubmit': true,'autoUnmask':true"
                                        value="{{ $bantuan->nominal_binaan }}">
                                </div>
                            </div>
                        </div>
                        <!--begin::Actions-->
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('bantuan.index', Crypt::encryptString($bantuan->t_pelaku_usaha_id)) }}"
                                class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                            <button class="btn btn-primary me-1 mb-1" type="submit" id="btn_submit">
                                <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                </div>
            </div>
        </div>
    </div>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @push('scripts')
        {{-- script currency separator --}}
        <script>
            $(document).ready(function() {
                $('#nominal').inputmask();
            })
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>
        <!-- Usage select2 -->
        <script>
            $(document).ready(function() {
                $(".js-select2").select2({
                    /* Tema untuk select2 bs 5 */
                    theme: "bootstrap-5",
                });
            });
        </script>

        {{-- untuk menambahkan koma pada nominal secara otomatis --}}
        <script>
            $(document).ready(function() {
                $('#nominal').inputmask();
            })
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>

        <!-- Membuat ajax ketika submit formTambahJenisUsaha -->
        <script>
            $(document).ready(function() {
                var id = $('#pelaku_usaha_id').val();

                //function untuk menampilkan form input nama perusahaan apabila pilihan status kerjsama dengan perusahaan = ya
                if ($('#pihak_swasta').val() == 'ya') {
                    $('#swasta').show();
                } else {
                    $('#swasta').hide();
                    $("#nama_perusahaan option:selected").prop("selected", false)

                }

                if ($('#jenis_bantuan').val() == 1) {
                    $('#binaan').show();
                    $('#permodal').hide();

                    $("#jenis_permodalan option:selected").prop("selected", false)
                    $('#nama_barang').val("");
                    $('#jumlah_barang').val("");

                } else if ($('#jenis_bantuan').val() == 2) {
                    $('#binaan').hide();
                    $('#permodal').show();

                    $("#jenis_binaan option:selected").prop("selected", false)
                    $('#nama_binaan').val("");
                }

                $('#pihak_swasta').on('change', function() {
                    //$('#swasta').show

                    s = $(this).val();

                    if (s == "ya") {
                        $('#swasta').show();

                    } else if (s == 'tidak') {
                        $('#swasta').hide();
                        $("#nama_perusahaan option:selected").prop("selected", false)
                    }
                });

                $('#jenis_bantuan').on('change', function() {

                    b = $(this).val();
                    console.log(b);
                    if (b == 1) {
                        $('#binaan').show();
                        $('#permodal').hide();

                        $("#jenis_permodalan option:selected").prop("selected", false)
                        $('#nama_barang').val("");
                        $('#jumlah_barang').val("");

                    } else if (b == 2) {
                        $('#binaan').hide();
                        $('#permodal').show();

                        $("#jenis_binaan option:selected").prop("selected", false)
                        $('#nama_binaan').val("");
                    }
                })

                const submitButton = document.getElementById('btn_submit');
                submitButton.addEventListener('click', function(e) {
                    // Prevent default button action
                    e.preventDefault();

                    Swal.fire({
                        title: 'Yakin ?',
                        text: "Untuk menyimpan data",
                        // icon: 'warning',
                        icon: 'question',
                        iconHtml: '?',
                        showCancelButton: true,
                        confirmButtonColor: '#3085d6',
                        cancelButtonColor: '#d33',
                        confirmButtonText: 'Simpan',
                        cancelButtonText: 'Batal'
                    }).then((result) => {
                        if (result.isConfirmed) {

                            setTimeout(() => {
                                // console.log(new FormData($('.form_submit_bantuan')[0]));
                                $.ajaxSetup({
                                    headers: {
                                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                            .attr('content')
                                    }
                                });
                                $.ajax({
                                        type: "post",
                                        url: "{{ route('bantuan.update_bantuan', ['id' => Crypt::encryptString($bantuan->id)]) }}",
                                        dataType: "json",
                                        processData: false,
                                        contentType: false,
                                        data: new FormData($('#form_submit_bantuan')[0])
                                    })
                                    .done(function(data) {
                                        if (data.status == true) {
                                            Swal.fire({
                                                title: 'Berhasil',
                                                html: data.message,
                                                icon: 'success',
                                                confirmButtonText: 'OK'
                                            }).then((result) => {
                                                if (result.isConfirmed) {
                                                    window.location.href =
                                                    "{{ url('bantuan') }}/" + id;
                                                }
                                            })
                                        } else {
                                            Swal.fire({
                                                title: 'Gagal',
                                                html: data.message,
                                                icon: 'error',
                                                confirmButtonText: 'OK'
                                            })
                                        }
                                    })
                            }, 100);
                        }
                    })
                });


            });
        </script>
    @endpush
</x-templates.default>
