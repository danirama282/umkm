<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MJenisTempatUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'SWK',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'SKG',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'RUMAH KREATIF',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'APLIKASI PEKEN',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'INDUSTRI',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'PASAR',
        ]);

        // Create data di MJenisTempatUsahaModel
        \App\Models\MJenisTempatUsahaModel::create([
            'nama_jenis_tempat_usaha' => 'UMKM JAHIT',
        ]);
    }
}
