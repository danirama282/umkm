<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJenisTempatUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_jenis_tempat_usaha';
    protected $fillable = [
        'nama_jenis_tempat_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Relasi dengan MTempatUsahaModel */
    public function tempat_usaha()
    {
        return $this->hasMany(MTempatUsahaModel::class, 'm_jenis_tempat_usaha_id', 'id');
    }
}