@for ($i = 0; $i < count($data['master_intervensi']); $i++) <div class="card bg-success mb-3 px-2 pb-2 h-100">
    <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
        data-bs-target="#modal_gamisIntervensi"
        onclick="funDetailIntervensi('KumulatifAll','{{strtoupper($data['master_intervensi'][$i])}}','Kumulatif')">
        <div class="row g-0">
            <h3 class="pt-3 text-white text-uppercase fs-3 text-center">{{($data['master_intervensi'][$i])}}</h3>
            <div class="d-flex align-items-center justify-content-center flex-column pb-3">
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{$data['gamis_intervensi']['intervensi_kumulatif'][$i][0]->jiwa}} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{$data['gamis_intervensi']['intervensi_kumulatif'][$i][0]->kk}} KK
                </h3>
            </div>
        </div>
    </a>
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-6 mb-lg-0 mb-2">
            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#modal_gamisIntervensi"
                        onclick="funDetailIntervensi('WargaMiskinAll','{{strtoupper($data['master_intervensi'][$i])}}','Warga Miskin Kumulatif')">
                        <div class="card-title mb-0">
                            <h6 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Miskin
                            </h6>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                            <h6 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_miskin_kumulatif'][$i][0]->jiwa}} Jiwa
                            </h6>
                            <h6 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_miskin_kumulatif'][$i][0]->kk}} KK
                            </h6>
                        </div>
                    </a>
                    <div class="d-flex justify-content-between align-items-center" style="gap: 16px;">
                        <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#modal_gamisIntervensi"
                            onclick="funDetailIntervensi('MiskinEkstrim','{{strtoupper($data['master_intervensi'][$i])}}','Warga Miskin Esktrim')">
                            <div class="card border h-100 border-succsess w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h6 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Miskin
                                            Ekstrim
                                        </h6>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h6 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_miskin_ekstrim'][$i][0]->jiwa}} Jiwa
                                        </h6>
                                        <h6 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_miskin_ekstrim'][$i][0]->kk}} KK
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#modal_gamisIntervensi"
                            onclick="funDetailIntervensi('WargaMiskin','{{strtoupper($data['master_intervensi'][$i])}}','Warga Miskin Non Esktrim')">
                            <div class="card border h-100 border-succsess w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Miskin Non Ekstrim
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_miskin'][$i][0]->jiwa}} Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_miskin'][$i][0]->kk}} KK
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 px-lg-2 px-0 mb-lg-0 mb-2">
            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#modal_gamisIntervensi"
                        onclick="funDetailIntervensi('WargaPraMiskin','{{strtoupper($data['master_intervensi'][$i])}}','Warga Pra Miskin Kumulatif')">
                        <div class="card-title mb-0">
                            <h6 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Pra Miskin
                            </h6>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_pramiskin_kumulatif'][$i][0]->jiwa}} Jiwa
                            </h3>
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_pramiskin_kumulatif'][$i][0]->kk}} KK
                            </h3>
                        </div>
                    </a>
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#modal_gamisIntervensi"
                            onclick="funDetailIntervensi('WargaPraMiskin_pra','{{strtoupper($data['master_intervensi'][$i])}}','Warga Pra Miskin')">
                            <div class="card border h-100 border-succsess w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Pra
                                            Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_pramiskin'][$i][0]->jiwa}} Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_pramiskin'][$i][0]->kk}} KK
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#modal_gamisIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#modal_gamisIntervensi"
                        onclick="funDetailIntervensi('WargaLainnyaAll','{{strtoupper($data['master_intervensi'][$i])}}','Warga Lainnya Kumulatif')">
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Lainnya
                            </h3>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_wargalainnya'][$i][0]->jiwa}} Jiwa
                            </h3>
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{$data['gamis_intervensi']['intervensi_wargalainnya'][$i][0]->kk}} KK
                            </h3>
                        </div>
                    </a>
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="card border h-100 border-succsess w-100">
                            <div class="card-body">
                                <a href="#modal_gamisIntervensi" class="text-decoration-none w-100"
                                    data-bs-toggle="modal" data-bs-target="#modal_gamisIntervensi"
                                    onclick="funDetailIntervensi('WargaLainnyaAll','{{strtoupper($data['master_intervensi'][$i])}}','Warga Lainnya')">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_wargalainnya'][$i][0]->jiwa}}
                                            Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{$data['gamis_intervensi']['intervensi_wargalainnya'][$i][0]->kk}} KK
                                        </h3>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    @endfor

    {{-- MODAL KATEGORI KEGIATAN --}}
    <div class="modal fade" id="modal_gamisIntervensi" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
            <div class="modal-content position-relative">
                <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                    <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                        data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0">
                    <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                        <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                        <h5 class="fw-bold fs-1" id="judul_modal_gamis_intervensi"></h5>
                    </div>
                    <div class="p-4 pb-2">
                        <div class="table table-responsive">
                            <div class="table table-responsive">
                                <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                    id="tabel_gamisIntervensi" style="width: 100%">
                                    <thead class="bg-200 text-900 border-tabel">
                                        <tr>
                                            <th width=5%>No.</th>
                                            <th width=10%>NIK</th>
                                            <th width=10%>Nama Pelaku Usaha</th>
                                            <th width=10%>Nama Usaha</th>
                                            <th width=10%>Alamat</th>
                                            <th width=10%>Kecamatan</th>
                                            <th width=10%>Kelurahan</th>
                                            <th width=2%>RT</th>
                                            <th width=2%>RW</th>
                                            <th width=5%>NIB</th>
                                            <th width=5%>Intervensi</th>
                                            <th class="text-center" width=5%>#</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                    <button class="btn btn-succsess me-1 mb-1" type="button" data-bs-dismiss="modal">
                        <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                    </button>
                    <!-- <button class="btn btn-primary" type="button"> </button> -->

                </div>
            </div>
        </div>
    </div>

    @push('scripts')
    <script>
        function funDetailIntervensi(gamis,intervensi,jenis_gamis) {
            console.log(gamis,intervensi);

            $('#judul_modal_gamis_intervensi').html('Tabel Detail Jiwa '+ jenis_gamis + ' '+ intervensi.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            }));

            $('#tabel_gamisIntervensi').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{!! route('intervensiGamis') !!}",
                    "type": "POST",
                    "data": {
                        gamis: gamis,
                        intervensi: intervensi
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'intervensi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
                "bDestroy": true
            });

        }
    </script>
    @endpush