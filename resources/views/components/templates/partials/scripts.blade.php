<script src="{{ asset('dist/vendors/popper/popper.min.js') }}"></script>
<script src="{{ asset('dist/vendors/bootstrap/bootstrap.min.js') }}"></script>
<script src="{{ asset('dist/vendors/anchorjs/anchor.min.js') }}"></script>
<script src="{{ asset('dist/vendors/is/is.min.js') }}"></script>
<script src="{{ asset('dist/vendors/echarts/echarts.min.js') }}"></script>
<script src="{{ asset('dist/vendors/fontawesome/all.min.js') }}"></script>
<script src="{{ asset('dist/vendors/lodash/lodash.min.js') }}"></script>
<script src="https://polyfill.io/v3/polyfill.min.js?features=window.scroll"></script>
<!-- <script src="{{ asset('dist/vendors/list.js/list.min.js') }}"></script>? -->
<script src="{{ asset('dist/assets/js/theme.js') }}"></script>

<script>
    /* document ready for #popup */
    $(document).ready(function() {
        /* show modal with id="hut_kota_surabaya" */
        $('#hut_kota_surabaya').fadeIn(1000).modal('show');
    });

    /* if button close modal #hut_kota_surabaya is clicked, then fadeOut */
    $('#hut_kota_surabaya').on('hide.bs.modal', function() {
        $(this).fadeOut(1000);
    });

</script>
@stack('scripts')