<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MKlasifikasiKbliModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_klasifikasi_kbli';
    protected $primaryKey = 'id_m_klasifikasi_kbli';
    protected $fillable = [
        'id_m_kategori_kbli',
        'kode_klasifikasi_kbli',
        'nama_klasifikasi_kbli'

    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Fungsi relasi model MKategoriKbliModel */
    public function relasi_kategori_kbli()
    {
        return $this->belongsTo(MKategoriKbliModel::class, 'id_m_kategori_kbli', 'id_m_kategori_kbli');
    }
}
