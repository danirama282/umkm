<input type="hidden" name="" id="kecamatan">
    <input type="hidden" name="" id="kelurahan">
    <div class="row g-0 mb-3">
        <div class="col-sm-6 col-lg-4 mb-2 ">
            <a href="javascript:void(0)">
                <div class="card text-white bg-primary py-3" onclick="reload()">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Jumlah Pelaku Usaha</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['count_total_pelaku_keckel'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-3">
            <a href="javascript:void(0)">
                <div class="card text-white bg-success py-3" onclick="btn_sudah_verif()">
                    <input type="hidden" name="sudah" id="sudah" value="sudah">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Sudah Verifikasi</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['sudah_verif_keckel'][0]->jum_sudah_verif }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-3">
            <a href="javascript:void(0)">
                <div class="card text-white bg-danger py-3" onclick="btn_belum_verif()">
                    <input type="hidden" name="belum" id="belum" value="belum">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Belum Verifikasi</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['belum_verif_keckel'][0]->jum_belum_verif }}

                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row g-0 mb-3">
        <div class="card">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Filter Kecamatan Kelurahan Pelaku Usaha</h5>
                    </div>
                </div>
                <div class="card-body position-relative">

                    <form id="formMonitorPelakuUsaha">
                        @csrf
                        <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                        <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">

                        <div class="row">
                            <div class="row mb-3">
                                <label for="" class="col-2 col-form-label col-form-label">Intervensi</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" name="intervensi" id="intervensi">
                                        <option value="" selected disabled>Pilih Intervensi Usaha</option>
                                        <option value="csr">CSR</option>
                                        <option value="industri_rumahan">Industri Rumahan</option>
                                        <option value="kur">KUR</option>
                                        <option value="pameran">Pameran</option>
                                        <option value="pasar">Pasar</option>
                                        <option value="puspita">Puspita</option>
                                        <option value="padat_karya">Padat Karya</option>
                                        <option value="pelatihan">Pelatihan</option>
                                        <option value="peken_tokel">Peken</option>
                                        <option value="rumah_kreatif">Rumah Kreatif</option>
                                        <option value="swk">SWK</option>
                                        <option value="skg">SKG</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="" class="col-2 col-form-label col-form-label">Kategori Usaha</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" name="kategori_usaha" id="kategori_usaha">
                                        <option value="" selected disabled>Pilih Kategori Usaha</option>
                                        <option value="FASHION">Fashion</option>
                                        <option value="JASA">Jasa</option>
                                        <option value="KERAJINAN">Kerajinan</option>
                                        <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                                        <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian
                                        </option>
                                        <option value="TOKO">Toko</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-secondary me-1 mb-1 reset" id="reset">
                                <span data-fa-transform="shrink-3"></span> Reset
                            </a>
                            <a href="javascript:void(0)" class="btn btn-info me-1 mb-1 filter" type="button">
                                <span data-fa-transform="shrink-3"></span> Filter
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row g-0">
        <div class="card">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Daftar Pelaku Usaha</h5>
                    </div>
                    <div>
                        <a href="{{ route('pelaku_usaha.create') }}" class="btn btn-primary">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Tambah Data Pelaku
                            Usaha
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">No.</th>
                                    <th style="width: 100px;">NIK</th>
                                    <th style="width: 150px;">Nama Pelaku Usaha</th>
                                    <th style="width: 100px;">Nama Usaha</th>
                                    <th style="width: 250px;">Alamat</th>
                                    <th style="width: 100px;">Kategori Usaha</th>
                                    {{-- <th>Status Keaktifan</th> --}}
                                    <th style="width: 150px;">Intervensi</th>
                                    <th style="width: 150px;">Status</th>
                                    <th style="width: 130px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>