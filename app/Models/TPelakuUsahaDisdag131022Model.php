<?php

namespace App\Models;

use App\Models\TPelakuUsahaDisdag131022Model as ModelsTPelakuUsahaDisdag131022Model;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class TPelakuUsahaDisdag131022Model extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 't_pelaku_usaha_disdag_181022';
    protected $primaryKey = 'id_t_pelaku_usaha_disdag_181022';
    protected $guarded = [];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function detailGamis($gamis)
    {
        // dd($gamis);
        $query = "";
        // dd($gamis,$kategori,'cek modal');
        if ($gamis == 'KumulatifAll') {
            $query .= " WHERE A.deleted_at is null";
        } else if ($gamis == 'WargaMiskinAll') {
            $query .= " WHERE A.is_warga_miskin='1' and A.deleted_at is null";
        } else if ($gamis == 'MiskinEkstrim') {
            $query .= " WHERE A.is_miskin_ekstrim='1' and A.is_warga_miskin='1' and A.deleted_at is null";
        } else if ($gamis == 'WargaMiskin') {
            $query .= " WHERE A.is_miskin_ekstrim is null and A.is_warga_miskin='1' and A.deleted_at is null";
        } else if ($gamis == 'WargaPraMiskin') {
            $query .= " WHERE A.is_pramiskin='1' and A.deleted_at is null";
        } else if ($gamis == 'WargaPraMiskin_Ekstrim') {
            $query .= " WHERE A.is_miskin_ekstrim='1' and A.is_pramiskin='1' and A.deleted_at is null";
        } else if ($gamis == 'WargaPraMiskin_pra') {
            $query .= " WHERE A.is_miskin_ekstrim is null and A.is_pramiskin='1' and A.deleted_at is null";
        } else if ($gamis == 'WargaLainnyaAll') {
            $query .= " WHERE A.is_miskin_ekstrim is null and A.is_warga_miskin is null and A.is_pramiskin is null and A.deleted_at is null";
        } else if ($gamis == 'aktif') {
            $query .= " where A.status_keaktifan='AKTIF' and A.deleted_at is null";
        } else if ($gamis == 'tidak_aktif') {
            $query .= " where A.status_keaktifan in ('TIDAK_AKTIF', 'LIBUR') and A.deleted_at is null";
        }
        $data = DB::select("SELECT A
            .nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            C.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
            $query");

        // dd($data);
        return $data;
    }

    public function detailKategori($gamis, $kategori)
    {
        $query = "";
        // dd($gamis,$kategori,'cek modal');
        if ($gamis == 'KumulatifAll') {
            $query .= " WHERE A.deleted_at IS NULL AND A.kategori_jenis_produk = '" . $kategori . "'";
        } else if ($gamis == 'WargaMiskinAll') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_warga_miskin = '1'";
        } else if ($gamis == 'MiskinEkstrim') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_warga_miskin = '1' AND a.is_miskin_ekstrim = '1'";
        } else if ($gamis == 'WargaMiskin') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_warga_miskin = '1' AND a.is_miskin_ekstrim IS NULL";
        } else if ($gamis == 'WargaPraMiskin') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_pramiskin = '1'";
        } else if ($gamis == 'WargaPraMiskin_Ekstrim') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_pramiskin = '1' AND a.is_miskin_ekstrim = '1'";
        } else if ($gamis == 'WargaPraMiskin_pra') {
            $query .= " WHERE a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "' AND a.is_pramiskin = '1' AND a.is_miskin_ekstrim IS NULL";
        } else if ($gamis == 'WargaLainnyaAll') {
            $query .= " WHERE a.is_miskin_ekstrim IS NULL AND a.is_warga_miskin IS NULL AND a.is_pramiskin IS NULL AND a.deleted_at IS NULL AND a.kategori_jenis_produk = '" . $kategori . "'";
        }
        $data = DB::select("SELECT A
            .nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            C.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
            $query");

        // dd($data);
        return $data;
    }

    public function intervensiGamis($gamis, $intervensi)
    {
        # code...
        // $intervensi = strtolower(str_replace(" ", "_", $intervensi));
        $query = "";
        // dd($intervensi, $gamis, 'cek modal');
        if ($intervensi == 'belum_intervensi') {
            # code...
            if ($gamis == 'KumulatifAll') {
                $query .= " where a.deleted_at is null
                and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null";
            } else if ($gamis == 'WargaMiskinAll') {
                $query .= " where a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null and a.is_warga_miskin='1'";
            } else if ($gamis == 'MiskinEkstrim') {
                $query .= " where a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null and a.is_warga_miskin='1' and a.is_miskin_ekstrim='1'";
            } else if ($gamis == 'WargaMiskin') {
                $query .= " where a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null and a.is_warga_miskin='1' and a.is_miskin_ekstrim is null";
            } else if ($gamis == 'WargaPraMiskin') {
                $query .= " where a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null and a.is_pramiskin='1'";
            } else if ($gamis == 'WargaPraMiskin_pra') {
                $query .= " where a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null and a.is_pramiskin='1' and a.is_miskin_ekstrim is null";
            } else if ($gamis == 'WargaLainnyaAll') {
                $query .= " where is_miskin_ekstrim is null and a.is_warga_miskin is null and a.is_pramiskin is null and a.deleted_at is null 	and a.swk is null
                and a.skg is null
                and a.pasar is null
                and a.peken_tokel is null
                and a.pelatihan is null
                and a.pameran is null
                and a.bpum is null
                and a.industri_rumahan is null
                and a.rumah_kreatif is null
                and a.padat_karya is null
                and a.kur is null
                and a.csr is null
                and a.puspita is null";
            }
        } else if ($intervensi != 'belum_intervensi') {
            if ($gamis == 'KumulatifAll') {
                // dd("masuk");
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1'";
            } else if ($gamis == 'WargaMiskinAll') {
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1' and a.is_warga_miskin='1'";
            } else if ($gamis == 'MiskinEkstrim') {
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1' and a.is_warga_miskin='1' and a.is_miskin_ekstrim='1'";
            } else if ($gamis == 'WargaMiskin') {
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1' and a.is_warga_miskin='1' and a.is_miskin_ekstrim is null";
            } else if ($gamis == 'WargaPraMiskin') {
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1' and a.is_pramiskin='1'";
            } else if ($gamis == 'WargaPraMiskin_pra') {
                $query .= " WHERE a.deleted_at is null and " . $intervensi . "='1' and a.is_pramiskin='1' and a.is_miskin_ekstrim is null";
            } else if ($gamis == 'WargaLainnyaAll') {
                $query .= " WHERE a.is_miskin_ekstrim is null and a.is_warga_miskin is null and a.is_pramiskin is null and a.deleted_at is null and " . $intervensi . "='1'";
            }
        }
        # code...

        // dd($intervensi, $gamis, $query);
        $data = DB::select("SELECT A
            .nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            C.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib,
            intervensi
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
            LEFT JOIN (
            SELECT
                nik,
                json_agg (
                    ARRAY [
                CASE
                        peken_tokel
                        WHEN 1 THEN
                        '(peken)' ELSE''
                    END,
                CASE
                        peken_tokel
                        WHEN 2 THEN
                        'peken' ELSE''
                        END,
                CASE
                        swk
                        WHEN 1 THEN
                        'swk' ELSE''
                        END,
                CASE
                        pasar
                        WHEN 1 THEN
                        'pasar' ELSE''
                        END,
                CASE
                        pelatihan
                        WHEN 1 THEN
                        'pelatihan' ELSE''
                        END,
                CASE
                        pameran
                        WHEN 1 THEN
                        'pameran' ELSE''
                        END,
                CASE
                        industri_rumahan
                        WHEN 1 THEN
                        'industri_rumahan' ELSE''
                        END,
                CASE
                        rumah_kreatif
                        WHEN 1 THEN
                        'rumah_kreatif' ELSE''
                        END,
                CASE
                        padat_karya
                        WHEN 1 THEN
                        'padat_karya' ELSE''
                        END,
                CASE
                        kur
                        WHEN 1 THEN
                        'kur' ELSE''
                        END,
                CASE
                        csr
                        WHEN 1 THEN
                        'csr' ELSE''
                        END,
                CASE
                        puspita
                        WHEN 1 THEN
                        'puspita' ELSE''
                        END,
                CASE
                        oss
                        WHEN 1 THEN
                        'oss' ELSE''
                        END]
                ) AS intervensi
            FROM
                t_pelaku_usaha_disdag_181022 ds
            GROUP BY
                nik
            ) ds ON ds.nik = A.nik
            $query");
        // dd($data);
        return $data;
    }

    public static function KumulatifAll()
    {
        $data = DB::select("SELECT count(nik) AS jiwa, count(distinct (no_kk_ktp)) as kk FROM t_pelaku_usaha_disdag_181022 WHERE deleted_at IS NULL");
        return $data;
    }

    public static function WargaMiskinAll()
    {
        $data = DB::select("SELECT COUNT
        ( t_pelaku_usaha_disdag_181022.nik ) AS jiwa,
        COUNT ( DISTINCT ( t_pelaku_usaha_disdag_181022.no_kk_ktp ) ) AS kk 
    FROM
        t_pelaku_usaha_disdag_181022
        JOIN dinsos_mbr_new2 ON t_pelaku_usaha_disdag_181022.nik = dinsos_mbr_new2.nik 
    WHERE
        dinsos_mbr_new2.status_warga IN ( 1, 2 ) 
        AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL 
        ");
        return $data;
    }

    public static function MiskinEkstrim()
    {
        $data = DB::select("SELECT COUNT
        ( t_pelaku_usaha_disdag_181022.nik ) AS jiwa,
        COUNT ( DISTINCT ( t_pelaku_usaha_disdag_181022.no_kk_ktp ) ) AS kk 
    FROM
        t_pelaku_usaha_disdag_181022
        JOIN dinsos_mbr_new2 ON t_pelaku_usaha_disdag_181022.nik = dinsos_mbr_new2.nik 
    WHERE
        dinsos_mbr_new2.status_warga IN ( 1 ) 
        AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL");
        return $data;
    }

    public static function WargaMiskin()
    {
        $data = DB::select("SELECT COUNT
        ( t_pelaku_usaha_disdag_181022.nik ) AS jiwa,
        COUNT ( DISTINCT ( t_pelaku_usaha_disdag_181022.no_kk_ktp ) ) AS kk 
    FROM
        t_pelaku_usaha_disdag_181022
        JOIN dinsos_mbr_new2 ON t_pelaku_usaha_disdag_181022.nik = dinsos_mbr_new2.nik 
    WHERE
        dinsos_mbr_new2.status_warga IN ( 2 ) 
        AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL ");
        return $data;
    }

    public static function WargaPraMiskin()
    {
        $data = DB::select("SELECT COUNT
        ( t_pelaku_usaha_disdag_181022.nik ) AS jiwa,
        COUNT ( DISTINCT ( t_pelaku_usaha_disdag_181022.no_kk_ktp ) ) AS kk 
    FROM
        t_pelaku_usaha_disdag_181022
        JOIN dinsos_mbr_new2 ON t_pelaku_usaha_disdag_181022.nik = dinsos_mbr_new2.nik 
    WHERE
        dinsos_mbr_new2.status_warga IN ( 3 ) 
        AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL ");
        return $data;
    }

    public static function WargaPraMiskin_Ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim='1' and is_pramiskin='1' and deleted_at is null");
        return $data;
    }

    public static function WargaPraMiskin_pra()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_pramiskin='1' and deleted_at is null");
        return $data;
    }

    public static function WargaLainnyaAll()
    {
        $data = DB::select("SELECT COUNT
        ( t_pelaku_usaha_disdag_181022.nik ) AS jiwa,
        COUNT ( DISTINCT ( t_pelaku_usaha_disdag_181022.no_kk_ktp ) ) AS kk 
    FROM
        t_pelaku_usaha_disdag_181022
        LEFT JOIN dinsos_mbr_new2 ON t_pelaku_usaha_disdag_181022.nik = dinsos_mbr_new2.nik 
    WHERE
        dinsos_mbr_new2.status_warga IS NULL 
        AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL");
        return $data;
    }

    public static function fashion_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION'");
        return $data;
    }

    public static function fashion_miskinAll()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_warga_miskin='1'");
        return $data;
    }

    public static function fashion_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function fashion_warga_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function fashion_pramiskin_All()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_pramiskin='1'");
        return $data;
    }

    public static function fashion_praMiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_pramiskin='1' and is_miskin_ekstrim is null
        ");
        return $data;
    }

    public static function fashion_praMiskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='FASHION' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function fashion_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='FASHION'");
        return $data;
    }


    public static function toko_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO'");
        return $data;
    }

    public static function toko_miskinAll()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_warga_miskin='1'");
        return $data;
    }

    public static function toko_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function toko_miskin_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function toko_pramiskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_pramiskin='1'");
        return $data;
    }

    public static function toko_pramiskin_ektrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function toko_pramiskin_pramiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='TOKO' and is_pramiskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function toko_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='TOKO'");
        return $data;
    }

    public static function mamin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN'");
        return $data;
    }

    public static function mamin_miskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_warga_miskin='1'");
        return $data;
    }

    public static function mamin_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function mamin_miskin_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function mamin_pramiskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_pramiskin='1'");
        return $data;
    }

    public static function mamin_pramiskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function mamin_pramiskin_pramiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN' and is_pramiskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function mamin_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='MAKANAN DAN MINUMAN'");
        return $data;
    }

    public static function pertanian_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN'
        ");
        return $data;
    }

    public static function pertanian_miskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_warga_miskin='1'");
        return $data;
    }

    public static function pertanian_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function pertanian_miskin_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function pertanian_pramiskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_pramiskin='1'");
        return $data;
    }

    public static function pertanian_pramiskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function pertanian_pramiskin_pramiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' and is_pramiskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function pertanian_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN'");
        return $data;
    }

    public static function kerajinan_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN'");
        return $data;
    }

    public static function kerajinan_miskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_warga_miskin='1'");
        return $data;
    }

    public static function kerajinan_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function kerajinan_miskin_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function kerajinan_pramiskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_pramiskin='1'");
        return $data;
    }

    public static function kerajinan_pramiskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function kerajinan_pramiskin_pramiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='KERAJINAN' and is_pramiskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function kerajinan_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='KERAJINAN'");
        return $data;
    }

    public static function jasa_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA'
        ");
        return $data;
    }

    public static function jasa_miskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_warga_miskin='1'");
        return $data;
    }

    public static function jasa_miskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_warga_miskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function jasa_miskin_miskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_warga_miskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function jasa_pramiskin_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_pramiskin='1'");
        return $data;
    }

    public static function jasa_pramiskin_ekstrim()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_pramiskin='1' and is_miskin_ekstrim='1'");
        return $data;
    }

    public static function jasa_pramiskin_pramiskin()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and kategori_jenis_produk='JASA' and is_pramiskin='1' and is_miskin_ekstrim is null");
        return $data;
    }

    public static function jasa_wargalainnya_all()
    {
        $data = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and kategori_jenis_produk='JASA'");
        return $data;
    }

    public static function belum_verif()
    {
        $data = DB::select("SELECT COUNT(*) AS jum_belum_verif FROM t_pelaku_usaha_disdag_181022 WHERE tgl_verifikasi_kec IS NULL");
        return $data;
    }

    public static function sudah_verif()
    {
        $data = DB::select("SELECT COUNT(*) AS jum_sudah_verif FROM t_pelaku_usaha_disdag_181022 WHERE tgl_verifikasi_kec IS NOT NULL");
        return $data;
    }


    public static function scopeCountData()
    {
        $data = DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_181022");
        return $data[0]->total;
    }

    public static function scopeCountDataKeckel($data_user)
    {

        if (str_contains($data_user->nama_user, 'Kelurahan')) {
            // dd('masok');
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            // dd($keckel);
            $sql = DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_181022 WHERE kelurahan ILIKE '%$keckel%'");
        } elseif (str_contains($data_user->nama_user, 'Kecamatan')) {
            // dd('sini');
            // $keckel = str_replace('Kecamatan ', '', $data_user->nama_user);
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            $sql = DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_181022 WHERE kecamatan ILIKE '%$keckel%'");
        } else {
            $sql = DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_181022");
        }

        // $data = DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_181022");
        return $sql[0]->total;
    }

    public static function belum_verif_keckel($data_user)
    {
        // dd($data_user['user']->nama_user);
        if (str_contains($data_user->nama_user, 'Kelurahan')) {
            // dd('masok');
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            // dd($keckel);
            $sql = DB::select("SELECT COUNT(*) AS jum_belum_verif FROM t_pelaku_usaha_disdag_181022 WHERE kelurahan ILIKE '%$keckel%'AND tgl_verifikasi_kec IS NULL");
        } elseif (str_contains($data_user->nama_user, 'Kecamatan')) {
            // dd('sini');
            // $keckel = str_replace('Kecamatan ', '', $data_user['user']->nama_user);
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            $sql = DB::select("SELECT COUNT(*) AS jum_belum_verif FROM t_pelaku_usaha_disdag_181022 WHERE kecamatan ILIKE '%$keckel%'AND tgl_verifikasi_kec IS NULL");
        } else {
            $sql = DB::select("SELECT COUNT(*) AS jum_belum_verif FROM t_pelaku_usaha_disdag_181022 WHERE tgl_verifikasi_kec IS NULL");
        }

        // $keckel = substr(strstr($data_user->nama_user, " "), 1);
        // dd($keckel);
        // $data = DB::select("SELECT COUNT(*) AS jum_belum_verif FROM t_pelaku_usaha_disdag_181022 WHERE tgl_verifikasi_kec IS NULL");
        return $sql;
    }

    public static function sudah_verif_keckel($data_user)
    {
        if (str_contains($data_user->nama_user, 'Kelurahan')) {
            // dd('masok');
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            // dd($keckel);
            $sql = DB::select("SELECT COUNT(*) AS jum_sudah_verif FROM t_pelaku_usaha_disdag_181022 WHERE kelurahan ILIKE '%$keckel%'AND tgl_verifikasi_kec IS NOT NULL");
        } elseif (str_contains($data_user->nama_user, 'Kecamatan')) {
            // dd('sini');
            // $keckel = str_replace('Kecamatan ', '', $data_user['user']->nama_user);
            $keckel = substr(strstr($data_user->nama_user, " "), 1);
            $sql = DB::select("SELECT COUNT(*) AS jum_sudah_verif FROM t_pelaku_usaha_disdag_181022 WHERE kecamatan ILIKE '%$keckel%'AND tgl_verifikasi_kec IS NOT NULL");
        } else {
            $sql = DB::select("SELECT COUNT(*) AS jum_sudah_verif FROM t_pelaku_usaha_disdag_181022 WHERE tgl_verifikasi_kec IS NOT NULL");
        }
        return $sql;
    }

    public static function scopeCountKategoriUsaha($query)
    {
        $data = DB::select(
            "SELECT
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='MAKANAN DAN MINUMAN' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_mamin,
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='TOKO' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_toko,
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='JASA' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_jasa,
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='FASHION' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_fashion,
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='KERAJINAN' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_kerajinan,
                COUNT ( CASE WHEN tpu.kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_budidaya
            FROM
                t_pelaku_usaha_disdag_181022 tpu
            WHERE deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeCountIntervrensiSebaranUmkmDisdag($query)
    {
        $data = DB::select(
            "SELECT
            kec.nm_kec,
            tpu.kecamatan_pemilik as kecamatan_pemilik,
            0 AS jumlah_swk_mbr,
            COUNT ( CASE WHEN tpu.swk = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_swk_non_mbr,
            0 AS jumlah_skg_mbr,
            COUNT ( CASE WHEN tpu.skg = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_skg_non_mbr,
            0 AS jumlah_pasar_mbr,
            COUNT ( CASE WHEN tpu.pasar = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pasar_non_mbr,
            0 AS jumlah_tp_mbr,
            COUNT ( CASE WHEN tpu.peken_tokel IN ( 1, 2 ) THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_tp_non_mbr,
            0 AS jumlah_pelatihan_mbr,
            COUNT ( CASE WHEN tpu.pelatihan = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pelatihan_non_mbr,
            0 AS jumlah_pameran_mbr,
            COUNT ( CASE WHEN tpu.pameran = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pameran_non_mbr,
            0 AS jumlah_bpum_mbr,
            COUNT ( CASE WHEN tpu.bpum = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_bpum_non_mbr,
        -- 0 AS jumlah_ir_mbr,
        -- COUNT ( CASE WHEN tpu.industri_rumahan = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_ir_non_mbr,
            0 AS jumlah_rk_mbr,
            COUNT ( CASE WHEN tpu.rumah_kreatif = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_rk_non_mbr,
            0 AS jumlah_padat_karya_mbr,
            COUNT ( CASE WHEN tpu.padat_karya = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_padat_karya_non_mbr,
            0 AS jumlah_kur_mbr,
            COUNT ( CASE WHEN tpu.kur = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_kur_non_mbr,
            0 AS jumlah_csr_mbr,
            COUNT ( CASE WHEN tpu.csr = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_csr_non_mbr,
            0 AS jumlah_puspita_mbr,
            COUNT ( CASE WHEN tpu.puspita = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_puspita_non_mbr,
            COUNT ( CASE WHEN tpu.oss = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_oss_non_mbr,
            COUNT (
            CASE

                    WHEN tpu.swk != '1'
                    AND tpu.skg != '1'
                    AND tpu.pasar != '1'
                    AND tpu.peken_tokel NOT IN ( '1', '2' )
                    AND tpu.pelatihan != '1'
                    AND tpu.pameran != '1'
                    AND tpu.industri_rumahan != '1'
                    AND tpu.industri_rumahan != '1'
                    AND tpu.oss != '1'
                    AND tpu.rumah_kreatif != '1'
                    AND tpu.padat_karya != '1'
                    AND tpu.kur != '1'
                    AND tpu.csr != '1'
                    AND tpu.puspita != '1' THEN
                        tpu.id_t_pelaku_usaha_disdag_181022
                    END
                    ) AS jumlah_belum_intervensi
                FROM
                    t_pelaku_usaha_disdag_181022 tpu
                    LEFT JOIN m_setup_kec kec ON kec.id_m_setup_kec = tpu.kecamatan_pemilik
                WHERE
                    kec.id_m_setup_kab = 78
                    AND kec.id_m_setup_prop = 35
                    AND tpu.deleted_at IS NULL
            GROUP BY
            kec.nm_kec, tpu.kecamatan_pemilik"
        );

        // dd($data);
        return $data;
    }

    /* public static function scopeGrafikJenisUsaha()
	{
		$data = \DB::select(
        "SELECT
            kec.nm_kec,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'SWK' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_swk,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'UMKM' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_umkm,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'LAUNDRY' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_laundry,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'CUCI HELM' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_cuci_helm,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'CUCI SEPATU' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_cuci_sepatu,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'JAHIT' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_jahit,
            COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'TOKEL' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_tokel
        FROM
            m_setup_kec kec
            JOIN t_pelaku_usaha tpu ON kec.id_m_setup_kec = tpu.kecamatan_pemilik
            JOIN m_sub_kategori_usaha msku ON tpu.m_sub_kategori_usaha_id = msku.id
        WHERE
            kec.id_m_setup_kab = 78
            AND kec.id_m_setup_prop = 35
        GROUP BY
            kec.nm_kec
        ORDER BY
            kec.nm_kec desc"
        );

		return $data;
	} */

    public static function scopeGrafikIntervensi()
    {
        $data = DB::select(
            "SELECT
            kec.nm_kec,
            0 AS jumlah_swk_mbr,
            COUNT ( CASE WHEN tpu.swk = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_swk_non_mbr,
            0 AS jumlah_skg_mbr,
            COUNT ( CASE WHEN tpu.skg = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_skg_non_mbr,
            0 AS jumlah_pasar_mbr,
            COUNT ( CASE WHEN tpu.pasar = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pasar_non_mbr,
            0 AS jumlah_tp_mbr,
            COUNT ( CASE WHEN tpu.peken_tokel IN ( 1, 2 ) THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_tp_non_mbr,
            0 AS jumlah_pelatihan_mbr,
            COUNT ( CASE WHEN tpu.pelatihan = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pelatihan_non_mbr,
            0 AS jumlah_pameran_mbr,
            COUNT ( CASE WHEN tpu.pameran = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pameran_non_mbr,
            0 AS jumlah_bpum_mbr,
            COUNT ( CASE WHEN tpu.bpum = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_bpum_non_mbr,
        -- 0 AS jumlah_ir_mbr,
        -- COUNT ( CASE WHEN tpu.industri_rumahan = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022  END ) AS jumlah_ir_non_mbr,
            0 AS jumlah_rk_mbr,
            COUNT ( CASE WHEN tpu.rumah_kreatif = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_rk_non_mbr,
            0 AS jumlah_padat_karya_mbr,
            COUNT ( CASE WHEN tpu.padat_karya = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_padat_karya_non_mbr,
            0 AS jumlah_kur_mbr,
            COUNT ( CASE WHEN tpu.kur = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_kur_non_mbr,
            0 AS jumlah_csr_mbr,
            COUNT ( CASE WHEN tpu.csr = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_csr_non_mbr,
            0 AS jumlah_puspita_mbr,
            COUNT ( CASE WHEN tpu.puspita = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_puspita_non_mbr,
            COUNT ( CASE WHEN tpu.oss = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_oss_non_mbr,
            COUNT (
            CASE

			WHEN tpu.swk != '1'
			AND tpu.skg != '1'
			AND tpu.pasar != '1'
			AND tpu.peken_tokel NOT IN ( '1', '2' )
			AND tpu.pelatihan != '1'
			AND tpu.pameran != '1'
			AND tpu.industri_rumahan != '1'
			AND tpu.oss != '1'
			AND tpu.rumah_kreatif != '1'
			AND tpu.padat_karya != '1'
			AND tpu.kur != '1'
			AND tpu.csr != '1'
			AND tpu.puspita != '1' THEN
				tpu.id_t_pelaku_usaha_disdag_181022
			END
			) AS jumlah_belum_intervensi
		FROM
			t_pelaku_usaha_disdag_181022 tpu
			LEFT JOIN m_setup_kec kec ON kec.id_m_setup_kec = tpu.kecamatan_pemilik
		WHERE
                kec.id_m_setup_kab = 78
                AND kec.id_m_setup_prop = 35
                AND tpu.deleted_at IS NULL
        GROUP BY
            kec.nm_kec
        ORDER BY nm_kec DESC"
        );

        return $data;
    }

    /* Fungsi untuk scopeCountIntervensi yang berisi raw query */
    public static function scopeCountIntervensi()
    {
        $data = \DB::select(
            "SELECT COUNT
            ( CASE WHEN tpu.swk = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_swk,
            COUNT ( CASE WHEN tpu.skg = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_skg,
            COUNT ( CASE WHEN tpu.pasar = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pasar,
            COUNT ( CASE WHEN tpu.peken_tokel in (1,2) THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_tokel,
            COUNT ( CASE WHEN tpu.pelatihan = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pelatihan,
            COUNT ( CASE WHEN tpu.pameran = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pameran,
            COUNT ( CASE WHEN tpu.bpum = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_bpum,
            COUNT ( CASE WHEN tpu.rumah_kreatif = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_rk,
            COUNT ( CASE WHEN tpu.padat_karya = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_padatkarya,
            COUNT ( CASE WHEN tpu.kur = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_kur,
            COUNT ( CASE WHEN tpu.csr = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_csr,
            COUNT ( CASE WHEN tpu.puspita = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_puspita,
            COUNT ( CASE WHEN tpu.oss = '1' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_oss,
            COUNT (
            CASE

                    WHEN
                    tpu.bpum is null
                    AND tpu.swk is null
                    AND tpu.skg is null
                    AND tpu.pasar is null
                    AND tpu.oss is null
                    AND tpu.peken_tokel is null
                    AND tpu.pelatihan is null
                    AND tpu.pameran is null
                    AND tpu.industri_rumahan is null
                    AND tpu.rumah_kreatif is null
                    AND tpu.padat_karya is null
                    AND tpu.kur is null
                    AND tpu.csr is null
                    AND tpu.puspita is null
                    THEN
                        tpu.id_t_pelaku_usaha_disdag_181022
                    END
                    ) AS jumlah_belum_taging
            FROM
            t_pelaku_usaha_disdag_181022 tpu
            WHERE
             tpu.deleted_at IS NULL"

        );

        return $data;
    }

    public static function scopePelakuUsahaMbr()
    {
        $data = \DB::select(
            "SELECT
                count(*)
            FROM
                t_pelaku_usaha_disdag_181022
            WHERE
                is_mbr_okt='1'
                AND deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopePelakuUsahaNonMbr()
    {
        $data = \DB::select(
            "SELECT
                count(*)
            FROM
                t_pelaku_usaha_disdag_181022
            WHERE
                is_mbr_okt
            IS NULL AND
                deleted_at IS NULL"
        );

        return $data;
    }

    // KATEGORI USAHA
    public static function scopeKategoriFashion($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'FASHION'
            AND A.deleted_at IS NULL"
        );

        return $data;
    }
    public static function scopeCountKategoriMamin($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'MAKANAN DAN MINUMAN'
            AND A.deleted_at IS NULL"
        );

        return $data;
    }
    public static function scopeCountKategoriKerjinan($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'KERAJINAN'
            AND A.deleted_at IS NULL"
        );

        return $data;
        dd($data);
    }
    public static function scopeCountKategoriToko($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'TOKO'
            AND A.deleted_at IS NULL"
        );

        return $data;
    }
    public static function scopeCountKategoriJasa($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'JASA'
            AND A.deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeKategoriBudidaya($query)
    {
        $data = DB::select(
            "SELECT A
            .nik AS nik,
            A.nama_pelaku_usaha AS nama_pelaku_usaha,
            A.alamat_ktp AS alamat,
            A.nama_usaha AS nama_usaha,
            b.nm_kec AS kecamatan,
            C.nm_kel AS kelurahan,
            A.rw_ktp AS rw,
            A.rt_ktp AS rt,
            A.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
            AND b.id_m_setup_prop = '35'
            AND b.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
            AND b.id_m_setup_kec = C.id_m_setup_kec
            AND C.id_m_setup_prop = '35'
            AND C.id_m_setup_kab = '78'
        WHERE
            A.kategori_jenis_produk = 'PEMBUDIDAYA SEKTOR PERTANIAN'
            AND A.deleted_at IS NULL"
        );

        return $data;
    }


    // INTERVENSI

    public static function scopeCountIntervensiSwk($query)
    {
        $data = DB::select("SELECT A
        .nik as nik,
        A.nama_pelaku_usaha as nama_pelaku_usaha,
        A.alamat_ktp as alamat,
        A.nama_usaha as nama_usaha, b.nm_kec as kecamatan, c.nm_kel as kelurahan,
        A.rw_ktp as rw,
        A.rt_ktp as rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.swk = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiSkg($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.skg = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiPasar($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.pasar = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiTokel($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.peken_tokel IN ( 1, 2 )
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiBPUM($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.bpum = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiPelatihan($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.pelatihan = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiPameran($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.pameran = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiIndustriRumahan($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.industri_rumahan = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiRumahKreatif($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.rumah_kreatif = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiPadatKarya($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.padat_karya = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiKur($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.kur = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiCsr($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.csr = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }
    public static function scopeCountIntervensiPuspita($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.puspita = '1'
        AND A.deleted_at IS NULL");

        return $data;
    }

    public static function scopeCountMbr()
    {
        $data = DB::select("SELECT
        tpud.nama_pelaku_usaha as nama_pelaku_usaha,
        tpud.alamat_ktp as alamat,
        tpud.nama_usaha as nama_usaha,
        kec.nm_kec as kecamatan,
        kel.nm_kel as kelurahan,
        tpud.rw_ktp as rw,
        tpud.rt_ktp as rt
    FROM
        t_pelaku_usaha_disdag_181022 tpud
        LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
        AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
        AND kel.id_m_setup_kab = '78'
        AND kel.id_m_setup_prop = '35'
        LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
        AND kec.id_m_setup_kab = '78'
        AND kec.id_m_setup_prop = '35'
    WHERE
        tpud.is_mbr_okt = '1'
        AND tpud.deleted_at IS NULL ");

        return $data;
    }

    public static function scopeCountNonMbr()
    {
        $data = DB::select("SELECT
        tpud.nama_pelaku_usaha as nama_pelaku_usaha,
        tpud.alamat_ktp as alamat,
        tpud.nama_usaha as nama_usaha,
        kec.nm_kec as kecamatan,
        kel.nm_kel as kelurahan,
        tpud.rw_ktp as rw,
        tpud.rt_ktp as rt
    FROM
        t_pelaku_usaha_disdag_181022 tpud
        LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
        AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
        AND kel.id_m_setup_kab = '78'
        AND kel.id_m_setup_prop = '35'
        LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
        AND kec.id_m_setup_kab = '78'
        AND kec.id_m_setup_prop = '35'
    WHERE
        tpud.is_mbr_okt IS NULL
        AND tpud.deleted_at IS NULL");

        return $data;
    }

    public static function scopeCountKategoriKBLI($query)
    {
        $data = DB::select("SELECT
			mkak.id_m_kategori_kbli,
			mkak.nama_kategori_kbli as nm_kategori,
			count(tpud.id_pelaku_usaha) as jumlah_pelaku_usaha
		FROM
			m_kategori_kbli mkak
		LEFT JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
		LEFT JOIN (
			SELECT
			id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
			-- unnest ( string_to_array( substr( kode_kbli_2, 2, LENGTH ( kode_kbli_2 ) - 2 ), ',' ) ) AS kode_kbli
            unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
				FROM
				t_pelaku_usaha_disdag_181022 WHERE deleted_at is NULL
		) tpud on mklk.kode_klasifikasi_kbli = tpud.kode_kbli
		GROUP BY
			mkak.id_m_kategori_kbli,
		  mkak.nama_kategori_kbli");

        return $data;
    }

    public static function scopeDataPelakuUsahaIndustriPengolahan()
    {
        $data =  DB::select(
            "SELECT
                rs.nik,
                rs.alamat,
                rs.id_pelaku_usaha,
                rs.nama_pelaku_usaha,
                rs.nama_usaha,
                rs.kecamatan,
                rs.kelurahan,
                rs.rw,
                rs.rt
            FROM
                m_kategori_kbli mkak
                INNER JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
                INNER JOIN (
                SELECT
                tpud.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                tpud.nik as nik,
                tpud.alamat_ktp as alamat,
                tpud.nama_pelaku_usaha as nama_pelaku_usaha,
                tpud.nama_usaha,
                kec.nm_kec as kecamatan,
                kel.nm_kel as kelurahan,
                tpud.rw_ktp as rw,
                tpud.rt_ktp as rt,
                -- unnest(string_to_array(substr( tpud.kode_kbli_2, 2, (LENGTH ( tpud.kode_kbli_2 ) - 2) ), ',' ) ) AS kode_kbli
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022 tpud
                LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
                AND kec.id_m_setup_prop = '35'
                AND kec.id_m_setup_kab = '78'
                LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
                AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
                AND kel.id_m_setup_prop = '35'
                AND kel.id_m_setup_kab = '78'
                ) rs on mklk.kode_klasifikasi_kbli = rs.kode_kbli
            WHERE
                mkak.id_m_kategori_kbli = 1
                AND mkak.deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeDataPelakuUsahaPerdagangan()
    {
        $data =  DB::select(
            "SELECT
                rs.nik,
                rs.alamat,
                rs.id_pelaku_usaha,
                rs.nama_pelaku_usaha,
                rs.nama_usaha,
                rs.kecamatan,
                rs.kelurahan,
                rs.rw,
                rs.rt
            FROM
                m_kategori_kbli mkak
                INNER JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
                INNER JOIN (
                SELECT
                tpud.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                tpud.nik as nik,
                tpud.alamat_ktp as alamat,
                tpud.nama_pelaku_usaha as nama_pelaku_usaha,
                tpud.nama_usaha,
                kec.nm_kec as kecamatan,
                kel.nm_kel as kelurahan,
                tpud.rw_ktp as rw,
                tpud.rt_ktp as rt,
                -- unnest(string_to_array(substr( tpud.kode_kbli_2, 2, (LENGTH ( tpud.kode_kbli_2 ) - 2) ), ',' ) ) AS kode_kbli
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022 tpud
                LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
                AND kec.id_m_setup_prop = '35'
                AND kec.id_m_setup_kab = '78'
                LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
                AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
                AND kel.id_m_setup_prop = '35'
                AND kel.id_m_setup_kab = '78'
                ) rs on mklk.kode_klasifikasi_kbli = rs.kode_kbli
            WHERE
                mkak.id_m_kategori_kbli = 2
                AND mkak.deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeDataPelakuUsahaAkomodasiMamin()
    {
        $data =  DB::select(
            "SELECT
                rs.nik,
                rs.alamat,
                rs.id_pelaku_usaha,
                rs.nama_pelaku_usaha,
                rs.nama_usaha,
                rs.kecamatan,
                rs.kelurahan,
                rs.rw,
                rs.rt
            FROM
                m_kategori_kbli mkak
                INNER JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
                INNER JOIN (
                SELECT
                tpud.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                tpud.nik as nik,
                tpud.alamat_ktp as alamat,
                tpud.nama_pelaku_usaha as nama_pelaku_usaha,
                tpud.nama_usaha,
                kec.nm_kec as kecamatan,
                kel.nm_kel as kelurahan,
                tpud.rw_ktp as rw,
                tpud.rt_ktp as rt,
                -- unnest(string_to_array(substr( tpud.kode_kbli_2, 2, (LENGTH ( tpud.kode_kbli_2 ) - 2) ), ',' ) ) AS kode_kbli
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022 tpud
                LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
                AND kec.id_m_setup_prop = '35'
                AND kec.id_m_setup_kab = '78'
                LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
                AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
                AND kel.id_m_setup_prop = '35'
                AND kel.id_m_setup_kab = '78'
                ) rs on mklk.kode_klasifikasi_kbli = rs.kode_kbli
            WHERE
                mkak.id_m_kategori_kbli = 3
                AND mkak.deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeDataPelakuUsahaAktivitasJasa()
    {
        $data =  DB::select(
            "SELECT
                rs.nik,
                rs.alamat,
                rs.id_pelaku_usaha,
                rs.nama_pelaku_usaha,
                rs.nama_usaha,
                rs.kecamatan,
                rs.kelurahan,
                rs.rw,
                rs.rt
            FROM
                m_kategori_kbli mkak
                INNER JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
                INNER JOIN (
                SELECT
                tpud.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                tpud.nik as nik,
                tpud.alamat_ktp as alamat,
                tpud.nama_pelaku_usaha as nama_pelaku_usaha,
                tpud.nama_usaha,
                kec.nm_kec as kecamatan,
                kel.nm_kel as kelurahan,
                tpud.rw_ktp as rw,
                tpud.rt_ktp as rt,
                -- unnest(string_to_array(substr( tpud.kode_kbli_2, 2, (LENGTH ( tpud.kode_kbli_2 ) - 2) ), ',' ) ) AS kode_kbli
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022 tpud
                LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
                AND kec.id_m_setup_prop = '35'
                AND kec.id_m_setup_kab = '78'
                LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
                AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
                AND kel.id_m_setup_prop = '35'
                AND kel.id_m_setup_kab = '78'
                ) rs on mklk.kode_klasifikasi_kbli = rs.kode_kbli
            WHERE
                mkak.id_m_kategori_kbli = 4
                AND mkak.deleted_at IS NULL"
        );

        return $data;
    }

    public static function scopeDataPertanianKehutananPerikanan()
    {
        $data =  DB::select(
            "SELECT
                rs.nik,
                rs.alamat,
                rs.id_pelaku_usaha,
                rs.nama_pelaku_usaha,
                rs.nama_usaha,
                rs.kecamatan,
                rs.kelurahan,
                rs.rw,
                rs.rt
            FROM
                m_kategori_kbli mkak
                INNER JOIN m_klasifikasi_kbli mklk ON mkak.id_m_kategori_kbli = mklk.id_m_kategori_kbli
                INNER JOIN (
                SELECT
                tpud.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                tpud.nik as nik,
                tpud.alamat_ktp as alamat,
                tpud.nama_pelaku_usaha as nama_pelaku_usaha,
                tpud.nama_usaha,
                kec.nm_kec as kecamatan,
                kel.nm_kel as kelurahan,
                tpud.rw_ktp as rw,
                tpud.rt_ktp as rt,
                -- unnest(string_to_array(substr( tpud.kode_kbli_2, 2, (LENGTH ( tpud.kode_kbli_2 ) - 2) ), ',' ) ) AS kode_kbli
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022 tpud
                LEFT JOIN m_setup_kec kec ON tpud.kecamatan_pemilik = kec.id_m_setup_kec
                AND kec.id_m_setup_prop = '35'
                AND kec.id_m_setup_kab = '78'
                LEFT JOIN m_setup_kel kel ON tpud.kelurahan_pemilik = kel.id_m_setup_kel
                AND tpud.kecamatan_pemilik = kel.id_m_setup_kec
                AND kel.id_m_setup_prop = '35'
                AND kel.id_m_setup_kab = '78'
                ) rs on mklk.kode_klasifikasi_kbli = rs.kode_kbli
            WHERE
                mkak.id_m_kategori_kbli = 5
                AND mkak.deleted_at IS NULL"
        );

        return $data;
    }

    public static function getDataPelakuUsaha($id = '', array $params = [], $data_user = null, $request)
    {
        // dd($request['status_belum_verif']);
        // dd($request);
        $where = ($id != "") ? "AND t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 = $id" : "";

        $query = "SELECT
			t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
			t_pelaku_usaha_disdag_181022.nik AS nik,
			UPPER(t_pelaku_usaha_disdag_181022.nama_pelaku_usaha) AS nama_pelaku_usaha,
			UPPER(t_pelaku_usaha_disdag_181022.nama_usaha) AS nama_usaha,
			t_pelaku_usaha_disdag_181022.status_mbr AS status_mbr,
			t_pelaku_usaha_disdag_181022.kategori_kbli AS kategori_kbli,
			t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
			t_pelaku_usaha_disdag_181022.status_keaktifan AS status_keaktifan,
			t_pelaku_usaha_disdag_181022.swk AS swk,
			t_pelaku_usaha_disdag_181022.skg AS skg,
			t_pelaku_usaha_disdag_181022.bpum AS bpum,
			t_pelaku_usaha_disdag_181022.pasar AS pasar,
			t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
			t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
			t_pelaku_usaha_disdag_181022.industri_rumahan AS industri_rumahan,
			t_pelaku_usaha_disdag_181022.kur AS kur,
			t_pelaku_usaha_disdag_181022.csr AS csr,
			t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
			t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
			t_pelaku_usaha_disdag_181022.pameran AS pameran,
			t_pelaku_usaha_disdag_181022.puspita AS puspita,
			t_pelaku_usaha_disdag_181022.oss AS oss,
			t_pelaku_usaha_disdag_181022.beras AS beras,
			t_pelaku_usaha_disdag_181022.gula AS gula,
			t_pelaku_usaha_disdag_181022.telur AS telur,
			t_pelaku_usaha_disdag_181022.cabai AS cabai,
			t_pelaku_usaha_disdag_181022.ayam_bebek AS ayam_bebek,
			t_pelaku_usaha_disdag_181022.daging AS daging,
			t_pelaku_usaha_disdag_181022.minyak AS minyak,
			t_pelaku_usaha_disdag_181022.buah AS buah,
			t_pelaku_usaha_disdag_181022.sayur AS sayur,
			t_pelaku_usaha_disdag_181022.tepung AS tepung,
			t_pelaku_usaha_disdag_181022.kedelai AS kedelai,
			t_pelaku_usaha_disdag_181022.kain AS kain,
			t_pelaku_usaha_disdag_181022.kayu AS kayu,
			UPPER(t_pelaku_usaha_disdag_181022.alamat_ktp) AS alamat_pemilik,
			t_pelaku_usaha_disdag_181022.rt_ktp AS rt_pemilik ,
			t_pelaku_usaha_disdag_181022.rw_ktp AS rw_pemilik ,
			kec_kel.nm_kec AS kecamatan,
			kec_kel.nm_kel AS kelurahan,
            t_pelaku_usaha_disdag_181022.tgl_verifikasi_kec as tgl_verif
		FROM
			t_pelaku_usaha_disdag_181022
			LEFT JOIN (SELECT
				kel.id_m_setup_kel,
				kel.nm_kel,
				kel.id_m_setup_kec,
				kec.nm_kec
			FROM
				m_setup_kel kel
				LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec
			WHERE
				kel.id_m_setup_prop = '35'
				AND kel.id_m_setup_kab = '78'
				AND kec.id_m_setup_prop = '35'
				AND kec.id_m_setup_kab = '78' ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik
			AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik
		WHERE
			t_pelaku_usaha_disdag_181022.deleted_at IS NULL " . $where;


        // MENAMPILKAN DATA PELAKU USAHA SESUAI DENGAN USERNAME KECAMATAN/KELURAHAN YANG LOGIN
        if (str_contains($data_user['user']->nama_user, 'Kelurahan')) {
            // $keckel = str_replace('Kelurahan ', '', $data_user['user']->nama_user);
            // $keckel = explode("", $data_user['user']->nama_user,1);
            $keckel = substr(strstr($data_user['user']->nama_user, " "), 1);
            // dd($keckel);
            $query .= " AND t_pelaku_usaha_disdag_181022.kelurahan ILIKE '%$keckel%'";
        } elseif (str_contains($data_user['user']->nama_user, 'Kecamatan')) {
            // $keckel = str_replace('Kecamatan ', '', $data_user['user']->nama_user);
            $keckel = substr(strstr($data_user['user']->nama_user, " "), 1);
            $query .= " AND t_pelaku_usaha_disdag_181022.kecamatan ILIKE '%$keckel%'";
        }

        if ($request['status'] == 'belum') {
            $query .= " AND t_pelaku_usaha_disdag_181022.tgl_verifikasi_kec IS NULL ";
        } elseif ($request['status'] == 'sudah') {
            $query .= " AND t_pelaku_usaha_disdag_181022.tgl_verifikasi_kec IS NOT NULL ";
        }
        // KECAMATAN
        if ($request['kecamatan']) {
            $query .= " AND kec_kel.id_m_setup_kec = " . $request['kecamatan'];
        }
        // END KECAMATAN
        // KELURAHAN
        if ($request['kelurahan']) {
            $query .= " AND kec_kel.id_m_setup_kel  = " . $request['kelurahan'];
        }

        # KATEGORI USAHA
        if ($request['kategori_usaha'] == "FASHION") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'FASHION'";
            // dd(DB::select($query));
        }
        if ($request['kategori_usaha'] == "JASA") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'JASA'";
        }
        if ($request['kategori_usaha'] == "KERAJINAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'KERAJINAN'";
        }
        if ($request['kategori_usaha'] == "MAKANAN DAN MINUMAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'MAKANAN DAN MINUMAN'";
        }
        if ($request['kategori_usaha'] == "PEMBUDIDAYA SEKTOR PERTANIAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'PEMBUDIDAYA SEKTOR PERTANIAN'";
        }
        if ($request['kategori_usaha'] == "TOKO") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'TOKO'";
        }
        // END KATEGORI USAHA
        // BAHAN BAKU
        // dd($request['bahan_baku']);


        // INTERVENSI
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'industri_rumahan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.industri_rumahan = 1";
        }
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'pameran') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pameran = 1";
        }
        if ($request['intervensi'] == 'pasar') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pasar = 1";
        }
        if ($request['intervensi'] == 'padat_karya') {
            $query .= " AND t_pelaku_usaha_disdag_181022.padat_karya = 1";
        }
        if ($request['intervensi'] == 'pelatihan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pelatihan = 1";
        }
        if ($request['intervensi'] == 'peken_tokel') {
            $query .= " AND (t_pelaku_usaha_disdag_181022.peken_tokel = 1 or  t_pelaku_usaha_disdag_181022.peken_tokel = 2)";
        }
        if ($request['intervensi'] == 'rumah_kreatif') {
            $query .= " AND t_pelaku_usaha_disdag_181022.rumah_kreatif = 1";
        }
        if ($request['intervensi'] == 'swk') {
            $query .= " AND t_pelaku_usaha_disdag_181022.swk = 1";
        }
        if ($request['intervensi'] == 'skg') {
            $query .= " AND t_pelaku_usaha_disdag_181022.skg = 1";
        }
        if ($request['intervensi'] == 'puspita') {
            $query .= " AND t_pelaku_usaha_disdag_181022.puspita = 1";
        }
        if ($request['intervensi'] == 'oss') {
            $query .= " AND t_pelaku_usaha_disdag_181022.oss = 1";
        }
        // Query all
        $query_all = $query;

        // Apply params
        $offset = 0;
        $limit = 0;
        $dir = "";
        $order = "";
        // dd($params);

        if (!empty($params)) {
            $offset = $params['start'];
            $limit = $params['limit'];
            $dir = $params['dir'];
            $order = $params['order'];


            // Apply search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $query .= "AND nama_pelaku_usaha ILIKE '%$search%' OR nama_usaha ILIKE '%$search%' OR nik ILIKE '%$search%'";
                $query .= "AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL ";
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
                $query_filtered = $query;
            } else {
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
            }
        }

        // Count total
        $total = count(DB::select($query_all));
        // Total filtered
        $filtered = !isset($query_filtered) ? $total : count(DB::select($query_filtered));
        // Get data
        $data = DB::select($query);
        // dd($data);
        foreach ($data as $key => $dat) {
            $data[$key]->DT_RowIndex = $offset + ($key + 1);

            // dd($dat);
            // INTERVENSI
            $intervensi = "<ul>";

            if ($dat->swk == 1) {
                $intervensi .= "<li>SWK</li>";
            }
            if ($dat->bpum == 1) {
                $intervensi .= "<li>BPUM</li>";
            }
            if ($dat->skg == 1) {
                $intervensi .= "<li>SKG</li>";
            }
            if ($dat->pasar == 1) {
                $intervensi .= "<li>PASAR</li>";
            }

            if ($dat->peken_tokel == 1 || $dat->peken_tokel == 2) {
                $intervensi .= "<li>PEKEN</li>";
            }
            if ($dat->rumah_kreatif == 1) {
                $intervensi .= "<li>RUMAH KREATIF</li>";
            }
            if ($dat->industri_rumahan == 1) {
                $intervensi .= "<li>INDUSTRI RUMAHAN</li>";
            }
            if ($dat->kur == 1) {
                $intervensi .= "<li>KUR</li>";
            }
            if ($dat->csr == 1) {
                $intervensi .= "<li>CSR</li>";
            }
            if ($dat->pelatihan == 1) {
                $intervensi .= "<li>PELATIHAN</li>";
            }
            if ($dat->padat_karya == 1) {
                $intervensi .= "<li>PADAT KARYA</li>";
            }
            if ($dat->pameran == 1) {
                $intervensi .= "<li>PAMERAN</li>";
            }
            if ($dat->puspita == 1) {
                $intervensi .= "<li>PUSPITA</li>";
            }
            if ($dat->oss == 1) {
                $intervensi .= "<li>OSS</li>";
            }
            $intervensi .= "</ul>";

            $data[$key]->intervensi = $intervensi;

            $cek_omset = TOmsetUsahaModel::where('id_t_pelaku_usaha', $dat->id)->first();
            $data[$key]->omset = $cek_omset ? $cek_omset->toarray() : null;

            $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $dat])->render();
        }

        // Return data
        return [
            'total' => $total,
            'filtered' => $filtered,
            'data' => $data,
        ];
    }

    public static function getDataMonitorOmset($id, array $params = [], $request = null)
    {
        $where = ($id != "") ? "AND t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 = $id" : "";

        $query = "SELECT
            t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
			t_pelaku_usaha_disdag_181022.nik AS nik,
            UPPER(t_pelaku_usaha_disdag_181022.nama_pelaku_usaha) AS nama_pelaku_usaha,
            UPPER(t_pelaku_usaha_disdag_181022.nama_usaha) AS nama_usaha,
            t_pelaku_usaha_disdag_181022.status_mbr AS status_mbr,
			t_pelaku_usaha_disdag_181022.kategori_kbli AS kategori_kbli,
			t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
			t_pelaku_usaha_disdag_181022.status_keaktifan AS status_keaktifan,
			t_pelaku_usaha_disdag_181022.swk AS swk,
			t_pelaku_usaha_disdag_181022.skg AS skg,
			t_pelaku_usaha_disdag_181022.bpum AS bpum,
			t_pelaku_usaha_disdag_181022.pasar AS pasar,
			t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
			t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
			t_pelaku_usaha_disdag_181022.industri_rumahan AS industri_rumahan,
			t_pelaku_usaha_disdag_181022.kur AS kur,
			t_pelaku_usaha_disdag_181022.csr AS csr,
			t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
			t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
			t_pelaku_usaha_disdag_181022.pameran AS pameran,
			t_pelaku_usaha_disdag_181022.puspita AS puspita,
			t_pelaku_usaha_disdag_181022.oss AS oss,
			t_pelaku_usaha_disdag_181022.beras AS beras,
			t_pelaku_usaha_disdag_181022.gula AS gula,
			t_pelaku_usaha_disdag_181022.telur AS telur,
			t_pelaku_usaha_disdag_181022.cabai AS cabai,
			t_pelaku_usaha_disdag_181022.ayam_bebek AS ayam_bebek,
			t_pelaku_usaha_disdag_181022.daging AS daging,
			t_pelaku_usaha_disdag_181022.minyak AS minyak,
			t_pelaku_usaha_disdag_181022.buah AS buah,
			t_pelaku_usaha_disdag_181022.sayur AS sayur,
			t_pelaku_usaha_disdag_181022.tepung AS tepung,
			t_pelaku_usaha_disdag_181022.kedelai AS kedelai,
			t_pelaku_usaha_disdag_181022.kain AS kain,
			t_pelaku_usaha_disdag_181022.kayu AS kayu,
			UPPER(t_pelaku_usaha_disdag_181022.alamat_ktp) AS alamat_pemilik,
			t_pelaku_usaha_disdag_181022.rt_ktp AS rt_pemilik ,
			t_pelaku_usaha_disdag_181022.rw_ktp AS rw_pemilik ,
			kec_kel.nm_kec AS kecamatan,
			kec_kel.nm_kel AS kelurahan,
            t_pelaku_usaha_disdag_181022.omset_bulanan AS omset_awal,
            jumlah_omset.jumlah_omset
        FROM
            t_pelaku_usaha_disdag_181022
            LEFT JOIN (
            SELECT
                kel.id_m_Setup_kel,
                kel.nm_kel,
                kel.id_m_setup_kec,
                kec.nm_kec 
            FROM
                m_setup_kel kel
                LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec 
            WHERE
                kel.id_m_setup_prop = '35' 
                AND kel.id_m_setup_kab = '78' 
                AND kec.id_m_setup_prop = '35' 
                AND kec.id_m_Setup_kab = '78' 
            ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik 
            AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik 
        JOIN (
            SELECT
                id_t_pelaku_usaha, sum(jumlah_omset) as jumlah_omset
            FROM
                t_omset_usaha
            WHERE
                deleted_at is null
                AND jumlah_omset!=0
            GROUP BY
                id_t_pelaku_usaha
            ) jumlah_omset ON jumlah_omset.id_t_pelaku_usaha = t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 
        WHERE
            t_pelaku_usaha_disdag_181022.deleted_at IS NULL " . $where;

        // KECAMATAN
        if ($request['kecamatan']) {
            $query .= " AND kec_kel.id_m_setup_kec = " . $request['kecamatan'];
        }
        // END KECAMATAN
        // KELURAHAN
        if ($request['kelurahan']) {
            $query .= " AND kec_kel.id_m_setup_kel  = " . $request['kelurahan'];
        }
        // END KELURAHAN
        // KATEGORI USAHA
        if ($request['kategori_usaha'] == "FASHION") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'FASHION'";
            // dd(DB::select($query));
        }
        if ($request['kategori_usaha'] == "JASA") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'JASA'";
        }
        if ($request['kategori_usaha'] == "KERAJINAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'KERAJINAN'";
        }
        if ($request['kategori_usaha'] == "MAKANAN DAN MINUMAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'MAKANAN DAN MINUMAN'";
        }
        if ($request['kategori_usaha'] == "PEMBUDIDAYA SEKTOR PERTANIAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'PEMBUDIDAYA SEKTOR PERTANIAN'";
        }
        if ($request['kategori_usaha'] == "TOKO") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'TOKO'";
        }
        // END KATEGORI USAHA
        // BAHAN BAKU
        /* if ($request['bahan_baku'] == 'beras') {
            $query .= " AND t_pelaku_usaha_disdag_181022.beras = 1";
        }
        if ($request['bahan_baku'] == 'kayu') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kayu = 1";
        }
        if ($request['bahan_baku'] == 'gula') {
            $query .= " AND t_pelaku_usaha_disdag_181022.gula = 1";
        }
        if ($request['bahan_baku'] == 'cabai') {
            $query .= " AND t_pelaku_usaha_disdag_181022.cabai = 1";
        }
        if ($request['bahan_baku'] == 'ayam_bebek') {
            $query .= " AND t_pelaku_usaha_disdag_181022.ayam_bebek = 1";
        }
        if ($request['bahan_baku'] == 'daging') {
            $query .= " AND t_pelaku_usaha_disdag_181022.daging = 1";
        }
        if ($request['bahan_baku'] == 'minyak') {
            $query .= " AND t_pelaku_usaha_disdag_181022.minyak = 1";
        }
        if ($request['bahan_baku'] == 'buah') {
            $query .= " AND t_pelaku_usaha_disdag_181022.buah = 1";
        }
        if ($request['bahan_baku'] == 'sayur') {
            $query .= " AND t_pelaku_usaha_disdag_181022.sayur = 1";
        }
        if ($request['bahan_baku'] == 'tepung') {
            $query .= " AND t_pelaku_usaha_disdag_181022.tepung = 1";
        }
        if ($request['bahan_baku'] == 'kedelai') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kedelai = 1";
        }
        if ($request['bahan_baku'] == 'kain') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kain = 1";
        } */
        // END BAHAN BAKU

        // INTERVENSI
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'industri_rumahan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.industri_rumahan = 1";
        }
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'pameran') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pameran = 1";
        }
        if ($request['intervensi'] == 'pasar') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pasar = 1";
        }
        if ($request['intervensi'] == 'padat_karya') {
            $query .= " AND t_pelaku_usaha_disdag_181022.padat_karya = 1";
        }
        if ($request['intervensi'] == 'pelatihan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pelatihan = 1";
        }
        if ($request['intervensi'] == 'peken_tokel') {
            $query .= " AND (t_pelaku_usaha_disdag_181022.peken_tokel = 1 or  t_pelaku_usaha_disdag_181022.peken_tokel = 2)";
        }
        if ($request['intervensi'] == 'rumah_kreatif') {
            $query .= " AND t_pelaku_usaha_disdag_181022.rumah_kreatif = 1";
        }
        if ($request['intervensi'] == 'swk') {
            $query .= " AND t_pelaku_usaha_disdag_181022.swk = 1";
        }
        if ($request['intervensi'] == 'skg') {
            $query .= " AND t_pelaku_usaha_disdag_181022.skg = 1";
        }
        if ($request['intervensi'] == 'puspita') {
            $query .= " AND t_pelaku_usaha_disdag_181022.puspita = 1";
        }
        if ($request['intervensi'] == 'oss') {
            $query .= " AND t_pelaku_usaha_disdag_181022.oss = 1";
        }
        // END INTERVENSI

        $query_all = $query;

        $offset = 0;
        $limit = 0;
        $dir = "";
        $order = "";

        if (!empty($params)) {
            $offset = $params['start'];
            $limit = $params['limit'];
            $dir = $params['dir'];
            $order = $params['order'];


            // Apply search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $query .= "AND nama_pelaku_usaha ILIKE '%$search%' OR nama_usaha ILIKE '%$search%'";
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
                $query_filtered = $query;
            } else {
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
            }
        }

        $total = count(DB::select($query_all));
        $filtered = !isset($query_filtered) ? $total : count(DB::select($query_filtered));
        $data = DB::select($query);
        foreach ($data as $key => $value) {
            $data[$key]->DT_RowIndex = $offset + ($key + 1);
            // INTERVENSI
            $intervensi = "<ul>";

            if ($value->swk == 1) {
                $intervensi .= "<li>SWK</li>";
            }
            if ($value->bpum == 1) {
                $intervensi .= "<li>BPUM</li>";
            }
            if ($value->skg == 1) {
                $intervensi .= "<li>SKG</li>";
            }
            if ($value->pasar == 1) {
                $intervensi .= "<li>PASAR</li>";
            }

            if ($value->peken_tokel == 1 || $value->peken_tokel == 2) {
                $intervensi .= "<li>PEKEN</li>";
            }
            if ($value->rumah_kreatif == 1) {
                $intervensi .= "<li>RUMAH KREATIF</li>";
            }
            if ($value->kur == 1) {
                $intervensi .= "<li>KUR</li>";
            }
            if ($value->csr == 1) {
                $intervensi .= "<li>CSR</li>";
            }
            if ($value->pelatihan == 1) {
                $intervensi .= "<li>PELATIHAN</li>";
            }
            if ($value->padat_karya == 1) {
                $intervensi .= "<li>PADAT KARYA</li>";
            }
            if ($value->pameran == 1) {
                $intervensi .= "<li>PAMERAN</li>";
            }
            if ($value->puspita == 1) {
                $intervensi .= "<li>PUSPITA</li>";
            }
            if ($value->oss == 1) {
                $intervensi .= "<li>OSS</li>";
            }

            $intervensi .= "</ul>";
            // dd($intervensi);

            // $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $dat])->render();
            $data[$key]->intervensi = $intervensi;
            // $data[$key]['intervensi'] = $intervensi;

            // END INTERVENSI


            // BAHAN BAKU

            $bahan_baku = "<ul>";

            if ($value->beras == 1) {
                $bahan_baku .= "<li>BERAS</li>";
            }

            if ($value->gula == 1) {
                $bahan_baku .= "<li>GULA</li>";
            }
            if ($value->telur == 1) {
                $bahan_baku .= "<li>TELUR</li>";
            }
            if ($value->cabai == 1) {
                $bahan_baku .= "<li>CABAI</li>";
            }
            if ($value->ayam_bebek == 1) {
                $bahan_baku .= "<li>AYAM / BEBEK</li>";
            }
            if ($value->daging == 1) {
                $bahan_baku .= "<li>DAGING</li>";
            }
            if ($value->minyak == 1) {
                $bahan_baku .= "<li>MINYAK</li>";
            }
            if ($value->sayur == 1) {
                $bahan_baku .= "<li>SAYUR</li>";
            }
            if ($value->tepung == 1) {
                $bahan_baku .= "<li>TEPUNG</li>";
            }
            if ($value->kedelai == 1) {
                $bahan_baku .= "<li>KEDELAI</li>";
            }
            if ($value->kayu == 1) {
                $bahan_baku .= "<li>KAYU</li>";
            }
            if ($value->kain == 1) {
                $bahan_baku .= "<li>KAIN</li>";
            }

            $bahan_baku .= "</ul>";

            // $datatable[$key]['bahan_baku'] = $bahan_baku;
            $data[$key]->bahan_baku = $bahan_baku;



            $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $value])->render();
        }
        return [
            'total' => $total,
            'filtered' => $filtered,
            'data' => $data,
        ];
    }

    public static function getDataMonitorPelakuUsaha($id, array $params = [], $request = null)
    {
        $where = ($id != "") ? "AND t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 = $id" : "";

        $query = "SELECT
			t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
			t_pelaku_usaha_disdag_181022.nik AS nik,
			UPPER(t_pelaku_usaha_disdag_181022.nama_pelaku_usaha) AS nama_pelaku_usaha,
			UPPER(t_pelaku_usaha_disdag_181022.nama_usaha) AS nama_usaha,
			t_pelaku_usaha_disdag_181022.status_mbr AS status_mbr,
			t_pelaku_usaha_disdag_181022.kategori_kbli AS kategori_kbli,
			t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
			t_pelaku_usaha_disdag_181022.swk AS swk,
			t_pelaku_usaha_disdag_181022.skg AS skg,
			t_pelaku_usaha_disdag_181022.pasar AS pasar,
			t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
			t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
			t_pelaku_usaha_disdag_181022.bpum AS bpum,
			t_pelaku_usaha_disdag_181022.kur AS kur,
			t_pelaku_usaha_disdag_181022.csr AS csr,
			t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
			t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
			t_pelaku_usaha_disdag_181022.pameran AS pameran,
			t_pelaku_usaha_disdag_181022.puspita AS puspita,
			t_pelaku_usaha_disdag_181022.beras AS beras,
			t_pelaku_usaha_disdag_181022.gula AS gula,
			t_pelaku_usaha_disdag_181022.telur AS telur,
			t_pelaku_usaha_disdag_181022.cabai AS cabai,
			t_pelaku_usaha_disdag_181022.ayam_bebek AS ayam_bebek,
			t_pelaku_usaha_disdag_181022.daging AS daging,
			t_pelaku_usaha_disdag_181022.minyak AS minyak,
			t_pelaku_usaha_disdag_181022.buah AS buah,
			t_pelaku_usaha_disdag_181022.sayur AS sayur,
			t_pelaku_usaha_disdag_181022.tepung AS tepung,
			t_pelaku_usaha_disdag_181022.kedelai AS kedelai,
			t_pelaku_usaha_disdag_181022.kain AS kain,
			t_pelaku_usaha_disdag_181022.kayu AS kayu,
			t_pelaku_usaha_disdag_181022.alamat_ktp AS alamat_pemilik,
			t_pelaku_usaha_disdag_181022.rt_ktp AS rt_pemilik ,
			t_pelaku_usaha_disdag_181022.rw_ktp AS rw_pemilik ,
			kec_kel.nm_kec AS kecamatan,
			kec_kel.nm_kel AS kelurahan,
            kec_kel.id_m_setup_kec,
            kec_kel.id_m_setup_kel
		FROM
			t_pelaku_usaha_disdag_181022
			LEFT JOIN (SELECT
				kel.id_m_setup_kel,
				kel.nm_kel,
				kel.id_m_setup_kec,
				kec.nm_kec
			FROM
				m_setup_kel kel
				LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec
			WHERE
				kel.id_m_setup_prop = '35'
				AND kel.id_m_setup_kab = '78'
				AND kec.id_m_setup_prop = '35'
				AND kec.id_m_setup_kab = '78' ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik
			AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik
		WHERE
		-- t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022=94547
			t_pelaku_usaha_disdag_181022.deleted_at IS NULL " . $where;


        // KECAMATAN
        if ($request['kecamatan']) {
            $query .= " AND kec_kel.id_m_setup_kec = " . $request['kecamatan'];
        }
        // END KECAMATAN
        // KELURAHAN
        if ($request['kelurahan']) {
            $query .= " AND kec_kel.id_m_setup_kel  = " . $request['kelurahan'];
        }
        // END KELURAHAN
        // KATEGORI USAHA
        // dd($request['kategori_usaha']);
        if ($request['kategori_usaha'] == "FASHION") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'FASHION'";
            // dd(DB::select($query));
        }
        if ($request['kategori_usaha'] == "JASA") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'JASA'";
        }
        if ($request['kategori_usaha'] == "KERAJINAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'KERAJINAN'";
        }
        if ($request['kategori_usaha'] == "MAKANAN DAN MINUMAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'MAKANAN DAN MINUMAN'";
        }
        if ($request['kategori_usaha'] == "PEMBUDIDAYA SEKTOR PERTANIAN") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'PEMBUDIDAYA SEKTOR PERTANIAN'";
        }
        if ($request['kategori_usaha'] == "TOKO") {
            $query .= " AND t_pelaku_usaha_disdag_181022.kategori_jenis_produk = 'TOKO'";
        }
        // END KATEGORI USAHA
        // BAHAN BAKU
        // dd($request['bahan_baku']);
        if ($request['bahan_baku'] == 'beras') {
            $query .= " AND t_pelaku_usaha_disdag_181022.beras = 1";
        }
        if ($request['bahan_baku'] == 'kayu') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kayu = 1";
        }
        if ($request['bahan_baku'] == 'gula') {
            $query .= " AND t_pelaku_usaha_disdag_181022.gula = 1";
        }
        if ($request['bahan_baku'] == 'cabai') {
            $query .= " AND t_pelaku_usaha_disdag_181022.cabai = 1";
        }
        if ($request['bahan_baku'] == 'ayam_bebek') {
            $query .= " AND t_pelaku_usaha_disdag_181022.ayam_bebek = 1";
        }
        if ($request['bahan_baku'] == 'daging') {
            $query .= " AND t_pelaku_usaha_disdag_181022.daging = 1";
        }
        if ($request['bahan_baku'] == 'minyak') {
            $query .= " AND t_pelaku_usaha_disdag_181022.minyak = 1";
        }
        if ($request['bahan_baku'] == 'buah') {
            $query .= " AND t_pelaku_usaha_disdag_181022.buah = 1";
        }
        if ($request['bahan_baku'] == 'sayur') {
            $query .= " AND t_pelaku_usaha_disdag_181022.sayur = 1";
        }
        if ($request['bahan_baku'] == 'tepung') {
            $query .= " AND t_pelaku_usaha_disdag_181022.tepung = 1";
        }
        if ($request['bahan_baku'] == 'kedelai') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kedelai = 1";
        }
        if ($request['bahan_baku'] == 'kain') {
            $query .= " AND t_pelaku_usaha_disdag_181022.kain = 1";
        }
        // END BAHAN BAKU

        // INTERVENSI
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'industri_rumahan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.industri_rumahan = 1";
        }
        if ($request['intervensi'] == 'csr') {
            $query .= " AND t_pelaku_usaha_disdag_181022.csr = 1";
        }
        if ($request['intervensi'] == 'pameran') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pameran = 1";
        }
        if ($request['intervensi'] == 'pasar') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pasar = 1";
        }
        if ($request['intervensi'] == 'padat_karya') {
            $query .= " AND t_pelaku_usaha_disdag_181022.padat_karya = 1";
        }
        if ($request['intervensi'] == 'pelatihan') {
            $query .= " AND t_pelaku_usaha_disdag_181022.pelatihan = 1";
        }
        if ($request['intervensi'] == 'peken_tokel') {
            $query .= " AND (t_pelaku_usaha_disdag_181022.peken_tokel = 1 or  t_pelaku_usaha_disdag_181022.peken_tokel = 2)";
        }
        if ($request['intervensi'] == 'rumah_kreatif') {
            $query .= " AND t_pelaku_usaha_disdag_181022.rumah_kreatif = 1";
        }
        if ($request['intervensi'] == 'swk') {
            $query .= " AND t_pelaku_usaha_disdag_181022.swk = 1";
        }
        if ($request['intervensi'] == 'skg') {
            $query .= " AND t_pelaku_usaha_disdag_181022.skg = 1";
        }
        if ($request['intervensi'] == 'puspita') {
            $query .= " AND t_pelaku_usaha_disdag_181022.puspita = 1";
        }
        // END INTERVENSI
        // Query all
        $query_all = $query;

        // Apply params

        $offset = 0;
        $limit = 0;
        $dir = "";
        $order = "";

        // $total = $filtered =  "";

        // $data = [];

        // dd($params);


        if (!empty($params)) {
            $offset = $params['start'];
            $limit = $params['limit'];
            $dir = $params['dir'];
            $order = $params['order'];


            // Apply search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $query .= "AND nama_pelaku_usaha ILIKE '%$search%' OR nama_usaha ILIKE '%$search%'
                OR nama_usaha ILIKE '%$search%'OR kecamatan ILIKE '%$search%'
                OR kelurahan ILIKE '%$search%'";
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
                $query_filtered = $query;
            } else {
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
            }
        }

        // Count total
        $total = count(DB::select($query_all));
        // Total filtered
        $filtered = !isset($query_filtered) ? $total : count(DB::select($query_filtered));
        // Get data
        $data = DB::select($query);
        foreach ($data as $key => $value) {
            $data[$key]->DT_RowIndex = $offset + ($key + 1);

            // INTERVENSI
            $intervensi = "<ul>";

            if ($value->swk == 1) {
                $intervensi .= "<li>SWK</li>";
            }
            if ($value->bpum == 1) {
                $intervensi .= "<li>BPUM</li>";
            }
            if ($value->skg == 1) {
                $intervensi .= "<li>SKG</li>";
            }
            if ($value->pasar == 1) {
                $intervensi .= "<li>PASAR</li>";
            }

            if ($value->peken_tokel == 1 || $value->peken_tokel == 2) {
                $intervensi .= "<li>PEKEN</li>";
            }
            if ($value->rumah_kreatif == 1) {
                $intervensi .= "<li>RUMAH KREATIF</li>";
            }
            if ($value->kur == 1) {
                $intervensi .= "<li>KUR</li>";
            }
            if ($value->csr == 1) {
                $intervensi .= "<li>CSR</li>";
            }
            if ($value->pelatihan == 1) {
                $intervensi .= "<li>PELATIHAN</li>";
            }
            if ($value->padat_karya == 1) {
                $intervensi .= "<li>PADAT KARYA</li>";
            }
            if ($value->pameran == 1) {
                $intervensi .= "<li>PAMERAN</li>";
            }
            if ($value->puspita == 1) {
                $intervensi .= "<li>PUSPITA</li>";
            }

            $intervensi .= "</ul>";
            // dd($intervensi);

            // $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $dat])->render();
            $data[$key]->intervensi = $intervensi;
            // $data[$key]['intervensi'] = $intervensi;

            // END INTERVENSI

            // BAHAN BAKU

            $bahan_baku = "<ul>";

            if ($value->beras == 1) {
                $bahan_baku .= "<li>BERAS</li>";
            }

            if ($value->gula == 1) {
                $bahan_baku .= "<li>GULA</li>";
            }
            if ($value->telur == 1) {
                $bahan_baku .= "<li>TELUR</li>";
            }
            if ($value->cabai == 1) {
                $bahan_baku .= "<li>CABAI</li>";
            }
            if ($value->ayam_bebek == 1) {
                $bahan_baku .= "<li>AYAM / BEBEK</li>";
            }
            if ($value->daging == 1) {
                $bahan_baku .= "<li>DAGING</li>";
            }
            if ($value->minyak == 1) {
                $bahan_baku .= "<li>MINYAK</li>";
            }
            if ($value->sayur == 1) {
                $bahan_baku .= "<li>SAYUR</li>";
            }
            if ($value->tepung == 1) {
                $bahan_baku .= "<li>TEPUNG</li>";
            }
            if ($value->kedelai == 1) {
                $bahan_baku .= "<li>KEDELAI</li>";
            }
            if ($value->kayu == 1) {
                $bahan_baku .= "<li>KAYU</li>";
            }
            if ($value->kain == 1) {
                $bahan_baku .= "<li>KAIN</li>";
            }

            $bahan_baku .= "</ul>";

            // $datatable[$key]['bahan_baku'] = $bahan_baku;
            $data[$key]->bahan_baku = $bahan_baku;


            // END BAHAN BAKU
            $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $value])->render();
        }
        // dd($data);





        // Return data
        return [
            'total' => $total,
            'filtered' => $filtered,
            'data' => $data,
        ];
    }

    /* Fungsi untuk scopeCountBelumIntervensi yang berisi raw query*/
    public static function scopeCountBelumIntervensi($query)
    {
        $data = \DB::select(
            "SELECT
            tpu.nik AS nik,
            tpu.nama_pelaku_usaha AS nama_pelaku_usaha,
            tpu.alamat_ktp AS alamat,
            tpu.nama_usaha AS nama_usaha,
            kec.nm_kec AS kecamatan,
            kel.nm_kel AS kelurahan,
            tpu.rw_ktp AS rw,
            tpu.rt_ktp AS rt,
            tpu.nib AS nib
        FROM
            t_pelaku_usaha_disdag_181022 tpu
            LEFT JOIN m_setup_kec kec ON tpu.kecamatan_pemilik = kec.id_m_setup_kec
            AND kec.id_m_setup_prop = '35'
            AND kec.id_m_setup_kab = '78'
            LEFT JOIN m_setup_kel kel ON tpu.kelurahan_pemilik = kel.id_m_setup_kel
            AND kec.id_m_setup_kec = kel.id_m_setup_kec
            AND kel.id_m_setup_prop = '35'
            AND kel.id_m_setup_kab = '78'
        WHERE
        tpu.bpum is null
        AND tpu.swk is null
        AND tpu.skg is null
        AND tpu.pasar is null
        AND tpu.peken_tokel is null
        AND tpu.pelatihan is null
        AND tpu.pameran is null
        AND tpu.industri_rumahan is null
        AND tpu.rumah_kreatif is null
        AND tpu.padat_karya is null
        AND tpu.kur is null
        AND tpu.csr is null
        AND tpu.puspita is null"
        );

        return $data;
    }
    public static function scopeCountOss($query)
    {
        $data = DB::select("SELECT
        A.nik AS nik,
        A.nama_pelaku_usaha AS nama_pelaku_usaha,
        A.alamat_ktp AS alamat,
        A.nama_usaha AS nama_usaha,
        b.nm_kec AS kecamatan,
        C.nm_kel AS kelurahan,
        A.rw_ktp AS rw,
        A.rt_ktp AS rt,
        A.nib as nib
    FROM
        t_pelaku_usaha_disdag_181022
        A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
        AND b.id_m_setup_prop = '35'
        AND b.id_m_setup_kab = '78'
        LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
        AND b.id_m_setup_kec = C.id_m_setup_kec
        AND C.id_m_setup_prop = '35'
        AND C.id_m_setup_kab = '78'
    WHERE
        A.oss = '1'
        AND A.deleted_at IS NULL");

        return $data;
        // dd($data);
    }

    /* get detail pelaku usaha */
    public static function getDetailPelakuUsaha($id)
    {
        $query = "
        SELECT
        t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
        t_pelaku_usaha_disdag_181022.nama_pelaku_usaha AS nama_pelaku_usaha,
        t_pelaku_usaha_disdag_181022.nik AS nik,
        t_pelaku_usaha_disdag_181022.nama_usaha AS nama_usaha,
        t_pelaku_usaha_disdag_181022.status_mbr AS status_mbr,
        t_pelaku_usaha_disdag_181022.kategori_kbli AS kategori_kbli,
        t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
        t_pelaku_usaha_disdag_181022.swk AS swk,
        t_pelaku_usaha_disdag_181022.skg AS skg,
        t_pelaku_usaha_disdag_181022.pasar AS pasar,
        t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
        t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
        t_pelaku_usaha_disdag_181022.kur AS kur,
        t_pelaku_usaha_disdag_181022.csr AS csr,
        t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
        t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
        t_pelaku_usaha_disdag_181022.pameran AS pameran,
        t_pelaku_usaha_disdag_181022.puspita AS puspita,
        t_pelaku_usaha_disdag_181022.beras AS beras,
        t_pelaku_usaha_disdag_181022.gula AS gula,
        t_pelaku_usaha_disdag_181022.telur AS telur,
        t_pelaku_usaha_disdag_181022.cabai AS cabai,
        t_pelaku_usaha_disdag_181022.ayam_bebek AS ayam_bebek,
        t_pelaku_usaha_disdag_181022.daging AS daging,
        t_pelaku_usaha_disdag_181022.minyak AS minyak,
        t_pelaku_usaha_disdag_181022.buah AS buah,
        t_pelaku_usaha_disdag_181022.sayur AS sayur,
        t_pelaku_usaha_disdag_181022.tepung AS tepung,
        t_pelaku_usaha_disdag_181022.kedelai AS kedelai,
        t_pelaku_usaha_disdag_181022.kain AS kain,
        t_pelaku_usaha_disdag_181022.kayu AS kayu,
        t_pelaku_usaha_disdag_181022.alamat_ktp AS alamat_pemilik,
        t_pelaku_usaha_disdag_181022.rt_ktp AS rt_pemilik ,
        t_pelaku_usaha_disdag_181022.rw_ktp AS rw_pemilik ,
        kec_kel.nm_kec AS kecamatan,
        kec_kel.nm_kel AS kelurahan
    FROM
        t_pelaku_usaha_disdag_181022
        LEFT JOIN (SELECT
            kel.id_m_setup_kel,
            kel.nm_kel,
            kel.id_m_setup_kec,
            kec.nm_kec
        FROM
            m_setup_kel kel
            LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec
        WHERE
            kel.id_m_setup_prop = '35'
            AND kel.id_m_setup_kab = '78'
            AND kec.id_m_setup_prop = '35'
            AND kec.id_m_setup_kab = '78' ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik
        AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik
    WHERE
        t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022='$id' AND t_pelaku_usaha_disdag_181022.deleted_at IS NULL

        ";

        $data = DB::select($query);

        return $data;
    }

    public static function getDataOmsePelakuUsaha($id, array $params = [])
    {
        $where = ($id != "") ? "AND t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 = $id" : "";

        $query = "SELECT
			t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
			t_pelaku_usaha_disdag_181022.nik AS nik,
			t_pelaku_usaha_disdag_181022.nama_pelaku_usaha AS nama_pelaku_usaha,
			t_pelaku_usaha_disdag_181022.nama_usaha AS nama_usaha,
			t_pelaku_usaha_disdag_181022.status_mbr AS status_mbr,
			t_pelaku_usaha_disdag_181022.kategori_kbli AS kategori_kbli,
			t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
			t_pelaku_usaha_disdag_181022.swk AS swk,
			t_pelaku_usaha_disdag_181022.skg AS skg,
			t_pelaku_usaha_disdag_181022.pasar AS pasar,
			t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
			t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
			t_pelaku_usaha_disdag_181022.kur AS kur,
			t_pelaku_usaha_disdag_181022.csr AS csr,
			t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
			t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
			t_pelaku_usaha_disdag_181022.pameran AS pameran,
			t_pelaku_usaha_disdag_181022.puspita AS puspita,
			t_pelaku_usaha_disdag_181022.beras AS beras,
			t_pelaku_usaha_disdag_181022.gula AS gula,
			t_pelaku_usaha_disdag_181022.telur AS telur,
			t_pelaku_usaha_disdag_181022.cabai AS cabai,
			t_pelaku_usaha_disdag_181022.ayam_bebek AS ayam_bebek,
			t_pelaku_usaha_disdag_181022.daging AS daging,
			t_pelaku_usaha_disdag_181022.minyak AS minyak,
			t_pelaku_usaha_disdag_181022.buah AS buah,
			t_pelaku_usaha_disdag_181022.sayur AS sayur,
			t_pelaku_usaha_disdag_181022.tepung AS tepung,
			t_pelaku_usaha_disdag_181022.kedelai AS kedelai,
			t_pelaku_usaha_disdag_181022.kain AS kain,
			t_pelaku_usaha_disdag_181022.kayu AS kayu,
			t_pelaku_usaha_disdag_181022.alamat_ktp AS alamat_pemilik,
			t_pelaku_usaha_disdag_181022.rt_ktp AS rt_pemilik ,
			t_pelaku_usaha_disdag_181022.rw_ktp AS rw_pemilik ,
			kec_kel.nm_kec AS kecamatan,
			kec_kel.nm_kel AS kelurahan
		FROM
			t_pelaku_usaha_disdag_181022
			LEFT JOIN (SELECT
				kel.id_m_setup_kel,
				kel.nm_kel,
				kel.id_m_setup_kec,
				kec.nm_kec
			FROM
				m_setup_kel kel
				LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec
			WHERE
				kel.id_m_setup_prop = '35'
				AND kel.id_m_setup_kab = '78'
				AND kec.id_m_setup_prop = '35'
				AND kec.id_m_setup_kab = '78' ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik
			AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik
		WHERE
		-- t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022=94547
			t_pelaku_usaha_disdag_181022.deleted_at IS NULL " . $where;

        // Query all
        $query_all = $query;

        // Apply params
        $offset = 0;
        $limit = 0;
        $dir = "";
        $order = "";
        // dd($params);

        if (!empty($params)) {
            $offset = $params['start'];
            $limit = $params['limit'];
            $dir = $params['dir'];
            $order = $params['order'];


            // Apply search
            if (!empty($params['search'])) {
                $search = $params['search'];
                $query .= "AND nama_pelaku_usaha ILIKE '%$search%' OR nama_usaha ILIKE '%$search%'";
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
                $query_filtered = $query;
            } else {
                $query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
            }
        }

        // Count total
        $total = count(DB::select($query_all));
        // Total filtered
        $filtered = !isset($query_filtered) ? $total : count(DB::select($query_filtered));
        // Get data
        $data = DB::select($query);
        foreach ($data as $key => $dat) {
            $data[$key]->DT_RowIndex = $offset + ($key + 1);
            $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $dat])->render();
        }

        // Return data
        return [
            'total' => $total,
            'filtered' => $filtered,
            'data' => $data,
        ];
    }

    public static function CekDataUmkm() /*cek apakah ada data di umkm yang sudah terhapus di padat karya */
    {
        // dd($nik);
        $data = DB::select("SELECT a.nik FROM t_pelaku_usaha_disdag_181022 a LEFT JOIN padat_karya b
        ON a.nik=b.nik WHERE b.nik IS NULL AND a.deleted_at IS NULL AND a.padat_karya='1'");

        return $data;
    }
    public static function CekNikIntervensiSelainPadatKarya($nik) /*jika ada, kemudian cek apakah nik tersebut mendapat intervensi selain padat karya*/
    {
        // dd($nik)
        $data = DB::select("SELECT A
            .id_t_pelaku_usaha_disdag_181022,
            A.nik,
            b.nik AS nik_pk,
            A.padat_karya,
            A.swk,
            A.skg,
            A.pasar,
            A.peken_tokel,
            A.rumah_kreatif AS rk,
            A.kur,
            A.csr,
            A.pelatihan,
            A.pameran,
            A.puspita
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN padat_karya b ON A.nik = b.nik
        WHERE
            A.padat_karya = '1'
            AND (
                A.swk = '1'
                OR A.skg = '1'
                OR A.pasar = '1'
                OR A.peken_tokel IN ( '1', '2' )
                OR A.rumah_kreatif = '1'
                OR A.kur = '1'
                OR A.csr = '1'
                OR A.pelatihan = '1'
                OR A.pameran = '1'
                OR A.puspita = '1'
            )
            AND A.deleted_at IS NULL
            AND a.padat_karya IS NULL");
        // $data = DB::table('t_pelaku_usaha_disdag_181022 as A')
        //     ->select(
        //         'A.id_t_pelaku_usaha_disdag_181022',
        //         'A.nik',
        //         'B.nik as nik_pk',
        //         'A.padat_karya',
        //         'A.swk',
        //         'A.industri_rumahan as ir',
        //         'A.skg',
        //         'A.pasar',
        //         'A.peken_tokel',
        //         'A.rumah_kreatif as rk',
        //         'A.kur',
        //         'A.csr',
        //         'A.pelatihan',
        //         'A.pameran',
        //         'A.puspita'
        //     )
        //     ->leftJoin('padat_karya as B', 'A.nik', 'B.nik')
        //     ->where('A.padat_karya', 1)
        //     ->where(function ($query) {
        //         $query->where('A.swk', 1)
        //             ->orWhere('A.industri_rumahan', 1)
        //             ->orWhere('A.skg', 1)
        //             ->orWhere('A.pasar', 1)
        //             ->whereIn('A.peken_tokel', [1, 2])
        //             ->orWhere('A.rumah_kreatif', 1)
        //             ->orWhere('A.kur', 1)
        //             ->orWhere('A.csr', 1)
        //             ->orWhere('A.pelatihan', 1)
        //             ->orWhere('A.pameran', 1)
        //             ->orWhere('A.puspita', 1);
        //     })
        //     ->whereNull('A.deleted_at')
        //     ->where('A.nik', $nik)->get();
        return $data;
    }
    public static function UpdateNikIntervensiSelainPadatKarya($nik) /*jika nik tersebut mendapatkan intervensi selain padat karya, maka melakukan update kolom padat karya menjadi null*/
    {
        // dd($nik);
        // dd($cek_intervensi, 'masuk func');
        $data =
            DB::table('t_pelaku_usaha_disdag_181022')
            ->where('nik', '=', $nik)
            ->update([
                'padat_karya' => null
            ]);
        // foreach ($cek_intervensi as $key => $value) {
        //     // $nik[$key] = $value->nik;
        //     // $data[$key] = DB::select("UPDATE t_pelaku_usaha_disdag_181022 SET padat_karya = null where nik=xxx'");
        // }
        // $nik = implode(' ', $nik);
        // dd($nik);
        // dd($data);
        return $data;
    }
    public static function CekNikHanyaPadatKarya($nik) /*lakukan pengecekan nik lagi apakah nik tersebut hanya mendapatkan intervensi padat karya*/
    {
        // dd($nik);
        $data = DB::select("SELECT A
            .id_t_pelaku_usaha_disdag_181022,
            A.nik,
            A.padat_karya,
            A.swk,
            A.skg,
            A.pasar,
            A.peken_tokel,
            A.rumah_kreatif AS rk,
            A.kur,
            A.csr,
            A.pelatihan,
            A.pameran,
            A.puspita
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN padat_karya b ON A.nik = b.nik
        WHERE
            A.padat_karya = '1'
            AND (
                ( A.swk != 1 OR A.swk IS NULL )
                AND ( A.skg != 1 OR A.skg IS NULL )
                AND ( A.pasar != 1 OR A.pasar IS NULL )
                AND ( A.peken_tokel NOT IN ( 1, 2 ) OR A.peken_tokel IS NULL )
                AND ( A.rumah_kreatif != 1 OR A.rumah_kreatif IS NULL )
                AND ( A.kur != 1 OR A.kur IS NULL )
                AND ( A.csr != 1 OR A.csr IS NULL )
                AND ( A.pelatihan != 1 OR A.pelatihan IS NULL )
                AND ( A.pameran != 1 OR A.pameran IS NULL )
                AND ( A.puspita != 1 OR A.puspita IS NULL )
            )
            AND b.nik IS NULL
            AND a.nik='$nik'");

        // $data = DB::table('id_t_pelaku_usaha_disdag_181022 as A')
        //     ->select('A.nik', 'A.padat_karya', 'A.swk', 'A.industri_rumahan as ir', 'A.skg', 'A.pasar', 'A.peken_tokel', 'A.rumah_kreatif as rk', 'A.kur', 'A.csr', 'A.pelatihan', 'A.pameran', 'A.puspita')
        //     ->leftJoin('padat_karya as B', 'A.nik', 'B.nik')
        //     ->where('A.padat_karya', 1)
        //     ->where(function ($query) {
        //         $query->where('votes', '>', 100)
        //             ->orWhere('title', '=', 'Admin');
        //     })
        //     ->where('A.swk', '!=', 1)->orWhereNull('A.swk')
        //     ->where('A.industri_rumahan', '!=', 1)->orWhereNull('A.industri_rumahan')
        //     ->where('A.skg', '!=', 1)->orWhereNull('A.skg')
        //     ->where('A.pasar', '!=', 1)->orWhereNull('A.pasar')
        //     ->whereNotIn('A.peken_tokel',  [1, 2])->orWhereNull('A.peken_tokel')
        //     ->where('A.rumah_kreatif', '!=', 1)->orWhereNull('A.rumah_kreatif')
        //     ->where('A.kur', '!=', 1)->orWhereNull('A.kur')
        //     ->where('A.csr', '!=', 1)->orWhereNull('A.csr')
        //     ->where('A.pelatihan', '!=', 1)->orWhereNull('A.pelatihan')
        //     ->where('A.pameran', '!=', 1)->orWhereNull('A.pameran')
        //     ->where('A.puspita', '!=', 1)->orWhereNull('A.puspita')
        //     ->whereNull('B.nik')
        //     ->where('A.nik', $nik)->first();

        return $data;
    }
    public static function Update_PadatKaryaTanpaIntervensiLain($nik) /*mengecek apakah ada nik padat karya yang sudah ada di umkm*/
    {
        // dd($cek_nik_padat_karya);
        // $nik = implode(' ', $nik);
        // dd($nik, 'masuk ke func');
        $data = DB::table('t_pelaku_usaha_disdag_181022')
            ->where('nik', '=', $nik)
            ->update([
                'deleted_at' => Carbon::now()
            ]);
        // $data = DB::update("UPDATE t_pelaku_usaha_disdag_181022 SET deleted_at=NOW() WHERE nik= $nik");
        // dd($data);
        return $data;
    }
    public static function CekNikPadatKaryaDiUmkm($nik)/*mengecek apakah ada nik padat karya yang sudah ada di umkm*/
    {
        // dd($nik);
        // $nik = implode(' ', $nik);
        // $data = DB::select("SELECT  a.nik as nik_padat_karya, b.nik as nik_pelaku_usaha, a.pekerjaan from padat_karya_281122 a
        //  join t_pelaku_usaha_disdag_181022 b on a.nik=b.nik where a.nik='$nik' and b.deleted_at is null");
        // $data = DB::select("SELECT  a.nik as nik_padat_karya, b.nik as nik_pelaku_usaha, a.pekerjaan from padat_karya_281122 a join
        // t_pelaku_usaha_disdag_181022 b on a.nik=b.nik where a.nik='$nik' and b.deleted_at is null");




        $data = DB::table('padat_karya')
            ->select('padat_karya.nik as nik_padat_karya', 't_pelaku_usaha_disdag_181022.nik', 'padat_karya.pekerjaan')
            // ->where('padat_karya', '!=', 1)
            ->join('t_pelaku_usaha_disdag_181022', 't_pelaku_usaha_disdag_181022.nik', 'padat_karya.nik')
            ->where('padat_karya.nik', '=', $nik)
            ->where('t_pelaku_usaha_disdag_181022.deleted_at', '=', NULL)
            ->first();

        // dd($data);

        // $data = DB::select("select a.nik from t_pelaku_usaha_disdag_181022 a where a.padat_karya!='1' and a.nik='3578160611710002' and a.deleted_at is null");
        return $data;
        // dd($data);
    }
    public static function Update_PadatKarya($nik, $pekerjaan)/*jika ada maka melakukan update di kolom padat karya dan ket padat karya sesuai dengan data tarikan */
    {
        // dd($cek_nik_pk_umkm, 'masuk func');
        // foreach ($cek_nik_pk_umkm as $key => $value) {
        // dd($value->nik);
        // dd($nik, $pekerjaan, 'masuk func');
        $pekerjaan = $pekerjaan ?? '';
        $data = DB::update("UPDATE t_pelaku_usaha_disdag_181022
                SET padat_karya='1', ket_padat_karya = '$pekerjaan', is_surabaya='1', updated_at = NOW()
                WHERE nik='$nik'");
        // }

        // $sub = DB::table('t_pelaku_usaha_disdag_181022')->select('t_pelaku_usaha_disdag_181022.nik', 't_pelaku_usaha_disdag_181022.ket_padat_karya', 'padat_karya_281122.pekerjaan')
        //     ->leftJoin('padat_karya_281122', 't_pelaku_usaha_disdag_181022.nik', '=', 'padat_karya_281122.nik')
        //     ->where('t_pelaku_usaha_disdag_181022.padat_karya', '=', 1)
        //     ->whereNull('t_pelaku_usaha_disdag_181022.deleted_at')
        //     ->where('t_pelaku_usaha_disdag_181022.nik', '=', $cek_nik_pk_umkm);


        // $data = DB::table(DB::raw("({$sub->toSql()}) as sub")->mergeBindings($sub->getQuery())
        //     ->where('t_pelaku_usaha_disdag_181022', 't_pelaku_usaha_disdag_181022.nik', '=', $sub)
        //     ->update('padat_karya', '=', 1)
        //     ->update('ket_padat_karya', '=', $sub);

        // dd($data);
        return $data;
    }

    public static function storeDataFilterPadatKarya($nik) /* jika tidak ada maka melakukan insert data */
    {
        // dd($cek_nik_pk_umkm, 'masuk func');
        $data = DB::insert("INSERT INTO t_pelaku_usaha_disdag_181022 (
                    nik,
                    nama_pelaku_usaha,
                    alamat_ktp,
                    provinsi_pemilik,
                    kabupaten_pemilik,
                    kecamatan,
                    kecamatan_pemilik,
                    kelurahan,
                    kelurahan_pemilik,
                    rw_ktp,
                    rt_ktp,
                    status_mbr,
                    ket_padat_karya,
                    padat_karya,
                    is_surabaya,
                    kecamatan_domisili,
                    kelurahan_domisili,
                    created_at,
                    tgl_insert_padatkarya
                    ) (
                SELECT
                    x.nik,
                    x.nama as nama_pelaku_usaha,
                    x.alamat as alamat_ktp,
                    35 as provinsi_pemilik,
                    78 as kabupaten_pemilik,
                    x.kecamatan,
                    x.id_m_setup_kec as kecamatan_pemilik,
                    x.kelurahan,
                    x.id_m_setup_kel as kelurahan_pemilik,
                    x.no_rw as rw_ktp,
                    x.no_rt as rt_ktp,
                    'MBR' as status_mbr,
                    x.pekerjaan as ket_padat_karya,
                    1 as padat_karya,
                    1 as is_surabaya,
                    x.id_m_setup_kec as kecamatan_domisili,
                    x.id_m_setup_kel as kelurahan_domisili,
                    NOW(),
                    NOW()
                from padat_karya x
                WHERE x.nik ='$nik'
                    )");
        // return $data;
        return response()->json(['Berhasil Insert Data Padat Karya']);
        // foreach ($cek_nik_pk_umkm as $key => $value) {

        //     // dd($data);

        // }
    }


    /* FUNCTION FILTER DATA PEKEN KE UMKM */

    public static function update_keckel_ktp($nik)
    {
        $data = DB::update("UPDATE peken
        SET kecamatan_ktp=subquery.id_m_setup_kec, kelurahan_ktp=subquery.id_m_setup_kel
        FROM
            ( SELECT x.id_m_setup_kel, x.id_m_setup_kec, x.id_kel_peken FROM m_setup_kel x
            ) AS subquery
        WHERE
            peken.kelurahan_pemilik = subquery.id_kel_peken::int
            AND peken.nik='$nik'");

        return $data;
    }

    public static function update_keckel_usaha($nik)
    {
        $data = DB::update("UPDATE peken
        set kecamatan_usaha_umkm=subquery.id_m_setup_kec, kelurahan_usaha_umkm=subquery.id_m_setup_kel
        FROM
            ( SELECT x.id_m_setup_kel, x.id_m_setup_kec, x.id_kel_peken FROM m_setup_kel x
            ) AS subquery
        WHERE
            peken.kelurahan_usaha = subquery.id_kel_peken::int
            AND peken.nik='$nik'");

        return $data;
    }
    public static function cek_nik_peken()
    {

        $data = DB::select("SELECT A
            .nik AS nik_umkm,
            b.nik AS nik_peken
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN peken b ON A.nik = b.nik
        WHERE
            b.nik IS NULL
            AND A.deleted_at IS NULL
            AND ( A.peken_tokel = '1' OR A.peken_tokel = '2' )
            -- and a.nik='1901032102800001'
            -- and a.nik='3512076705850001'
            ");

        /* $data = DB::table('t_pelaku_usaha_disdag_181022 as a')
            ->select('a.nik as nik_umkm', 'b.nik as nik_peken')
            ->leftJoin('peken as b', 'a.nik', 'b.nik')
            ->whereNull('b.nik')
            ->whereNull('a.deleted_at')
            // ->where(['a.peken_tokel', 1, 'a.peken_tokel', 2])
            ->where(function ($query) {
                $query->where('a.peken_tokel', 1)
                    ->orWhere('a.peken_tokel', 2);
            })
            // ->Where('a.nik', 1901032102800001)
            // ->toSql()
            ->get(); */
        // dd($data);

        return $data;
    }

    public static function inter_selain_peken($nik)
    {
        $data = DB::select("SELECT
            B.nik as nik_peken,
            A.nik,
            A.padat_karya,
            A.swk,
            A.skg,
            A.pasar,
            A.peken_tokel,
            A.rumah_kreatif AS rk,
            A.kur,
            A.csr,
            A.pelatihan,
            A.pameran,
            A.puspita
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN peken b ON A.nik = b.nik
        WHERE
            ( A.peken_tokel = '1' OR A.peken_tokel = '2' )
            AND (
                A.swk IS NOT NULL
                OR A.skg IS NOT NULL
                OR A.pasar IS NOT NULL
                OR A.padat_karya IS NOT NULL
                OR A.rumah_kreatif IS NOT NULL
                OR A.kur IS NOT NULL
                OR A.csr IS NOT NULL
                OR A.pelatihan IS NOT NULL
                OR A.pameran IS NOT NULL
                OR A.puspita IS NOT NULL
            )
            AND A.deleted_at IS NULL
            AND b.nik IS NULL
            AND A.nik='$nik'");
        // dd($data);
        return $data;
    }

    public static function update_inter_selain_peken($nik)
    {
        // dd($nik, 'test param');
        $data = DB::table('t_pelaku_usaha_disdag_181022')
            ->where('nik', $nik)
            ->update(['peken_tokel' => null]);
        return $data;
    }

    public static function inter_hanya_peken($nik)
    {

        // $data = DB::select("SELECT A
        //     .id_t_pelaku_usaha_disdag_181022,
        //     A.nik,
        //     A.padat_karya,
        //     A.swk,
        //     A.industri_rumahan AS ir,
        //     A.skg,
        //     A.pasar,
        //     A.peken_tokel,
        //     A.rumah_kreatif AS rk,
        //     A.kur,
        //     A.csr,
        //     A.pelatihan,
        //     A.pameran,
        //     A.puspita
        // FROM
        //     t_pelaku_usaha_disdag_181022
        //     A LEFT JOIN peken b ON A.nik = b.nik
        // WHERE
        //     (A.peken_tokel = '1' or A.peken_tokel = '2')
        //     AND (
        //         ( A.swk != 1 OR A.swk IS NULL )
        //         AND ( A.industri_rumahan != 1 OR A.industri_rumahan IS NULL )
        //         AND A.industri_rumahan != 2
        //         AND ( A.skg != 1 OR A.skg IS NULL )
        //         AND ( A.pasar != 1 OR A.pasar IS NULL )
        //         AND ( A.padat_karya !=1 OR A.padat_karya IS NULL )
        //         AND ( A.rumah_kreatif != 1 OR A.rumah_kreatif IS NULL )
        //         AND ( A.kur != 1 OR A.kur IS NULL )
        //         AND ( A.csr != 1 OR A.csr IS NULL )
        //         AND ( A.pelatihan != 1 OR A.pelatihan IS NULL )
        //         AND ( A.pameran != 1 OR A.pameran IS NULL )
        //         AND ( A.puspita != 1 OR A.puspita IS NULL )
        //     )
        //     AND A.nik = '$nik'
        //     AND b.nik IS NULL
        // ");

        $data = DB::select("SELECT A
            .id_t_pelaku_usaha_disdag_181022,
            A.nik,
            A.padat_karya,
            A.swk,
            A.skg,
            A.pasar,
            A.peken_tokel,
            A.rumah_kreatif AS rk,
            A.kur,
            A.csr,
            A.pelatihan,
            A.pameran,
            A.puspita,
            A.deleted_at
        FROM
            t_pelaku_usaha_disdag_181022
            A LEFT JOIN peken b ON A.nik = b.nik
        WHERE
            ( A.peken_tokel = '1' OR A.peken_tokel = '2' )
            AND
                A.swk IS NULL
                AND A.skg IS NULL
                AND A.pasar IS NULL
                AND A.padat_karya IS NULL
                AND A.rumah_kreatif IS NULL
                AND A.kur IS NULL
                AND A.csr IS NULL
                AND A.pelatihan IS NULL
                AND A.pameran IS NULL
                AND A.puspita IS NULL
                AND b.nik IS NULL
                AND A.nik = '$nik'
                ");
        return $data;
    }

    public static function update_inter_hanya_peken($nik)
    {
        $data = DB::table('t_pelaku_usaha_disdag_181022')
            ->where('nik', $nik)
            ->update(['deleted_at' => now()]);

        return $data;
    }

    public static function nikPeken_diumkm($nik)
    {
        // dd($nik);
        // $data = DB::table('peken as A')
        //     ->select('B.nik as nik_pelaku_usaha', 'A.*')
        //     ->leftJoin('t_pelaku_usaha_disdag_181022 as B', 'A.nik', 'B.nik')
        //     ->where('A.nik', $nik)
        //     ->whereNull('B.deleted_at')
        //     // ->limit(10)
        //     ->get();
        // dd($data);s
        $data = DB::select("SELECT
        b.nik as nik_pelaku_usaha, a.*
        FROM peken a INNER JOIN t_pelaku_usaha_disdag_181022 b ON a.nik=b.nik
        WHERE a.nik='$nik'
        -- AND b.deleted_at IS NULL
        ");
        return $data;
    }

    public static function update_nik_pelaku_null($data)
    {
        // dd($data[0]->nik, 'ini data param');
        // dd($data->nik);
        // $raw_data =
        //     [

        //         'peken_tokel' => $data->peken_tokel,
        //         'nama_pelaku_usaha' => $data->nama_pelaku_usaha,
        //         'alamat_ktp' => $data->alamat_ktp,
        //         'tempat_lahir' => $data->tempat_lahir,
        //         'tanggal_lahir' => $data->tanggal_lahir,
        //         'jenis_kelamin' => $data->jenis_kelamin,
        //         'nama_usaha' => $data->nama_usaha,
        //         'alamat_usaha' => $data->alamat_usaha,
        //         'kecamatan_usaha' => $data->kecamatan_usaha_umkm,
        //         'kelurahan_usaha' => $data->kelurahan_usaha_umkm,
        //         'no_telp' => $data->no_telp,
        //         'sertifikat_merk' => $data->sertifikat_merk,
        //         'sertifikat_halal' => $data->sertifikat_halal,
        //         'pirt' => $data->pirt,
        //         'provinsi_pemilik' => 5,
        //         'kabupaten_pemilik' => 8,
        //         'kecamatan_pemilik' => $data->kecamatan_ktp,
        //         'kelurahan_pemilik' => $data->kelurahan_ktp,
        //         'alamat_domisili' => $data->alamat_ktp,
        //         'kecamatan_domisili' => $data->kecamatan_ktp,
        //         'kelurahan_domisili' => $data->kelurahan_ktp,
        //         'is_surabaya' => 1
        //     ];
        // dd($raw_data, 'ini array raw data');
        // $data_peken = ModelsTPelakuUsahaDisdag131022Model::where('nik',$raw_data[])

        $data = DB::table('t_pelaku_usaha_disdag_181022')
            ->where('nik', $data[0]->nik)
            ->update(array(
                'peken_tokel' => $data[0]->peken_tokel,
                'nama_pelaku_usaha' => $data[0]->nama_pelaku_usaha,
                'alamat_ktp' => $data[0]->alamat_ktp,
                'tempat_lahir' => $data[0]->tempat_lahir,
                'tanggal_lahir' => $data[0]->tanggal_lahir,
                'jenis_kelamin' => $data[0]->jenis_kelamin,
                'nama_usaha' => $data[0]->nama_usaha,
                'alamat_usaha' => $data[0]->alamat_usaha,
                'kecamatan_usaha' => $data[0]->kecamatan_usaha_umkm,
                'kelurahan_usaha' => $data[0]->kelurahan_usaha_umkm,
                'no_telp' => $data[0]->no_telp,
                'sertifikat_merk' => $data[0]->sertifikat_merk,
                'sertifikat_halal' => $data[0]->sertifikat_halal,
                'pirt' => $data[0]->pirt,
                'provinsi_pemilik' => 5,
                'kabupaten_pemilik' => 8,
                'kecamatan_pemilik' => $data[0]->kecamatan_ktp,
                'kelurahan_pemilik' => $data[0]->kelurahan_ktp,
                'alamat_domisili' => $data[0]->alamat_ktp,
                'kecamatan_domisili' => $data[0]->kecamatan_ktp,
                'kelurahan_domisili' => $data[0]->kelurahan_ktp,
                'is_surabaya' => 1,
                'deleted_at' => null,
                'updated_at' => now()
            ));
        // dd($raw_data);
        // $tes = $data->update($raw_data);
        // dd($tes);
        // foreach ($data as $key => $value) {
        //     # code...
        //     nik_pelaku_usaha
        //     ukm_id
        //     nik
        //     nama_pelaku_usaha
        //     alamat_ktp
        //     kelurahan_pemilik
        //     tempat_lahir
        //     tanggal_lahir
        //     jenis_kelamin
        //     nama_usaha
        //     alamat_usaha
        //     kecamatan_usaha
        //     kelurahan_usaha
        //     no_telp
        //     sertifikat_merk
        //     sertifikat_halal
        //     pirt
        //     peken_tokel
        //     kecamatan_ktp
        //     kelurahan_ktp
        //     kecamatan_usaha_umkm
        //     kelurahan_usaha_umkm
        // }

        // $data = DB::update("UPDATE t_pelaku_usaha_disdag_181022
        //             SET peken_tokel=peken_tokel,
        //                 nama_pelaku_usaha=nama_pelaku_usaha,
        //                 alamat_ktp=alamat_ktp,
        //                 tempat_lahir=tempat_lahir,
        //                 tanggal_lahir=tanggal_lahir,
        //                 jenis_kelamin=jenis_kelamin,
        //                 nama_usaha=nama_usaha,
        //                 alamat_usaha=alamat_usaha,
        //                 kecamatan_usaha=kecamatan_usaha_umkm,
        //                 kelurahan_usaha=kelurahan_usaha_umkm,
        //                 no_telp=no_telp,
        //                 sertifikat_merk=sertifikat_merk,
        //                 sertifikat_halal=sertifikat_halal,
        //                 pirt=pirt,
        //                 provinsi_pemilik='35',
        //                 kabupaten_pemilik='78',
        //                 kecamatan_pemilik=kecamatan_ktp,
        //                 kelurahan_pemilik=kelurahan_ktp,
        //                 alamat_domisili=alamat_ktp,
        //                 kecamatan_domisili=kecamatan_ktp,
        //                 kelurahan_domisili=kelurahan_ktp,
        //                 is_surabaya='1'
        //     WHERE nik='$nik'");
        // dd($data->tosql());
        // dd($data, 'tes update_nik_pelaku_null');
        return $data;
    }

    public static function insert_peken($nik)
    {
        $data = DB::insert("INSERT INTO t_pelaku_usaha_disdag_181022 (
                    nik,
                    peken_tokel,
                    nama_pelaku_usaha,
                    alamat_ktp,
                    tempat_lahir,
                    tanggal_lahir,
                    jenis_kelamin,
                    nama_usaha,
                    alamat_usaha,
                    kecamatan_usaha,
                    kelurahan_usaha,
                    no_telp,
                    sertifikat_merk,
                    sertifikat_halal,
                    pirt,
                    provinsi_pemilik,
                    kabupaten_pemilik,
                    kecamatan_pemilik,
                    kelurahan_pemilik,
                    alamat_domisili,
                    kecamatan_domisili,
                    kelurahan_domisili,
                    is_surabaya,
                    created_at,
                    tgl_insert_peken
                    ) (
                SELECT
                    x.nik,
                    x.peken_tokel,
                    x.nama_pelaku_usaha,
                    x.alamat_ktp,
                    x.tempat_lahir,
                    x.tanggal_lahir,
                    x.jenis_kelamin,
                    x.nama_usaha,
                    x.alamat_usaha,
                    x.kecamatan_usaha_umkm AS kecamatan_usaha,
                    x.kelurahan_usaha_umkm AS kelurahan_usaha,
                    x.no_telp,
                    x.sertifikat_merk,
                    x.sertifikat_halal,
                    x.pirt,
                    '35',
                    '78',
                    x.kecamatan_ktp AS kecamatan_pemilik,
                    x.kelurahan_ktp AS kelurahan_pemilik,
                    x.alamat_ktp AS alamat_domisili,
                    x.kecamatan_ktp AS kecamatan_domisili,
                    x.kelurahan_ktp AS kelurahan_domisili,
                    '1',
                    NOW(),
                    NOW()
                from peken x
                where x.nik='$nik')");
        // $data = DB::table('t_pelaku_usaha_disdag_181022')
        //     ->insert([
        //         'nik',
        //         'peken_tokel',
        //         'nama_pelaku_usaha',
        //         'alamat_ktp',
        //         'tempat_lahir',
        //         'tanggal_lahir',
        //         'jenis_kelamin',
        //         'nama_usaha',
        //         'alamat_usaha',
        //         'kecamatan_usaha',
        //         'kelurahan_usaha',
        //         'no_telp',
        //         'sertifikat_merk',
        //         'sertifikat_halal',
        //         'pirt',
        //         'provinsi_pemilik',
        //         'kabupaten_pemilik',
        //         'kecamatan_pemilik',
        //         'kelurahan_pemilik',
        //         'alamat_domisili',
        //         'kecamatan_domisili',
        //         'kelurahan_domisili',
        //         'is_surabaya'
        //     ], DB::table('peken as X')
        //         ->select(
        //             'X.nik',
        //             'X.peken_tokel',
        //             'X.nama_pelaku_usaha',
        //             'X.alamat_ktp',
        //             'X.tempat_lahir',
        //             'X.tanggal_lahir',
        //             'X.jenis_kelamin',
        //             'X.nama_usaha',
        //             'X.alamat_usaha',
        //             'X.kecamatan_usaha_umkm AS kecamatan_usaha',
        //             'X.kelurahan_usaha_umkm AS kelurahan_usaha',
        //             'X.no_telp',
        //             'X.sertifikat_merk',
        //             'X.sertifikat_halal',
        //             'X.pirt',
        //             35,
        //             78,
        //             'X.kecamatan_ktp AS kecamatan_pemilik',
        //             'X.kelurahan_ktp AS kelurahan_pemilik',
        //             'X.alamat_ktp AS alamat_domisili',
        //             'X.kecamatan_ktp AS kecamatan_domisili',
        //             'X.kelurahan_ktp AS kelurahan_domisili',
        //             1
        //         )->where('X.nik', $nik));
        // dd($data, 'tes insert peken');

        return $data;
    }

    /* query untuk data tabel scope sebaran kategori usaha*/
    public static function scopeCountSebaranKategoriUsaha($query)
    {
        $data = \DB::select(
            "SELECT
            kec.nm_kec as nm_kec,
            tpu.kecamatan_pemilik as kecamatan_pemilik,
            COUNT(CASE WHEN tpu.kategori_jenis_produk='FASHION' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_fashion,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='TOKO' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_toko,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='MAKANAN DAN MINUMAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_mamin,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='KERAJINAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_kerajinan,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pertanian,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='JASA' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_jasa
            FROM
            t_pelaku_usaha_disdag_181022 tpu
	        LEFT JOIN m_setup_kec kec ON kec.id_m_setup_kec =  tpu.kecamatan_pemilik
            WHERE
                kec.id_m_setup_kab = 78
	            AND kec.id_m_setup_prop = 35
                AND tpu.deleted_at IS NULL
             GROUP BY
	            kec.nm_kec, tpu.kecamatan_pemilik"
        );

        return $data;
    }

    /* query untuk menampilkan grafik sebaran kategori usaha*/
    public static function scopeGrafikSebaranKategoriUsaha()
    {
        $data = DB::select(
            "SELECT
            kec.nm_kec,
            -- 0 AS fashion,
            COUNT(CASE WHEN tpu.kategori_jenis_produk='FASHION' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_fashion,
            -- 0 AS toko,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='TOKO' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_toko,
            -- 0 AS mamin,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='MAKANAN DAN MINUMAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_mamin,
            -- 0 AS kerajinan,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='KERAJINAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_kerajinan,
            -- 0 AS pertanian,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_pertanian,
            -- 0 AS jasa,
            COUNT ( CASE WHEN tpu.kategori_jenis_produk='JASA' THEN tpu.id_t_pelaku_usaha_disdag_181022 END ) AS jumlah_jasa
            FROM
            t_pelaku_usaha_disdag_181022 tpu
	        LEFT JOIN m_setup_kec kec ON kec.id_m_setup_kec =  tpu.kecamatan_pemilik
            WHERE
                kec.id_m_setup_kab = 78
	            AND kec.id_m_setup_prop = 35
                AND tpu.deleted_at IS NULL
             GROUP BY
	            kec.nm_kec"
        );

        return $data;
    }

    public function detailGamisKbli($kbli, $gamis)
    {
        // dd($kbli,$gamis);
        $query = '';
        if ($gamis == 'jiwa_kumulatif') {
            $query .= " AND kategori.id_m_kategori_kbli = '$kbli' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_kumulatif_warkin') {
            $query .= " AND pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_misek_warkin') {
            $query .= " AND pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_sub_warkin') {
            $query .= " AND pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_kumulatif_prakin') {
            $query .= " AND pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_misek_prakin') {
            $query .= " AND pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_sub_prakin') {
            $query .= " AND pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL ";
        } else if ($gamis == 'jiwa_kumulatif_lainnya') {
            $query .= " AND pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin IS NULL AND pelaku_usaha.is_pramiskin IS NULL AND pelaku_usaha.deleted_at IS NULL ";
        }
        $query = DB::SELECT("SELECT
                kategori.id_m_kategori_kbli,
                kategori.nama_kategori_kbli AS nm_kategori,
                pelaku_usaha.jiwa as nik,
                pelaku_usaha.nama_pelaku_usaha as nama_pelaku_usaha,
                pelaku_usaha.nama_usaha as nama_usaha,
                pelaku_usaha.alamat_ktp as alamat,
                pelaku_usaha.nm_kec as kecamatan,
                pelaku_usaha.nm_kel as kelurahan,
                pelaku_usaha.rw_ktp as rw,
                pelaku_usaha.rt_ktp as rt,
                pelaku_usaha.nib as nib
                FROM
                m_klasifikasi_kbli AS klasifikasi
                LEFT JOIN m_kategori_kbli AS kategori ON klasifikasi.id_m_kategori_kbli = kategori.id_m_kategori_kbli
                LEFT JOIN (
                SELECT A
                    .nik AS jiwa,
                    A.no_kk_ktp AS kk,
                    A.id_t_pelaku_usaha_disdag_181022 AS id_pelaku_usaha,
                    A.nama_pelaku_usaha,
                    A.nama_usaha,
                    A.alamat_ktp,
                    b.nm_kec,
                    C.nm_kel,
                    A.rw_ktp,
                    A.rt_ktp,
                    A.nib,
                    A.is_warga_miskin,
                    A.is_miskin_ekstrim,
                    A.is_pramiskin,
                    A.deleted_at,
                    UNNEST (
                        string_to_array(
                            substr( kode_kbli_2, 2, ( CASE WHEN LENGTH ( kode_kbli_2 ) - 2 >= 0 THEN LENGTH ( kode_kbli_2 ) - 2 ELSE 0 END ) ),
                            ','
                        )
                    ) AS kode_kbli
                FROM
                    t_pelaku_usaha_disdag_181022
                    A LEFT JOIN m_setup_kec b ON A.kecamatan_pemilik = b.id_m_setup_kec
                    AND b.id_m_setup_prop = '35'
                    AND b.id_m_setup_kab = '78'
                    LEFT JOIN m_setup_kel C ON A.kelurahan_pemilik = C.id_m_setup_kel
                    AND b.id_m_setup_kec = C.id_m_setup_kec
                    AND C.id_m_setup_prop = '35'
                    AND C.id_m_setup_kab = '78'
                ) AS pelaku_usaha ON pelaku_usaha.kode_kbli = klasifikasi.kode_klasifikasi_kbli
            WHERE
                kategori.id_m_kategori_kbli = '$kbli'
                $query
                AND pelaku_usaha.jiwa IS NOT NULL
            GROUP BY
                kategori.id_m_kategori_kbli,
                kategori.nama_kategori_kbli,
                pelaku_usaha.id_pelaku_usaha,
                pelaku_usaha.kode_kbli,
                pelaku_usaha.deleted_at,
                pelaku_usaha.jiwa,
                pelaku_usaha.nama_pelaku_usaha,
                pelaku_usaha.nama_usaha,
                pelaku_usaha.alamat_ktp,
                pelaku_usaha.nm_kec,
                pelaku_usaha.nm_kel,
                pelaku_usaha.rw_ktp,
                pelaku_usaha.rt_ktp,
                pelaku_usaha.nib,
                pelaku_usaha.is_warga_miskin,
                pelaku_usaha.is_miskin_ekstrim,
                pelaku_usaha.is_pramiskin
            ORDER BY
                pelaku_usaha.nama_pelaku_usaha ASC");

        return $query;
    }
    public function get_data_gamis_intervensi($intervensi)
    {
        foreach ($intervensi as $key => $value) {
            if ($value != "Belum Intervensi") {
                // dd('masuk');
                $value = strtolower(str_replace(" ", "_", $value));

                $data['intervensi_kumulatif'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1'");

                $data['intervensi_miskin_kumulatif'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1' and is_warga_miskin='1'");

                $data['intervensi_miskin_ekstrim'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1' and is_warga_miskin='1' and is_miskin_ekstrim='1'");

                $data['intervensi_miskin'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1' and is_warga_miskin='1' and is_miskin_ekstrim is null");

                $data['intervensi_pramiskin_kumulatif'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1' and is_pramiskin='1'");

                $data['intervensi_pramiskin'][$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and " . $value . "='1' and is_pramiskin='1' and   is_miskin_ekstrim is null");

                $data['intervensi_wargalainnya'][$key] =  DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and " . $value . "='1'");
            }
        }
        $data_blm_intervensi_kumulatif[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null
                and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null");

        $data_blm_intervensi_miskin_kumulatif[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null 	and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null
                and is_warga_miskin='1'");

        $data_blm_intervensi_miskin_ekstrim[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null 	and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null
                and is_warga_miskin='1' and is_miskin_ekstrim='1'");

        $data_blm_intervensi_miskin[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null 	and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null
                and is_warga_miskin='1' and is_miskin_ekstrim is null");

        $data_blm_intervensi_pramiskin_kumulatif[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null
                and is_pramiskin='1'");

        $data_blm_intervensi_pramiskin[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where deleted_at is null and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null
                and is_pramiskin='1' and is_miskin_ekstrim is null");

        $data_blm_intervensi_wargalainnya[$key] = DB::select("SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk from t_pelaku_usaha_disdag_181022 where is_miskin_ekstrim is null and is_warga_miskin is null and is_pramiskin is null and deleted_at is null and swk is null
                and skg is null
                and pasar is null
                and peken_tokel is null
                and pelatihan is null
                and pameran is null
                and bpum is null
                and industri_rumahan is null
                and rumah_kreatif is null
                and padat_karya is null
                and kur is null
                and csr is null
                and puspita is null
                and oss is null");


        $data['intervensi_kumulatif'] = array_merge($data['intervensi_kumulatif'], $data_blm_intervensi_kumulatif);
        $data['intervensi_miskin_kumulatif'] = array_merge($data['intervensi_miskin_kumulatif'], $data_blm_intervensi_miskin_kumulatif);
        $data['intervensi_miskin_ekstrim'] = array_merge($data['intervensi_miskin_ekstrim'], $data_blm_intervensi_miskin_ekstrim);
        $data['intervensi_miskin'] = array_merge($data['intervensi_miskin'], $data_blm_intervensi_miskin);
        $data['intervensi_pramiskin_kumulatif'] = array_merge($data['intervensi_pramiskin_kumulatif'], $data_blm_intervensi_pramiskin_kumulatif);
        $data['intervensi_pramiskin'] = array_merge($data['intervensi_pramiskin'], $data_blm_intervensi_pramiskin);
        $data['intervensi_wargalainnya'] = array_merge($data['intervensi_wargalainnya'], $data_blm_intervensi_wargalainnya);
        return $data;
    }
    public static function get_data_warga_miskin()
    {
        /* query builder for get data is_warga_miskin berdasarkan m_klasifikasi_kbli on m_kategori_kbli */
        $query = DB::table('m_klasifikasi_kbli as klasifikasi')
            ->leftJoin('m_kategori_kbli as kategori', 'klasifikasi.id_m_kategori_kbli', '=', 'kategori.id_m_kategori_kbli')
            /* subquery select unnest contains leftJoin */
            ->leftJoin(
                DB::raw("(SELECT
                nik as jiwa,
                no_kk_ktp as kk,
                is_warga_miskin,
                is_miskin_ekstrim,
                is_pramiskin,
                deleted_at,
                unnest(string_to_array(substr( kode_kbli_2, 2,  ( case when LENGTH ( kode_kbli_2 ) - 2 >= 0 then LENGTH  (kode_kbli_2 )-2 else 0 end ) ), ',' ) ) AS kode_kbli
            FROM
                t_pelaku_usaha_disdag_181022) AS pelaku_usaha"),

                function ($join) {
                    /* convert pelaku_usaha.kode_kbli to integer */
                    // $join->on(DB::raw("cast(pelaku_usaha.kode_kbli as integer)"), '=', 'klasifikasi.id_m_klasifikasi_kbli');
                    $join->on('pelaku_usaha.kode_kbli', '=', 'klasifikasi.kode_klasifikasi_kbli');
                }
            )
            /* function case when */


            ->select(
                'kategori.id_m_kategori_kbli',
                'kategori.nama_kategori_kbli as nm_kategori',

                /* tag general case by id_m_kategori_kbli */
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_industri_pengolahan"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_industri_pengolahan"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '2' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_perdagangan"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '2' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_perdagangan"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '3' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_akomodasi_mamin"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '3' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_akomodasi_mamin"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '4' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_aktivitas_jasa"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '4' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_aktivitas_jasa"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '5' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_pertanian_kehutanan_perikanan"),
                DB::raw("count(CASE WHEN kategori.id_m_kategori_kbli = '5' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_pertanian_kehutanan_perikanan"),

                /* tag warga miskin */
                DB::raw("count(CASE WHEN pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_warkin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_warkin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_misek_warkin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_misek_warkin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_sub_warkin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_sub_warkin"),

                /* tag pramiskin */
                DB::raw("count(CASE WHEN pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_prakin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_prakin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_misek_prakin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.is_miskin_ekstrim = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_misek_prakin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_sub_prakin"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_pramiskin = '1' AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_sub_prakin"),

                /* tag warga lain nya */
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin IS NULL AND pelaku_usaha.is_pramiskin IS NULL AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.jiwa END) as jiwa_kumulatif_lainnya"),
                DB::raw("count(CASE WHEN pelaku_usaha.is_miskin_ekstrim IS NULL AND pelaku_usaha.is_warga_miskin IS NULL AND pelaku_usaha.is_pramiskin IS NULL AND pelaku_usaha.deleted_at IS NULL THEN pelaku_usaha.kk END) as kk_kumulatif_lainnya"),
            )
            ->groupBy('kategori.id_m_kategori_kbli', 'kategori.nama_kategori_kbli')
            ->get();

        return $query;
    }

    public static function ModaldetailIntervensi($intervensi, $kec)
    {
        // $query = 'swk';

        $where = "";
        if ($intervensi == 'swk_aja') {
            $where .= " WHERE a.deleted_at IS NULL AND a.swk in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'skg') {
            $where .= " WHERE a.deleted_at IS NULL AND a.skg in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'pasar') {
            $where .= " WHERE a.deleted_at IS NULL AND a.pasar in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'peken_tokel') {
            $where .= " WHERE a.deleted_at IS NULL AND a.peken_tokel in ('1','2') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'pelatihan') {
            $where .= " WHERE a.deleted_at IS NULL AND a.pelatihan in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'pameran') {
            $where .= " WHERE a.deleted_at IS NULL AND a.pameran in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'bpum') {
            $where .= " WHERE a.deleted_at IS NULL AND a.bpum in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'rumah_kreatif') {
            $where .= " WHERE a.deleted_at IS NULL AND a.rumah_kreatif in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'padat_karya') {
            $where .= " WHERE a.deleted_at IS NULL AND a.padat_karya in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'kur') {
            $where .= " WHERE a.deleted_at IS NULL AND a.kur in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'csr') {
            $where .= " WHERE a.deleted_at IS NULL AND a.csr in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'puspita') {
            $where .= " WHERE a.deleted_at IS NULL AND a.puspita in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        } else if ($intervensi == 'oss') {
            $where .= " WHERE a.deleted_at IS NULL AND a.oss in ('1') and b.id_m_setup_kab = 78 and b.id_m_setup_prop = 35 and a.kecamatan_pemilik = " . $kec . "";
        }
        // dd($intervensi, $kec, $where);

        // dd($where);

        $data = DB::select("
        SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
        FROM
            t_pelaku_usaha_disdag_181022 a
            left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
            left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
        $where
        ");

        return $data;
    }

    public static function um()
    {
        $um_aktif = DB::select(
            "SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk
            from t_pelaku_usaha_disdag_181022
            where status_keaktifan='AKTIF'
            and deleted_at is null"
        );

        $um_tidak_aktif = DB::select(
            "SELECT count(nik) as jiwa, count(distinct (no_kk_ktp)) as kk
            from t_pelaku_usaha_disdag_181022
            where status_keaktifan in ('TIDAK_AKTIF', 'LIBUR')
            and deleted_at is null"
        );

        $um = [
            'um_aktif' => $um_aktif,
            'um_tidak_aktif' => $um_tidak_aktif
        ];

        // dd($um);
        return $um;
    }
}
