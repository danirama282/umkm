<x-templates.default>
    <x-slot name="title">Data KBLI</x-slot>
    <input type="hidden" name="id_disdag" id="id_disdag" value="{{ $id_disdag }}">
    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Detail Data KBLI</h5>
                    </div>
                </div>
                <div class="card-body position-relative">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="namaUsaha">Nama Usaha</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8">
                                    <input class="form-control-plaintext outline-none" id="namaUsaha"
                                        type="text" readonly=""
                                        value="{{ $result_disdag->nama_usaha ?? '-' }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="nik">NIK</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8">
                                    <input class="form-control-plaintext outline-none" id="nik"
                                        type="text" readonly=""
                                        value="{{ $result_disdag->nik ?? '-' }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="statusMBR">Status MBR</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none" id="statusMBR"
                                        type="text" readonly=""
                                        value="{{ $result_disdag->status_mbr ?? '-' }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="namaPemilik">Nama Pemilik</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="namaPemilik" type="text" readonly=""
                                        value="{{ $result_disdag->nama_pelaku_usaha }}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="alamatPemilik">Alamat Pemilik</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="alamatPemilik" type="text" readonly=""
                                        value="{{ $result_disdag->alamat_pemilik }}, RT {{ $result_disdag->rt_pemilik }}, RW {{ $result_disdag->rw_pemilik }}, Kelurahan  {{ $result_disdag->kelurahan }}, Kecamatan  {{ $result_disdag->kecamatan }}" />
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('pelaku_usaha.index') }}"
                                class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Daftar KBLI</h5>
                    </div>
                </div>

                <div class="card-body ">
                    <div class="d-flex flex-column align-content-md-end align-items-end">
                        <a href="{{ route('klasifikasi_kbli.edit', $id_disdag) }}"
                            class="btn btn-primary">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Tambah Data KBLI
                        </a>
                    </div>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10%">No</th>
                                    <th class="text-center" width="20%">Kode Kategori</th>
                                    <th class="text-center" width="20%">Nama Kategori</th>
                                    <th class="text-center" scope="col">Kode Klasifikasi</th>
                                    <th class="text-center" scope="col">Nama Klasifikasi</th>
                                    <th class="text-center" scope="col">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>

    <script>
        $(function() {
            /* get value from attr name="id_disdag" */
            var id_disdag = $('#id_disdag').val();
            var url = "{{ url('klasifikasi_kbli') }}/" + id_disdag;
            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'kode_kategori_kbli',
                        name: 'kode_kategori_kbli'
                    },
                    {
                        data: 'nama_kategori_kbli',
                        name: 'nama_kategori_kbli'
                    },
                    {
                        data: 'kode_klasifikasi_kbli',
                        name: 'kode_klasifikasi_kbli'
                    },
                    {
                        data: 'nama_klasifikasi_kbli',
                        name: 'nama_klasifikasi_kbli'
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan  _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Pencarian",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });

        /* #dataTable on click #destroy_kbli */
        $(document).on('click', '#destroy_kbli', function(e) {
            e.preventDefault();
            var id_disdag = $(this).attr('data-1');
            var kode_kbli = $(this).attr('data-2');
            /* swal2 */
            Swal.fire({
                title: 'Apakah anda yakin?',
                text: "Data yang dihapus tidak dapat dikembalikan!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, hapus data!'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        /* url berisi route klasifikasi_kbli.destroyKodeKbli */
                        url: "{{ route('klasifikasi_kbli.destroyKodeKbli') }}",
                        type: 'PUT',
                        data: {
                            "_token": "{{ csrf_token() }}",
                            "id_disdag": id_disdag,
                            "kode_kbli": kode_kbli,
                        },
                        success: function(data) {
                            Swal.fire(
                                'Terhapus!',
                                'Data berhasil dihapus.',
                                'success'
                            )
                            $('#dataTable').DataTable().ajax.reload();
                        }
                    });
                }
            })
        });
    </script>
    @endpush

</x-templates.default>
