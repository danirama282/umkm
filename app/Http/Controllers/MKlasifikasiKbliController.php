<?php

namespace App\Http\Controllers;

use App\Models\MKategoriKbliModel;
use App\Models\MKlasifikasiKbliModel;
use App\Models\TPelakuUsahaDisdag131022Model;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MKlasifikasiKbliController extends Controller
{
    //
    public function index(Request $request, $id)
    {
        $id_disdag = Crypt::decryptString($id);

        $raw_detail_disdag = TPelakuUsahaDisdag131022Model::getDetailPelakuUsaha($id_disdag);

        $result_disdag = $raw_detail_disdag[0];

        if ($request->ajax()) {
            $raw_kode_kbli = TPelakuUsahaDisdag131022Model::select('kode_kbli_2')->where('id_t_pelaku_usaha_disdag_181022', $id_disdag)->first();

            /* if $raw_kode_kbli = null then set null */
            if ($raw_kode_kbli->kode_kbli_2 != null) {
                /* substr $raw_kode_kbli and explode */
                $kode_kbli = substr($raw_kode_kbli->kode_kbli_2, 1, -1);
                $kode_kbli = explode(',', $kode_kbli);

                for ($i = 0; $i < count($kode_kbli); $i++) {
                    /* get data from relasi_kategori_kbli */
                    $result[$i]['result'] = MKlasifikasiKbliModel::where('kode_klasifikasi_kbli', $kode_kbli[$i])->with('relasi_kategori_kbli')->get();
                }
                // dd($result);

                /* looping for untuk ditampilkan ke dataTables */
                for ($i = 0; $i < count($result); $i++) {
                    $data[$i] = [
                        'id_m_klasifikasi_kbli' => $result[$i]['result'][0]->id_m_klasifikasi_kbli,
                        'kode_kategori_kbli' => $result[$i]['result'][0]->relasi_kategori_kbli->kode_kategori_kbli,
                        'nama_kategori_kbli' => $result[$i]['result'][0]->relasi_kategori_kbli->nama_kategori_kbli,
                        'kode_klasifikasi_kbli' => $result[$i]['result'][0]->kode_klasifikasi_kbli,
                        'nama_klasifikasi_kbli' => $result[$i]['result'][0]->nama_klasifikasi_kbli,
                    ];
                    // dd($data[$i]);
                }

                return datatables()
                    ->of($data)
                    ->addIndexColumn()
                    ->addColumn('kode_kategori_kbli', function ($row) {
                        return $row['kode_kategori_kbli'];
                    })
                    ->addColumn('nama_kategori_kbli', function ($row) {
                        return $row['nama_kategori_kbli'];
                    })
                    ->addColumn('action', function ($row) use ($id_disdag) {
                        /* return dt-action */
                        return view('klasifikasi_kbli.dt-action', [
                            'id_disdag' => $id_disdag,
                            'kode_klasifikasi_kbli' => $row['kode_klasifikasi_kbli'],
                        ]);
                    })
                    ->rawColumns(['action'])
                    ->make(true);
            } else {
                return datatables()
                    ->of([])
                    ->addIndexColumn()
                    ->addColumn('kode_kategori_kbli', function ($row) {
                        return null;
                    })
                    ->addColumn('nama_kategori_kbli', function ($row) {
                        return null;
                    })
                    ->make(true);
            }
        }

        /* encrypt id_disdag */
        $id_disdag = Crypt::encryptString($id_disdag);

        return view('klasifikasi_kbli.index', compact('result_disdag', 'id_disdag'));
    }

    public function edit($id_disdag)
    {
        $id_disdag = Crypt::decryptString($id_disdag);

        /* get data all kategori kbli */
        $data['kategori_kbli'] = MKategoriKbliModel::all();

        /* encrypt $id_disdag again */
        $id_disdag = Crypt::encryptString($id_disdag);

        return view('klasifikasi_kbli.edit', compact('id_disdag', 'data'));
    }

    public function update(Request $request, $id_disdag)
    {
        /* decrypt */
        $id_disdag = Crypt::decryptString($id_disdag);

        /* get $raw_kode_kbli form find $id_disdag */
        $raw_kode_kbli = TPelakuUsahaDisdag131022Model::select('kode_kbli_2')->where('id_t_pelaku_usaha_disdag_181022', $id_disdag)->first();

        $validator = Validator::make($request->all(), [
            'id_m_kategori_kbli' => 'required',
            'id_m_klasifikasi_kbli' => 'required',
        ]);

        $attrNames = [
            'id_m_kategori_kbli' => 'Kategori KBLI',
            'id_m_klasifikasi_kbli' => 'Klasifikasi KBLI',
        ];

        $validator->setAttributeNames($attrNames);

        if ($validator->passes()) {
            /* get klasifikasi_kbli and kategori_kbli from request */
            $id_m_kategori_kbli = $request->id_m_kategori_kbli;
            $id_m_klasifikasi_kbli = $request->id_m_klasifikasi_kbli;

            /* get data klasifikasi kbli */
            $raw_klasifikasi_kbli = MKlasifikasiKbliModel::select('kode_klasifikasi_kbli')->where('id_m_klasifikasi_kbli', $id_m_klasifikasi_kbli)->where('id_m_kategori_kbli', $id_m_kategori_kbli)->first();

            $raw_klasifikasi_kbli = $raw_klasifikasi_kbli->kode_klasifikasi_kbli;

            if ($raw_kode_kbli->kode_kbli_2 == null) {
                $kode_kbli = $raw_klasifikasi_kbli;
            } else {
                /* implode $raw_kode_kbli */
                $kode_kbli = substr($raw_kode_kbli->kode_kbli_2, 1, -1);
                $kode_kbli = explode(',', $kode_kbli);

                /* array push raw_klasifikasi_kbli to kode_kbli */
                array_push($kode_kbli, $raw_klasifikasi_kbli);
                /* implode kode_kbli */
                $kode_kbli = implode(',', $kode_kbli);
            }

            /* add '[' and ']' to $kode_kbli */
            $kode_kbli = '[' . $kode_kbli . ']';

            /* update kode_kbli_2 to TPelakuUsahaDisdag131022Model where $id_disdag */
            $update_kbli = TPelakuUsahaDisdag131022Model::where('id_t_pelaku_usaha_disdag_181022', $id_disdag);
            $update_kbli->update(['kode_kbli_2' => $kode_kbli, 'updated_at' => Carbon::now(), 'updated_by' => Auth::user()['id']]);

            return response()->json([
                'status' => true,
                'message' => 'Klasifikasi KBLI berhasil ditambahkan',
            ]);
            /* try {
            } catch (\Throwable $e) {
                return response()->json([
                    'status' => false,
                    'message' => 'Klasifikasi KBLI gagal ditambahkan',
                ]);
            } */
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error,
            ]);
        }
    }

    public function getDataKlasifikasiKbli(Request $request)
    {
        $id_m_kategori = $request->id;

        $klasifikasi_kbli = MKlasifikasiKbliModel::where('id_m_kategori_kbli', $id_m_kategori)->get();

        return response()->json([
            'status' => true,
            'data' => $klasifikasi_kbli
        ]);
    }

    public function destroyKodeKbli(Request $request)
    {
        $id_disdag = Crypt::decryptString($request->id_disdag);
        $request_kbli = $request->kode_kbli;

        /* get $raw_kode_kbli form find $id_disdag */
        $raw_kode_kbli = TPelakuUsahaDisdag131022Model::select('kode_kbli_2')->where('id_t_pelaku_usaha_disdag_181022', $id_disdag)->first();

        /* implode $raw_kode_kbli */
        $kode_kbli = substr($raw_kode_kbli->kode_kbli_2, 1, -1);
        $kode_kbli = explode(',', $kode_kbli);
        // dd($kode_kbli);

        /* merubah $request_kbli menjadi array */
        $request_kbli = explode(',', $request_kbli);
        // dd($request_kbli);
        /* menghapus kode kbli yang di request */
        $kode_kbli = array_diff($kode_kbli, $request_kbli);

        /* implode kode_kbli */
        $kode_kbli = implode(',', $kode_kbli);
        // dd($kode_kbli);

        if ($kode_kbli == "") {
            $kode_kbli = null;
        } else {
            /* add '[' and ']' to $kode_kbli */
            $kode_kbli = '[' . $kode_kbli . ']';
        }

        /* update kode_kbli_2 to TPelakuUsahaDisdag131022Model where $id_disdag */
        $update_kbli = TPelakuUsahaDisdag131022Model::where('id_t_pelaku_usaha_disdag_181022', $id_disdag);
        $update_kbli->update(['kode_kbli_2' => $kode_kbli, 'updated_at' => Carbon::now()]);

        return response()->json([
            'status' => true,
            'message' => 'Klasifikasi KBLI berhasil dihapus',
        ]);
    }
}
