<div class="row g-0 light mb-3">
    <div class="col-sm-6 col-lg-6 mb-2">
        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
            data-bs-target="#detailGamis" onclick="funDetailGamis('aktif','UM Aktif')">
            <div class="card text-white py-3" style="height: 100%;border: 6px solid #ac58ff!important; background-color: #8b17ff;">
                <div class="card-body d-flex align-items-center justify-content-center">
                    <div class="text-center">
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2 text-white text-uppercase fs-2">UM Aktif</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                {{$data['um']['um_aktif'][0]->jiwa}} Jiwa
                            </h3>
                            <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                {{$data['um']['um_aktif'][0]->kk}} KK
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
    <div class="col-sm-6 col-lg-6 mb-2 ps-lg-2">
        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
            data-bs-target="#detailGamis" onclick="funDetailGamis('tidak_aktif','UM Tidak Aktif')">
            <div class="card text-white py-3" style="height: 100%; border: 6px solid #ac58ff!important; background-color: #8b17ff;">
                <div class="card-body d-flex align-items-center justify-content-center" style="">
                    <div class="text-center">
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2 text-white text-uppercase fs-2">UM Tidak Aktif</h3>
                        </div>
                        <div class="card-title mb-0">
                            <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                {{$data['um']['um_tidak_aktif'][0]->jiwa}} Jiwa
                            </h3>
                            <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                {{$data['um']['um_tidak_aktif'][0]->kk}} KK
                            </h3>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>
</div>

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>

    function funDetailGamis(gamis,judul) {
        // console.log(gamis);
        $('#judul_modal').html('Tabel Detail '+ judul.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        }));

        $('#tabel_detailGamis').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{!! route('detailGamis') !!}",
                "type": "POST",
                "data": {
                    gamis: gamis
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nik',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_pelaku_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'alamat',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kecamatan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kelurahan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rw',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rt',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nib',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'aksi',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },

            ],
            "pageLength": 5,
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "language": {
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                "infoFiltered": "(disaring dari _MAX_ total data)",
                "lengthMenu": "Menampilkan _MENU_ data",
                "search": "Cari:",
                "zeroRecords": "Tidak ada data yang sesuai",
                /* Kostum pagination dengan element baru */
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            },
            "bDestroy": true
        });

    }
</script>
@endpush
