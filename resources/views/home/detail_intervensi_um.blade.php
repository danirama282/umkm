<div class="modal fade" id="detail_intervensi_um" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1 modal_judul">Detail</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="table_detail_intervensi_um" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    function detailIntervensi(inv, kec, judul, nama) {
            // console.log(inv, kec);
            let modal = '#detail_intervensi_um';
            $(modal).modal('show');

            $('#detail_intervensi_um .modal_judul').text('DETAIL INTERVENSI ' + judul + ' KECAMATAN ' + nama);
            $('#table_detail_intervensi_um').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{!! route('detailIntervensi') !!}",
                    "type": "POST",
                    "data": {
                        intervensi: inv,
                        kec: kec
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
                "bDestroy": true
            })
        }
</script>
@endpush