<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTBantuanUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_bantuan_usaha', function (Blueprint $table) {
            $table->bigIncrements('id');
            /* Membuat foreignid t_pelaku_usaha_id dari tabel t_pelaku_usaha */
            $table->unsignedBigInteger('t_pelaku_usaha_id');
            $table->foreign('t_pelaku_usaha_id')->references('id')->on('t_pelaku_usaha')->onUpdate('cascade')->onDelete('cascade');
            /* Membuat foreignid m_jenis_bantuan_id dari tabel m_jenis_bantuan */
            $table->unsignedBigInteger('m_jenis_bantuan_id');
            $table->foreign('m_jenis_bantuan_id')->references('id')->on('m_jenis_bantuan')->onUpdate('cascade')->onDelete('cascade');
            /* Membuat foreignid m_jenis_binaan_id dari tabel m_jenis_binaan */
            $table->unsignedBigInteger('m_jenis_binaan_id');
            $table->foreign('m_jenis_binaan_id')->references('id')->on('m_jenis_binaan')->onUpdate('cascade')->onDelete('cascade');
            /* Membuat foreignid m_jenis_permodalan_id dari tabel m_jenis_permodalan */
            $table->unsignedBigInteger('m_jenis_permodalan_id');
            $table->foreign('m_jenis_permodalan_id')->references('id')->on('m_jenis_permodalan')->onUpdate('cascade')->onDelete('cascade');
            /* Membuat foreignid m_perusahaan_id dari tabel m_perusahaan */
            $table->unsignedBigInteger('m_perusahaan_id');
            $table->foreign('m_perusahaan_id')->references('id')->on('m_perusahaan')->onUpdate('cascade')->onDelete('cascade');

            $table->date('tanggal_menerima_bantuan');
            $table->string('status_kerjasama');
            $table->string('nama_binaan');
            $table->decimal('nominal_binaan', 15, 2);
            $table->string('nama_barang');
            $table->integer('jumlah_barang');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_bantuan_usaha');
    }
}
