<x-templates.default>
    <x-slot name="title">Entri Data Pelaku Usaha</x-slot>

    <!-- Konten baris keempat -->
    {{-- {{ dd($data['nik']) }} --}}
    @hasanyrole('superadmin|admin')
    @include('pelaku_usaha.create_admin')
    @else
    @include('pelaku_usaha.create_user')
    @endrole
    <meta name="csrf-token" content="{{ csrf_token() }}">

    @push('scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.0/jquery.min.js"></script>

    {{-- TANGGAL --}}
    <link rel="stylesheet"
        href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js">
    </script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.id.min.js">
    </script>


    <script>
        $(document).ready(function() {

            // Add an event listener to detect when the user changes the select input
            $('#formTambahPelakuUsaha').on('change', function() {
                if (document.getElementById('nonSurabaya').checked) {
                    document.getElementById('cek_nik').disabled = true;
                } else {
                    document.getElementById('cek_nik').disabled = false;
                }
            });



            $('#nik').on('input', function() {
                var nik = $('#nik').val();
                this.value = this.value.replace(/[^0-9]/g, '');

                if (nik.length > 16) {
                    $('#nik').val(nik.substring(0, 16));
                }
            });

            $('#submit_keckel').attr('disabled', true); //disable input

            $('#chck_submit').click(function() {
                //check if checkbox is checked
                if ($(this).is(':checked')) {

                    $('#submit_keckel').removeAttr('disabled'); //enable input

                } else {
                    $('#submit_keckel').attr('disabled', true); //disable input
                }
            });

            $('#formTambahPelakuUsaha').submit(function(e) {
                e.preventDefault();
                Swal.fire({
                    icon: 'warning',
                    title: 'Periksa Kembali Inputan Anda',
                    text: 'Apakah Anda yakin data yang Anda masukkan sudah benar?',
                    showCancelButton: true,
                    confirmButtonText: 'Ya, Lanjutkan!',
                    cancelButtonText: 'Tidak, Periksa Kembali'
                }).then((result) => {
                    if (result.isConfirmed) {
                        // Jika dikonfirmasi, lanjutkan ke langkah selanjutnya
                        $.ajax({
                            url: "{{ route('pelaku_usaha.store') }}",
                            type: "POST",
                            dataType: "json",
                            data: $(this).serialize(),
                            success: function(data) {
                                if (data.status == true) {
                                    Swal.fire({
                                        title: 'Berhasil',
                                        html: data.message,
                                        icon: 'success',
                                        confirmButtonText: 'OK'
                                    }).then((result) => {
                                        // if (result.isConfirmed) {
                                        //     window.location.href =
                                        //         "{{ route('pelaku_usaha.index') }}";
                                        // }
                                    })
                                } else {
                                    Swal.fire({
                                        title: 'Gagal',
                                        html: data.message,
                                        icon: 'error',
                                        confirmButtonText: 'OK'
                                    })
                                }
                            },
                            error: function(xhr, status, error) {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Peringatan!',
                                    text: 'Terjadi Kesalahan Teknis, Silahkan Refresh Kembali Browser Anda, Atau Hubungi Admin!',
                                    footer: 'Error code: ' + xhr.status
                                });
                            }
                        });
                    }
                });
            });


            $('select[name="kelurahan_usaha"]').prop('disabled', true);

            $('[name="kecamatan_usaha"]').on('change', function() {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function(data) {


                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel +
                                    '">' + data
                                    .data[i].nm_kel + '</option>';
                            }

                            $('[name="kelurahan_usaha"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan_usaha"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan_usaha"]').empty();
                }
            });


            $('[name="kecamatan_domisili"]').on('change', function() {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function(data) {


                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel +
                                    '">' + data
                                    .data[i].nm_kel + '</option>';
                            }

                            $('[name="kelurahan_domisili"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan_domisili"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan_domisili"]').empty();
                }
            });


            $('select[name="kabupaten_kota"]').attr('readonly', true);
            $('#provinsi').change(function() {

                setTimeout(() => {
                    $.ajax({
                            type: "POST",
                            url: "{{ route('cari_kabupaten') }}",
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id_m_setup_prop: $('#provinsi').val()
                            }
                        })
                        .done(function(data) {
                            if (data.status) {
                                var render = '<option value="" disabled selected>Pilih :</option>';

                                for (var a = 0; a < data.data.length; a++) {
                                    render += '<option value="' + data.data[a].id_m_setup_kab +
                                        '">' + data
                                        .data[a].nm_kab + '</option>';
                                }

                                $('#kabupaten_kota').html(render);
                                $('select[name="kabupaten_kota"]').removeAttr('readonly', true);


                                swal.close();
                            } else {
                                $('#kabupaten_kota').empty();

                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                title: "Perhatian!",
                                text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            })
                        });
                }, 100);
            })

            $('select[name="kecamatan"]').attr('readonly', true);
            $('#kabupaten_kota').change(function() {

                setTimeout(() => {
                    $.ajax({
                            type: "POST",
                            url: "{{ route('cari_kecamatan') }}",
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id_m_setup_prop: $('#provinsi').val(),
                                id_m_setup_kab: $('#kabupaten_kota').val()
                            }
                        })
                        .done(function(data) {
                            if (data.status) {
                                var render = '<option value="" disabled selected>Pilih :</option>';

                                for (var a = 0; a < data.data.length; a++) {
                                    render += '<option value="' + data.data[a].id_m_setup_kec +
                                        '">' + data
                                        .data[a].nm_kec + '</option>';
                                }

                                $('#kecamatan').html(render);
                                $('select[name="kecamatan"]').removeAttr('readonly', true);


                                swal.close();
                            } else {
                                Swal.fire({
                                    title: "Perhatian!",
                                    text: data.pesan,
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                title: "Perhatian!",
                                text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            })
                        });
                }, 100);
            })

            $('select[name="kelurahan"]').attr('readonly', true);
            $('#kecamatan').change(function() {

                setTimeout(() => {
                    $.ajax({
                            type: "POST",
                            url: "{{ route('cari_kelurahan') }}",
                            dataType: "json",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: {
                                id_m_setup_prop: $('#provinsi').val(),
                                id_m_setup_kab: $('#kabupaten_kota').val(),
                                id_m_setup_kec: $('#kecamatan').val()
                            }
                        })
                        .done(function(data) {
                            if (data.status) {
                                var render = '<option value="" disabled selected>Pilih :</option>';

                                for (var a = 0; a < data.data.length; a++) {
                                    render += '<option value="' + data.data[a].id_m_setup_kel +
                                        '">' + data
                                        .data[a].nm_kel + '</option>';
                                }

                                $('#kelurahan').html(render);
                                $('select[name="kelurahan"]').removeAttr('readonly', true);

                                swal.close();
                            } else {
                                Swal.fire({
                                    title: "Perhatian!",
                                    text: data.pesan,
                                    icon: "error",
                                    buttonsStyling: !1,
                                    confirmButtonText: "OK",
                                    customClass: {
                                        confirmButton: "btn btn-primary"
                                    }
                                })
                            }
                        }).fail(function(jqXHR, textStatus, errorThrown) {
                            Swal.fire({
                                title: "Perhatian!",
                                text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                                icon: "error",
                                buttonsStyling: !1,
                                confirmButtonText: "OK",
                                customClass: {
                                    confirmButton: "btn btn-primary"
                                }
                            })
                        });
                }, 100);
            })

            // var kecamatan = <?php echo json_encode($data['kecamatan']); ?>;



            renderKecamatanDomisili();
            renderKelurahanDomisili();
            kec_kel_string();
            domisiliKtp();
            cekNik();
        });
        function showButton() {
            document.getElementById("cek_nik").style.display = "block";
            $('#provinsi').attr('disabled', true); //disable input
            $('#kabupaten_kota').attr('disabled', true); //disable input
        }

        function hideButton() {
            document.getElementById("cek_nik").style.display = "none";
            $('#provinsi').removeAttr('disabled', true); //disable input
            $('#kabupaten_kota').removeAttr('disabled', true); //disable input
        }

        function cekNik() {
            $('#cek_nik').click(function (e) {
                e.preventDefault();
                // var NIK_PEMOHON = $(this).val();
                var NIK_PEMOHON = $('#nik').val();

                if (NIK_PEMOHON.length == '16') {
                    var input = $("input[name=nik]").val();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]')
                                .attr('content')
                        }
                    });
                    $.ajax({
                        /* Memanggil route pelaku_usaha.cek_nik */
                        url: "{{ route('pelaku_usaha.cek_nik') }}",
                        type: 'POST',
                        data: {
                            nik: input
                        },
                        success: function(response) {
                            if (response.message == "OK") {
                                $('#is_surabaya').val('1');
                                $('#nik').val(response.nik);
                                $('#nama_pelaku_usaha').val(response
                                    .nama_lengkap);
                                $('#jenis_kelamin').val(response.jenis_kelamin);
                                $('#tempat_lahir').val(response.tempat_lahir);
                                $('#alamat_ktp').val(response.alamat);
                                $('#rt_ktp').val(response.rt);
                                $('#rw_ktp').val(response.rw);
                                $('#tanggal_lahir').val(response.tgl_lahir);
                                $('#provinsi').val(35);
                                renderKabupaten(78);
                                renderKecamatan(response.id_kecamatan);
                                renderKelurahan(response.id_kelurahan);
                                domisiliKtp(response.id_kecamatan, response.id_kelurahan);
                                $(':input[type="submit"]').removeAttr(
                                    'readonly');

                            } else {
                                swal.fire('Peringatan!',
                                    response.error,
                                    "error");
                                $('#is_surabaya').val('1');
                                $('#nik').val('');
                                $('#nama_pelaku_usaha').val('');
                                $('#alamat_ktp').val('');
                                $('#kecamatan').val('');
                                $('#kelurahan').val('');
                                $('#rt_ktp').val('');
                                $('#rw_ktp').val('');
                                $('#tanggal_lahir').val('');
                                $('#nama_pelaku_usaha').removeAttr('readonly');
                                $('#alamat_ktp').removeAttr('readonly');
                                $('#kecamatan').removeAttr('readonly');
                                $('#kelurahan').removeAttr('readonly');
                                $('#provinsi_pemilik').val('');
                                $('#kabupaten_pemilik').val('');
                                $('#kecamatan_pemilik').val('');
                                $('#kelurahan_pemilik').val('');
                                $('#rt_ktp').removeAttr('readonly');
                                $('#rw_ktp').removeAttr('readonly');
                                $('#tanggal_lahir').removeAttr('readonly');
                                $(':input[type="submit"]').prop('disabled',
                                    true);
                            }
                        },
                        error: function() {
                            swal.fire("Telah terjadi kesalahan pada sistem",
                                "Mohon refresh halaman browser Anda",
                                "error");
                        }
                    })
                }
            });
        }
        function kec_kel_string() {

            $('#kecamatan').change(function(e) {
                var selectedIndex = $(this).prop('selectedIndex');
                var selectedOptionLabel = $(this).children().eq(selectedIndex).text();
                $('#kecamatan_string').val(selectedOptionLabel);
            });

            $('#kelurahan').change(function(e) {
                var selectedIndex = $(this).prop('selectedIndex');
                var selectedOptionLabel = $(this).children().eq(selectedIndex).text();
                $('#kelurahan_string').val(selectedOptionLabel);
            });
        }
        function renderKecamatanDomisili(params) {
            // $('select[name="kelurahan_domisili"]').attr('disabled', true);
            $.ajax({
                type: "POST",
                url: "{{ route('cari_kecamatan') }}",
                dataType: "json",
                data: {
                    '_token': "{{ csrf_token() }}",
                    id_m_setup_prop: $('#provinsi').val() == null ? '35' : $('#provinsi').val(),
                    id_m_setup_kab: $('#kabupaten_kota').val() == null ? '78' : $('#kabupaten_kota').val()
                }
            })
            .done(function(response) {
                if (response.status) {
                    let render = '<option disabled selected>Pilih :</option>';
                    let renderNama = '';
                    response.data.forEach(function(item) {
                        if (item.id_m_setup_kec == params) {
                            render +=
                                `<option value="${item.id_m_setup_kec}" data-name="${item.nm_kec}" selected>${item.nm_kec}</option>`;
                                renderNama = item.nm_kec;
                        } else {
                            render +=
                                `<option value="${item.id_m_setup_kec}" data-name="${item.nm_kec}">${item.nm_kec}</option>`;
                        }
                    });

                    $('#kecamatan_domisili').html(render);
                } else {
                    /* Swal fire */
                    Swal.fire({
                        icon: 'error',
                        title: 'Eror, Mohon Hubungi Admin',
                        text: response.message,
                    });
                }
            })
        }
        function renderKelurahanDomisili(params) {
            $.ajax({
                type: "POST",
                url: "{{ route('cari_kelurahan') }}",
                dataType: "json",
                data: {
                    '_token': "{{ csrf_token() }}",
                    id_m_setup_prop: $('#provinsi').val() == null ? '35' : $('#provinsi').val(),
                    id_m_setup_kab: $('#kabupaten_kota').val() == null ? '78' : $('#kabupaten_kota').val(),
                    id_m_setup_kec: $('[name="kecamatan"]').val(),
                }
            })
            .done(function(response) {
                if (response.status == 'success') {
                    let render = '<option disabled selected>Pilih :</option>';
                    let renderNama = '';
                    response.data.forEach(function(item) {
                        if (item.id_m_setup_kel == params) {
                            render +=
                                `<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}" selected>${item.nm_kel}</option>`;
                                renderNama = item.nm_kel;
                        } else {
                            render +=
                                `<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}">${item.nm_kel}</option>`;
                        }
                    });
                    // $('select[name="kelurahan_domisili"]').removeAttr('readonly', true);
                    $('#kelurahan_domisili').html(render);
                } else {
                    /* Swal fire */
                    Swal.fire({
                        icon: 'error',
                        title: 'Eror, Mohon Hubungi Admin',
                        text: response.message,
                    });
                }
            })
        }
        function domisiliKtp(id_kecamatan_domisili,id_kelurahan_domisili) {
            // var alamat = $('#alamat_ktp').val();
            var input1 = document.getElementById("alamat_ktp");
            var input2 = document.getElementById("alamat_domisili");


            // Get the checkbox input element
            const checkbox = document.getElementById('check_alamat_domisili');
            checkbox.addEventListener('change', function() {
                if(this.checked) {
                    // Checkbox is checked
                    if (id_kecamatan_domisili == undefined && id_kelurahan_domisili == undefined) {
                        id_kecamatan_domisili = $('#kecamatan').val();
                        id_kelurahan_domisili = $('#kelurahan').val();
                    }
                    input2.value = input1.value;
                    renderKecamatanDomisili(id_kecamatan_domisili);
                    renderKelurahanDomisili(id_kelurahan_domisili);
                } else {
                    $('#alamat_domisili').val('');
                    $('#kecamatan_domisili').append('<option disabled selected>Pilih :</option>');
                    $('#kelurahan_domisili').append('<option disabled selected>Pilih :</option>');
                }
            });

        }
        function renderKabupaten(params) {
            $.ajax({
                    type: "POST",
                    url: "{{ route('cari_kabupaten') }}",
                    dataType: "json",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        id_m_setup_prop: $('[name="provinsi"]').val(),
                    }
                })
                .done(function(response) {
                    let render = '';
                    response.data.forEach(function(item) {
                        if (item.id_m_setup_kab == params) {
                            render +=
                                `<option value="${item.id_m_setup_kab}" selected>${item.nm_kab}</option>`;
                        } else {
                            render +=
                                `<option value="${item.id_m_setup_kab}">${item.nm_kab}</option>`;
                        }
                    });

                    $('#kabupaten_kota').html(render);
                })
                .fail(function(xhr, status, error) {
                    Swal.fire({
                        icon: 'error',
                        title: 'Eror, Mohon Hubungi Admin',
                        text: response.message,
                        footer: 'Error code: ' + xhr.status
                    });
                });
        }

        function renderKecamatan(params) {
            /* get {{ $data['kecamatan'] }} to array variable */
            var kecamatan = <?php echo json_encode($data['kecamatan']); ?>;
            /* foreach kecamatan, when same echo attr selected */
            let render = '';
            let renderNama = '';
            kecamatan.forEach(function(item) {
                if (item.id_m_setup_kec == params) {
                    render +=
                        `<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}" selected>${item.nm_kec}</option>`;
                    // renderNama = `"${item.nm_kec}"`;
                    renderNama = item.nm_kec;
                } else {
                    render += `<option value="${item.id_m_setup_kec}" value-name="${item.nm_kec}">${item.nm_kec}</option>`;
                }
            });
            $('#kecamatan').html(render);
            $('#kecamatan_string').val(renderNama);

        }

        function renderKelurahan(params) {
            $.ajax({
                    type: "POST",
                    url: "{{ route('getKelurahan') }}",
                    dataType: "json",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        id_m_setup_kec: $('[name="kecamatan"]').val(),
                    }
                })
                .done(function(response) {
                    if (response.status == 'success') {
                        let render = '';
                        let renderNama = '';
                        response.data.forEach(function(item) {
                            if (item.id_m_setup_kel == params) {
                                render +=
                                    `<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}" selected>${item.nm_kel}</option>`;
                                    renderNama = item.nm_kel;
                            } else {
                                render +=
                                    `<option value="${item.id_m_setup_kel}" data-name="${item.nm_kel}">${item.nm_kel}</option>`;
                            }
                        });

                        $('#kelurahan').html(render);
                        $('#kelurahan_string').val(renderNama);
                    } else {
                        /* Swal fire */
                        Swal.fire({
                            icon: 'error',
                            title: 'Eror, Mohon Hubungi Admin',
                            text: response.message,
                        });
                    }
                })
        }
    </script>
    @endpush

</x-templates.default>