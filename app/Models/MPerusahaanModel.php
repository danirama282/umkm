<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MPerusahaanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_perusahaan';
    protected $fillable = [
        'nama_perusahaan',
        'alamat_perusahaan',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];


}
