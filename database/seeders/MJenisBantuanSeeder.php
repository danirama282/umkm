<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MJenisBantuanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat data untuk MJenisBantuanModel
        \DB::table('m_jenis_bantuan')->insert([
            'nama_jenis_bantuan' => 'PEMBINAAN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_jenis_bantuan')->insert([
            'nama_jenis_bantuan' => 'PERMODALAN',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
        
    }
}
