<x-templates.default>
    <x-slot name="title">Beranda</x-slot>
    {{-- <div class="row g-0 pt-3">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card h-lg-100">
                <div class="card-body position-relative">
                    <h5 class="text-dark">Selamat Datang, <span class="fw-bold">Yth. Bapak/Ibu
                            {{ $data_user['user']->nama_user }}</span>!</h5>
                </div>
            </div>
        </div>
    </div> --}}
    <div class="row g-0 pt-10">
        <div class="col-lg-12 col-xl-12 col-xxl-12 d-flex justify-content-center">
            <img src="{{ asset('dist/assets/img/illustrations/welcome-ilus.png') }}" alt="" width="500">
        </div>
    </div>
</x-templates.default>
