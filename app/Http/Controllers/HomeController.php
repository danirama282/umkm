<?php

namespace App\Http\Controllers;

use App\Models\MKategoriKbliModel;
use App\Models\MKategoriUsahaModel;
use App\Models\MKlasifikasiKbliModel;
use App\Models\PadatKaryaModel;
use App\Models\TOmsetUsahaModel;
use App\Models\TPelakuUsahaDisdag131022Model;
use Illuminate\Http\Request;
use App\Models\TPelakuUsahaModel;
use Carbon\Carbon;
use Facade\FlareClient\Http\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use stdClass;
use Yajra\DataTables\DataTables;

use App\Http\Controllers\Exception;
use App\Models\PekenModel;
use Carbon\Exceptions\Exception as ExceptionsException;
use Illuminate\Support\Facades\Log;

use function PHPUnit\Framework\isEmpty;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        // dd('test');
        /* Mengambil data user yang login beserta dengan role yang dipunya */
        $data['user'] = auth()->user()->load('roles');

        /* Mengambil data role yang dipunya user */
        $data['role'] = $data['user']->roles->first()->name;
        // dd($data);
        /* Memanggil fungsi scopeCountData pada model TPelakuUsahaDisdag131022Model */
        $data['total_pelaku_usaha'] = TPelakuUsahaDisdag131022Model::scopeCountData();

        /* $data['total_pelaku_usaha'] = TPelakuUsahaModel::scopeCountData();
        $data['total_mbr'] = TPelakuUsahaModel::scopeCountDataMbr('MBR');
        $data['total_non_mbr'] = TPelakuUsahaModel::scopeCountDataNonMbr('NON MBR');
        $data['kategori_usaha'] = TPelakuUsahaModel::with('kategori_usaha')->get();
        $data['total_perdagangan'] = TPelakuUsahaModel::scopeCountJumlahProduksi('total_perdagangan');
        $data['total_jasa'] = TPelakuUsahaModel::scopeCountJumlahJasa('total_jasa');
        $data['total_industri'] = TPelakuUsahaModel::scopeCountJumlahIndustri('total_industri');
        $data['total_tokel'] = TPelakuUsahaModel::scopeCountJumlahTokel('total_tokel');
        $data['total_tokel_mbr'] = TPelakuUsahaModel::scopeCountJumlahTokelMbr('total_tokel_mbr');
        $data['total_tokel_non_mbr'] = TPelakuUsahaModel::scopeCountJumlahTokenNonMbr('total_tokel_non_mbr');
        $data['total_jahit'] = TPelakuUsahaModel::scopeCountJumlahJahit('total_jahit');

        $data['total_jahit_mbr'] = TPelakuUsahaModel::scopeCountJumlahJahitMbr('total_jahit_mbr');
        $data['total_jahit_non_mbr'] = TPelakuUsahaModel::scopeCountJumlahJahitNonMbr('total_jahit_non_mbr');
        $data['total_cuci_sepatu'] = TPelakuUsahaModel::scopeCountJumlahCuciSepatu('total_cuci_sepatu');
        $data['total_cuci_sepatu_mbr'] = TPelakuUsahaModel::scopeCountJumlahCuciSepatuMbr('total_cuci_sepatu_mbr');
        $data['total_cuci_sepatu_non_mbr'] = TPelakuUsahaModel::scopeCountJumlahCuciSepatuNonMbr('total_cuci_sepatu_non_mbr');
        $data['total_cuci_helm'] = TPelakuUsahaModel::scopeCountDataCuciHelm('total_cuci_helm');
        $data['total_cuci_helm_mbr'] = TPelakuUsahaModel::scopeCountDataCuciHelmMbr('total_cuci_helm_mbr');
        $data['total_cuci_helm_non_mbr'] = TPelakuUsahaModel::scopeCountDataCuciHelmNonMbr('total_cuci_helm_non_mbr');
        $data['total_jumlah_laundry'] = TPelakuUsahaModel::scopeCountDataJumlahLaundry('total_jumlah_laundry');
        $data['total_jumlah_laundry_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahLaundryMbr('total_jumlah_laundry_mbr');
        $data['total_jumlah_laundry_non_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahLaundryNonMbr('total_jumlah_laundry_non_mbr');
        $data['total_jumlah_laundry_non_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahLaundryNonMbr('total_jumlah_laundry_non_mbr');
        $data['total_jumlah_swk'] = TPelakuUsahaModel::scopeCountDataJumlahSwk('total_jumlah_swk');
        $data['total_jumlah_swk_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahSwkMbr('total_jumlah_swk_mbr');
        $data['total_jumlah_swk_non_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahSwkNonMbr('total_jumlah_swk_non_mbr');
        $data['total_jumlah_umkm'] = TPelakuUsahaModel::scopeCountDataJumlahUmkm('total_jumlah_umkm');
        $data['total_jumlah_umkm_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahUmkmMbr('total_jumlah_umkm_mbr');
        $data['total_jumlah_umkm_non_mbr'] = TPelakuUsahaModel::scopeCountDataJumlahUmkmNonMbr('total_jumlah_umkm_non_mbr');
        $data['sebaran_wilayah_umkm'] = TPelakuUsahaModel::scopeCountSebaranUmkm('sebaran_wilayah_umkm');
		$data['kategori_umkm'] = MKategoriUsahaModel::get(); */

        /* "Industri Pengolahan" => 127
        "Perdagangan" => 1943
        "Akomodasi Mamin" => 1192
        "Aktivitas Jasa" => 5 */

        /* $trial = MKategoriKbliModel::leftJoin('m_klasifikasi_kbli', 'm_klasifikasi_kbli.id_m_kategori_kbli', 'm_kategori_kbli.id_m_kategori_kbli')
            ->select('kode_klasifikasi_kbli', 'nama_kategori_kbli')
            ->limit(10)
            ->get();
        dd($trial); */

        /* Memanggil data MKlasifikasiKbliModel dengan relasinya */
        // $data['klasifikasi_kbli'] = MKlasifikasiKbliModel::select('id_m_kategori_kbli', 'kode_klasifikasi_kbli', 'nama_klasifikasi_kbli')->with('relasi_kategori_kbli')->orderBy('kode_klasifikasi_kbli', 'asc')->get();

        // /* Inisiasi variabel untuk membungkus $data['klasifikasi_kbli'] */
        // $kode_klasifikasi_kbli = $data['klasifikasi_kbli'];

        // /* Inisiasi variabel $total_count yang berisi array kosong */
        // $total_count = [];
        // /* Melakukan perulangan/looping untuk mendapatkan data yang dinginkan */
        // for ($h = 0; $h < count($kode_klasifikasi_kbli); $h++) {
        //     $_kode_klasifikasi_kbli = $kode_klasifikasi_kbli[$h]->kode_klasifikasi_kbli;
        //     $_nama_kategori_klasifikasi_kbli = $kode_klasifikasi_kbli[$h]->relasi_kategori_kbli->nama_kategori_kbli;
        //     $pelaku_usaha_disdag = TPelakuUsahaDisdag131022Model::select('kode_kbli_2')->where('kode_kbli_2', 'LIKE', '%' . $_kode_klasifikasi_kbli . '%')->orderBy('kode_kbli_2', 'ASC')->get();

        // 	// dd($pelaku_usaha_disdag);

        //     $total = 0;

        //     if (count($pelaku_usaha_disdag) > 0){
        //         /* Melakukan perulangan/looping untuk mencocokkan kolom kode_kbli_2 dengan $_kode_klasifikasi_kbli */
        //         for ($i = 0; $i < count($pelaku_usaha_disdag); $i++) {
        //             $item = json_decode($pelaku_usaha_disdag[$i]->kode_kbli_2);

        //             if(in_array($_kode_klasifikasi_kbli, $item)){
        //                 $total++;
        //             }
        //         }
        //     }

        //     /* Mengisi variabel $total_count dengan value $h, dimana didalam variabel terdapat value kode_klasifikasi_kbli, nama_kategori_kbli, dan total */
        //     $total_count[$h] = [
        //         'kode_klasifikasi_kbli' => $_kode_klasifikasi_kbli,
        //         'nama_kategori_kbli' => $_nama_kategori_klasifikasi_kbli,
        //         'total' => $total
        //     ];
        // }

        // // dd($total_count);

        // /* Mencari jumlah total per nama_kategori_kbli terhadap nilai total */
        // $total_per_kategori = [];
        // for ($i = 0; $i < count($total_count); $i++) {
        //     $total_per_kategori[$total_count[$i]['nama_kategori_kbli']][] = $total_count[$i]['total'];
        // }

        // /* Menghitung jumlah total per nama_kategori_kbli */
        // $total_per_kategori = array_map(function($item){
        //     return array_sum($item);
        // }, $total_per_kategori);

        /* Mengisi variabel $data['total_per_kategori'] dengan value $total_per_kategori */
        // $data['total_per_kategori'] = $total_per_kategori;

        // dd($data['total_per_kategori']);

        $total_per_kategori = TPelakuUsahaDisdag131022Model::scopeCountKategoriKBLI('total_per_kategori');
        // dd($total_per_kategori);

        // dd($data['total_per_kategori']);
        $data['total_kbli'] = [];
        foreach ($total_per_kategori as $key => $value) {
            $data['total_kbli'][$value->nm_kategori] = $value->jumlah_pelaku_usaha;
            // dd($value->nm_kategori);
        }

        // dd($data['total_per_kategori']);
        $data['total_kategori_usaha'] = TPelakuUsahaDisdag131022Model::scopeCountKategoriUsaha('total_kategori_usaha');

        /* Memanggil fungsi scopeCountIntervensi dari TPelakuUsahaDisdag131022 */
        $data['total_intervensi'] = TPelakuUsahaDisdag131022Model::scopeCountIntervensi('total_intervensi');
        /* Memanggil fungsi scopePelakuUsahaMbr dari TPelakuUsahaDisdag131022 */
        $data['total_pelaku_usaha_mbr'] = TPelakuUsahaDisdag131022Model::scopePelakuUsahaMbr('total_pelaku_usaha_mbr');

        /* Memanggil fungsi scopePelakuUsahaNonMbr dari TPelakuUsahaDisdag131022 */
        $data['total_pelaku_usaha_non_mbr'] = TPelakuUsahaDisdag131022Model::scopePelakuUsahaNonMbr('total_pelaku_usaha_non_mbr');


        $data['KumulatifAll'] = TPelakuUsahaDisdag131022Model::KumulatifAll();
        $data['WargaMiskinAll'] = TPelakuUsahaDisdag131022Model::WargaMiskinAll();
        $data['MiskinEkstrim'] = TPelakuUsahaDisdag131022Model::MiskinEkstrim();
        $data['WargaMiskin'] = TPelakuUsahaDisdag131022Model::WargaMiskin();
        $data['WargaPraMiskin'] = TPelakuUsahaDisdag131022Model::WargaPraMiskin();
        $data['WargaPraMiskin_Ekstrim'] = TPelakuUsahaDisdag131022Model::WargaPraMiskin_Ekstrim();
        $data['WargaPraMiskin_pra'] = TPelakuUsahaDisdag131022Model::WargaPraMiskin_pra();
        $data['WargaLainnyaAll'] = TPelakuUsahaDisdag131022Model::WargaLainnyaAll();
        $data['um'] = TPelakuUsahaDisdag131022Model::um();
        // dd($data['um']);









        $data['all'] = [
            TPelakuUsahaDisdag131022Model::fashion_all(),
            TPelakuUsahaDisdag131022Model::toko_all(),
            TPelakuUsahaDisdag131022Model::mamin_all(), TPelakuUsahaDisdag131022Model::pertanian_all(), TPelakuUsahaDisdag131022Model::kerajinan_all(), TPelakuUsahaDisdag131022Model::jasa_all()
        ];
        // dd($data['all']);

        $data['miskin_all'] = [
            TPelakuUsahaDisdag131022Model::fashion_miskinAll(),
            TPelakuUsahaDisdag131022Model::toko_miskinAll(),
            TPelakuUsahaDisdag131022Model::mamin_miskin_all(),
            TPelakuUsahaDisdag131022Model::pertanian_miskin_all(),
            TPelakuUsahaDisdag131022Model::kerajinan_miskin_all(),
            TPelakuUsahaDisdag131022Model::jasa_miskin_all()
        ];

        $data['miskin_ekstrim'] = [
            TPelakuUsahaDisdag131022Model::fashion_miskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::toko_miskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::mamin_miskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::pertanian_miskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::kerajinan_miskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::jasa_miskin_ekstrim()
        ];

        $data['miskin_miskin'] = [
            TPelakuUsahaDisdag131022Model::fashion_warga_miskin(),
            TPelakuUsahaDisdag131022Model::toko_miskin_miskin(),
            TPelakuUsahaDisdag131022Model::mamin_miskin_miskin(),
            TPelakuUsahaDisdag131022Model::pertanian_miskin_miskin(),
            TPelakuUsahaDisdag131022Model::kerajinan_miskin_miskin(),
            TPelakuUsahaDisdag131022Model::jasa_miskin_miskin(),
        ];

        $data['pramiskin_all'] = [
            TPelakuUsahaDisdag131022Model::fashion_pramiskin_All(),
            TPelakuUsahaDisdag131022Model::toko_pramiskin_all(),
            TPelakuUsahaDisdag131022Model::mamin_pramiskin_all(),
            TPelakuUsahaDisdag131022Model::pertanian_pramiskin_all(),
            TPelakuUsahaDisdag131022Model::kerajinan_pramiskin_all(),
            TPelakuUsahaDisdag131022Model::jasa_pramiskin_all(),
        ];

        $data['pramiskin_ekstrim'] = [
            TPelakuUsahaDisdag131022Model::fashion_pramiskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::toko_pramiskin_ektrim(),
            TPelakuUsahaDisdag131022Model::mamin_pramiskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::pertanian_pramiskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::kerajinan_pramiskin_ekstrim(),
            TPelakuUsahaDisdag131022Model::jasa_pramiskin_ekstrim(),
        ];

        $data['pramiskin_pramiskin'] = [
            TPelakuUsahaDisdag131022Model::fashion_pramiskin(),
            TPelakuUsahaDisdag131022Model::toko_pramiskin_pramiskin(),
            TPelakuUsahaDisdag131022Model::mamin_pramiskin_pramiskin(),
            TPelakuUsahaDisdag131022Model::pertanian_pramiskin_pramiskin(),
            TPelakuUsahaDisdag131022Model::kerajinan_pramiskin_pramiskin(),
            TPelakuUsahaDisdag131022Model::jasa_pramiskin_pramiskin(),
        ];

        $data['wargalainnya_all'] = [
            TPelakuUsahaDisdag131022Model::fashion_wargalainnya_all(),
            TPelakuUsahaDisdag131022Model::toko_wargalainnya_all(),
            TPelakuUsahaDisdag131022Model::mamin_wargalainnya_all(),
            TPelakuUsahaDisdag131022Model::pertanian_wargalainnya_all(),
            TPelakuUsahaDisdag131022Model::kerajinan_wargalainnya_all(),
            TPelakuUsahaDisdag131022Model::jasa_wargalainnya_all(),
        ];

        $data['gamis_kbli'] = TPelakuUsahaDisdag131022Model::get_data_warga_miskin()->toArray();
        // dd($data['wargalainnya_all']);

        // dd($data['gamis_kbli']);

        $data['judul'] = ['Fashion', 'Toko', 'Makanan Dan Minuman', 'Budidaya Pertanian', 'Kerajinan', 'Jasa'];
        $data['kategori_usaha'] = ['Fashion', 'Toko', 'Makanan Dan Minuman', 'Pembudidaya Sektor Pertanian', 'Kerajinan', 'Jasa'];
        $data['modal'] = ['detailFashionModal', 'detailTokoModal', 'detailKategoriMaminModal', 'detailBudidayaModal', 'detailKerajinanModal', 'detailJasaModal'];

        # JIKA ADA PENAMBAHAN DATA MASTER_INTERVENSI MAKA TAMBAHKAN SEBELUM INVERVENSI "BELUM INVERVENSI"
        $data['master_intervensi'] = ['SWK', 'SKG', 'Pasar', 'Peken Tokel', 'Pelatihan', 'Pameran', 'BPUM', 'Rumah Kreatif', 'Padat Karya', 'Kur', 'Csr', 'Puspita', 'Oss', 'Belum Intervensi'];
        $data['gamis_intervensi'] = TPelakuUsahaDisdag131022Model::get_data_gamis_intervensi($data['master_intervensi']);

        // dd($data['gamis_intervensi']['intervensi_kumulatif'][0][0], $data);
        // dd($data['gamis_intervensi'], $data);

        /* Mengirimkan data user dan role ke view */
        return view('home', compact('data'));
    }

    public function detailGamis(Request $request)
    {
        # code...
        $data = TPelakuUsahaDisdag131022Model::detailGamis($request->gamis);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }
    public function detailKategori(Request $request)
    {
        $data = TPelakuUsahaDisdag131022Model::detailKategori($request->gamis, $request->kategori);
        // dd($data);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function intervensiGamis(Request $request)
    {
        $intervensi = $request->intervensi;
        $intervensi = strtolower(str_replace(" ", "_", $intervensi));
        // dd($request->gamis, $intervensi);
        $data = TPelakuUsahaDisdag131022Model::intervensiGamis($request->gamis, $intervensi);
        // dd($data);
        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('intervensi', function ($data) {
                // dd(json_decode($data->intervensi));
                $arr_intervensi = json_decode($data->intervensi);
                // dd($arr_intervensi[0]);
                $arr_filter_intervensi = array_filter($arr_intervensi[0], function ($val) {
                    return $val !== "";
                });
                // dd($arr_filter_intervensi);
                $intervensi = "<ul>";
                foreach ($arr_filter_intervensi as $value) {
                    // dd($row);
                    $intervensi .= "<li>" . $value . "</li>";
                }
                $intervensi .= "</ul>";
                // dd($intervensi);
                return $intervensi;
            })
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailGamisKbli(Request $request) /*jika nik tersebut mendapatkan intervensi selain padat karya, maka melakukan update kolom padat karya menjadi null*/
    {
        $data = TPelakuUsahaDisdag131022Model::detailGamisKbli($request->kbli, $request->gamis);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    /* public function dataTablesWilayah()
    {
        $data = TPelakuUsahaModel::scopeCountSebaranUmkm('tabel_sebaran_wilayah');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    } */

    // public function dataTableIntervensi()
    // {
    //     $data = TPelakuUsahaDisdag131022Model::scopeCountKategoriIntervensi('tabel_kategori_intervensi');

    //     return DataTables::of($data)
    //     ->addIndexColumn()
    //     ->toJson();
    // }

    public function dataTablesWilayahDisdag()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervrensiSebaranUmkmDisdag('tabel_intervrensi_sebaran_umkm_disdag');
        // $swk = 'swk';

        return DataTables::of($data)
            ->addIndexColumn()
            ->editColumn('jumlah_swk_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`swk_aja`, `' . $data->kecamatan_pemilik . '`,`SWK`,`' . $data->nm_kec . '`)">' . $data->jumlah_swk_non_mbr . '</button>
                ';
            })->editColumn('jumlah_skg_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`skg`, `' . $data->kecamatan_pemilik . '`,`SKG`,`' . $data->nm_kec . '`)">' . $data->jumlah_skg_non_mbr . '</button>
                ';
            })
            ->editColumn('jumlah_pasar_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`pasar`, `' . $data->kecamatan_pemilik . '`,`PASAR`,`' . $data->nm_kec . '`)">' . $data->jumlah_pasar_non_mbr . '</button>
                ';
            })->editColumn('jumlah_tp_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`peken_tokel`, `' . $data->kecamatan_pemilik . '`,`PEKEN`,`' . $data->nm_kec . '`)">' . $data->jumlah_tp_non_mbr . '</button>
                ';
            })->editColumn('jumlah_pelatihan_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`pelatihan`, `' . $data->kecamatan_pemilik . '`,`PELATIHAN`,`' . $data->nm_kec . '`)">' . $data->jumlah_pelatihan_non_mbr . '</button>
                ';
            })->editColumn('jumlah_pameran_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`pameran`, `' . $data->kecamatan_pemilik . '`,`PAMERAN`,`' . $data->nm_kec . '`)">' . $data->jumlah_pameran_non_mbr . '</button>
                ';
            })->editColumn('jumlah_bpum_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`bpum`, `' . $data->kecamatan_pemilik . '`,`BPUM`,`' . $data->nm_kec . '`)">' . $data->jumlah_bpum_non_mbr . '</button>
                ';
            })->editColumn('jumlah_rk_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`rumah_kreatif`, `' . $data->kecamatan_pemilik . '`,`RUMAH KREATIF`,`' . $data->nm_kec . '`)">' . $data->jumlah_rk_non_mbr . '</button>
                ';
            })->editColumn('jumlah_padat_karya_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`padat_karya`, `' . $data->kecamatan_pemilik . '`,`PADAT KARYA`,`' . $data->nm_kec . '`)">' . $data->jumlah_padat_karya_non_mbr . '</button>
                ';
            })->editColumn('jumlah_kur_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`kur`, `' . $data->kecamatan_pemilik . '`,`KUR`,`' . $data->nm_kec . '`)">' . $data->jumlah_kur_non_mbr . '</button>
                ';
            })->editColumn('jumlah_csr_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`csr`, `' . $data->kecamatan_pemilik . '`,`CSR`,`' . $data->nm_kec . '`)">' . $data->jumlah_csr_non_mbr . '</button>
                ';
            })->editColumn('jumlah_puspita_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`puspita`, `' . $data->kecamatan_pemilik . '`,`PUSPITA`,`' . $data->nm_kec . '`)">' . $data->jumlah_puspita_non_mbr . '</button>
                ';
            })->editColumn('jumlah_oss_non_mbr', function ($data) {
                return '
                <button class="btn btn-link" onclick="detailIntervensi(`oss`, `' . $data->kecamatan_pemilik . '`,`OSS`,`' . $data->nm_kec . '`)">' . $data->jumlah_oss_non_mbr . '</button>
                ';
            })->editColumn('jumlah_belum_intervensi', function ($data) {
                return '
                <button class="btn btn-link" disabled>' . $data->jumlah_belum_intervensi . '</button>
                ';
            })
            ->rawColumns(['jumlah_swk_non_mbr', 'jumlah_skg_non_mbr', 'jumlah_pasar_non_mbr', 'jumlah_tp_non_mbr', 'jumlah_pelatihan_non_mbr', 'jumlah_pameran_non_mbr', 'jumlah_bpum_non_mbr', 'jumlah_rk_non_mbr', 'jumlah_padat_karya_non_mbr', 'jumlah_kur_non_mbr', 'jumlah_csr_non_mbr', 'jumlah_puspita_non_mbr'])
            ->escapeColumns([])
            ->toJson();
    }

    public function grafikInterverensi()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeGrafikIntervensi();
        // dd($data);
        return json_encode($data);
    }

    public function dataTablesPenerimaCsr()
    {
        $data = TPelakuUsahaModel::scopeCountPenerimaCsr('tabel_penerima_csr');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }

    public function dataTablesWilayahSebaranUkm()
    {
        $data = TPelakuUsahaModel::scopeCountWilayahSebaranUkm('tabel_penerima_omset');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }

    public function grafikStatusMBR()
    {
        $data = TPelakuUsahaModel::scopeGrafikStatusMBR();

        return json_encode($data);
    }

    public function grafikJenisUsaha()
    {
        $data = TPelakuUsahaModel::scopeGrafikJenisUsaha();

        return json_encode($data);
    }

    public function tableOmset(Request $request)
    {
        $where = "WHERE om.m_kategori_usaha_id = " . $request->jenis_umkm . " AND om.tahun_laporan =  '" . $request->tahun_laporan . "'";

        $data = TOmsetUsahaModel::dataTotalOmset($where);

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }

    public function dataTablePelakuUsahaIndustriPengolahan()
    {
        /* Memanggil data MKlasifikasiKbliModel dengan relasinya */
        $data['klasifikasi_kbli'] = MKlasifikasiKbliModel::select('id_m_kategori_kbli', 'kode_klasifikasi_kbli', 'nama_klasifikasi_kbli')->with('relasi_kategori_kbli')->whereNull('deleted_at')->orderBy('kode_klasifikasi_kbli', 'asc')->limit(10)->get();

        foreach ($data['klasifikasi_kbli'] as $item) {
            $id_m_kategori = $item->id_m_kategori_kbli;
            $kode_klasifikasi_kbli = $item->kode_klasifikasi_kbli;
            $nama_klasifikasi_kbli = $item->nama_klasifikasi_kbli;
            $nama_kategori_kbli = $item->relasi_kategori_kbli->nama_kategori_kbli;

            $test = [47222];

            /* Memanggil TPelakuUsahaDisdag131022Model where kode_kbli_2 like kode_klasifikasi_kbli */
            $pelaku_usaha_disdag = TPelakuUsahaDisdag131022Model::select('kode_kbli_2')->where('kode_kbli_2', 'like', '%' . $kode_klasifikasi_kbli . '%')->get();

            $data['pelaku_usaha_disdag'] = $pelaku_usaha_disdag;
        }
    }

    /* Fungsi datatables untuk Kategori Usaha */
    public function dataTableFashion()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeKategoriFashion('tabel_fashion');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTableMamin()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountKategoriMamin('tabel_mamin');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTableKerajinan()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountKategoriKerjinan('tabel_kerajinan');

        return DataTables::of($data)
            ->addColumn('aksi', function ($data) {
                return '
            <div class="text-center">
                <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                <i class="fas fa-search"></i>
            </div>
            ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->addIndexColumn()
            ->toJson();
    }
    public function dataTableFashionToko()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountKategoriToko('tabel_toko');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTableJasa()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountKategoriJasa('tabel_jasa');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }

    /* Fungsi datatable KBLI */
    public function dataTablesIndustriPengolahan()
    {
        // dd('test');
        $data = TPelakuUsahaDisdag131022Model::scopeDataPelakuUsahaIndustriPengolahan('industri_pengolahan');
        // dd($data);

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTablesPerdagangan()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeDataPelakuUsahaPerdagangan('industri_pengolahan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTablesAkomodasiUsaha()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeDataPelakuUsahaAkomodasiMamin('industri_pengolahan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTablesAktivitasJasa()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeDataPelakuUsahaAktivitasJasa('industri_pengolahan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }

    public function datatablePertanianKehutananPerikanan()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeDataPertanianKehutananPerikanan('industri_pengolahan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }

    // INTERVENSI
    public function dataTableSwk()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiSwk('tabel_swk');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTableSkg()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiSkg('tabel_skg');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTablePasar()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiPasar('tabel_pasar');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function dataTableTokel()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiTokel('tabel_tokel');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTablePelatihan()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiPelatihan('tabel_pelatihan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTablePameran()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiPameran('tabel_pemeran');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function dataTableIndustriRumahan()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiIndustriRumahan('tabel_industri_rumahan');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }
    public function dataTableRumahKreatif()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiRumahKreatif('tabel_rumah_kreatif');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTablePadatKarya()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiPadatKarya('tabel_padat_karya');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function dataTableKur()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiKur('tabel_kur');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTableCsr()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiCsr('tabel_csr');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTablePuspita()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiPuspita('tabel_puspita');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }
    public function dataTableMbr()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountMbr('tabel_mbr');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }
    public function dataTableNonMbr()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountNonMbr('tabel_non_mbr');

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }
    public function dataTableBudidaya()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeKategoriBudidaya('tabel_budidaya');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])
            ->toJson();
    }
    public function dataTableBPUM()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountIntervensiBPUM('tabel_BPUM');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function dataTableBelumIntervensi()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountBelumIntervensi('tabel_belum_intervensi');

        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset ``">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function dataTableOss()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeCountOss('tabel_oss');
        // dd($data);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset ``">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aski'])
            ->escapeColumns([])

            ->toJson();
    }

    public function getDataPadatKarya()
    {
        DB::beginTransaction();
        $url = Http::withHeaders([
            'user' => 'dashboard_padatkarya',
            'password' => '5Ert19Bc'
        ])->post('https://api.surabaya.go.id/integrasi/api/padat-karya/umkm')->object();
        $result = $url->data;
        try {
            foreach ($result as $val_real) {
                $data[] = [
                    'no_kk'             => $val_real->no_kk,
                    'nik'               => $val_real->nik,
                    'nama'              => $val_real->nama,
                    'alamat'            => $val_real->alamat,
                    'no_rt'             => $val_real->no_rt,
                    'no_rw'             => $val_real->no_rw,
                    'kecamatan'         => $val_real->kecamatan,
                    'kelurahan'         => $val_real->kelurahan,
                    'id_m_setup_kec'    => $val_real->id_m_setup_kec,
                    'id_m_setup_kel'    => $val_real->id_m_setup_kel,
                    'pekerjaan'         => $val_real->pekerjaan,
                    'tgl_tarik'         => date('Y-m-d H:i:s'),
                ];
            }
            PadatKaryaModel::query()->delete();
            PadatKaryaModel::insert($data);
            $this->filterDataPadatKaryaDua();

            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message' => 'gagal',
                'error' => $e->getMessage()
            ]);
        }
    }
    public function getDataPeken()
    {
        DB::beginTransaction();
        // PekenModel::query()->delete();
        $url = Http::withHeaders([
            'user' => 'dashboard_peken',
            'password' => '829yTFrw'
        ])->post('https://api.surabaya.go.id/integrasi/api/dashboard/peken/pelaku-usaha')->object();
        $result = $url->data;
        // dd($result);

        try {

            foreach ($result as $key => $item) /* API */ {

                $check_nik = DB::table('peken')->select('ukm_id')->where('ukm_id', $item->ukm_id)->get()->toArray();
                /* $check_nik_testing = DB::table('peken')->select('nik')->where('nik', $item->nik)->where('nik', '=!', 3515182611900006)->get()->toArray();
                dd(empty($check_nik)); */
                if (empty($check_nik)) {

                    $data[$key] = [
                        'ukm_id'                => $item->ukm_id,
                        'nik'                   => $item->nik,
                        'nama_pelaku_usaha'     => $item->nama_pelaku_usaha,
                        'alamat_ktp'            => $item->alamat_ktp,
                        'kelurahan_pemilik'     => $item->kelurahan_pemilik,
                        'tempat_lahir'          => $item->tempat_lahir,
                        'tanggal_lahir'         => $item->tanggal_lahir,
                        'jenis_kelamin'         => $item->jenis_kelamin,
                        'nama_usaha'            => $item->nama_usaha,
                        'alamat_usaha'          => $item->alamat_usaha,
                        'kecamatan_usaha'       => $item->kecamatan_usaha,
                        'kelurahan_usaha'       => $item->kelurahan_usaha,
                        'no_telp'               => $item->no_telp,
                        'sertifikat_merk'       => $item->sertifikat_merk,
                        'sertifikat_halal'      => $item->sertifikat_halal,
                        'pirt'                  => $item->pirt,
                        'peken_tokel'           => $item->peken_tokel,
                        'tgl_tarik'             => date('Y-m-d H:i:s'),
                    ];

                    PekenModel::insert($data[$key]);
                } else {
                    continue;
                }
            }
            $this->filter_peken();
            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'berhasil'
            ]);
        } catch (\Exception $e) {
            DB::rollBack();

            return response()->json([
                'status' => false,
                'message' => 'gagal',
                'error' => $e->getMessage()
            ]);
        }
    }
    public function filterDataPadatKaryaDua()
    {
        DB::beginTransaction();

        $cek_nik = $this->CekDataUmkm();
        // dd($cek_nik);
        for ($i = 0; $i < count($cek_nik); $i++) {
            // dd('masuk sini');
            if ($cek_nik) {
                // dd('masok');
                $cek_data = $this->CekNikIntervensiSelainPadatKarya($cek_nik[$i]->nik);
                if ($cek_data) {
                    try {
                        //code...
                        // dd($cek_nik[$i]->nik);
                        // dd($cek_nik[$i]->nik);
                        $this->UpdateNikIntervensiSelainPadatKarya($cek_nik[$i]->nik);
                    } catch (\Exception $e) {
                        return response()->json(['GAGAL Update 1', $e]);
                    }
                } else {
                    $cek = $this->CekNikHanyaPadatKarya($cek_nik[$i]->nik);
                    // dd($cek);
                    if (!empty($cek)) {
                        try {
                            //code...
                            $this->Update_PadatKaryaTanpaIntervensiLain(($cek_nik[$i]->nik));
                            // dd($this->Update_PadatKaryaTanpaIntervensiLain(($nik[$i]->nik)));
                        } catch (\Exception $e) {
                            //exception $e;
                            return response()->json(['GAGAL Update 2', $e]);
                        }
                    }
                }
            }
        }
        $nik_pk = db::table('padat_karya')
            ->select('nik')
            ->get();
        for ($i = 0; $i < count($nik_pk); $i++) {
            // dd($nik_pk[$i]->nik);
            // dd($nik_pk);
            $cek_data_lagi = $this->CekNikPadatKaryaDiUmkm($nik_pk[$i]->nik);
            // dd($cek_data_lagi);
            if ($cek_data_lagi) {
                // dd($cek_data_lagi, 'masok');
                // array_push($arr_nik, $cek_data_lagi->nik);

                // dd($nik[$i]->nik);
                try {
                    //code...
                    $update_datas =  TPelakuUsahaDisdag131022Model::Update_PadatKarya($nik_pk[$i]->nik, $cek_data_lagi->pekerjaan);
                } catch (\Exception $e) {
                    return response()->json(['GAGAL Update 3', $e]);
                }
            } else {
                // dd('kesini');
                try {
                    //code...
                    $data = TPelakuUsahaDisdag131022Model::storeDataFilterPadatKarya($nik_pk[$i]->nik);
                } catch (\Exception $e) {
                    //exception $e;
                    return response()->json(['GAGAL Store', $e]);
                }
            }
        }
        DB::commit();
        return response()->json(['Berhasil']);
    }



    public function filter_peken()
    {
        DB::beginTransaction();

        $nik_peken = DB::table('peken')->select('nik')
            // ->where('nik', '1901032102800001')
            // ->where('nik', '3512076705850001')
            ->get();
        for ($i = 0; $i < count($nik_peken); $i++) {
            // dd($nik_peken);
            // dd($nik_peken[$i]->nik);
            try {
                //code...
                $update_keckel_ktp = TPelakuUsahaDisdag131022Model::update_keckel_ktp($nik_peken[$i]->nik);
            } catch (\Exception $e) {
                //exception $e;
                return response()->json('Gagal Update KecKel KTP & Usaha', $e);
            }
            try {
                //code...
                $update_keckel_usaha = TPelakuUsahaDisdag131022Model::update_keckel_usaha($nik_peken[$i]->nik);
            } catch (\Exception $e) {
                //exception $e;
                return response()->json('Gagal Update KecKel Usaha', $e);
            }
        }
        $cek_nik_peken = TPelakuUsahaDisdag131022Model::cek_nik_peken();
        // dd($cek_nik_peken, 'cek');
        for ($i = 0; $i < count($cek_nik_peken); $i++) {
            // dd($cek_nik_peken[$i]->nik_umkm, $cek_nik_peken[$i]->nik_peken);
            if ($cek_nik_peken[$i]->nik_umkm && isEmpty($cek_nik_peken[$i]->nik_peken)) {
                // dd($cek_nik_peken[$i]->nik_umkm, $cek_nik_peken[$i]->nik_peken, 'masok');
                $inter_selain_peken = TPelakuUsahaDisdag131022Model::inter_selain_peken($cek_nik_peken[$i]->nik_umkm);
                // dd($inter_selain_peken, 'tes');
                if ($inter_selain_peken) {
                    try {
                        # code...
                        // dd('masok');
                        $update_inter_nonPeken = TPelakuUsahaDisdag131022Model::update_inter_selain_peken($cek_nik_peken[$i]->nik_umkm);
                        // dd($update_inter_nonPeken);
                    } catch (\Exception $e) {
                        return response()->json(['Gagal Update 1', $e]);
                    }
                } else {
                    # code...
                    // dd($cek_nik_peken[$i]->nik_umkm);
                    $cek_inter_hanya_peken = TPelakuUsahaDisdag131022Model::inter_hanya_peken($cek_nik_peken[$i]->nik_umkm);
                    // dd($cek_inter_hanya_peken);
                    if ($cek_inter_hanya_peken) {
                        // dd($cek_nik_peken[$i]->nik_umkm, 'hay');
                        # code...
                        try {
                            //code...
                            $update_inter_hanya_peken = TPelakuUsahaDisdag131022Model::update_inter_hanya_peken($cek_nik_peken[$i]->nik_umkm);
                            // dd($update_inter_hanya_peken);
                        } catch (\Exception $e) {
                            //exception $e;
                            return response()->json(['Gagal Update 2', $e]);
                        }
                    }
                }
            }
        }
        // dd('stop');

        $nik_peken2 = DB::table('peken')->select('nik')
            // ->where('nik', '1901032102800001')
            // ->where('nik', '3512076705850001')
            // ->where('nik', '3578035305687777')
            ->get();
        // dd($nik_peken2, 'nik di peken');
        for ($i = 0; $i < count($nik_peken2); $i++) {
            # code...
            // dd($nik_peken2[$i]);
            $nikPeken_diumkm = TPelakuUsahaDisdag131022Model::nikPeken_diumkm($nik_peken2[$i]->nik);
            // dd($nikPeken_diumkm, 'nik di umkm');
            if ($nikPeken_diumkm) {
                // dd('masok if');
                // dd($update_nik_pelaku_null, 'update');
                try {
                    //code...
                    // dd($nik_peken2[$i]->nik, 'update');

                    $update_nik_pelaku_null = TPelakuUsahaDisdag131022Model::update_nik_pelaku_null($nikPeken_diumkm);
                    // dd($update_nik_pelaku_null, 'tes hasil update_nik_pelaku_null');
                } catch (\Exception $e) {
                    //exception $e;
                    return response()->json(['Gagal Update 3', $e]);
                }
            } else {
                # code...
                // dd($nik_peken2[$i]);
                try {
                    //code...
                    // dd($nik_peken2[$i]->nik, 'insert');
                    $insert_peken = TPelakuUsahaDisdag131022Model::insert_peken($nik_peken2[$i]->nik);
                    // dd($insert_peken);
                } catch (\Exception $e) {
                    //exception $e;
                    return response()->json(['Gagal Insert Data Peken', $e]);
                }
            }
            // dd($nik_peken2[$i]);
            // dd('masok else');
        }
        DB::commit();
        return response()->json("Berhasil");
    }


    /* LIST FUNCTION FILTER TARIK DATA PADAT KARYA */
    public function CekDataUmkm() /*cek apakah ada data di umkm yang sudah terhapus di padat karya */
    {
        $data = TPelakuUsahaDisdag131022Model::CekDataUmkm();
        // dd($data);
        // return json_encode($data);
        return $data;
    }
    public function CekNikIntervensiSelainPadatKarya($nik) /*jika ada, kemudian cek apakah nik tersebut mendapat intervensi selain padat karya*/
    {
        $data = TPelakuUsahaDisdag131022Model::CekNikIntervensiSelainPadatKarya($nik);

        return $data;
    }
    public function UpdateNikIntervensiSelainPadatKaryaBackup($cek_intervensi) /*jika nik tersebut mendapatkan intervensi selain padat karya, maka melakukan update kolom padat karya menjadi null*/
    {
        $data = TPelakuUsahaDisdag131022Model::UpdateNikIntervensiSelainPadatKarya($cek_intervensi);

        return $data;
    }

    public function UpdateNikIntervensiSelainPadatKarya($nik) /*jika nik tersebut mendapatkan intervensi selain padat karya, maka melakukan update kolom padat karya menjadi null*/
    {
        $data = TPelakuUsahaDisdag131022Model::UpdateNikIntervensiSelainPadatKarya($nik);

        return $data;
    }
    public function CekNikHanyaPadatKarya($nik) /*lakukan pengecekan nik lagi apakah nik tersebut hanya mendapatkan intervensi padat karya*/
    {
        $data = TPelakuUsahaDisdag131022Model::CekNikHanyaPadatKarya($nik);

        return $data;
    }
    public function Update_PadatKaryaTanpaIntervensiLain($nik) /*mengecek apakah ada nik padat karya yang sudah ada di umkm*/
    {
        $data = TPelakuUsahaDisdag131022Model::Update_PadatKaryaTanpaIntervensiLain($nik);

        return $data;
    }
    public function CekNikPadatKaryaDiUmkm($nik)/*mengecek apakah ada nik padat karya yang sudah ada di umkm*/
    {
        $data = TPelakuUsahaDisdag131022Model::CekNikPadatKaryaDiUmkm($nik);

        return $data;
    }
    public function Update_PadatKarya($nik, $pekerjaan)/*jika ada maka melakukan update di kolom padat karya dan ket padat karya sesuai dengan data tarikan */
    {
        $data = TPelakuUsahaDisdag131022Model::Update_PadatKarya($nik, $pekerjaan);

        $this->filterDataPadatKarya();
        // return $data;
    }

    public function storeDataFilterPadatKarya() /* jika tidak ada maka melakukan insert data */
    {
        $data = TPelakuUsahaDisdag131022Model::storeDataFilterPadatKarya($nik);

        return $data;
    }

    /* END LIST FUNCTION FILTER TARIK DATA PADAT KARYA */

    public function dataTableSebaranKategoriUsaha()
    {

        $data = TPelakuUsahaDisdag131022Model::scopeCountSebaranKategoriUsaha('tabel_sebaran_kategori_usaha');

        return DataTables::of($data)
            ->addIndexColumn('scopeCountSebaranKategoriUsaha')
            ->editColumn('jumlah_fashion', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaFashion', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`FASHION`)">' . $data->jumlah_fashion . '</button>
                ';
            })
            ->editColumn('jumlah_toko', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaToko', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`TOKO`)">' . $data->jumlah_toko . '</button>
                ';
            })
            ->editColumn('jumlah_mamin', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaMamin', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`MAKANAN DAN MINUMAN`)">' . $data->jumlah_mamin . '</button>
                ';
            })
            ->editColumn('jumlah_kerajinan', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaKerajinan', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`KERAJINAN`)">' . $data->jumlah_kerajinan . '</button>
                ';
            })
            ->editColumn('jumlah_pertanian', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaPertanian', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`PERTANIAN`)">' . $data->jumlah_pertanian . '</button>
                ';
            })
            ->editColumn('jumlah_jasa', function ($data) {
                return '
                <button type="button" class="btn btn-link" onclick="detailKategoriUsaha(`' . route('detailKategoriUsahaJasa', $data->kecamatan_pemilik) . '`,`' . $data->nm_kec . '`,`JASA`)">' . $data->jumlah_jasa . '</button>
                ';
            })
            ->rawColumns(['jumlah_fashion', 'jumlah_toko', 'jumlah_mamin', 'jumlah_kerajinan', 'jumlah_pertanian', 'jumlah_jasa'])
            ->escapeColumns([])
            ->toJson();
    }

    public function grafikSebaranKategoriUsaha()
    {
        $data = TPelakuUsahaDisdag131022Model::scopeGrafikSebaranKategoriUsaha();

        return json_encode($data);
    }

    public function detailKategoriUsahaFashion($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='FASHION'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailKategoriUsahaToko($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='TOKO'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailKategoriUsahaMamin($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='MAKANAN DAN MINUMAN'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailKategoriUsahaKerajinan($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='KERAJINAN'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailKategoriUsahaPertanian($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='PEMBUDIDAYA SEKTOR PERTANIAN'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailKategoriUsahaJasa($id)
    {
        // $data = TPelakuUsahaDisdag131022Model::scopeDetailKategoriUsahaFashion();
        $data = DB::select(
            "SELECT
            A.nik as nik,
            A.nama_pelaku_usaha as nama_pelaku_usaha,
            A.nama_usaha as nama_usaha,
            A.alamat_ktp as alamat,
            b.nm_kec as kecamatan,
            c.nm_kel as kelurahan,
            A.rw_ktp as rw,
            A.rt_ktp as rt,
            A.nib as nib
            FROM
                t_pelaku_usaha_disdag_181022 a
                left join m_setup_kec b on a.kecamatan_pemilik=b.id_m_setup_kec and b.id_m_setup_prop='35' and b.id_m_setup_kab='78'
                left join m_setup_kel c on a.kelurahan_pemilik=c.id_m_setup_kel and b.id_m_setup_kec=c.id_m_setup_kec and c.id_m_setup_prop='35' and c.id_m_setup_kab='78'
            WHERE
                a.kategori_jenis_produk='JASA'
                and b.id_m_setup_kab = 78
                and b.id_m_setup_prop = 35
                and a.kecamatan_pemilik= " . $id . "
        "
        );
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailIntervensi(Request $request)
    {
        // dd($request->all());
        // $tod = $request->query;
        // dd($request->intervensi, $request->kec);

        $data = TPelakuUsahaDisdag131022Model::ModaldetailIntervensi($request->intervensi, $request->kec);

        // dd($data);
        return DataTables::of($data)
            ->addIndexColumn()
            ->addColumn('aksi', function ($data) {
                return '
                <div class="text-center">
                    <button type="button" class="btn btn-primary btn-sm" onclick="detailNyo(' . $data->nik . ', `' . $data->nama_pelaku_usaha . '`, `' . $data->alamat . '`)" title="Omset `' . $data->nama_pelaku_usaha . '`">
                    <i class="fas fa-search"></i>
                </div>
                ';
            })
            ->rawColumns(['aksi'])
            ->escapeColumns([])
            ->toJson();
    }

    public function detailOmset(Request $request)
    {
        $data = TPelakuUsahaDisdag131022Model::leftJoin('t_omset_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha')
            ->select('t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 as bulan_laporan', 't_pelaku_usaha_disdag_181022.nik', 't_omset_usaha.bulan_laporan', 't_omset_usaha.jumlah_omset as jumlah_omset')
            ->where('t_pelaku_usaha_disdag_181022.nik', $request->nik)
            ->get();

        return DataTables::of($data)
            ->addIndexColumn()
            ->toJson();
    }

    public function detailOmsetGrafik(Request $request)
    {
        // dd($request->nik);
        $data = TPelakuUsahaDisdag131022Model::leftJoin('t_omset_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha')
            ->select('t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022', 't_pelaku_usaha_disdag_181022.nik', 't_omset_usaha.bulan_laporan', 't_omset_usaha.jumlah_omset as jumlah_omset')
            ->where('t_pelaku_usaha_disdag_181022.nik', $request->nik)
            ->orderBy('t_omset_usaha.bulan_laporan', 'asc')
            ->get()->toarray();

        // dd($data);
        // foreach ($data as $key => $value) {
        //     $data['bulan'] = $value['bulan_laporan'];
        //     $data['omset'] = $value['jumlah_omset'];
        // }


        // dd(array_count_values($data['bulan']));
        // dd(array_unique($data));
        return json_encode($data);
    }
}
