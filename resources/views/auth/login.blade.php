<!DOCTYPE html>
<html lang="en-US" dir="ltr">

<!-- Head -->
@include('components.templates.partials.head')

<!-- <style>
    .custom-bg-hut-kota-surabaya {
        /* background transparent */
        background: transparent;
        border: none;
        box-shadow: none;
    }

    .expandIn {
        animation-duration: 0.3s;
        animation-fill-mode: both;
        animation-name: expandIn;
    }
</style> -->

<body>

    <!-- Content -->
    <!-- <div id="popup" style="display:none;">
        <a href="https://surabaya.go.id/id/agenda/73808/semarak-acara-hjks-ke-730">
            <img class="popup-content" src="{{ asset('dist/assets/img/hjks-2023-pop-up.png') }}" alt="Image">
        </a>
        <div style="position: absolute;top: 85px;right: 400px;">
            <button class="btn btn-danger" onclick="closePopup()">X</button>
        </div>
    </div> -->

    <!-- modal element with button close and animation popup -->
    <!-- <div class="modal fade" id="hut_kota_surabaya" tabindex="-1" data-easein="flipXIn" role="dialog" aria-labelledby="hut_kota_surabayaLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1000px;">
            <div class="modal-content position-relative custom-bg-hut-kota-surabaya">
                <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                    <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body p-0">
                    @php $url = 'https://surabaya.go.id/id/agenda/73808/semarak-acara-hjks-ke-730' @endphp
                    <a href="{{ $url }}" target="_blank">
                        <img class="img-fluid rounded-top-lg" src="{{ asset('dist/assets/img/hjks-2023-pop-up.png') }}" alt="Image Description" />
                    </a>
                </div>
            </div>
        </div>
    </div> -->

    <main class="main bg-image" id="top">
        <div class="container-fluid">
            <div class="row min-vh-100 flex-center g-0">
                <div class="col-lg-8 col-xxl-5 py-3 position-relative">
                    <img class="bg-auth-circle-shape" src="{{ asset('dist/assets/img/shape-1.png') }}" alt="" width="250">
                    <img class="bg-auth-circle-shape-2" src="{{ asset('dist/assets/img/shape-2.png') }}" alt="" width="150">
                    <div class="card overflow-hidden z-index-1">
                        <div class="card-body p-0">
                            <div class="row g-0 h-100">
                                <div class="col-md-5 text-center bg-card-gradient bg-ilus">
                                    <div class="position-relative p-4 pt-md-5 pb-md-7 light h-ilus-log">
                                        {{-- <div class="bg-holder bg-auth-card-shape"
                                            style="background-image:url(dist/assets/img/ilus-login.png);">
                                        </div> --}}
                                        <!--/.bg-holder-->

                                        {{-- <div class="z-index-1 position-relative"><a
                                                class="link-light mb-4 font-sans-serif fs-4 d-inline-block fw-bolder"
                                                href="../../../index.html">WKWKWKKW</a>
                                            <p class="opacity-75 text-white">Lorem ipsum dolor sit amet consectetur
                                                adipisicing elit. Nulla deleniti ex deserunt voluptatum soluta culpa,
                                                eius maxime magni quibusdam adipisci.</p>
                                        </div> --}}
                                    </div>
                                    <!-- <div class="mt-3 mb-4 mt-md-4 mb-md-5 light">
                                        <p class="text-white">Don't have an account?<br><a class="text-decoration-underline link-light" href="../../../pages/authentication/card/register.html">Get started!</a></p>
                                        <p class="mb-0 mt-4 mt-md-5 fs--1 fw-semi-bold text-white opacity-75">Read our <a class="text-decoration-underline text-white" href="#!">terms</a> and <a class="text-decoration-underline text-white" href="#!">conditions </a></p>
                                    </div> -->
                                </div>
                                <div class="col-md-7 d-flex flex-center">
                                    <div class="p-4 p-md-5 flex-grow-1">
                                        <div class="row mb-3 d-flex justify-content-center">
                                            <div class="col-auto">
                                                <span style="display: flex; align-items: center;">
                                                    <img src="{{ asset('dist/assets/img/icons/icon-disdag.png') }}" alt="" width="50" height="50">
                                                    <h4 class="mb-0 fw-bold text-danger ms-2">{{ __('UMKM SURABAYA') }}
                                                    </h4>
                                                </span>
                                            </div>
                                        </div>
                                        <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <div class="mb-3">
                                                <label class="form-label" for="name">Username</label>
                                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" />

                                                <!-- Menampilkan validasi error -->
                                                @error('name')
                                                <span class="invalid-feedback">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="mb-3">
                                                <div class="d-flex justify-content-between">
                                                    <label class="form-label" for="password">Password</label>
                                                </div>
                                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password">

                                                <!-- Menampilkan validasi error -->
                                                @error('password')
                                                <span class="invalid-feedback">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>

                                            <div class="mb-3">
                                                {!! NoCaptcha::renderJs('id', false, 'onloadCallback') !!}
                                                {!! NoCaptcha::display() !!}

                                                @if ($errors->has('g-recaptcha-response'))
                                                <span class="text-danger font-sans-serif" style="font-size: 12px;">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                                @endif
                                            </div>

                                            <!--  <div class="row flex-between-center">
                                                <div class="col-auto">
                                                    <div class="form-check mb-0">
                                                        <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                                        <label class="form-check-label" for="remember">
                                                            {{ __('Remember Me') }}
                                                        </label>
                                                    </div>
                                                </div>
                                                @if (Route::has('password.request'))
                                                <div class="col-auto mb-2">
                                                    <a class="fs--1 href="{{ route('password.request') }}">
                                                        {{ __('Forgot Password?') }}
                                                    </a>
                                                </div>
                                                @endif
                                                <div class="col-auto"><a class="fs--1" href="../../../pages/authentication/card/forgot-password.html">Forgot Password?</a></div>
                                            </div> -->

                                            <div class="mb-3">
                                                <button type="submit" class="btn btn-danger d-block w-100 mt-3">
                                                    {{ __('Login') }}
                                                </button>


                                                <!-- <button class="btn btn-primary d-block w-100 mt-3" type="submit" name="submit">Log in</button> -->
                                            </div>
                                        </form>
                                        <!-- <div class="position-relative mt-4">
                                            <hr class="bg-300" />
                                            <div class="divider-content-center">or log in with</div>
                                        </div>
                                        <div class="row g-2 mt-2">
                                            <div class="col-sm-6"><a class="btn btn-outline-google-plus btn-sm d-block w-100" href="#"><span class="fab fa-google-plus-g me-2" data-fa-transform="grow-8"></span> google</a></div>
                                            <div class="col-sm-6"><a class="btn btn-outline-facebook btn-sm d-block w-100" href="#"><span class="fab fa-facebook-square me-2" data-fa-transform="grow-8"></span> facebook</a></div>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <!-- End Content -->


    <!-- Scripts -->
    @include('components.templates.partials.scripts')

</body>

</html>