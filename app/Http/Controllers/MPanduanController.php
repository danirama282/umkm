<?php

namespace App\Http\Controllers;

use App\Models\MPanduan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;


class MPanduanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data['roles'] = auth()->user()->load('roles')->toArray();
        $data['roles'] = $data['roles']['roles'][0]['name'];
        $data['panduan'] = MPanduan::select('id_panduan', 'nama_panduan', 'kategori_panduan', 'file_panduan')->whereNull('deleted_at')->get()->toArray();
        $datatable = $data['panduan'];
        // dd($datatable);
        // dd(str_ireplace('/panduan', '', $datatable->file_panduan));

        /* foreach ($data['panduan'] as $key => $value) {
            # code...
            // $datatable[$key]['id_panduan'] = $value->id_panduan;
            $datatable[$key]['nama_panduan'] = $value->nama_panduan;
            $datatable[$key]['kategori_panduan'] = $value->kategori_panduan;
            dd(Str::replace('panduan/', '', $datatable[$key]['file_panduan'] = $value->file_panduan));
        } */

        $datatable = json_encode($datatable);

        $datatable = json_decode($datatable);
        // dd($datatable);

        if ($request->ajax()) {
            return DataTables($datatable)
                ->addIndexColumn()
                ->editColumn('nama_panduan', function ($datatable) {
                    /* $file_panduan = str_ireplace('panduan/', '', $datatable->file_panduan);
                    return substr($file_panduan, 11); */
                    return ucwords($datatable->nama_panduan);
                })
                ->editColumn('kategori_panduan', function ($datatable) {
                    /* $file_panduan = str_ireplace('panduan/', '', $datatable->file_panduan);
                    return substr($file_panduan, 11); */
                    return ucwords($datatable->kategori_panduan);
                })
                ->addColumn('aksi', function ($row) {
                    $roles = auth()->user()->load('roles')->toArray();
                    $roles = $roles['roles'][0]['name'];

                    if ($roles == "superadmin") {
                        # code...
                        // dd('mwasok');
                        return '
                        <div class="d-flex flex-column bd-highlight mb-3">
                            <a href="' . asset('storage/' . $row->file_panduan) . '" target="_blank" class="btn btn-sm btn-success datatable_btn_edit w-80px">
                            <span class="fas fa-file-alt"></span> Lihat File
                            </a>
                        </div>
                        <div class="d-flex flex-column bd-highlight mb-3">
                            <button class="btn btn-danger" id="delete_data" data-id="' . Crypt::encryptString($row->id_panduan) . '" data-nama="" title="Hapus ">
                            <span class="fas fa-trash-alt" data-fa-transform="shrink-3"></span> Hapus
                            </button>
                        </div>';
                    } else {
                        return '
                        <div class="d-flex flex-column bd-highlight mb-3">
                            <a href="' . asset('storage/' . $row->file_panduan) . '" target="_blank" class="btn btn-sm btn-success datatable_btn_edit w-80px">
                            <span class="fas fa-file-alt"></span> Lihat File
                            </a>
                        </div>';
                    }
                })
                ->rawColumns(['aksi', 'file_panduan'])
                ->make(true);
            # code...
        }
        // dd($data);
        return view('buku_panduan.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreMPanduanRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->file('file_panduan')) {
            // dd($request->all());
            // dd('hehe');
            $panduan = new MPanduan;
            $panduan->nama_panduan = $request->nama_panduan;
            $panduan->kategori_panduan = $request->kategori_panduan;
            $panduan->updated_at =  null;

            // $panduan->save();

            $filename = time() . '_' . $request->file_panduan->getClientOriginalName();
            // $filename =  str_ireplace('.pdf', '', $request->file_panduan->getClientOriginalName()) . '_' . time() . '.pdf';
            $folder = "panduan";
            $file_path = Storage::disk('public')->putFileAs($folder, $request->file('file_panduan'), $filename);
            $panduan->file_panduan = $file_path;

            // dd($panduan->toArray());


            $panduan->save();
            return response()->json([
                'status' => true,
                'pesan'  => "Berhasil Simpan Data",
            ]);
        } else {
            // dd('uhuy');
            $panduan = new MPanduan;
            $panduan->nama_panduan = $request->nama_panduan;
            $panduan->kategori_panduan = $request->kategori_panduan;

            $panduan->save();

            return response()->json([
                'status' => true,
                'pesan'  => "Berhasil Simpan Data",
            ]);
        }
        return response()->json([
            'status' => false,
            'pesan'  => 'Maaf, Data Gagal Tersimpan'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\MPanduan  $mPanduan
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\MPanduan  $mPanduan
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateMPanduanRequest  $request
     * @param  \App\Models\MPanduan  $mPanduan
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\MPanduan  $mPanduan
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        //
        // dd($request->all());
        DB::beginTransaction();
        $panduan = MPanduan::find(Crypt::decryptString($request->id));
        // dd($panduan->toArray());
        if ($panduan->delete()) {
            DB::commit();
            return response()->json([
                'status' => true,
                'pesan'  => "Data Terhapus",
            ]);
        } else {
            return response()->json([
                'status' => false,
                'pesan'  => 'Maaf, Data Gagal Terhapus'
            ]);
        }
    }
}