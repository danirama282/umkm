<x-templates.default>
    <x-slot name="title">Entri Data KBLI</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Entri Data KBLI</h5>
                    </div>
                </div>
                <div class="card-body">
                    <form id="formEntriKbli">
                        @method('PUT')
                        @csrf
                        <!-- input hidden $id_disdag -->
                        <input type="hidden" name="id_disdag" id="id_disdag" value="{{ $id_disdag }}">
                        <!-- form structure select2 option -->
                        <div class="row mb-3">
                            <div class="col-md-6">
                                <label for="id_m_kategori_kbli" class="form-label">Kategori KBLI</label>
                                <select class="form-select js-select2" id="id_m_kategori_kbli" name="id_m_kategori_kbli">
                                    <option value="">- Pilih -</option>
                                    @foreach ($data['kategori_kbli'] as $item)
                                    <option value="{{ $item->id_m_kategori_kbli }}">
                                        {{ $item->kode_kategori_kbli }} - {{ $item->nama_kategori_kbli }}
                                    </option>
                                    @endforeach
                                </select>
                               <!-- div #fb_ for display some error notifications -->
                               <div class="mt-1 fs--1 fw-semi-bold font-sans-serif text-danger" id="fb_id_m_kategori_kbli"></div>
                            </div>
                            <div class="col-md-6">
                                <label for="id_m_klasifikasi_kbli" class="form-label">Klasifikasi KBLI</label>
                                <select class="form-select js-select2" id="id_m_klasifikasi_kbli" name="id_m_klasifikasi_kbli" disabled>

                                </select>
                                <!-- div #fb_ for display some error notifications -->
                               <div class="mt-1 fs--1 fw-semi-bold font-sans-serif text-danger" id="fb_id_m_kategori_kbli"></div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('klasifikasi_kbli.index', $id_disdag) }}" class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                            <button class="btn btn-primary me-1 mb-1 btn_filter" type="submit"><span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <meta name="csrf-token" content="{{ csrf_token() }}">

    @push('scripts')
    <!-- script ajax on change for -->
    <script>
        $('[name="id_m_kategori_kbli"]').on('change', function(e) {
            $.ajax({
                    type: "POST",
                    url: "{{ route('klasifikasi_kbli.getDataKlasifikasiKbli') }}",
                    dataType: "json",
                    data: {
                        '_token': "{{ csrf_token() }}",
                        id: $('[name="id_m_kategori_kbli"]').val(),
                    }
                })
                .done(function(response) {
                    if (response.status == true) {
                        $('[name="id_m_klasifikasi_kbli"]').prop('disabled', false);

                        var render = '<option value="" selected disabled>- Pilih -</option>';
                        for (var i = 0; i < response.data.length; i++) {
                            render += '<option value="' + response.data[i].id_m_klasifikasi_kbli + '">' + response.data[i].kode_klasifikasi_kbli + ' - ' + response.data[i].nama_klasifikasi_kbli + '</option>';
                        }

                        $('[name="id_m_klasifikasi_kbli"]').html(render);
                    } else {
                        /* Swal fire */
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                        });
                    }
                })
        });

        /* ajax form id="formEntriKbli" */
        $('#formEntriKbli').on('submit', function(e) {
            e.preventDefault();
            $.ajax({
                /* method PUT */
                type: "PUT",
                url: "{{ route('klasifikasi_kbli.update', $id_disdag) }}",
                dataType: "json",
                data: {
                    '_token': "{{ csrf_token() }}",
                    id_m_kategori_kbli: $('[name="id_m_kategori_kbli"]').val(),
                    id_m_klasifikasi_kbli: $('[name="id_m_klasifikasi_kbli"]').val(),
                }
            })
            .done(function(response) {
                console.log(response);
                if (response.status == true) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Berhasil',
                        text: response.message,
                    }).then((result) => {
                        if (result.isConfirmed) {
                            window.location.href = "{{ route('klasifikasi_kbli.index', $id_disdag) }}";
                        }
                    });
                } else {
                   console.log(response);
                    
                }
            }).fail(function(response) {
                console.log(response);
                var errors = response.responseJSON.errors;
                $.each(errors, function(key, value) {
                    $('#fb_' + key).html(value);
                });
            });
        });

    </script>
    @endpush
</x-templates.default>