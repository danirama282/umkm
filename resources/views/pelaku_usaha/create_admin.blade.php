<div class="row g-0">
    <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
        <div class="card">
            <div class="card-header">
                <h5 class="fw-bold fs-1 mt-2">Entri Data Pelaku Usaha</h5>
            </div>
            <div class="card-body ">
                <form id="formTambahPelakuUsaha" multipart="enctype/form-data">
                    @csrf
                    <input type="hidden" name="is_surabaya">
                    {{-- {{ dd($data['provinsi']->id_m_setup_prop, $data['kabupaten'][0]->id_m_setup_kab) }} --}}
                    <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                    <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">
                    <div class="row mb-3">
                        <div class="col-sm-12">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Pelaku Usaha
                            </h6>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="status">Status
                            Kependudukan</label>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" id="surabaya" type="radio"
                                    name="status" value="1" onclick="showButton()" /><label class="form-check-label"
                                    for="surabaya">Surabaya</label>
                            </div>
                            <div class="form-check"><input class="form-check-input" id="nonSurabaya" type="radio"
                                    name="status" value="0" onclick="hideButton()" /><label class="form-check-label"
                                    for="nonSurabaya">Non
                                    Surabaya</label></div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nik">NIK Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input class="form-control" id="nik" name="nik" type="number" />
                                <button class="btn btn-sm btn-primary" type="button" id="cek_nik"
                                    style="display:none;"><i class="fas fa-search"></i> Cari NIK </button>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_pelaku_usaha">Nama
                            Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="nama_pelaku_usaha" name="nama_pelaku_usaha" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_ktp">Alamat
                            Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="alamat_ktp" name="alamat_ktp" type="text" />
                        </div>
                    </div> {{-- {{ dd($data['kecamatan']) }} --}}
                    {{-- {{ dd($data) }} --}}
                    <div class="row mb-3 provinsi">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="provinsi_pelaku">Provinsi
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="provinsi" name="provinsi"
                                data-control="select2" data-placeholder="- Pilih Provinsi -">
                                <option value="" selected disabled>Pilih :</option>
                                @foreach ($data['provinsi_non_sby'] as $prov)
                                {{-- {{ dd($data->id_m_setup_prop) }} --}}
                                <option value="{{ $prov->id_m_setup_prop }}">
                                    {{ $prov->nm_prop }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3 kabupaten_kota">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kabupaten_kota">Kabupaten
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kabupaten_kota"
                                name="kabupaten_kota" data-control="select2"
                                data-placeholder="- Pilih Kabupaten/Kota -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan">Kecamatan
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kecamatan" name="kecamatan"
                                data-control="select2" data-placeholder="- Pilih Kecamatan -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="kecamatan_string" id="kecamatan_string">
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan">Kelurahan
                            Pelaku Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <select class="form-select form-control form-control-solid" id="kelurahan" name="kelurahan"
                                data-control="select2" data-placeholder="- Pilih Kelurahan -">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <input type="hidden" name="kelurahan_string" id="kelurahan_string">
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="rt_ktp">RT Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="rt_ktp" name="rt_ktp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="rw_ktp">RW Pelaku
                            Usaha (KTP)</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="rw_ktp" name="rw_ktp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="tanggal_lahir">Tanggal
                            Lahir Pelaku Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" type="date" id="tanggal_lahir" name="tanggal_lahir">
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_domisili">Alamat
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <input type="checkbox" name="check_alamat_domisili" id="check_alamat_domisili">
                            <label class="form-check-label">Sesuai Dengan KTP</label>
                            <input class="form-control" id="alamat_domisili" name="alamat_domisili" type="text" />
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_domisili">Kecamatan
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-select js-select2"
                                aria-label="Default select example">
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_domisili">Kelurahan
                            Pelaku
                            Usaha (Domisili)</label>
                        <div class="col-sm-10">
                            <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-select js-select2"
                                aria-label="Default select example">
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="no_telp">No. Telepon
                            Pelaku
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="no_telp" name="no_telp" type="number" />
                        </div>
                    </div>


                    <div class="row mb-3">
                        <div class="col-sm-12 mt-3">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Usaha</h6>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_usaha">Nama
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="nama_usaha" name="nama_usaha" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_usaha">Alamat
                            Usaha</label>
                        <div class="col-sm-10">
                            <!-- Text Area bootstrap 5 -->
                            <textarea class="form-control" name="alamat_usaha" id=alamat_usaha"></textarea>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_usaha">Kecamatan
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kecamatan_usaha" id="kecamatan_usaha" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Menjalankan foreach dari $data['kecamatan'] -->
                                @foreach ($data['kecamatan'] as $kecamatan)
                                <option value="{{ $kecamatan->id_m_setup_kec }}">{{ $kecamatan->nm_kec }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_usaha">Kelurahan
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kelurahan_usaha" id="kelurahan_usaha" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kategori_jenis_produk">Kategori
                            Usaha</label>
                        <div class="col-sm-10">
                            <select name="kategori_jenis_produk" id="kategori_jenis_produk"
                                class="form-select js-select2" aria-label="Default select example">
                                <option value="" selected disabled>Pilih Kategori Usaha</option>
                                <option value="FASHION">Fashion</option>
                                <option value="JASA">Jasa</option>
                                <option value="KERAJINAN">Kerajinan</option>
                                <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                                <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian</option>
                                <option value="TOKO">Toko</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="jenis_produk">Produk
                            Usaha</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="jenis_produk" name="jenis_produk" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="skala_pemasaran">Skala
                            Pemasaran</label>
                        <div class="col-sm-10">
                            <select name="skala_pemasaran" id="skala_pemasaran" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <option value="LOKAL">Lokal</option>
                                <option value="REGIONAL">Regional</option>
                                <option value="NASIONAL">Nasional</option>
                                <option value="INTERNASIONAL">Internasional</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="status_bangunan">Status
                            Bangunan</label>
                        <div class="col-sm-10">
                            <select name="status_bangunan" id="status_bangunan" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <option value="MILIK SENDIRI">Milik Sendiri</option>
                                <option value="SEWA">Sewa</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="tahun_binaan">Tahun
                            Binaan</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="tahun_binaan" name="tahun_binaan" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="sumber_permodalan">Sumber
                            Permodalan</label>
                        <div class="col-sm-10">
                            <select name="sumber_permodalan" id="sumber_permodalan" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Option yang berisi nilai boolean -->
                                <option value="SENDIRI">Sendiri</option>
                                <option value="PINJAMAN BANK">Pinjaman Bank</option>
                                <option value="PINJAMAN NON BANK">Pinjaman Non Bank</option>
                                <option value="HIBAH">Hibah</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="jumlah_permodalan">Jumlah
                            Permodalan</label>
                        <div class="col-sm-10">
                            <div class="input-group mb-3"><span class="input-group-text">Rp.</span><input
                                    class="form-control" type="number" id="jumlah_permodalan"
                                    name="jumlah_permodalan" /></div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="omset_awal">Omset
                            Awal</label>
                        <div class="col-sm-10">
                            <div class="input-group mb-3"><span class="input-group-text">Rp.</span><input
                                    class="form-control" type="number" id="omset_awal" name="omset_awal" />
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="kapasitas_produksi">Kapasitas
                            Produksi</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="kapasitas_produksi" name="kapasitas_produksi"
                                type="number" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="biaya_produksi">Biaya
                            Produksi</label>
                        <div class="col-sm-10">
                            <div class="input-group mb-3"><span class="input-group-text">Rp.</span><input
                                    class="form-control" type="number" id="biaya_produksi" name="biaya_produksi" />
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="jumlah_tenaga_kerja">Jumlah
                            Tenaga
                            Kerja</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="jumlah_tenaga_kerja" name="jumlah_tenaga_kerja"
                                type="number" />
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="col-form-label-sm" for="bahan_pokok">Bahan
                                    Pokok</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check"><input class="form-check-input" name="ayam_bebek"
                                        id="ayam_bebek" type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Ayam / Bebek</label></div>
                                <div class="form-check"><input class="form-check-input" name="beras" id="beras"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Beras</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="buah" id="buah"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Buah</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="cabai" id="cabai"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Cabai</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="daging" id="daging"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Daging</label></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check"><input class="form-check-input" name="gula" id="gula"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Gula</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="kain" id="kain"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Kain</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="kayu" id="kayu"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Kayu</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="kedelai" id="kedelai"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Kedelai</label></div>
                                <div class="form-check"><input class="form-check-input" name="minyak" id="minyak"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Minyak</label></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check"><input class="form-check-input" name="sayur" id="sayur"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Sayur</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="tepung" id="tepung"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Tepung</label></div>
                                <div class="form-check"><input class="form-check-input" name="telur" id="telur"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Telur</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="keanggotaan_koperasi">Status
                            Anggota Koperasi</label>
                        <div class="col-sm-10">
                            <select name="keanggotaan_koperasi" id="keanggotaan_koperasi" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Option yang berisi nilai boolean -->
                                <option value="YA">YA</option>
                                <option value="TIDAK">TIDAK</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="status_keaktifan">Status
                            Keaktifan</label>
                        <div class="col-sm-10">
                            <select name="status_keaktifan" id="status_keaktifan" class="form-select js-select2"
                                aria-label="Default select example">
                                <option value="" selected disabled>Pilih :</option>
                                <!-- Option yang berisi nilai boolean -->
                                <option value="AKTIF">AKTIF</option>
                                <option value="TIDAK AKTIF">TIDAK AKTIF</option>
                                <option value="LIBUR">LIBUR</option>
                            </select>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <div class="col-sm-12 mt-3">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Lain Lain</h6>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="npwp">Nomor
                            NPWP</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="npwp" name="npwp" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="nib">Nomor NIB</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="nib" name="nib" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="iumk">Nomor IUMK</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="iumk" name="iumk" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="pirt">Nomor PIRT</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="pirt" name="pirt" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="bpom">Nomor BPOM</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="bpom" name="bpom" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="sertifikat_merk">Sertifikat
                            Merk</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="sertifikat_merk" name="sertifikat_merk" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="sertifikat_halal">Sertifikat
                            Halal</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="sertifikat_halal" name="sertifikat_halal" type="text" />
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="haki">HAKI</label>
                        <div class="col-sm-10">
                            <input class="form-control" id="haki" name="haki" type="text" />
                        </div>
                    </div>
                    <!-- Sub judul dengan garis -->
                    <div class="row mb-3">
                        <div class="col-sm-12 mt-3">
                            <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Intervensi</h6>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-sm-2">
                                <label class="col-form-label-sm" for="intervensi">Intervensi Yang
                                    Diberikan</label>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check"><input class="form-check-input" name="csr" id="csr"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">CSR</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="kur" id="kur"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">KUR</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="pameran" id="pameran"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Pameran</label></div>
                                <div class="form-check"><input class="form-check-input" name="industri_rumahan"
                                        id="industri_rumahan" type="checkbox" value="1" /><label
                                        class="form-check-label" for="">Industri Rumahan</label></div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-check"><input class="form-check-input" name="pasar" id="pasar"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">Pasar</label>
                                </div>
                                {{-- <div class="form-check"><input class="form-check-input" name="padat_karya"
                                        id="padat_karya" type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Padat Karya</label></div> --}}
                                <div class="form-check"><input class="form-check-input" name="puspita" id="puspita"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Puspita</label></div>
                                <div class="form-check"><input class="form-check-input" name="swk" id="swk"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">SWK</label>
                                </div>
                                <div class="form-check"><input class="form-check-input" name="skg" id="skg"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">SKG</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                {{-- <div class="form-check"><input class="form-check-input" name="peken_tokel"
                                        id="peken_tokel" type="checkbox" value="1" /><label class="form-check-label"
                                        for="">PEKEN Tokel</label></div>
                                <div class="form-check"><input class="form-check-input" name="peken_tokel"
                                        id="peken_tokel" type="checkbox" value="2" /><label class="form-check-label"
                                        for="">PEKEN UKM</label></div> --}}
                                <div class="form-check"><input class="form-check-input" name="rumah_kreatif"
                                        id="rumah_kreatif" type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Rumah Kreatif</label></div>
                                <div class="form-check"><input class="form-check-input" name="pelatihan" id="pelatihan"
                                        type="checkbox" value="1" /><label class="form-check-label"
                                        for="">Pelatihan</label></div>
                                <div class="form-check"><input class="form-check-input" name="oss" id="oss"
                                        type="checkbox" value="1" /><label class="form-check-label" for="">OSS</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-3">
                        <label class="col-sm-2 col-form-label col-form-label-sm" for="keterangan">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" id="keterangan" name="keterangan" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                            <span class="fas fas fa-backward me-2" data-fa-transform="shrink-3"></span> Kembali
                        </a>
                        <button class="btn btn-primary me-1 mb-1" type="submit" id="submit" name="submit">
                            <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>