<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MKategoriUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create Kategori Usaha dari MKategoriUsahaModel
        $kategori_usaha = [
            [
                'nama_kategori_usaha' => 'PERDAGANGAN',
            ],
            [
                'nama_kategori_usaha' => 'JASA',
            ],
            [
                'nama_kategori_usaha' => 'INDUSTRI',
            ],
        ];

        // Looping untuk insert data ke database
        foreach ($kategori_usaha as $key => $value) {
            \App\Models\MKategoriUsahaModel::create($value);
        }
    }
}
