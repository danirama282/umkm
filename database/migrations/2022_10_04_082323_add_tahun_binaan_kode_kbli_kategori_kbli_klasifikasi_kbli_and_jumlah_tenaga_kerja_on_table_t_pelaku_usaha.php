<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTahunBinaanKodeKbliKategoriKbliKlasifikasiKbliAndJumlahTenagaKerjaOnTableTPelakuUsaha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->string('tahun_binaan', 255)->nullable()->default(null)->after('m_tempat_usaha_id');
            $table->string('kode_kbli', 255)->nullable()->default(null)->after('tahun_binaan');
            $table->string('kategori_kbli', 255)->nullable()->default(null)->after('kode_kbli');
            $table->string('klasifikasi_kbli', 255)->nullable()->default(null)->after('kategori_kbli');
            $table->integer('jumlah_tenaga_kerja')->nullable()->default(null)->after('klasifikasi_kbli');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
        });
    }
}
