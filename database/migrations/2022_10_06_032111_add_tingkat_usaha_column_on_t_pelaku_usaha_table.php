<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTingkatUsahaColumnOnTPelakuUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->string('tingkat_usaha', 255)->after('m_kategori_usaha_id')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->dropColumn('tingkat_usaha');
        });
    }
}
