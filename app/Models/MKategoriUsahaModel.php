<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MKategoriUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_kategori_usaha';
    protected $fillable = [
        'nama_kategori_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function sub_kategori_usaha()
    {
        return $this->hasMany(MSubKategoriUsahaModel::class, 'm_kategori_usaha_id', 'id');
    }

    public function pelaku_usaha()
    {
        return $this->hasMany(TPelakuUsahaModel::class, 'm_kategori_usaha_id', 'id');
    }
}
