<x-templates.default>
    <x-slot name="title">Daftar Pelaku Usaha</x-slot>
    {{-- {{ dd($data_user['user']->nama_user) }} --}}
    <!-- Konten baris keempat -->
    @hasanyrole('superadmin|admin')
    <div class="row g-0 mb-3">
        <div class="col-sm-6 col-lg-4 mb-2 ">
            <a href="javascript:void(0)">
                <div class="card text-white bg-primary py-3" onclick="reload()">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Jumlah Pelaku Usaha</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['count_total_pelaku'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-3">
            <a href="javascript:void(0)">
                <div class="card text-white bg-success py-3" onclick="btn_sudah_verif()">
                    <input type="hidden" name="sudah" id="sudah" value="sudah">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Sudah Verifikasi</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['sudah_verif'][0]->jum_sudah_verif }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-3">
            <a href="javascript:void(0)">
                <div class="card text-white bg-danger py-3" onclick="btn_belum_verif()">
                    <input type="hidden" name="belum" id="belum" value="belum">
                    <div class="card-body d-flex justify-content-center text-center">
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2 text-white">Belum Verifikasi</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1 text-white">
                                    {{ $data['belum_verif'][0]->jum_belum_verif }}

                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row g-0 mb-3">
        <div class="card">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Filter Kecamatan Kelurahan Pelaku Usaha</h5>
                    </div>
                </div>
                <div class="card-body position-relative">

                    <form id="formMonitorPelakuUsaha">
                        @csrf
                        <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                        <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">

                        <div class="row">
                            <div class="row mb-3">
                                <label class="col-12 col-md-2 col-lg-2 col-form-label col-form-label"
                                    for="kecamatan">Kecamatan</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select name="kecamatan" id="kecamatan" class="form-select js-select2"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Pilih Kecamatan</option>
                                        @foreach ($data['kecamatan'] as $kecamatan)
                                        <option value="{{ $kecamatan->id_m_setup_kec }}">{{ $kecamatan->nm_kec }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-12 col-md-2 col-lg-2 col-form-label col-form-label"
                                    for="kelurahan">Kelurahan</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select name="kelurahan" id="kelurahan" class="form-select js-select2"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Pilih Kelurahan</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for=""
                                    class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Intervensi</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="intervensi" id="intervensi">
                                        <option value="" selected disabled>Pilih Intervensi Usaha</option>
                                        <option value="csr">CSR</option>
                                        <option value="industri_rumahan">Industri Rumahan</option>
                                        <option value="kur">KUR</option>
                                        <option value="pameran">Pameran</option>
                                        <option value="pasar">Pasar</option>
                                        <option value="puspita">Puspita</option>
                                        <option value="padat_karya">Padat Karya</option>
                                        <option value="pelatihan">Pelatihan</option>
                                        <option value="peken_tokel">Peken</option>
                                        <option value="rumah_kreatif">Rumah Kreatif</option>
                                        <option value="swk">SWK</option>
                                        <option value="skg">SKG</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Kategori
                                    Usaha</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="kategori_usaha" id="kategori_usaha">
                                        <option value="" selected disabled>Pilih Kategori Usaha</option>
                                        <option value="FASHION">Fashion</option>
                                        <option value="JASA">Jasa</option>
                                        <option value="KERAJINAN">Kerajinan</option>
                                        <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                                        <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian
                                        </option>
                                        <option value="TOKO">Toko</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-2">
                                <div class="col">
                                    <div class="d-flex justify-content-start">
                                        <!-- label mini size Pilih atau Ketikkan Nama Instansi -->
                                        <h6 class="fw-bold fst-italic mb-0 text-danger" style="font-size: 10pt;">
                                            * Tombol Hapus akan tampil apabila tidak ada data omset usaha pada pelaku usaha tersebut</h6>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a class="btn btn-secondary me-1 mb-1 reset" id="reset">
                                <span data-fa-transform="shrink-3"></span> Reset
                            </a>
                            <a href="javascript:void(0)" class="btn btn-info me-1 mb-1 filter" type="button">
                                <span data-fa-transform="shrink-3"></span> Filter
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row g-0">
        <div class="card">
            <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Daftar Pelaku Usaha</h5>
                    </div>

                    {{-- <div class="btn-group">
                    </div> --}}
                </div>

                <div class="card-body">
                    {{-- <div class="btn-excel text-right" style="float:right; mb-3">
                        <a class="btn btn-success">Export Excel</a>
                    </div> --}}
                    <div class="d-flex flex-column align-content-md-end align-items-end">
                        <a href="{{ route('pelaku_usaha.create') }}" class="btn btn-primary my-3">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Tambah Data Pelaku
                            Usaha
                        </a>
                        {{-- <a href="{{ route('pelaku_usaha.excel_omset') }}" class="btn btn-success my-3">
                            <span class="far fa-file-excel" data-fa-transform="shrink-3"></span> Export Excel Omset UM
                        </a> --}}

                        {{-- <button class="btn btn-success my-3" id="exportExcel" ><i class="far fa-file-excel"></i> Export Excel Omset UM</button> --}}

                    </div>
                    {{-- <div class="d-flex   ">
                    </div> --}}
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th style="width: 20px;">No.</th>
                                    <th style="width: 100px;">NIK</th>
                                    <th style="width: 150px;">Nama Pelaku Usaha</th>
                                    <th style="width: 100px;">Nama Usaha</th>
                                    <th style="width: 250px;">Alamat</th>
                                    <th style="width: 100px;">Kategori Usaha</th>
                                    {{-- <th>Status Keaktifan</th> --}}
                                    <th style="width: 150px;">Intervensi</th>
                                    <th style="width: 150px;">Status</th>
                                    <th style="width: 130px;">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole

    {{-- @role('admin')
    @include('pelaku_usaha.index_admin')
    @endrole --}}

    @role('user')
    @include('pelaku_usaha.index_user')
    @endrole

    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>

    <script>
        table("");

        function reload() {
            table("");
        }

        $('#exportExcel').click(function (e) { 
            e.preventDefault();

            Swal.fire({
                title: 'Mohon Tunggu!',
                html: '<h6 class="text-center font-sans-serif">Sedang memproses data...</h6>',
                // timer: 1000,
                // timerProgressBar: true,
                allowOutsideClick: false,
                allowEscapeKey: false,
                didOpen: () => {
                    Swal.showLoading()
                },
                willClose: () => {
                    // windows location href to set urls
                    // window.location.href = url;
                }
            });

            fetch('{{ route("pelaku_usaha.excel_omset") }}')
            .then(function (response) {
                if (response.ok) {
                    return response.blob().then(function (blob) {
                        const url = window.URL.createObjectURL(blob);
                        const a = document.createElement('a');
                        
                        var dateRaw = new Date();
                        dateStart = dateRaw.getDate() + '/' + (dateRaw.getMonth() + 1) + '/' + dateRaw.getFullYear();

                        a.style.display = 'none';
                        a.href = url;
                        // the filename you want
                        a.download = 'omset_umkm_per_'+ dateStart +'.xlsx';
                        document.body.appendChild(a);
                        a.click();
                        window.URL.revokeObjectURL(url);
                        // alert('your file has downloaded!'); // or you know, something with better UX...

                        Swal.close();
                    })
                }else
                {
                    return response.json().then(function(response) {
                        // ...
                        console.log(response);
                        Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong! ' + response.error,
                        })
                    });
                }
            })
            .catch(function(error) {
                // console.log('There has been a problem with your fetch operation: ', error.message);
                Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Something went wrong! ',
                })
            });
        });


        var status = null;

        function btn_belum_verif() {
            status = $('#belum').val();
            // $('#dataTable').DataTable().ajax.reload();
            // return status;
            table(status);

        }
        // console.log(btn_belum_verif());

        function btn_sudah_verif() {
            status = $('#sudah').val();
            // $('#dataTable').DataTable().ajax.reload();
            table(status);
            // return status;
        }

        // $('.filter').click(function() {
        //     var table = $('#datatable').DataTable();
        //     table.ajax.reload();
        // });

        $('.reset').on("click", function () {
            $('#kecamatan').val('');
            $('#kecamatan').trigger('change');
            $('#kelurahan').prop('disabled', true);
            $('#kelurahan').html('<option value="" selected disabled>Pilih Kelurahan</option>');
            $('#intervensi').val('');
            $('#intervensi').trigger('change');
            $('#kategori_usaha').val('');
            $('#kategori_usaha').trigger('change');
            $('#dataTable').DataTable().ajax.reload();
        });


        function table(status) {
            var table = null;
            var url = "{{ route('pelaku_usaha.datatable') }}";


            // var status_belum_verif = null;
            // var status_sudah_verif = null;
            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                bDestroy: true,
                ajax: {
                    url,
                    type: 'POST',
                    data: function (d) {
                        d.status = status;
                        d.kecamatan = $('#kecamatan').val();
                        d.kelurahan = $('#kelurahan').val();
                        d.intervensi = $('#intervensi').val();
                        d.kategori_usaha = $('#kategori_usaha').val();
                        d._token = "{{ csrf_token() }}";
                        // etc
                    },
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        name: 'nik',

                    },
                    {
                        data: 'nama_pelaku_usaha',
                        name: 'nama_pelaku_usaha',


                    },
                    {
                        data: 'nama_usaha',
                        name: 'nama_usaha',

                    },
                    {
                        data: null,
                        render: function (data, type, row) {
                            var mergeAlamat = '';
                            if (data.alamat_pemilik != null && data.alamat_pemilik != 0) {
                                mergeAlamat += data.alamat_pemilik;
                            }
                            if (data.kecamatan != null) {
                                mergeAlamat += ' , KECAMATAN ' + data.kecamatan;

                            }
                            if (data.kelurahan != null) {
                                mergeAlamat += ' , KELURAHAN ' + data.kelurahan;
                            }
                            return mergeAlamat;
                        }

                    },
                    {
                        data: 'kategori_usaha',
                        name: 'kategori_usaha',

                    },
                    /*
                                        {
                                            data: 'status_keaktifan',
                                            name: 'status_keaktifan',

                                        }, */
                    {
                        data: 'intervensi',
                        name: 'intervensi',
                        // orderable: false,
                        // searchable: false

                    },
                    {
                        // data: 'tgl_verif',
                        data: null,
                        render: function (data, type, row) {
                            var status = '';
                            var verif = data.tgl_verif;
                            var keaktifan = data.status_keaktifan;
                            // let newStr = str.replace(/_/g, " ");
                            if (verif != null) {
                                status +=
                                    '<span class="badge badge rounded-pill badge-soft-success">Terverifikasi Kec<i class="fas fa-check fa-w-16 ms-2"></i></span> <br>'
                            } else {
                                status +=
                                    '<span class="badge badge rounded-pill badge-soft-secondary">Belum Terverifikasi Kec<i class="fas fa-ban fa-w-16 ms-2"></i></span> <br>'
                            }

                            if (keaktifan != null) {
                                if (keaktifan == "TIDAK AKTIF") {
                                    status +=
                                        '<span class="badge badge rounded-pill badge-soft-danger">Tidak Aktif<i class="fas fa-ban fa-w-16 ms-2"></i></span> <br>'

                                } else if (keaktifan == "AKTIF") {
                                    status +=
                                        '<span class="badge badge rounded-pill badge-soft-success">Aktif<i class="fas fa-check fa-w-16 ms-2"></i></span> <br>'
                                }
                            } else if (keaktifan == null) {
                                status +=
                                    '<span class="badge badge rounded-pill badge-soft-secondary">Belum Ada Status Keaktifan<i class="fas fa-question-circle fa-w-16 ms-2"></i></span> <br>'
                            }
                            return status;
                        },
                        orderable: false,
                        searchable: false

                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50],
                    [5, 10, 25, 50]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    searchPlaceholder: "Nama Pelaku Usaha/Usaha",
                    "zeroRecords": "Tidak ada data",
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
                /* order: [
                    [3, 'asc'],
                ] */
            });

            $('.filter').click(function () {
                $('#dataTable').DataTable().ajax.reload();
            });


            $('select[name="kelurahan"]').prop('disabled', true);

            $('[name="kecamatan"]').on('change', function () {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function (data) {
                            console.log(data);
                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render = '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel + '">' +
                                    data
                                    .data[i].nm_kel + '</option>';
                            }
                            console.log(render);
                            $('[name="kelurahan"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan"]').empty();
                }
            });

        }

        $('#dataTable').on('click', '#delete_data', function () {
            var id = $(this).data('id');
            var nik = $(this).data('nik');
            console.log(id,nik);
            // var nama_instansi = $(this).data('nama_instansi');

            swal.fire({
                    title: 'Apakah Anda Yakin ?',
                    text: 'Menghapus data ini ?',
                    // text: 'Menghapus '+nama_instansi+' dari data instansi',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                })
                .then(function (e) {
                    if (e.value) {
                        $.ajax({
                            url: "{{ route('pelaku_usaha.destroy') }}",
                            method: 'delete',
                            data: {
                                'id': id,
                                'nik': nik,
                                '_token': '{{ csrf_token() }}',
                            },
                            // data: {id: id},
                            // headers: { 'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content') },
                            success: function (data) {
                                if (data.status == 'success') {
                                    swal.fire('Deleted!', 'berhasil menghapus data !',
                                            'success')
                                        .then(function (e) {
                                            // $('#dataTable').DataTable().ajax.reload();
                                            window.location.reload();
                                        });
                                }
                                else{
                                    swal.fire(data.pesan, data.desc,'error')
                                        .then(function (e) {
                                            // $('#dataTable').DataTable().ajax.reload();
                                            window.location.reload();
                                        });
                                }
                            },
                            error: function () {
                                swal.fire("Telah terjadi kesalahan pada sistem",
                                    "Mohon refresh halaman browser Anda", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                })
        });

    </script>
    @endpush

</x-templates.default>
