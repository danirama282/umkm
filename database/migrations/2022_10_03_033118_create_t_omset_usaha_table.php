<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTOmsetUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_omset_usaha', function (Blueprint $table) {
            $table->bigIncrements('id');
            /* Membuat foreignid t_pelaku_usaha_id dari tabel t_pelaku_usaha */
            $table->unsignedBigInteger('id_t_pelaku_usaha');
            $table->foreign('id_t_pelaku_usaha')->references('id')->on('t_pelaku_usaha')->onUpdate('cascade')->onDelete('cascade');
            $table->date('bulan_laporan');
            $table->decimal('jumlah_omset', 15, 2);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_omset_usaha');
    }
}
