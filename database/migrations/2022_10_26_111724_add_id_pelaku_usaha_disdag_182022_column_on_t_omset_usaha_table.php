<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddIdPelakuUsahaDisdag182022ColumnOnTOmsetUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_omset_usaha', function (Blueprint $table) {
            // Membuat kolom baru id_t_pelaku_usaha_disdag_181022
            $table->unsignedBigInteger('id_t_pelaku_usaha_disdag_181022')->after('id_t_pelaku_usaha')->nullable()->default(null);
            // Menambahkan foreign pada kolom id_t_pelaku_usaha_disdag_181022 cascade on update dan on delete
            $table->foreign('id_t_pelaku_usaha_disdag_181022')->references('id_t_pelaku_usaha_disdag_181022')->on('t_pelaku_usaha_disdag_181022')->cascadeOnUpdate()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_omset_usaha', function (Blueprint $table) {
            // Menghapus foreign pada kolom id_t_pelaku_usaha_disdag_181022
            $table->dropForeign(['id_t_pelaku_usaha_disdag_181022']);
            // Menghapus kolom id_t_pelaku_usaha_disdag_181022
            $table->dropColumn('id_t_pelaku_usaha_disdag_181022');
        });
    }
}
