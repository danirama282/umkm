<?php

namespace App\Http\Controllers;

use App\Models\MJenisTempatUsahaModel;
use App\Models\MSetupKabModel;
use App\Models\MSetupKecModel;
use App\Models\MSetupPropModel;
use App\Models\MKategoriUsahaModel;
use App\Models\MSetupKelModel;
use App\Models\MSubKategoriUsahaModel;
use App\Models\MSkalaPemasaranModel;
use App\Models\MSkalaUsahaModel;
use App\Models\MStatusBangunanModel;
use App\Models\MTempatUsahaModel;
use App\Models\TPelakuUsahaModel;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class TPelakuUsahaController extends Controller
{
    public function index(Request $request, TPelakuUsahaModel $pelakuUsaha)
    {
        /* Memanggil relasi dengan with dari model $pelakuUsaha  */
        $data = $pelakuUsaha->with('kategori_usaha', 'jenis_tempat_usaha')->get();

        if ($request->ajax()) {
            return DataTables::of($data)
                ->addIndexColumn()
                // Menambah tampilan dt-action kedalam kolom action
                ->addColumn('action', function ($data) {
                    return view('pelaku_usaha.dt-action', compact('data'));
                })
                ->rawColumns(['action'])
                ->toJson();
        }

        return view('pelaku_usaha.index');
    }

    public function create()
    {
        /* Menyiapkan $data_wilayah yang berisi data array dari provinsi, kabupaten, dan kecamatan */
        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')->where('nm_prop', 'JAWA TIMUR')->first();
        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('nm_kab', 'KOTA SURABAYA')->get();

        /* Menambahkan order by */
        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->orderBy('nm_kec', 'ASC')->get();

        /* Mengambil data MKategoriUsahaModel */
        $data['kategori_usaha'] = MKategoriUsahaModel::select('id', 'nama_kategori_usaha')->get();

        /* Mengambil data MJenisTempatUsahaModel */
        $data['jenis_tempat_usaha'] = MJenisTempatUsahaModel::select('id', 'nama_jenis_tempat_usaha')->get();

        /* Mengambil data MStatusBangunanModel */
        $data['status_bangunan'] = MStatusBangunanModel::select('id', 'nama_status_bangunan')->get();

        /* Mengambil data MSkalaPemasaranModel */
        $data['skala_pemasaran'] = MSkalaPemasaranModel::select('id', 'nama_skala_pemasaran')->get();

        /* Mengambil data MSkalaUsahaModel */
        $data['skala_usaha'] = MSkalaUsahaModel::select('id', 'nama_skala_usaha')->get();

        return view('pelaku_usaha.create', compact('data'));
    }

    public function store(Request $request, TPelakuUsahaModel $pelakuUsaha)
    {
        /* Membuat validasi sesusai dengan $request */
        $validator = Validator::make($request->all(), [
            "nama_usaha" => "required",
            "m_kategori_usaha_id" => "required",
            "m_sub_kategori_usaha_id" => "required",
            "m_jenis_tempat_usaha_id" => "required",
            "m_tempat_usaha_id" => "required",
            "alamat_usaha" => "required",
            "kecamatan_usaha" => "required",
            "kelurahan_usaha" => "required",
            "rt_usaha" => "required",
            "rw_usaha" => "required",
            "telp_usaha" => "required",
            "tgl_berdiri_usaha" => "required",
            "m_skala_pemasaran_id" => "required",
            "m_skala_usaha_id" => "required",
            "m_status_bangunan_id" => "required",
            "nik_pemilik" => "required",
            "nama_pemilik" => "required",
            "alamat_pemilik" => "required",
            "kecamatan_pemilik" => "required",
            "kelurahan_pemilik" => "required",
            "rt_pemilik" => "required",
            "rw_pemilik" => "required",
            "telp_pemilik" => "required",
            "tgl_lahir_pemilik" => "required",
            "jenis_kelamin_pemilik" => "required",
            "status_mbr_pemilik" => "required",
            "akun_tokopedia" => "required",
            "akun_shopee" => "required",
            "no_npwp_pemilik" => "required",
            "no_npwp_usaha" => "required",
            "no_nop" => "required",
            "no_nib" => "required",
            "status_binaan_pemilik" => "required",
            "tingkat_usaha" => "required",
        ]);

        /* Inisiasi nama atribut */
        $attrNames = array(
            "nama_usaha" => "Nama Usaha",
            "m_kategori_usaha_id" => "Kategori Usaha",
            "m_sub_kategori_usaha_id" => "Sub Kategori Usaha",
            "m_jenis_tempat_usaha_id" => "Jenis Tempat Usaha",
            "m_tempat_usaha_id" => "Tempat Usaha",
            "alamat_usaha" => "Alamat Usaha",
            "kecamatan_usaha" => "Kecamatan Usaha",
            "kelurahan_usaha" => "Kelurahan Usaha",
            "rt_usaha" => "RT Usaha",
            "rw_usaha" => "RW Usaha",
            "telp_usaha" => "Telp Usaha",
            "tgl_berdiri_usaha" => "Tanggal Berdiri Usaha",
            "m_skala_pemasaran_id" => "Skala Pemasaran",
            "m_skala_usaha_id" => "Skala Usaha",
            "m_status_bangunan_id" => "Status Bangunan",
            "nik_pemilik" => "NIK Pemilik",
            "nama_pemilik" => "Nama Pemilik",
            "alamat_pemilik" => "Alamat Pemilik",
            "kecamatan_pemilik" => "Kecamatan Pemilik",
            "kelurahan_pemilik" => "Kelurahan Pemilik",
            "rt_pemilik" => "RT Pemilik",
            "rw_pemilik" => "RW Pemilik",
            "telp_pemilik" => "Telp Pemilik",
            "tgl_lahir_pemilik" => "Tanggal Lahir Pemilik",
            "jenis_kelamin_pemilik" => "Jenis Kelamin Pemilik",
            "status_mbr_pemilik" => "Status MBR Pemilik",
            "akun_tokopedia" => "Akun Tokopedia",
            "akun_shopee" => "Akun Shopee",
            "no_npwp_pemilik" => "No NPWP Pemilik",
            "no_npwp_usaha" => "No NPWP Usaha",
            "no_nop" => "No NOP",
            "no_nib" => "No NIB",
            "status_binaan_pemilik" => "Status Binaan Pemilik",
            "tingkat_usaha" => "Tingkat Usaha",
        );

        /* setAttributesName dengan parameter $attrNames ke dalam $validator */
        $validator->setAttributeNames($attrNames);

        /* Melakukan pengecekan pada $validator */
        if ($validator->passes()) {

            /* Inisiasi proses transaksi pada database */
            DB::beginTransaction();

            try {
                /* Menyimpan ke $pelakuUsaha */
                $pelakuUsaha->nama_usaha = $request->nama_usaha;
                $pelakuUsaha->m_kategori_usaha_id = $request->m_kategori_usaha_id;
                $pelakuUsaha->m_sub_kategori_usaha_id = $request->m_sub_kategori_usaha_id;
                $pelakuUsaha->m_jenis_tempat_usaha_id = $request->m_jenis_tempat_usaha_id;
                $pelakuUsaha->m_tempat_usaha_id = $request->m_tempat_usaha_id;
                $pelakuUsaha->alamat_usaha = $request->alamat_usaha;
                $pelakuUsaha->kecamatan_usaha = $request->kecamatan_usaha;
                $pelakuUsaha->kelurahan_usaha = $request->kelurahan_usaha;
                $pelakuUsaha->rt_usaha = $request->rt_usaha;
                $pelakuUsaha->rw_usaha = $request->rw_usaha;
                $pelakuUsaha->telp_usaha = $request->telp_usaha;
                $pelakuUsaha->tgl_berdiri_usaha = $request->tgl_berdiri_usaha;
                $pelakuUsaha->m_skala_pemasaran_id = $request->m_skala_pemasaran_id;
                $pelakuUsaha->m_skala_usaha_id = $request->m_skala_usaha_id;
                $pelakuUsaha->m_status_bangunan_id = $request->m_status_bangunan_id;
                $pelakuUsaha->nik_pemilik = $request->nik_pemilik;
                $pelakuUsaha->nama_pemilik = $request->nama_pemilik;
                $pelakuUsaha->alamat_pemilik = $request->alamat_pemilik;
                $pelakuUsaha->kecamatan_pemilik = $request->kecamatan_pemilik;
                $pelakuUsaha->kelurahan_pemilik = $request->kelurahan_pemilik;
                $pelakuUsaha->rt_pemilik = $request->rt_pemilik;
                $pelakuUsaha->rw_pemilik = $request->rw_pemilik;
                $pelakuUsaha->telp_pemilik = $request->telp_pemilik;
                $pelakuUsaha->tgl_lahir_pemilik = $request->tgl_lahir_pemilik;
                $pelakuUsaha->jenis_kelamin_pemilik = $request->jenis_kelamin_pemilik;
                $pelakuUsaha->status_mbr_pemilik = $request->status_mbr_pemilik;
                $pelakuUsaha->akun_tokopedia = $request->akun_tokopedia;
                $pelakuUsaha->akun_shopee = $request->akun_shopee;
                $pelakuUsaha->no_npwp_pemilik = $request->no_npwp_pemilik;
                $pelakuUsaha->no_npwp_usaha = $request->no_npwp_usaha;
                $pelakuUsaha->no_nop = $request->no_nop;
                $pelakuUsaha->no_nib = $request->no_nib;
                $pelakuUsaha->status_binaan_pemilik = $request->status_binaan_pemilik;
                $pelakuUsaha->tingkat_usaha = $request->tingkat_usaha;

                /* Melakukan operasi simpan pada data table */
                $pelakuUsaha->save();

                /* Menyimpan hasil operasi */
                DB::commit();

                return response()->json([
                    'status' => true,
                    'message' => 'Data berhasil disimpan!'
                ]);
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function edit($id, TPelakuUsahaModel $pelakuUsaha)
    {
        $data['pelaku_usaha'] = $pelakuUsaha->find(Crypt::decryptString($id));
        dd($data['pelaku_usaha']->toarray());
        $provinsi = $data['pelaku_usaha']['provinsi_pemilik'];
        dd($provinsi);
        // Menyiapkan $data_wilayah yang berisi data array dari provinsi, kabupaten, dan kecamatan
        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')->where('nm_prop', 'JAWA TIMUR')->first();
        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('nm_kab', 'KOTA SURABAYA')->get();

        /* Melakukan pengecekan terhadap variable $data['pelaku_usaha'] khususnya pada object kecamatan_usaha dan kelurahan_usaha */
        if ($data['pelaku_usaha']->kecamatan_usaha != null && $data['pelaku_usaha']->kelurahan_usaha != null) {
            /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_usaha, kelurahan_usaha */
            $data['kelurahan_usaha'] = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->where('id_m_setup_kec', $data['pelaku_usaha']->kecamatan_usaha)->where('id_m_setup_kel', $data['pelaku_usaha']->kelurahan_usaha)->orderBy('nm_kel', 'ASC')->get();
        } else {
            /* Mengisi variable $data['kelurahan_usaha'] dengan value null atau 0 */
            $data['kelurahan_usaha'] = null;
        }

        /* Mengambil data kecamatan order by nm_kel where id_m_setup_prop dan id_m_setup_kab */
        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->orderBy('nm_kec', 'ASC')->get();

        /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_pemilik, kelurahan_pemilik */
        $data['kelurahan_pemilik'] = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->where('id_m_setup_kec', $data['pelaku_usaha']->kecamatan_pemilik)->where('id_m_setup_kel', $data['pelaku_usaha']->kelurahan_pemilik)->orderBy('nm_kel', 'ASC')->get();

        /* Mengambil data MKategoriUsahaModel */
        $data['kategori_usaha'] = MKategoriUsahaModel::select('id', 'nama_kategori_usaha')->get();

        /* Mengambil data MSubKategoriUsahaModel berdasarkan m_sub_kategori_usaha_id yang dimiliki oleh $data['pelaku_usaha'] */
        $data['sub_kategori_usaha'] = MSubKategoriUsahaModel::select('id', 'nama_sub_kategori_usaha')->where('id', $data['pelaku_usaha']->m_sub_kategori_usaha_id)->where('m_kategori_usaha_id', $data['pelaku_usaha']->m_kategori_usaha_id)->first();

        /* Mengambil data MJenisTempatUsahaModel */
        $data['jenis_tempat_usaha'] = MJenisTempatUsahaModel::select('id', 'nama_jenis_tempat_usaha')->get();

        /* Mengambil data MTempatUsahaModel berdasarkan m_tempat_usaha_id yang dimiliki oleh $data['pelaku_usaha'] */
        $data['tempat_usaha'] = MTempatUsahaModel::select('id', 'nama_jenis_tempat_usaha')->where('id', $data['pelaku_usaha']->m_tempat_usaha_id)->where('m_jenis_tempat_usaha_id', $data['pelaku_usaha']->m_jenis_tempat_usaha_id)->first();

        /* Mengambil data MStatusBangunanModel */
        $data['status_bangunan'] = MStatusBangunanModel::select('id', 'nama_status_bangunan')->get();

        /* Mengambil data MSkalaPemasaranModel */
        $data['skala_pemasaran'] = MSkalaPemasaranModel::select('id', 'nama_skala_pemasaran')->get();

        /* Mengambil data MSkalaUsahaModel */
        $data['skala_usaha'] = MSkalaUsahaModel::select('id', 'nama_skala_usaha')->get();

        return view('pelaku_usaha.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        /* Membuat validasi sesusai dengan $request */
        $validator = Validator::make($request->all(), [
            "nama_usaha" => "required",
            "m_kategori_usaha_id" => "required",
            "m_sub_kategori_usaha_id" => "required",
            "m_jenis_tempat_usaha_id" => "required",
            "m_tempat_usaha_id" => "required",
            "alamat_usaha" => "required",
            "kecamatan_usaha" => "required",
            "kelurahan_usaha" => "required",
            "rt_usaha" => "required",
            "rw_usaha" => "required",
            "telp_usaha" => "required",
            "tgl_berdiri_usaha" => "required",
            "m_skala_pemasaran_id" => "required",
            "m_skala_usaha_id" => "required",
            "m_status_bangunan_id" => "required",
            "nik_pemilik" => "required",
            "nama_pemilik" => "required",
            "alamat_pemilik" => "required",
            "kecamatan_pemilik" => "required",
            "kelurahan_pemilik" => "required",
            "rt_pemilik" => "required",
            "rw_pemilik" => "required",
            "telp_pemilik" => "required",
            "tgl_lahir_pemilik" => "required",
            "jenis_kelamin_pemilik" => "required",
            "status_mbr_pemilik" => "required",
            "akun_tokopedia" => "required",
            "akun_shopee" => "required",
            "no_npwp_pemilik" => "required",
            "no_npwp_usaha" => "required",
            "no_nop" => "required",
            "no_nib" => "required",
            "status_binaan_pemilik" => "required",
            "tingkat_usaha" => "required",
        ]);

        /* Inisiasi nama atribut */
        $attrNames = array(
            "nama_usaha" => "Nama Usaha",
            "m_kategori_usaha_id" => "Kategori Usaha",
            "m_sub_kategori_usaha_id" => "Sub Kategori Usaha",
            "m_jenis_tempat_usaha_id" => "Jenis Tempat Usaha",
            "m_tempat_usaha_id" => "Tempat Usaha",
            "alamat_usaha" => "Alamat Usaha",
            "kecamatan_usaha" => "Kecamatan Usaha",
            "kelurahan_usaha" => "Kelurahan Usaha",
            "rt_usaha" => "RT Usaha",
            "rw_usaha" => "RW Usaha",
            "telp_usaha" => "Telp Usaha",
            "tgl_berdiri_usaha" => "Tanggal Berdiri Usaha",
            "m_skala_pemasaran_id" => "Skala Pemasaran",
            "m_skala_usaha_id" => "Skala Usaha",
            "m_status_bangunan_id" => "Status Bangunan",
            "nik_pemilik" => "NIK Pemilik",
            "nama_pemilik" => "Nama Pemilik",
            "alamat_pemilik" => "Alamat Pemilik",
            "kecamatan_pemilik" => "Kecamatan Pemilik",
            "kelurahan_pemilik" => "Kelurahan Pemilik",
            "rt_pemilik" => "RT Pemilik",
            "rw_pemilik" => "RW Pemilik",
            "telp_pemilik" => "Telp Pemilik",
            "tgl_lahir_pemilik" => "Tanggal Lahir Pemilik",
            "jenis_kelamin_pemilik" => "Jenis Kelamin Pemilik",
            "status_mbr_pemilik" => "Status MBR Pemilik",
            "akun_tokopedia" => "Akun Tokopedia",
            "akun_shopee" => "Akun Shopee",
            "no_npwp_pemilik" => "No NPWP Pemilik",
            "no_npwp_usaha" => "No NPWP Usaha",
            "no_nop" => "No NOP",
            "no_nib" => "No NIB",
            "status_binaan_pemilik" => "Status Binaan Pemilik",
            "tingkat_usaha" => "Tingkat Usaha",
        );

        /* setAttributesName dengan parameter $attrNames ke dalam $validator */
        $validator->setAttributeNames($attrNames);

        if ($validator->passes()) {

            /* Inisiasi data untuk diinput ke dalam database */
            DB::beginTransaction();

            try {
                /* Mengambil data $pelaku_usaha berdasarkan $id */
                $pelaku_usaha = TPelakuUsahaModel::find(Crypt::decryptString($id));

                /* Mengupdate data pelaku_usaha berdasarkan $id */
                $pelaku_usaha->update([
                    "id_m_setup_prop" => $request->id_m_setup_prop,
                    "id_m_setup_kab" => $request->id_m_setup_kab,
                    "nama_usaha" => $request->nama_usaha,
                    "m_kategori_usaha_id" => $request->m_kategori_usaha_id,
                    "m_sub_kategori_usaha_id" => $request->m_sub_kategori_usaha_id,
                    "m_jenis_tempat_usaha_id" => $request->m_jenis_tempat_usaha_id,
                    "m_tempat_usaha_id" => $request->m_tempat_usaha_id,
                    "alamat_usaha" => $request->alamat_usaha,
                    "kecamatan_usaha" => $request->kecamatan_usaha,
                    "kelurahan_usaha" => $request->kelurahan_usaha,
                    "rt_usaha" => $request->rt_usaha,
                    "rw_usaha" => $request->rw_usaha,
                    "telp_usaha" => $request->telp_usaha,
                    "tgl_berdiri_usaha" => $request->tgl_berdiri_usaha,
                    "m_skala_pemasaran_id" => $request->m_skala_pemasaran_id,
                    "m_skala_usaha_id" => $request->m_skala_usaha_id,
                    "m_status_bangunan_id" => $request->m_status_bangunan_id,
                    "nik_pemilik" => $request->nik_pemilik,
                    "nama_pemilik" => $request->nama_pemilik,
                    "alamat_pemilik" => $request->alamat_pemilik,
                    "kecamatan_pemilik" => $request->kecamatan_pemilik,
                    "kelurahan_pemilik" => $request->kelurahan_pemilik,
                    "rt_pemilik" => $request->rt_pemilik,
                    "rw_pemilik" => $request->rw_pemilik,
                    "telp_pemilik" => $request->telp_pemilik,
                    "tgl_lahir_pemilik" => $request->tgl_lahir_pemilik,
                    "jenis_kelamin_pemilik" => $request->jenis_kelamin_pemilik,
                    "status_mbr_pemilik" => $request->status_mbr_pemilik,
                    "akun_tokopedia" => $request->akun_tokopedia,
                    "akun_shopee" => $request->akun_shopee,
                    "no_npwp_pemilik" => $request->no_npwp_pemilik,
                    "no_npwp_usaha" => $request->no_npwp_usaha,
                    "no_nop" => $request->no_nop,
                    "no_nib" => $request->no_nib,
                    "status_binaan_pemilik" => $request->status_binaan_pemilik,
                    "tingkat_usaha" => $request->tingkat_usaha,
                ]);

                DB::commit();

                return response()->json([
                    "status" => true,
                    "message" => "Data berhasil diupdate",
                ]);
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function destroy($id)
    {
        //
    }

    public function getSubKategoriUsaha(Request $request)
    {
        $m_kategori_usaha_id = $request->m_kategori_usaha_id;

        /* Memanggil relasi sub_kategori_usaha dari model MKategoriUsahaModel */
        $data = MKategoriUsahaModel::find($m_kategori_usaha_id)->sub_kategori_usaha;

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function getJenisTempatUsaha(Request $request)
    {
        $m_jenis_tempat_usaha_id = $request->m_jenis_tempat_usaha_id;

        /* Memanggil relasi tempat_usaha dari model MJenisTempatUsahaModel */
        $data = MJenisTempatUsahaModel::find($m_jenis_tempat_usaha_id)->tempat_usaha;

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function test()
    {
        $data_update = ["Becak", "Sepeda"];
        // $data_update = [
        //     'nama' => 'becak',
        //     'jenis' => 'kendaraan'
        // ];
        $data = TPelakuUsahaModel::where('id', 8177)->first();
        $data->alamat_usaha = json_encode($data_update);
        $data->save();

        $decode = json_decode($data->alamat_usaha);
        // $decode = json_decode($data->rt_usaha);
        // dd($decode);
    }
}
