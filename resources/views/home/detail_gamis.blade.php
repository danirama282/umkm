<div class="row g-0 light">
    <div class="col-sm-12 col-lg-6 mb-2 d-flex">
        <div class="row g-0 light flex-fill">
            <div class="col-sm-12 col-lg-12 mb-4">
                <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                    data-bs-target="#detailGamis" onclick="funDetailGamis('KumulatifAll','Total')">
                    <div class="card text-white bg-primary py-3" style="height: 100%;">
                        <div class="card-body d-flex align-items-center justify-content-center">
                            <div class="text-center">
                                <img src="{{ asset('dist/assets/img/icons/icon-dash.svg') }}" alt="" style="width: 50%">
                                <div class="card-title mb-0 mt-5">
                                    <h3 class="display-6 mt-2 text-white text-uppercase fs-5">Total</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-4">
                                        {{ $data['KumulatifAll'][0]->jiwa }} Jiwa
                                    </h3>
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-4">
                                        {{ $data['KumulatifAll'][0]->kk }} KK
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div>
    </div>
    <div class="col-sm-12 col-lg-6 mb-2 d-flex">
        <div class="row g-0 light flex-fill">
            <div class="col-sm-12 col-lg-12 ps-lg-2 pb-lg-2">
                <div class="card text-white bg-info">
                    <div class="card-body text-center" style="height: 300px;">
                        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailGamis"
                            onclick="funDetailGamis('WargaMiskinAll','Warga Miskin Kumulatif')">
                            <div style="height: 90px;">
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-white text-uppercase fs-1">Warga Miskin</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaMiskinAll'][0]->jiwa }} Jiwa</h3>
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaMiskinAll'][0]->kk }} KK</h3>
                                </div>
                            </div>
                        </a>
                        <div class="d-flex justify-content-between py-1" style="height: 155px;">
                            <div class="card text-white bg-light mx-lg-2" style="width: 100%">
                                <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                                    data-bs-target="#detailGamis"
                                    onclick="funDetailGamis('MiskinEkstrim','Warga Miskin Ekstrim')">
                                    <div class="card-body text-center">
                                        <div class="card-title mb-0">
                                            <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Miskin
                                                Ekstrim
                                            </h3>
                                        </div>
                                        <div class="card-title mb-0">
                                            <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['MiskinEkstrim'][0]->jiwa }} Jiwa</h3>
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['MiskinEkstrim'][0]->kk }} KK</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            <div class="card text-white bg-light mx-lg-2" style="width: 100%">
                                <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                                    data-bs-target="#detailGamis"
                                    onclick="funDetailGamis('WargaMiskin','Warga Miskin')">
                                    <div class="card-body text-center">
                                        <div class="card-title mb-0">
                                            <h3 class="display-6 mt-2 text-black text-uppercase fs-1">WARGA MISKIN NON EKSTRIM
                                            </h3>
                                        </div>
                                        <div class="card-title mb-0">
                                            <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaMiskin'][0]->jiwa }} Jiwa</h3>
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaMiskin'][0]->kk }} KK</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6 mb-4 ps-lg-2">
                <div class="card text-white bg-info">
                    <div class="card-body text-center" style="height: 300px;">
                        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailGamis"
                            onclick="funDetailGamis('WargaPraMiskin','Warga Pra Miskin Kumulatif')">
                            <div style="height: 90px;">
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-white text-uppercase fs-1">Warga Pra Miskin</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaPraMiskin'][0]->jiwa }} Jiwa</h3>
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaPraMiskin'][0]->kk }} KK</h3>
                                </div>
                            </div>
                        </a>
                        <div class="d-flex justify-content-center" style="height: 155px;">
                            <div class="card text-white bg-light" style="width: 100%">
                                <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                                    data-bs-target="#detailGamis"
                                    onclick="funDetailGamis('WargaPraMiskin_pra','Warga Pra Miskin')">
                                    <div class="card-body text-center">
                                        <div class="card-title mb-0">
                                            <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Pra
                                                Miskin
                                            </h3>
                                        </div>
                                        <div class="card-title mb-0">
                                            <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaPraMiskin_pra'][0]->jiwa }} Jiwa</h3>
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaPraMiskin_pra'][0]->kk }} KK</h3>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-lg-6 mb-4 ps-lg-2">
                <div class="card text-white bg-info">
                    <div class="card-body text-center" style="height: 300px;">
                        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailGamis"
                            onclick="funDetailGamis('WargaLainnyaAll','Warga Lainnya Kumulatif')">
                            <div style="height: 90px;">
                                <div class="card-title mb-0">
                                    <h3 class="display-6 mt-2 text-white text-uppercase fs-1">Warga Lainnya</h3>
                                </div>
                                <div class="card-title mb-0">
                                    <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs-4"></h3> -->
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaLainnyaAll'][0]->jiwa }} Jiwa</h3>
                                    <h3 class="fw-medium mt-2 text-white text-uppercase fs-1">
                                        {{ $data['WargaLainnyaAll'][0]->kk }} KK</h3>
                                </div>
                            </div>
                        </a>
                        <a href="#detailGamis" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailGamis" onclick="funDetailGamis('WargaLainnyaAll','Warga Lainnya')">
                            <div class="d-flex justify-content-center" style="height: 155px;">
                                <div class="card text-white bg-light" style="width: 100%">
                                    <div class="card-body text-center">
                                        <div class="card-title mb-0">
                                            <h3 class="display-6 mt-2 text-black text-uppercase fs-1">Warga Lainnya
                                            </h3>
                                        </div>
                                        <div class="card-title mb-0">
                                            <!-- <h3 class="fw-medium mt-2 text-white text-uppercase fs--2"></h3> -->
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaLainnyaAll'][0]->jiwa }} Jiwa</h3>
                                            <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                {{ $data['WargaLainnyaAll'][0]->kk }} KK</h3>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

{{-- MODAL KATEGORI KEGIATAN --}}
<div class="modal fade" id="detailGamis" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1" id="judul_modal"></h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="tabel_detailGamis" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th width=5%>No.</th>
                                        <th width=10%>NIK</th>
                                        <th width=10%>Nama Pelaku Usaha</th>
                                        <th width=10%>Nama Usaha</th>
                                        <th width=10%>Alamat</th>
                                        <th width=10%>Kecamatan</th>
                                        <th width=10%>Kelurahan</th>
                                        <th width=2%>RT</th>
                                        <th width=2%>RW</th>
                                        <th width=5%>NIB</th>
                                        <th class="text-center" width=5%>#</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    function funDetailGamis(gamis,judul) {
        // console.log(gamis);
        $('#judul_modal').html('Tabel Detail '+ judul.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        }));

        $('#tabel_detailGamis').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{!! route('detailGamis') !!}",
                "type": "POST",
                "data": {
                    gamis: gamis
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nik',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_pelaku_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'alamat',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kecamatan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kelurahan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rw',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rt',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nib',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'aksi',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },

            ],
            "pageLength": 5,
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "language": {
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                "infoFiltered": "(disaring dari _MAX_ total data)",
                "lengthMenu": "Menampilkan _MENU_ data",
                "search": "Cari:",
                "zeroRecords": "Tidak ada data yang sesuai",
                /* Kostum pagination dengan element baru */
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            },
            "bDestroy": true
        });

    }
</script>
@endpush