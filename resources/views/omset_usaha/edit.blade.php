<x-templates.default>
    <x-slot name="title">Edit Omset Usaha</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="fw-semi-bold">Edit Omset Usaha</h3>
                </div>
                <div class="card-body">
                    <form class="row g-3 needs-validation form_submit" id="frm-filter">
                        @csrf
                        <input type="hidden" name="id_pelaku_usaha" id="id_pelaku_usaha"
                            value="{{ Crypt::encryptString($pelaku_usaha->id_t_pelaku_usaha_disdag_181022) }}">
                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Bulan Laporan Omset</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <select class="form-select js-select2" name="bulan_laporan_omset"
                                    id="bulan_laporan_omset">
                                    {{-- <option selected disabled>
                                        {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MMMM')}}
                                    </option> --}}
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "01" ? 'selected' : ''}} value="01">Januari</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "02" ? 'selected' : ''}} value="02">Februari</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "03" ? 'selected' : ''}} value="03">Maret</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "04" ? 'selected' : ''}} value="04">April</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "05" ? 'selected' : ''}} value="05">Mei</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "06" ? 'selected' : ''}} value="06">Juni</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "07" ? 'selected' : ''}} value="07">Juli</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "08" ? 'selected' : ''}} value="08">Agustus</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "09" ? 'selected' : ''}} value="09">September</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "10" ? 'selected' : ''}} value="10">Oktober</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "11" ? 'selected' : ''}} value="11">November</option>
                                    <option {{\Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('MM') ==
                                        "12" ? 'selected' : ''}} value="12">Desember</option>
                                </select>
                                <div class="invalid-feedback">Pilih bulan</div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Tahun</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <select class="form-select js-select2" name="tahun_laporan" id="tahun_laporan">
                                    @php
                                    $tahun_now = date('Y');
                                    @endphp
                                    {{-- <option selected disabled>{{$pelaku_usaha->bulan_laporan,date('Y')}}</option>
                                    <option selected disabled>{{date('Y', strtotime($pelaku_usaha->bulan_laporan))}}
                                    </option> --}}
                                    @for ($i = 2022; $i<=$tahun_now ; $i++) <option {{
                                        \Carbon\Carbon::parse($pelaku_usaha->bulan_laporan)->isoFormat('Y') == "$i" ?
                                        'selected' : '' }} value="{{ $i }}">{{ $i }}</option>
                                        @endfor
                                </select>
                                <div class="invalid-feedback">Pilih Tahun</div>
                            </div>
                        </div>

                        <div class="row mb-3">
                            <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Masukkan jumlah omset</label>
                            <div class="col-12 col-md-10 col-lg-10">
                                <div class="input-group has-validation"><span class="input-group-text"
                                        id="input_jumlah_omset">Rp</span>
                                    <input class="form-control" name="jumlah_omset" id="jumlah_omset" type="text"
                                        onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
                                        class="form-control"
                                        data-inputmask="'alias': 'numeric','digits': 2,'groupSeparator': ',','removeMaskOnSubmit': true,'autoUnmask':true"
                                        value="{{(int)$pelaku_usaha->jumlah_omset}}" required />
                                    <div class="invalid-feedback">Masukkan jumlah omset</div>
                                </div>
                                <div class="invalid-feedback">Masukkan jumlah omset</div>
                            </div>
                        </div>

                        {{-- Simpan Omset --}}
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('omset_usaha.index', Crypt::encryptString($pelaku_usaha->id_t_pelaku_usaha_disdag_181022)) }}"
                                class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                            <button class="btn btn-primary me-1 mb-1 btn_filter" id="btn_submit">
                                <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                            </button>
                            {{-- <button type="button" class="btn btn-secondary ms-2">Kembali</button> --}}
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>



    <meta name="csrf-token" content="{{ csrf_token() }}">

    <script>
        $(document).ready(function(){
        $('#jumlah_omset').inputmask();
    })
    </script>
    <script>
        /* Select Pelaku Usaha */
    $(document).ready(function(){
        $('.js-select2').select2({
            theme: "bootstrap-5",
        });
    });



    function get_list_laporan_bulan({inputDate = undefined, range = 12, list_disabled = []}) {
        let date = inputDate == undefined ? new Date() : new Date(inputDate);
        let list = [{
            id: "",
            text: ""
        }];

        let year, month, month_format, value, disabled;

        for (let i = 1; i <= range; i++) {
            date.setMonth(date.getMonth() - 1);

            year = Intl.DateTimeFormat("id", { year: "numeric" }).format(date);
            month = Intl.DateTimeFormat("id", { month: "long" }).format(date);

            month_format = ((date.getMonth() + 1) < 10 ? "0" : "") + (date.getMonth() + 1);
            // console.log(date.getFullYear() + "-" + month_format);
            value = date.getFullYear() + "-" + month_format;
            disabled = list_disabled.includes(value);
            // console.log(value, disabled);
            list.push({
                id: value,
                text: year + " " + month,
                disabled: disabled
            });
        }

        return list;
    }

    // -----Submit Button ----------
const submitButton = document.getElementById('btn_submit');
   submitButton.addEventListener('click', function (e) {
       // Prevent default button action
       e.preventDefault();

       Swal.fire({
		title: 'Yakin ?',
		text: "Untuk menyimpan data",
		// icon: 'warning',
		icon: 'question',
  		iconHtml: '?',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Simpan',
		cancelButtonText: 'Batal'
		}).then((result) => {
		if (result.isConfirmed) {

       setTimeout(() =>
       {
        // console.log(new FormData($('.form_submit')[0])['tahun']);
        $.ajax(
            {
                type: "post",
                url: "{{ route('omset_usaha.update', ['id' => Crypt::encryptString($pelaku_usaha->id)]) }}",
                dataType: "json",
                processData: false,
                contentType: false,
                data: new FormData($('.form_submit')[0])
            })
            .done(function(data)
            {
                // Remove loading indication
            //    submitButton.removeAttribute('data-kt-indicator');

                // Enable button
                submitButton.disabled = false;

                Swal.close();

                if (data.status)
                {
                Swal.fire(
                    'Berhasil!',
                    'Data berhasil disimpan',
                    'success'
                    )
                    // location.reload();
                //    window.location = "{{ route('omset_usaha.index',$pelaku_usaha->id_t_pelaku_usaha_disdag_181022) }}";
                    // window.location = "{{ route('omset_usaha.index',  Crypt::encryptString($pelaku_usaha->id_t_pelaku_usaha_disdag_181022)) }}";
                }
                else
                {
                    Swal.fire(
                    {
                        title: "Perhatian!",
                        html: data.pesan,
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })

                //    e.setAttribute("data-kt-indicator", "off")
                    e.disabled = 0
                }
            }).fail(function(jqXHR, textStatus, errorThrown)
            {
                Swal.fire(
                {
                    text: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                    icon: "error",
                    buttonsStyling: !1,
                    confirmButtonText: "OK",
                    customClass: {
                        confirmButton: "btn btn-primary"
                    }
                })
            });
       }, 100);
    }
		})
   });

    </script>



</x-templates.default>