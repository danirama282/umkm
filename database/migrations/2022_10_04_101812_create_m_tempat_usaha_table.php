<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMTempatUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_tempat_usaha', function (Blueprint $table) {
            $table->bigIncrements('id');
            /* Membuat foreignid m_jenis_tempat_usaha_id cascade on update dan cascade on delete dari m_jenis_usaha */
            $table->unsignedBigInteger('m_jenis_tempat_usaha_id');
            $table->foreign('m_jenis_tempat_usaha_id')->references('id')->on('m_jenis_tempat_usaha')->onUpdate('cascade')->onDelete('cascade');
            $table->string('nama_jenis_usaha_det', 255);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_tempat_usaha');
    }
}
