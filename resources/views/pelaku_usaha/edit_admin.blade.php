<div class="row g-0">
    <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2"></div>
    <div class="card">
        <div class="card-header">
            <h5 class="fw-bold fs-1 mt-2">Ubah Data Pelaku Usaha</h5>
        </div>
        <div class="card-body ">
            <form id="formTambahPelakuUsaha">
                @csrf
                <input type="hidden" name="id_pelaku_usaha" id="id_pelaku_usaha"
                    value="{{ Crypt::encryptString($data['pelaku_usaha']->id_t_pelaku_usaha_disdag_181022) }}">
                <input type="hidden" name="status_peken" id="status_peken"
                    value="{{ $data['pelaku_usaha']->peken_tokel }}">
                <input type="hidden" name="status_padat_karya" id="status_padat_karya"
                    value="{{ $data['pelaku_usaha']->padat_karya }}">
                <!-- Sub judul dengan garis -->
                <div class="row mb-3">
                    <div class="col-sm-12">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Pelaku Usaha</h6>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="status_kependudukan">Status
                        Kependudukan</label>
                    <div class="col-sm-3">
                        <div class="form-check"><input class="form-check-input" id="surabaya" type="radio"
                                name="status" value=1 onclick="isSby()"
                                {{ $data['pelaku_usaha']->is_surabaya == 1 ? 'checked' : '' }} /><label
                                class="form-check-label" for="surabaya">Surabaya</label>
                        </div>
                        <div class="form-check"><input class="form-check-input" id="nonSurabaya" type="radio"
                                name="status" onclick="isNonSby()" value=0
                                {{ $data['pelaku_usaha']->is_surabaya == '0' ? 'checked' : '' }} /><label
                                class="form-check-label" for="nonSurabaya">Non Surabaya</label>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nik">NIK Pelaku
                        Usaha</label>
                    <div class="col-sm-10">
                        <div class="input-group">
                            <input class="form-control" id="nik" value="{{ $data['pelaku_usaha']->nik }}"
                                name="nik" type="text" onkeyup="checkLengthNIK()" />
                            <button class="btn btn-sm btn-primary" type="button" id="cek_nik"
                                style="display:none;"><i class="fas fa-search"></i> Cari NIK </button>
                        </div>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_pelaku_usaha">Nama Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nama_pelaku_usaha"
                            value="{{ $data['pelaku_usaha']->nama_pelaku_usaha }}" name="nama_pelaku_usaha"
                            type="text" />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_ktp">Alamat
                        Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <!-- Text Area bootstrap 5 -->
                        <textarea class="form-control" value="{{ $data['pelaku_usaha']->alamat_ktp }}" name="alamat_ktp" id="alamat_ktp">{{ $data['pelaku_usaha']->alamat_ktp }}</textarea>
                    </div>
                </div>
                {{-- @dd($data['provinsi_non_sby']->toarray(),$data['pelaku_usaha']->provinsi_pemilik) --}}
                <div class="row mb-3 provinsi">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="provinsi_pelaku">Provinsi
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select class="form-select js-select2" id="provinsi" name="provinsi" data-control="select2"
                            {{-- data-placeholder="- Pilih Provinsi -" --}}>
                            <option value="" selected disabled>Pilih :</option>
                            @foreach ($data['provinsi_non_sby'] as $item)
                                <option value="{{ $item->id_m_setup_prop }}"
                                    {{ $data['pelaku_usaha']->provinsi_pemilik == $item->id_m_setup_prop ? 'selected' : '' }}>
                                    {{ $item->nm_prop }}</option>
                            @endforeach
                            {{-- @foreach ($data['provinsi_non_sby'] as $prov)
                                <option value="{{ $prov->id_m_setup_prop }}">
                                    {{ $prov->nm_prop }}
                                </option>
                            @endforeach --}}
                        </select>
                    </div>
                </div>
                <div class="row mb-3 kabupaten_kota">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kabupaten_kota">Kabupaten
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select class="form-select js-select2" id="kabupaten_kota" name="kabupaten_kota"
                            data-control="select2" {{-- data-placeholder="- Pilih Kabupaten/Kota -" --}}>
                            {{-- <option value="" selected disabled>Pilih :</option> --}}
                            @if ($data['kabupaten_pemilik'] == 'kosong')
                                <option value="" selected disabled>Pilih :</option>
                            @else
                                <option value="{{ $data['kabupaten_pemilik'][0]->id_m_setup_kab }}" selected>
                                    {{ $data['kabupaten_pemilik'][0]->nm_kab }}</option>
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm my-lg-1" for="kecamatan">Kecamatan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select name="kecamatan" data-control="select2" id="kecamatan" class="form-select js-select2"
                            aria-label="Default select example" {{-- {{ $data['kelurahan_pemilik']=='kosong' ? ''
                            : 'readonly' }} --}}>
                            <!-- Menjalankan foreach dari $data['kecamatan'] -->
                            <option value="" selected disabled>Pilih :</option>
                            @foreach ($data['kecamatanKu'] as $item)
                                <option value="{{ $item->id_m_setup_kec }}"
                                    {{ $data['pelaku_usaha']->kecamatan_pemilik == $item->id_m_setup_kec ? 'selected' : '' }}>
                                    {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm my-lg-1" for="kelurahan">Kelurahan
                        Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <select name="kelurahan" data-control="select2" id="kelurahan"
                            class="form-select js-select2" aria-label="Default select example" {{-- {{ $data['kelurahan_pemilik']=='kosong' ? ''
                            : 'readonly' }} --}}>
                            <option value="" selected disabled>Pilih :</option>

                            <!-- Jika $data['kelurahan_usaha'][0]->id_m_setup_kel == null maka akan menampilkan option kosong -->
                            @if ($data['kelurahan_pemilik'] == 'kosong')
                                <option value="" selected disabled>Pilih :</option>
                            @else
                                <option value="{{ $data['kelurahan_pemilik'][0]->id_m_setup_kel }}" selected>
                                    {{ $data['kelurahan_pemilik'][0]->nm_kel }}</option>
                                <!-- Jika $data['kelurahan_pemilik'][0]->id_m_setup_kel != 'kosong' maka akan menampilkan option sesuai dengan data yang dipilih -->
                            @endif
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="rt_ktp">RT Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="rt_ktp" value="{{ $data['pelaku_usaha']->rt_ktp }}"
                            name="rt_ktp" type="text" {{-- {{ $data['pelaku_usaha']->rt_ktp == 0 ? '' : 'readonly' }}
                        --}} />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="rw_ktp">RW Pelaku
                        Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="rw_ktp" value="{{ $data['pelaku_usaha']->rw_ktp }}"
                            name="rw_ktp" type="text" {{-- {{ $data['pelaku_usaha']->rw_ktp == 0 ? '' : 'readonly' }}
                        --}} />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="tanggal_lahir">Tanggal
                        Lahir Pelaku Usaha (KTP)</label>
                    <div class="col-sm-10">
                        <!-- Input date -->
                        <input class="form-control" type="date" id="tanggal_lahir"
                            value="{{ $data['pelaku_usaha']->tanggal_lahir }}" name="tanggal_lahir">
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="no_telp">No. Telepon
                        Pelaku
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="no_telp" value="{{ $data['pelaku_usaha']->no_telp }}"
                            name="no_telp" type="number" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_domisili">Alamat
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <input type="checkbox" name="check_alamat_domisili" id="check_alamat_domisili"
                            {{ strtolower($data['pelaku_usaha']->alamat_domisili) == strtolower($data['pelaku_usaha']->alamat_ktp)
                                ? 'checked'
                                : '' }}>
                        <label class="form-check-label">Sesuai Dengan KTP</label>
                        <input class="form-control" id="alamat_domisili" name="alamat_domisili" type="text"
                            value="{{ $data['pelaku_usaha']->alamat_domisili }}" />
                    </div>
                </div>

                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_domisili">Kecamatan
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_domisili" id="kecamatan_domisili" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                            <!-- Menjalankan foreach dari $data['kecamatan'] -->
                            @foreach ($data['kecamatan'] as $item)
                                <option value="{{ $item->id_m_setup_kec }}"
                                    {{ $data['pelaku_usaha']->kecamatan_domisili == $item->id_m_setup_kec ? 'selected' : '' }}>
                                    {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_domisili">Kelurahan
                        Pelaku
                        Usaha (Domisili)</label>
                    <div class="col-sm-10">
                        <select name="kelurahan_domisili" id="kelurahan_domisili" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                        </select>
                    </div>
                </div>

                <!-- Sub judul dengan garis -->
                <div class="row mb-3">
                    <div class="col-sm-12 mt-3">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Usaha</h6>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nama_usaha">Nama
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nama_usaha" value="{{ $data['pelaku_usaha']->nama_usaha }}"
                            name="nama_usaha" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="alamat_usaha">Alamat Usaha
                    </label>
                    <div class="col-sm-10">
                        {{-- <input type="checkbox" name="check_alamat_usaha" id="check_alamat_usaha">
                        <label class="form-check-label">Sesuai Dengan KTP</label> --}}
                        <input class="form-control" id="alamat_usaha" name="alamat_usaha" type="text"
                            value="{{ $data['pelaku_usaha']->alamat_usaha }}" />
                    </div>
                </div>
                {{-- {{ dd($data['pelaku_usaha']->kelurahan_usaha) }} --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kecamatan_usaha">Kecamatan
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kecamatan_usaha" id="kecamatan_usaha" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['kecamatan'] as $item)
                                <option value="{{ $item->id_m_setup_kec }}"
                                    {{ $data['pelaku_usaha']->kecamatan_usaha == $item->id_m_setup_kec ? 'selected' : '' }}>
                                    {{ $item->nm_kec }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kelurahan_usaha">Kelurahan
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kelurahan_usaha" id="kelurahan_usaha" class="form-select js-select2"
                            aria-label="Default select example">
                            <option value="" selected disabled>Pilih :</option>
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kategori_jenis_produk">Kategori
                        Usaha</label>
                    <div class="col-sm-10">
                        <select name="kategori_jenis_produk" id="kategori_jenis_produk"
                            class="form-select js-select2" aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->kategori_jenis_produk }}">
                                {{ ucwords(str_replace('_', ' ',
                                ucwords(strtolower($data['pelaku_usaha']->kategori_jenis_produk
                                ?? 'pilih:')))) }}
                            </option> --}}
                            {{-- <option value="" selected disabled>Pilih :</option> --}}
                            {{-- <option value="FASHION">Fashion</option>
                            <option value="JASA">Jasa</option>
                            <option value="KERAJINAN">Kerajinan</option>
                            <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                            <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian</option>
                            <option value="TOKO">Toko</option> --}}
                            <option value="" selected disabled>Pilih :</option>
                            @foreach ($data['kategori_usaha'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->kategori_jenis_produk == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="jenis_produk">Produk
                        Usaha</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="jenis_produk"
                            value="{{ $data['pelaku_usaha']->jenis_produk }}" name="jenis_produk" type="text" />
                    </div>
                </div>
                {{-- @dd($data['pelaku_usaha']->skala_pemasaran) --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="skala_pemasaran">Skala
                        Pemasaran</label>
                    <div class="col-sm-10">
                        <select name="skala_pemasaran" id="skala_pemasaran" class="form-select js-select2"
                            aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->skala_pemasaran }}">
                                {{ ucwords(strtolower(str_replace('_',' ',$data['pelaku_usaha']->skala_pemasaran
                                ??'Pilih:',),))
                                }}
                            </option> --}}
                            {{-- <option value="" selected disabled>Pilih :</option> --}}
                            {{-- <option value="LOKAL">Lokal</option>
                            <option value="REGIONAL">Regional</option>
                            <option value="NASIONAL">Nasional</option>
                            <option value="INTERNASIONAL">Internasional</option> --}}
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['skala_pemasaran'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->skala_pemasaran == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="status_bangunan">Status
                        Bangunan</label>
                    <div class="col-sm-10">
                        <select name="status_bangunan" id="status_bangunan" class="form-select js-select2"
                            aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->status_bangunan }}">
                                {{ ucwords(strtolower(str_replace('_',' ',$data['pelaku_usaha']->status_bangunan
                                ??'Pilih:',),))}}
                            </option> --}}
                            {{-- <option value="MILIK_SENDIRI">Milik Sendiri</option>
                            <option value="SEWA">Sewa</option> --}}
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['status_bangunan'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->status_bangunan == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="tahun_binaan">Tahun
                        Binaan</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="tahun_binaan"
                            value="{{ $data['pelaku_usaha']->tahun_binaan }}" name="tahun_binaan" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="sumber_permodalan">Sumber
                        Permodalan</label>
                    <div class="col-sm-10">
                        <select name="sumber_permodalan" id="sumber_permodalan" class="form-select js-select2"
                            aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->sumber_permodalan }}">
                                {{ ucwords(strtolower(str_replace('_',' ',$data['pelaku_usaha']->sumber_permodalan
                                ??'Pilih:',),)) }}
                            </option> --}}
                            {{-- <option value="SENDIRI">Sendiri</option>
                            <option value="PINJAMAN_BANK">Pinjaman Bank</option>
                            <option value="PINJAMAN_NON_BANK">Pinjaman Non Bank</option>
                            <option value="HIBAH">Hibah</option> --}}
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['sumber_permodalan'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->sumber_permodalan == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="jumlah_permodalan">Jumlah
                        Permodalan</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="jumlah_permodalan"
                            value="{{ $data['pelaku_usaha']->jumlah_permodalan }}" name="jumlah_permodalan"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="omset_awal">Omset
                        Awal</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="omset_awal" name="omset_awal"
                            value="{{ $data['pelaku_usaha']->omset_bulanan }}" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="kapasitas_produksi">Kapasitas
                        Produksi</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="kapasitas_produksi"
                            value="{{ $data['pelaku_usaha']->kapasitas_produksi }}" name="kapasitas_produksi"
                            type="text" />
                    </div>
                </div>
                {{-- {{ dd($data['pelaku_usaha']->biaya_produksi) }} --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="biaya_produksi">Biaya
                        Produksi</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="biaya_produksi"
                            value="{{ $data['pelaku_usaha']->biaya_produksi }}" name="biaya_produksi"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="jumlah_tenaga_kerja">Jumlah
                        Tenaga
                        Kerja</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="jumlah_tenaga_kerja"
                            value="{{ $data['pelaku_usaha']->jumlah_tenaga_kerja }}" name="jumlah_tenaga_kerja"
                            type="text" />
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="col-form-label-sm" for="bahan_pokok">Bahan
                                Pokok</label>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" name="ayam_bebek"
                                    id="ayam_bebek" type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->ayam_bebek == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Ayam / Bebek</label>
                            </div>
                            <div class="form-check"><input class="form-check-input" name="beras" id="beras"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->beras == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Beras</label></div>
                            <div class="form-check"><input class="form-check-input" name="buah" id="buah"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->buah == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Buah</label></div>
                            <div class="form-check"><input class="form-check-input" name="cabai" id="cabai"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->cabai == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Cabai</label></div>
                            <div class="form-check"><input class="form-check-input" name="daging" id="daging"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->daging == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Daging</label></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" name="gula" id="gula"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->gula == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Gula</label></div>
                            <div class="form-check"><input class="form-check-input" name="kain" id="kain"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->kain == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Kain</label></div>
                            <div class="form-check"><input class="form-check-input" name="kayu" id="kayu"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->kayu == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Kayu</label></div>
                            <div class="form-check"><input class="form-check-input" name="kedelai" id="kedelai"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->kedelai == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Kedelai</label></div>
                            <div class="form-check"><input class="form-check-input" name="minyak" id="minyak"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->minyak == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Minyak</label></div>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" name="sayur" id="sayur"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->sayur == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Sayur</label></div>
                            <div class="form-check"><input class="form-check-input" name="tepung" id="tepung"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->tepung == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Tepung</label></div>
                            <div class="form-check"><input class="form-check-input" name="telur" id="telur"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->telur == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Telur</label></div>
                        </div>
                    </div>
                </div>
                {{-- BAHAN POKOK DISINI !!! --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="keanggotaan_koperasi">Status
                        Anggota Koperasi</label>
                    <div class="col-sm-10">
                        <select value="{{ $data['pelaku_usaha']->keanggotaan_koperasi }}" name="keanggotaan_koperasi"
                            id="keanggotaan_koperasi" class="form-select js-select2"
                            aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->keanggotaan_koperasi }}">
                                {{ ucwords(strtolower(str_replace('_',' ',$data['pelaku_usaha']->keanggotaan_koperasi
                                ??'Pilih:',),)) }}
                            </option> --}}
                            <!-- Option yang berisi nilai boolean -->
                            {{-- <option value="YA">Ya</option>
                            <option value="TIDAK">Tidak</option> --}}
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['keanggotaan_koperasi'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->keanggotaan_koperasi == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="status_keaktifan">Status
                        Keaktifan</label>
                    <div class="col-sm-10">
                        <select value="{{ $data['pelaku_usaha']->status_keaktifan }}" name="status_keaktifan"
                            id="status_keaktifan" class="form-select js-select2" aria-label="Default select example">
                            {{-- <option selected value="{{ $data['pelaku_usaha']->status_keaktifan }}">
                                {{ ucwords(strtolower(str_replace('_',' ',$data['pelaku_usaha']->status_keaktifan
                                ??'Pilih:',),))
                                }}
                            </option> --}}
                            <!-- Option yang berisi nilai boolean -->
                            {{-- <option value="AKTIF">Aktif</option>
                            <option value="TIDAK_AKTIF">Tidak Aktif</option>
                            <option value="LIBUR">Libur</option> --}}
                            <option value="" selected disabled>Pilih :</option>

                            @foreach ($data['status_keaktifan'] as $index => $value)
                                <option value="{{ str_replace('_',' ',$index) }}"
                                    {{ $data['pelaku_usaha']->status_keaktifan == str_replace('_',' ',$index) ? 'selected' : '' }}>
                                    {{ $value }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <!-- Sub judul dengan garis -->
                <div class="row mb-3">
                    <div class="col-sm-12 mt-3">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Lain Lain</h6>
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="npwp">Nomor
                        NPWP</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="npwp" value="{{ $data['pelaku_usaha']->npwp }}"
                            name="npwp" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="nib">Nomor NIB</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="nib" value="{{ $data['pelaku_usaha']->nib }}"
                            name="nib" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="iumk">Nomor IUMK</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="iumk" value="{{ $data['pelaku_usaha']->iumk }}"
                            name="iumk" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="pirt">Nomor PIRT</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="pirt" value="{{ $data['pelaku_usaha']->pirt }}"
                            name="pirt" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="bpom">Nomor BPOM</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="bpom" value="{{ $data['pelaku_usaha']->bpom }}"
                            name="bpom" type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="sertifikat_merk">Sertifikat
                        Merk</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="sertifikat_merk"
                            value="{{ $data['pelaku_usaha']->sertifikat_merk }}" name="sertifikat_merk"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="sertifikat_halal">Sertifikat
                        Halal</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="sertifikat_halal"
                            value="{{ $data['pelaku_usaha']->sertifikat_halal }}" name="sertifikat_halal"
                            type="text" />
                    </div>
                </div>
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="haki">HAKI</label>
                    <div class="col-sm-10">
                        <input class="form-control" id="haki" value="{{ $data['pelaku_usaha']->haki }}"
                            name="haki" type="text" />
                    </div>
                </div>
                <!-- Sub judul dengan garis -->
                <div class="row mb-3">
                    <div class="col-sm-12 mt-3">
                        <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Intervensi</h6>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-2">
                            <label class="col-form-label-sm" for="intervensi">Intervensi Yang
                                Diberikan</label>
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" name="csr" id="csr"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->csr == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">CSR</label></div>

                            <div class="form-check"><input class="form-check-input" name="kur" id="kur"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->kur == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">KUR</label></div>

                            <div class="form-check"><input class="form-check-input" name="pameran" id="pameran"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->pameran == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Pameran</label></div>

                            <div class="form-check"><input class="form-check-input" name="industri_rumahan"
                                    id="industri_rumahan" type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->industri_rumahan == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">industri rumahan</label></div>

                            {{-- <div class="form-check"><input class="form-check-input" name="bpum" id="bpum"
                                    type="checkbox" value=1 {{ $data['pelaku_usaha']->bpum ==
                                1 ? 'checked' : '' }} /><label class="form-check-label" for="">BPUM</label></div> --}}
                        </div>
                        <div class="col-sm-3">
                            <div class="form-check"><input class="form-check-input" name="pasar" id="pasar"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->pasar == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Pasar</label></div>

                            {{-- <div class="form-check"><input class="form-check-input" name="padat_karya"
                                    id="padat_karya" type="checkbox" value=1 {{ $data['pelaku_usaha']->padat_karya
                                == 1 ? 'checked' : '' }} /><label class="form-check-label" for="">Padat
                                    Karya</label></div> --}}

                            <div class="form-check"><input class="form-check-input" name="puspita" id="puspita"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->puspita == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Puspita</label></div>

                            <div class="form-check"><input class="form-check-input" name="swk" id="swk"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->swk == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">SWK</label></div>

                            <div class="form-check"><input class="form-check-input" name="skg" id="skg"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->skg == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">SKG</label></div>

                        </div>
                        <div class="col-sm-3">


                            {{-- <div class="form-check"><input class="form-check-input" name="peken_tokel"
                                    id="peken_tokel" type="checkbox" value=1 {{ $data['pelaku_usaha']->peken_tokel
                                == 1 ? 'checked' : '' }} /><label class="form-check-label" for="">PEKEN
                                    Tokel</label></div>

                            <div class="form-check"><input class="form-check-input" name="peken_ukm" id="peken_ukm"
                                    type="checkbox" value=2 {{ $data['pelaku_usaha']->peken_tokel == 2 ? 'checked' :
                                '' }} /><label class="form-check-label" for="">PEKEN UKM</label></div> --}}

                            <div class="form-check"><input class="form-check-input" name="rumah_kreatif"
                                    id="rumah_kreatif" type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->rumah_kreatif == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Rumah Kreatif</label>
                            </div>

                            <div class="form-check"><input class="form-check-input" name="pelatihan" id="pelatihan"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->pelatihan == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">Pelatihan</label></div>

                            <div class="form-check"><input class="form-check-input" name="oss" id="oss"
                                    type="checkbox" value=1
                                    {{ $data['pelaku_usaha']->oss == 1 ? 'checked' : '' }} /><label
                                    class="form-check-label" for="">OSS</label></div>
                        </div>
                    </div>
                </div>
                {{-- {{ dd($data['pelaku_usaha']->keterangan) }} --}}
                <div class="row mb-3">
                    <label class="col-sm-2 col-form-label col-form-label-sm" for="keterangan">Keterangan</label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="keterangan" name="keterangan" value="{{ $data['pelaku_usaha']->keterangan }}"
                            rows="3">{{ $data['pelaku_usaha']->keterangan }}</textarea>
                    </div>
                </div>
                <div class="d-flex justify-content-end">
                    <!-- Button kembali untuk back ke route pelaku_usaha.index -->
                    <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                        <span class="fas fas fa-backward me-2" data-fa-transform="shrink-3"></span> Kembali
                    </a>
                    <button class="btn btn-primary me-1 mb-1" type="submit">
                        <span class="fas fa-edit me-2" data-fa-transform="shrink-3"></span> Edit
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
