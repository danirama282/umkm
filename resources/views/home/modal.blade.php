{{-- MODAL KATEGORI USAHA --}}
<div class="modal fade" id="detailFashionModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Usaha Fashion</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_fashion" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th width=5%>No.</th>
                                        <th width=10%>NIK</th>
                                        <th width=10%>Nama Pelaku Usaha</th>
                                        <th width=10%>Nama Usaha</th>
                                        <th width=10%>Alamat</th>
                                        <th width=10%>Kecamatan</th>
                                        <th width=10%>Kelurahan</th>
                                        <th width=2%>RT</th>
                                        <th width=2%>RW</th>
                                        <th width=5%>NIB</th>
                                        <th class="text-center" width=5%>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailKategoriMaminModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Usaha Makanan Dan Minuman</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_mamin" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>NIK</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                        <th>NIB</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailKerajinanModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Usaha Kerajinan</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_kerajinan" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>NIK</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                        <th>NIB</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailTokoModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Usaha Toko</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_toko" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>NIK</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                        <th>NIB</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailJasaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Usaha Jasa</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_jasa" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>NIK</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                        <th>NIB</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

{{-- MODAL KBLI --}}
<!-- Modal #detailIndustriPengolahanModal -->
<div class="modal fade" id="detailIndustriPengolahanModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Industri Pengolahan</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0 w-100"
                            id="tableDetailIndustriPengolahan">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama Pelaku Usaha</th>
                                    <th class="text-center">Nama Usaha</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">NIB</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPerdaganganModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h5 class="fw-bold fs-1">Detail Perdagangan</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0 w-100"
                            id="tableDetailPerdagangan">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama Pelaku Usaha</th>
                                    <th class="text-center">Nama Usaha</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">NIB</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailAkomodasiMaminModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Akomodasi Mamin</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0 w-100"
                            id="tableDetailAkomodasiMamin">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama Pelaku Usaha</th>
                                    <th class="text-center">Nama Usaha</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">NIB</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailAktivitasJasaLainnya" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h5 class="fw-bold fs-1">Detail Industri Aktivitas Jasa Lainnya</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0 w-100"
                            id="tableDetailAktivitasJasa">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama Pelaku Usaha</th>
                                    <th class="text-center">Nama Usaha</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">NIB</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pertanianKehutananPerikanan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounted-top-lg py-3 ps-4 pe-6 bg-light">
                    <h5 class="fw-bold fs-1">Detail Pertanian, Kehutanan Dan Perikanan</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0 w-100"
                            id="tableDetailPertanianKehutananPerikanan">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr>
                                    <th class="text-center">No.</th>
                                    <th class="text-center">NIK</th>
                                    <th class="text-center">Nama Pelaku Usaha</th>
                                    <th class="text-center">Nama Usaha</th>
                                    <th class="text-center">Alamat</th>
                                    <th class="text-center">Kecamatan</th>
                                    <th class="text-center">Kelurahan</th>
                                    <th class="text-center">RT</th>
                                    <th class="text-center">RW</th>
                                    <th class="text-center">NIB</th>
                                    <th class="text-center">#</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
            </div>
        </div>
    </div>
</div>


{{-- MODAL INTERVENSI --}}
<div class="modal fade" id="detailIntervensiSwk" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi SWK</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_swk" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailSkgModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi SKG</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_skg" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPasarModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Pasar</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_pasar" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailTokelModel" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Tokel</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_tokel" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPelatihan" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Pelatihan</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_pelatihan" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPameranModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Pameran</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_pameran" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
</div>
</div> -->
<div class="modal fade" id="detailBPUMModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <h5 class="fw-bold fs-1">Detail BPUM</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_BPUM" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel text-center">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="detailRumahKreatif" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Rumah Kreatif</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_rumah_kreatif" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPadatKaryaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Padat Karya</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_padat_karya" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailKurModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Kur</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_kur" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailCsrModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi CSR</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_csr" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailPuspitaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Intervensi Puspita</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_puspita" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailMbrModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Data Warga Miskin</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_mbr" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailNonMbrModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Data Non Warga Miskin</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_non_mbr" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailBudidayaModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Kategori Budidaya Pertanian</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_budidaya" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th>No.</th>
                                        <th>NIK</th>
                                        <th>Nama Pelaku Usaha</th>
                                        <th>Nama Usaha</th>
                                        <th>Alamat</th>
                                        <th>Kecamatan</th>
                                        <th>Kelurahan</th>
                                        <th>RT</th>
                                        <th>RW</th>
                                        <th>NIB</th>
                                        <th>#</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detailBelumIntervensiModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail Belum Intervensi</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_belum_intervensi" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="detail_oss_modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1">Detail OSS</h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="datatable_oss" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th class="text-center">No.</th>
                                        <th class="text-center">NIK</th>
                                        <th class="text-center">Nama Pelaku Usaha</th>
                                        <th class="text-center">Nama Usaha</th>
                                        <th class="text-center">Alamat</th>
                                        <th class="text-center">Kecamatan</th>
                                        <th class="text-center">Kelurahan</th>
                                        <th class="text-center">RT</th>
                                        <th class="text-center">RW</th>
                                        <th class="text-center">NIB</th>
                                        <th class="text-center">#</th>

                                    </tr>
                                </thead>
                                <tbody class="text-center">

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

@push('scripts')
{{-- SCRIPT TABEL KATEGORI USAHA --}}
<script>
    $(function() {
            var url = "{!! route('dataTableBudidaya') !!}";
            $('#datatable_budidaya').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableFashion') !!}";
            $('#datatable_fashion').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableMamin') !!}";
            $('#datatable_mamin').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableKerajinan') !!}";
            $('#datatable_kerajinan').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableFashionToko') !!}";
            $('#datatable_toko').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableJasa') !!}";
            $('#datatable_jasa').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
{{-- END --}}

<!-- Script Datatable KBLI -->
<script>
    $(function() {
            var url = "{!! route('dataTablesIndustriPengolahan') !!}";
            $('#tableDetailIndustriPengolahan').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablesPerdagangan') !!}";
            $('#tableDetailPerdagangan').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablesAkomodasiUsaha') !!}";
            $('#tableDetailAkomodasiMamin').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablesAktivitasJasa') !!}";
            $('#tableDetailAktivitasJasa').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>

<script>
    $(function() {
            var url = "{!! route('datatablePertanianKehutananPerikanan') !!}";
            $('#tableDetailPertanianKehutananPerikanan').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>

{{-- SCRIPT TABEL INTERVENSI --}}
<script>
    $(function() {
            var url = "{!! route('dataTableSwk') !!}";
            $('#datatable_swk').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    }, {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableSkg') !!}";
            $('#datatable_skg').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablePasar') !!}";
            $('#datatable_pasar').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableTokel') !!}";
            $('#datatable_tokel').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablePelatihan') !!}";
            $('#datatable_pelatihan').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablePameran') !!}";
            $('#datatable_pameran').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data pameran",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableBPUM') !!}";
            $('#datatable_BPUM').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableBelumIntervensi') !!}";
            $('#datatable_belum_intervensi').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pemilik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat_pemilik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableOss') !!}";
            $('#datatable_oss').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableRumahKreatif') !!}";
            $('#datatable_rumah_kreatif').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablePadatKarya') !!}";
            $('#datatable_padat_karya').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableKur') !!}";
            $('#datatable_kur').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableCsr') !!}";
            $('#datatable_csr').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTablePuspita') !!}";
            $('#datatable_puspita').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nik',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nib',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'aksi',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
{{-- END --}}

{{-- SCRIPT TABEL MBR & NON MBR --}}
<script>
    $(function() {
            var url = "{!! route('dataTableMbr') !!}";
            $('#datatable_mbr').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
<script>
    $(function() {
            var url = "{!! route('dataTableNonMbr') !!}";
            $('#datatable_non_mbr').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'nama_pelaku_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'nama_usaha',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'alamat',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kecamatan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'kelurahan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rw',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'rt',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },

                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });
</script>
{{-- END --}}
@endpush