<?php

namespace App\Models;

use GuzzleHttp\Psr7\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TOmsetUsahaModel extends Model
{
	use HasFactory, SoftDeletes;

	protected $table = 't_omset_usaha';
	protected $guarded = 'id';
	protected $fillable = [
		'id_t_pelaku_usaha',
		'bulan_laporan',
		'jumlah_omset',
		'id_t_pelaku_usaha_disdag_181022',
	];

	protected $hidden = [
		'created_at',
		'updated_at',
		'deleted_at',
	];

	// public function pelaku_usaha()
	// {
	//     return $this->hasMany(TPelakuUsahaModel::class, 't_pelaku_usaha_id', 'id');
	// }

	public function dataTotalOmset($where)
	{

		$sql = "SELECT
					msku.nama_sub_kategori_usaha,
					SUM ( CASE WHEN om.bulan_laporan = '1' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_januari,
					SUM ( CASE WHEN om.bulan_laporan = '2' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_februari,
					SUM ( CASE WHEN om.bulan_laporan = '3' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_maret,
					SUM ( CASE WHEN om.bulan_laporan = '4' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_april,
					SUM ( CASE WHEN om.bulan_laporan = '5' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_mei,
					SUM ( CASE WHEN om.bulan_laporan =  '6' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_juni,
					SUM ( CASE WHEN om.bulan_laporan  = '7' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_juli,
					SUM ( CASE WHEN om.bulan_laporan  = '8' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_agustus,
					SUM ( CASE WHEN om.bulan_laporan  = '9'  THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_september,
					SUM ( CASE WHEN om.bulan_laporan  = '10' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_oktober,
					SUM ( CASE WHEN om.bulan_laporan  = '11' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_november,
					SUM ( CASE WHEN om.bulan_laporan  = '12' THEN om.jumlah_omset ELSE 0 END ) AS jum_omset_desember
				from
					m_sub_kategori_usaha msku
				LEFT JOIN
					(SELECT
						tpu.id as id_pelaku_usaha,
						tpu.nama_usaha,
						tpu.m_kategori_usaha_id,
						tpu.m_sub_kategori_usaha_id,
						tou.id as id_omset,
						date_part('month', tou.bulan_laporan::date)::TEXT as bulan_laporan,
						date_part('year', tou.bulan_laporan::date)::TEXT as tahun_laporan,
						tou.jumlah_omset
					FROM
						t_omset_usaha tou
					INNER JOIN t_pelaku_usaha tpu
						ON tou.id_t_pelaku_usaha = tpu.id
					WHERE
						tpu.deleted_at IS NULL
						AND tou.deleted_at IS NULL) om on msku.ID = om.m_sub_kategori_usaha_id
				$where
				GROUP BY
						msku.nama_sub_kategori_usaha";
		$data = \DB::select($sql);

		// dd($data);
		return $data;
	}


	// public function getDataMonitorOmset($id, array $params = [])
	// {
	// 	$where = ($id != "") ? "AND t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 = $id" : "";

	// 	$query = "SELECT 
	// 		t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022 AS id,
	// 		t_pelaku_usaha_disdag_181022.nama_pelaku_usaha AS nama_pelaku_usaha,
	// 		t_pelaku_usaha_disdag_181022.nama_usaha AS nama_usaha,
	// 		-- m_setup_kec.nm_kec AS nm_kec,
	// 		-- m_setup_kel.nm_kel AS nm_kel,
	// 		t_pelaku_usaha_disdag_181022.omset_bulanan AS omset_bulanan,
	// 		t_pelaku_usaha_disdag_181022.swk AS swk,
	// 		t_pelaku_usaha_disdag_181022.industri_rumahan AS industri_rumahan,
	// 		t_pelaku_usaha_disdag_181022.peken_tokel AS peken_tokel,
	// 		t_pelaku_usaha_disdag_181022.skg AS skg,
	// 		t_pelaku_usaha_disdag_181022.bpum AS bpum,
	// 		t_pelaku_usaha_disdag_181022.kur AS kur,
	// 		t_pelaku_usaha_disdag_181022.csr AS csr,
	// 		t_pelaku_usaha_disdag_181022.pelatihan AS pelatihan,
	// 		t_pelaku_usaha_disdag_181022.padat_karya AS padat_karya,
	// 		t_pelaku_usaha_disdag_181022.pameran AS pameran,
	// 		t_pelaku_usaha_disdag_181022.puapita AS puapita,
	// 		t_pelaku_usaha_disdag_181022.pasar AS pasar,
	// 		-- t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_jenis_produk,
	// 		t_pelaku_usaha_disdag_181022.rumah_kreatif AS rumah_kreatif,
	//         t_pelaku_usaha_disdag_181022.kategori_jenis_produk AS kategori_usaha,
	//         -- t_pelaku_usaha_disdag_181022.intervensi AS intervensi,
	//         kec_kel.nm_kec AS kecamatan,
	// 		kec_kel.nm_kel AS kelurahan
	//         FROM
	// 		t_pelaku_usaha_disdag_181022
	// 		LEFT JOIN (SELECT
	// 			kel.id_m_Setup_kel,
	// 			kel.nm_kel,
	// 			kel.id_m_setup_kec,
	// 			kec.nm_kec
	// 		FROM
	// 			m_Setup_kel kel
	// 			LEFT JOIN m_setup_kec kec ON kel.id_m_setup_kec = kec.id_m_setup_kec 
	// 		WHERE
	// 			kel.id_m_setup_prop = '35'
	// 			AND kel.id_m_setup_kab = '78'
	// 			AND kec.id_m_setup_prop = '35'
	// 			AND kec.id_m_Setup_kab = '78' ) kec_kel ON kec_kel.id_m_setup_kel = t_pelaku_usaha_disdag_181022.kelurahan_pemilik
	// 		AND kec_kel.id_m_setup_kec = t_pelaku_usaha_disdag_181022.kecamatan_pemilik
	// 	WHERE
	// 	t_pelaku_usaha_disdag_181022.deleted_at IS NULL " . $where;



	// 	$query_all = $query;

	// 	$offset = 0;
	// 	$limit = 0;
	// 	$dir = "";
	// 	$order = "";

	// 	if (!empty($params)) {
	// 		$offset = $params['start'];
	// 		$limit = $params['limit'];
	// 		$dir = $params['dir'];
	// 		$order = $params['order'];


	// 		// Apply search
	// 		if (!empty($params['search'])) {
	// 			$search = $params['search'];
	// 			$query .= "AND nama_pelaku_usaha ILIKE '%$search%' OR nama_usaha ILIKE '%$search%'";
	// 			$query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
	// 			$query_filtered = $query;
	// 		} else {
	// 			$query .= "ORDER BY $order $dir LIMIT $limit OFFSET $offset";
	// 		}
	// 	}

	// 	$total = count(DB::select($query_all));
	// 	$filtered = !isset($query_filtered) ? $total : count(DB::select($query_filtered));
	// 	$data = DB::select($query);
	// 	foreach ($data as $key => $value) {
	// 		$data[$key]->DT_RowIndex = $offset + ($key + 1);
	// 		// INTERVENSI
	// 		$intervensi = "<ul>";

	// 		if ($value->swk == 1) {
	// 			$intervensi .= "<li>SWK</li>";
	// 		}
	// 		if ($value->bpum == 1) {
	// 			$intervensi .= "<li>BPUM</li>";
	// 		}
	// 		if ($value->skg == 1) {
	// 			$intervensi .= "<li>SKG</li>";
	// 		}
	// 		if ($value->pasar == 1) {
	// 			$intervensi .= "<li>Pasar</li>";
	// 		}

	// 		if ($value->peken_tokel == 1 || $value->peken_tokel == 2) {
	// 			$intervensi .= "<li>Peken</li>";
	// 		}
	// 		if ($value->rumah_kreatif == 1) {
	// 			$intervensi .= "<li>Rumah Kreatif</li>";
	// 		}
	// 		if ($value->kur == 1) {
	// 			$intervensi .= "<li>KUR</li>";
	// 		}
	// 		if ($value->csr == 1) {
	// 			$intervensi .= "<li>CSR</li>";
	// 		}
	// 		if ($value->pelatihan == 1) {
	// 			$intervensi .= "<li>Pelatihan</li>";
	// 		}
	// 		if ($value->padat_karya == 1) {
	// 			$intervensi .= "<li>Padat Karya</li>";
	// 		}
	// 		if ($value->pameran == 1) {
	// 			$intervensi .= "<li>Pameran</li>";
	// 		}
	// 		if ($value->puspita == 1) {
	// 			$intervensi .= "<li>Puspita</li>";
	// 		}

	// 		$intervensi .= "</ul>";
	// 		// dd($intervensi);

	// 		// $data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $dat])->render();
	// 		$data[$key]->intervensi = $intervensi;
	// 		// $data[$key]['intervensi'] = $intervensi;

	// 		// END INTERVENSI
	// 		$data[$key]->action = view('pelaku_usaha.dt-action', ['data' => $value])->render();
	// 	}
	// 	return [
	// 		'total' => $total,
	// 		'filtered' => $filtered,
	// 		'data' => $data,
	// 	];
	// }
}
