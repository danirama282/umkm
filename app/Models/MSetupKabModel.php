<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MSetupKabModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "m_setup_kab";
    protected $primaryKey = "id_m_setup_kab";

    public $timestamps = true;
}
