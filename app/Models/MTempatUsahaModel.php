<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MTempatUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_tempat_usaha';
    protected $fillable = [
        'm_jenis_tempat_usaha_id',
        'nama_tempat_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Relasi dengan MJenisTempatUsahaModel */
    public function jenis_tempat_usaha()
    {
        return $this->belongsTo(MJenisTempatUsahaModel::class, 'm_jenis_tempat_usaha_id', 'id');
    }
}
