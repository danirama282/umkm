<x-templates.default>
    <x-slot name="title">Omset Usaha</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Detail Data Omset Usaha </h3>
                    </div>
                </div>
                <div class="card-body position-relative">
                    <input type="hidden" name="id_omset" id="id_omset"
                        value="{{ Crypt::encryptString(strval($detail_pelaku_usaha[0]->id)) }}">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="namaUsaha">Nama Usaha</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="namaUsaha" type="text" readonly=""
                                        value="{{ $detail_pelaku_usaha[0]->nama_usaha ?? '-'}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="nik">NIK</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="nik" type="text" readonly=""
                                        value="{{ $detail_pelaku_usaha[0]->nik ?? '-'}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="statusMBR">Status MBR</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="statusMBR" type="text" readonly=""
                                        value="{{ $detail_pelaku_usaha[0]->status_mbr ?? '-'}}" />
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="namaPemilik">Nama Pemilik</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="namaPemilik" type="text" readonly=""
                                        value="{{ $detail_pelaku_usaha[0]->nama_pelaku_usaha ?? '-'}}" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-3 col-md-2 col-lg-2">
                                    <label class="col-form-label" for="alamatPemilik">Alamat Pemilik</label>
                                </div>
                                <div class="col-2 col-md-1 col-lg-1">
                                    <label class="col-form-label">:</label>
                                </div>
                                <div class="col-7 col-md-8 col-lg-8"><input class="form-control-plaintext outline-none"
                                        id="alamatPemilik" type="text" readonly=""
                                        value="{{ $detail_pelaku_usaha[0]->alamat_pemilik ?? '-'}}, RT {{ $detail_pelaku_usaha[0]->rt_pemilik ?? '-'}}, RW {{ $detail_pelaku_usaha[0]->rw_pemilik ?? '-'}}, Kelurahan  {{ $detail_pelaku_usaha[0]->kelurahan ?? '-'}}, Kecamatan  {{ $detail_pelaku_usaha[0]->kecamatan ?? '-'}}" />
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('pelaku_usaha.index') }}" class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Daftar Omset Usaha</h3>
                    </div>
                </div>

                <div class="card-body">
                    <div class="d-flex flex-column align-content-md-end align-items-end">
                        <a href="{{ route('omset_usaha.create', Crypt::encryptString($detail_pelaku_usaha[0]->id)) }}"
                            class="btn btn-primary">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Tambah Data Omset
                            Usaha
                        </a>

                        <a href="{{ route('omset_usaha.excel', Crypt::encryptString($detail_pelaku_usaha[0]->id)) }}" class="btn btn-success my-3" id="excel">
                            <span class="fas fa-plus-square" data-fa-transform="shrink-3"></span> Export Excel
                        </a>
                    </div>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="text-center" width="10%">No</th>
                                    <th class="text-center" scope="col">Bulan Laporan</th>
                                    <th class="text-center" scope="col">Jumlah Omset</th>
                                    <th class="text-center" width="20%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Konten baris keempat -->


    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script>
        $(function () {
            let table;
            var id_pelaku_usaha = $('#id_omset').val();
            var url = "{{ url('omset_usaha') }}/" + id_pelaku_usaha;
            table = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: url,
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'bulan_laporan.display',
                        name: 'bulan_laporan'
                    },
                    {
                        data: 'jumlah_omset',
                        render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                    },
                    {
                        data: 'action',
                        name: 'action',
                        orderable: false,
                        searchable: false
                    },
                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan  _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Pencarian",
                    "zeroRecords": "Tidak ada data",
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
            });
        });


        // delete datatable
        $('#dataTable').on('click', '#delete_data', function () {
            var id = $(this).data('id');
            // console.log(id);
            // var nama_instansi = $(this).data('nama_instansi');

            swal.fire({
                    title: 'Apakah Anda Yakin ?',
                    text: 'Menghapus data ini ?',
                    // text: 'Menghapus '+nama_instansi+' dari data instansi',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                })
                .then(function (e) {
                    if (e.value) {
                        $.ajax({
                            url: "{{ route('omset_usaha.destroy') }}",
                            method: 'delete',
                            data: {
                                'id': id,
                                '_token': '{{ csrf_token() }}',
                            },
                            // data: {id: id},
                            // headers: { 'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content') },
                            success: function (data) {
                                if ($.isEmptyObject(data.error)) {
                                    swal.fire('Deleted!', 'berhasil menghapus data !',
                                            'success')
                                        .then(function (e) {
                                            // table.ajax.reload();
                                            location.reload();
                                        });
                                }
                            },
                            error: function () {
                                swal.fire("Telah terjadi kesalahan pada sistem",
                                    "Mohon refresh halaman browser Anda", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                })
        });

    </script>
    @endpush


</x-templates.default>
