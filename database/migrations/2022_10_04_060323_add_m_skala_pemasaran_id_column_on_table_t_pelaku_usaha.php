<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddMSkalaPemasaranIdColumnOnTableTPelakuUsaha extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            //
            $table->bigInteger('m_skala_pemasaran_id')->after('m_skala_usaha_id')->unsigned()->nullable();
            $table->foreign('m_skala_pemasaran_id')->references('id')->on('m_skala_pemasaran');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_pelaku_usaha', function (Blueprint $table) {
            // Drop foreign key
            $table->dropForeign('t_pelaku_usaha_m_skala_pemasaran_id_foreign');
            // Drop column
            $table->dropColumn('m_skala_pemasaran_id');
        });
    }
}
