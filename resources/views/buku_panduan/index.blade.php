<x-templates.default>
    <x-slot name="title">Buku Panduan (User Guide)</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Buku Panduan (User Guide)</h5>
                    </div>
                </div>

                <div class="card-body">
                    <div class="w-auto text-end" id="btn_aksi">
                        @if ($data['roles'] == "superadmin")
                        <button type="button" onclick="tambah_panduan()" class="btn btn-primary my-3">
                            <span class="align-middle">Tambah File Panduan</span>
                        </button>
                        @else

                        @endif
                    </div>
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="col-1">No</th>
                                    <th>Nama Panduan</th>
                                    <th>Kategori Panduan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


    {{-- MODAL --}}
    <div class="modal fade" id="modal_tambah_panduan" data-bs-keyboard="false" data-bs-backdrop="static" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg mt-6" role="document">
            <div class="modal-content border-0">
                <div class="position-absolute top-0 end-0 mt-3 me-3 z-index-1"><button
                        class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                        data-bs-dismiss="modal" aria-label="Close"></button></div>
                <div class="modal-body">
                    <div class="bg-light rounded-top-lg py-3 ps-4 pe-6">
                        <h4 class="mb-1" id="staticBackdropLabel">Tambah Data Buku Panduan</h4>
                    </div>
                    <form id="form_submit_panduan" enctype="multipart/form-data">
                        @csrf
                        <div class="mb-3">
                            <label class="col-form-label" for="nama_panduan">Nama Panduan:</label>
                            <input class="form-control" id="nama_panduan" name="nama_panduan" type="text" required />
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="kategori_panduan">Kategori
                                Panduan</label>
                            <select name="kategori_panduan" id="kategori_panduan"
                                class="form-control form-select js-select2" aria-label="Default select example"
                                required>
                                <option value="" selected disabled>Pilih :</option>
                                <option value="kecamatan">Kecamatan</option>
                                <option value="dinas">Dinas</option>
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="col-form-label" for="file_panduan">File:</label>
                            <input class="form-control" id="file_panduan" name="file_panduan" type="file" {{--
                                accept=".pdf" --}} required />
                        </div>
                        <div class=" modal-footer">
                            <button type="button" data-bs-dismiss="modal" class="btn btn-light me-3">Batal</button>
                            <button type="submit" id="submit" class="btn btn-primary">
                                <span class="indicator-label">Simpan</span>
                                {{-- <span class="indicator-progress">Mohon tunggu . . .
                                    <span class="spinner-border spinner-border-sm align-middle ms-2"></span></span> --}}
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>
    <script>
        function tambah_panduan() {
            $('#modal_tambah_panduan').modal('show');
            $('#form_submit_panduan').trigger("reset");
        }

        /* $("#file_panduan").on("change", function (e) {
            var file = $(this)[0].files[0];
            var upload = new Upload(file);

            // maby check size or type here with upload.getSize() and upload.getType()

            // execute upload
            upload.doUpload();
        }); */

        $('#form_submit_panduan').submit(function (e) {
            e.preventDefault();
            $.ajax({
                type: "post",
                url: "{{ route('buku_panduan.store') }}",
                dataType: "json",
                processData: false,
                contentType: false,
                data: new FormData($('#form_submit_panduan')[0]),
                success: function (data) {
                    if (data.status) {

                        Swal.fire({
                            title: 'Berhasil',
                            html: data.message,
                            icon: 'success',
                            confirmButtonText: 'OK'
                        }).then((result) => {
                            // datatable.ajax.reload();
                            location.reload();
                        });
                    } else {
                        Swal.fire({
                            title: 'Gagal',
                            html: data.message,
                            icon: 'error',
                            confirmButtonText: 'OK'
                        })
                    }
                },error: function(xhr, status, error) {
                    Swal.fire({
                        title: "Terjadi Kesalahan Teknis! Silahkan Coba Lagi.",
                        icon: "error",
                        buttonsStyling: !1,
                        confirmButtonText: "OK",
                        customClass: {
                            confirmButton: "btn btn-primary"
                        }
                    })
                }
            });
        });


        var url = "{{ route('buku_panduan.index') }}";

        $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nama_panduan',
                    // name: 'nama_panduan'
                },
                {
                    data: 'kategori_panduan',
                    // name: 'kategori_panduan'
                },
                /* {
                    data: 'file_panduan',
                    // name: 'file_panduan'
                }, */
                {
                    data: 'aksi',
                    // name: 'aksi',
                    orderable: false,
                    searchable: false
                },
            ],
            "pageLength": 5,
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "language": {
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                "infoFiltered": "(disaring dari _MAX_ total data)",
                "lengthMenu": "Menampilkan _MENU_ data",
                "search": "Cari:",
                "zeroRecords": "Tidak ada data yang sesuai",
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            },
        });

        $('#dataTable').on('click', '#delete_data', function() {
            var id = $(this).data('id');

            swal.fire({
                    title: 'Apakah Anda Yakin ?',
                    text: 'Ingin Menghapus Data Ini ?',
                    // text: 'Menghapus '+nama_instansi+' dari data instansi',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Ya',
                    cancelButtonText: 'Tidak'
                })
                .then(function(e) {
                    if (e.value) {
                        $.ajax({
                            url: '{{ route('buku_panduan.destroy') }}',
                            method: 'delete',
                            data: {
                                'id': id,
                                '_token': '{{ csrf_token() }}',
                            },
                            // data: {id: id},
                            // headers: { 'X-CSRF-TOKEN': $('meta[name=csrf-token]').attr('content') },
                            success: function(data) {
                                if ($.isEmptyObject(data.error)) {
                                    swal.fire('Data Terhapus!', 'berhasil menghapus data !',
                                            'success')
                                        .then(function(e) {
                                            // table.ajax.reload();
                                            location.reload();
                                        });
                                }
                            },
                            error: function() {
                                swal.fire("Telah terjadi kesalahan pada sistem",
                                    "Mohon refresh halaman browser Anda", "error");
                            }
                        });
                    } else {
                        return false;
                    }
                })
        });

    </script>
    @endpush
</x-templates.default>
