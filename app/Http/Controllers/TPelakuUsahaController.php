<?php

namespace App\Http\Controllers;

use App\Exports\ExportAllOmset;
use App\Models\MJenisTempatUsahaModel;
use App\Models\MSetupKabModel;
use App\Models\MSetupKecModel;
use App\Models\MSetupPropModel;
use App\Models\MKategoriUsahaModel;
use App\Models\MSetupKelModel;
use App\Models\MSubKategoriUsahaModel;
use App\Models\MSkalaPemasaranModel;
use App\Models\MSkalaUsahaModel;
use App\Models\MStatusBangunanModel;
use App\Models\MTempatUsahaModel;
use App\Models\PadatKaryaModel;
use App\Models\TOmsetUsahaModel;
use App\Models\TPelakuUsahaDisdag131022Model;
use App\Models\TPelakuUsahaModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;

use function PHPUnit\Framework\isEmpty;

class TPelakuUsahaController extends Controller
{
    public function index(Request $request, TPelakuUsahaDisdag131022Model $pelakuUsaha)
    {
        $data_user['user'] = auth()->user()->load('roles');
        // dd($data_user['user']);
        // $data['superadmin'] = Auth::user()->hasRole('superadmin');

        $data['sudah_verif'] = TPelakuUsahaDisdag131022Model::sudah_verif();
        // dd($data['sudah_verif']);

        $data['belum_verif'] = TPelakuUsahaDisdag131022Model::belum_verif();
        $data['count_total_pelaku'] = TPelakuUsahaDisdag131022Model::scopeCountData();
        $data['belum_verif_keckel'] = TPelakuUsahaDisdag131022Model::belum_verif_keckel($data_user['user']);
        // dd($data['belum_verif_keckel']);
        $data['sudah_verif_keckel'] = TPelakuUsahaDisdag131022Model::sudah_verif_keckel($data_user['user']);
        // dd($data['sudah_verif_keckel']);
        $data['count_total_pelaku_keckel'] = TPelakuUsahaDisdag131022Model::scopeCountDataKeckel($data_user['user']);

        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')->where('nm_prop', 'JAWA TIMUR')->first();
        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('nm_kab', 'KOTA SURABAYA')->get();
        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->orderBy('nm_kec', 'ASC')->get();

        return view('pelaku_usaha.index', compact('data_user', 'data'));
    }

    public function datatable(Request $request, TPelakuUsahaDisdag131022Model $pelakuUsaha)
    {
        $data_user['user'] = auth()->user()->load('roles');
        if ($request->ajax()) {
            $columns = [
                'nik',
                'nama_pelaku_usaha',
                'nama_usaha',
                'alamat_pemilik',
                'kategori_usaha',
                'kategori_usaha',
                'kategori_usaha',
            ];
            $params['limit'] = $request->input('length');
            $params['start'] = $request->input('start');
            $params['order'] = $columns[$request->input('order.0.column')];
            $params['dir'] = $request->input('order.0.dir');
            $params['search'] = $request->input('search.value');

            /* Memanggil getDataPelakuUsaha dari TPelakuUsahaDisdag131022Model */
            $data = $pelakuUsaha->getDataPelakuUsaha("", $params, $data_user, $request->all());

            $json_data = array(
                "draw"            => intval($request->input('draw')),
                "recordsTotal"    => intval($data['total']),
                "recordsFiltered" => intval($data['filtered']),
                "data"            => $data['data']
            );
            return response()->json($json_data);
        }

        return view('pelaku_usaha.index');
    }

    public function create()
    {
        /* Menyiapkan $data_wilayah yang berisi data array dari provinsi, kabupaten, dan kecamatan */
        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')->where('nm_prop', 'JAWA TIMUR')->first();
        // dd($data['provinsi']->id_m_setup_prop);
        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->where('nm_kab', 'KOTA SURABAYA')->get();

        /* Menambahkan order by */
        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->orderBy('nm_kec', 'ASC')->get();
        // $data['kec'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->orderBy('nm_kec', 'ASC')->get();
        // dd($data['kecamatan']->toArray());

        /* Mengambil data MKategoriUsahaModel */
        $data['kategori_usaha'] = MKategoriUsahaModel::select('id', 'nama_kategori_usaha')->get();

        /* Mengambil data MJenisTempatUsahaModel */
        $data['jenis_tempat_usaha'] = MJenisTempatUsahaModel::select('id', 'nama_jenis_tempat_usaha')->get();

        /* Mengambil data MStatusBangunanModel */
        $data['status_bangunan'] = MStatusBangunanModel::select('id', 'nama_status_bangunan')->get();

        /* Mengambil data MSkalaPemasaranModel */
        $data['skala_pemasaran'] = MSkalaPemasaranModel::select('id', 'nama_skala_pemasaran')->get();

        /* Mengambil data MSkalaUsahaModel */
        $data['skala_usaha'] = MSkalaUsahaModel::select('id', 'nama_skala_usaha')->get();

        $data['provinsi_non_sby'] = MSetupPropModel::orderBy('nm_prop', 'asc')->get();

        // dd($data);
        return view('pelaku_usaha.create', compact('data'));
    }

    public function store(Request $request)
    {
        /* Membuat validasi sesusai dengan $request */
        $validator = Validator::make($request->all(), [
            "nik" => "required",
            "nama_pelaku_usaha" => "required",
            "alamat_ktp" => "required",
            // "kecamatan_usaha" => "required",
            // "kelurahan_usaha" => "required",
            // "rt_usaha" => "required",
            // "rw_usaha" => "required",
            // "tanggal_lahir" => "required",
            // "no_telp_usaha" => "required",
            // "nama_usaha" => "required",
            // "kategori_kbli" => "required",
            // "produk_usaha" => "required",
            // "jenis_produk" => "required",
            // "skala_pemasaran" => "required",
            // "status_bangunan" => "required",
            // "tahun_binaan" => "required",
            // "jumlah_permodalan" => "required",
            // "kapasitas_produksi" => "required",
            // "biaya_produksi" => "required",
            // "jumlah_tenaga_kerja" => "required",
            // "no_npwp_pemilik" => "required",
            // "no_nib" => "required",
            // "no_iumk" => "required",
            // "no_pirt" => "required",
            // "no_bpom" => "required",
            // "sertifikat_merk" => "required",
            // "haki" => "required",
        ]);

        /* Inisiasi nama atribut */
        $attrNames = array(
            "nik" => "NIK Usaha",
            "nama_pelaku_usaha" => "Nama Pelaku Usaha",
            "alamat_ktp" => "Alamat Pelaku Usaha",
            // "kecamatan_usaha" => "Kecamatan Usaha",
            // "kelurahan_usaha" => "Kelurahan Usaha",
            // "rt_usaha" => "RT Usaha",
            // "rw_usaha" => "Rw Usaha",
            // "telp_usaha" => "Telp Usaha",
            // "tgl_lahir_pelaku_usaha" => "Tanggal Lahir Pelaku Usaha",
            // "nama_usaha" => "Nama Usaha",
            // "kategori_usaha" => "Kategori Usaha",
            // "produk_usaha" => "Produk Usaha",
            // "skala_pemasaran" => "Skala Pemasaran",
            // "status_bangunan" => "Status Bangunan",
            // "tahun_binaan" => "Tahun Binaan",
            // "biaya_produksi" => "Biaya Produksi",
            // "jumlah_tenaga_kerja" => "Jumlah Tenaga Kerja",
            // "satus_anggota_koperasi" => "Status Anggota Koperasi",
            // "sumber_keaktifan" => "Sumber Keaktifan",
            // "no_npwp_pemilik" => "Nomor NPWP Pemilik",
            // "no_nib" => "Nomor NIB",
            // "no_iumk" => "Nomor IUMK",
            // "no_pirt" => "Nomor PIRT",
            // "no_bpom" => "Nomor BPOM",
            // "sertifikat_merk" => "Sertifikat Merk",
            // "haki" => "HAKI",
        );

        /* setAttributesName dengan parameter $attrNames ke dalam $validator */
        $validator->setAttributeNames($attrNames);
        $roles = auth()->user()->load('roles')->toArray();
        $roles = $roles['roles'][0]['name'];
        if ($roles == "superadmin") {
            # code...
            // dd('mwasok');
            $tgl_verifikasi_kec =  null;
        } else {
            $tgl_verifikasi_kec = Carbon::now()->toDateTimeString();
            # code...
        }

        $cek_nik = DB::table('t_pelaku_usaha_disdag_181022')
            ->select('nik')
            ->where('nik', $request->nik)
            ->whereNull('deleted_at')
            ->first();


        // NOTE: CEK APAKAH NIK SUDAH TERDAFTAR
        if ($cek_nik) {
            return response()->json([
                'status' => false,
                'message' => 'Data NIK sudah terdaftar!'
            ]);
        }

        if ($validator->passes()) {
            /* Melakukan pengecekan pada $validator */

            $pelakuUsaha = new TPelakuUsahaDisdag131022Model();
            $pelakuUsaha->is_surabaya = $request->status;
            $pelakuUsaha->nik = $request->nik;
            $pelakuUsaha->nama_pelaku_usaha = $request->nama_pelaku_usaha;
            $pelakuUsaha->alamat_ktp = $request->alamat_ktp;
            $pelakuUsaha->provinsi_pemilik = $request->provinsi ? $request->provinsi : $request->id_m_setup_prop;
            $pelakuUsaha->kabupaten_pemilik = $request->kabupaten_kota ? $request->kabupaten_kota :  $request->id_m_setup_kab;
            $pelakuUsaha->kecamatan_pemilik = $request->kecamatan ? $request->kecamatan : $request->kecamatan_api;
            $pelakuUsaha->kelurahan_pemilik = $request->kelurahan ? $request->kelurahan : $request->kelurahan_api;
            $pelakuUsaha->rt_ktp = $request->rt_ktp;
            $pelakuUsaha->rw_ktp = $request->rw_ktp;
            $pelakuUsaha->tanggal_lahir = $request->tanggal_lahir;
            $pelakuUsaha->alamat_domisili = $request->alamat_domisili;
            $pelakuUsaha->kecamatan = $request->kecamatan_string;
            $pelakuUsaha->kelurahan = $request->kelurahan_string;

            $pelakuUsaha->kelurahan_domisili = $request->kelurahan_domisili ? $request->kelurahan_domisili : $request->kelurahan_domisili_ktp;
            $pelakuUsaha->kecamatan_domisili = $request->kecamatan_domisili ? $request->kecamatan_domisili : $request->kecamatan_domisili_ktp;
            $pelakuUsaha->no_telp = $request->no_telp;
            $pelakuUsaha->nama_usaha = $request->nama_usaha;
            $pelakuUsaha->alamat_usaha = $request->alamat_usaha;
            $pelakuUsaha->kecamatan_usaha = $request->kecamatan_usaha;
            $pelakuUsaha->kelurahan_usaha = $request->kelurahan_usaha;
            $pelakuUsaha->kategori_jenis_produk = $request->kategori_jenis_produk;
            $pelakuUsaha->jenis_produk = $request->jenis_produk;
            $pelakuUsaha->skala_pemasaran = $request->skala_pemasaran;
            $pelakuUsaha->status_bangunan = $request->status_bangunan;
            $pelakuUsaha->tahun_binaan = $request->tahun_binaan;
            $pelakuUsaha->sumber_permodalan = $request->sumber_permodalan;
            $pelakuUsaha->jumlah_permodalan = $request->jumlah_permodalan;
            $pelakuUsaha->kapasitas_produksi = $request->kapasitas_produksi;
            $pelakuUsaha->biaya_produksi = $request->biaya_produksi;
            $pelakuUsaha->jumlah_tenaga_kerja = $request->jumlah_tenaga_kerja;
            // $pelakuUsaha->beras = ($request->beras == 1) ? 1 : 0;
            $pelakuUsaha->ayam_bebek = $request->ayam_bebek ? 1 : 0;
            $pelakuUsaha->beras = $request->beras ? 1 : 0;
            $pelakuUsaha->buah = $request->buah ? 1 : 0;
            $pelakuUsaha->cabai = $request->cabai ? 1 : 0;
            $pelakuUsaha->daging = $request->daging ? 1 : 0;
            $pelakuUsaha->gula = $request->gula ? 1 : 0;
            $pelakuUsaha->kain = $request->kain ? 1 : 0;
            $pelakuUsaha->kayu = $request->kayu ? 1 : 0;
            $pelakuUsaha->kedelai = $request->kedelai ? 1 : 0;
            $pelakuUsaha->minyak = $request->minyak ? 1 : 0;
            $pelakuUsaha->sayur = $request->sayur ? 1 : 0;
            $pelakuUsaha->tepung = $request->tepung ? 1 : 0;
            $pelakuUsaha->telur = $request->telur ? 1 : 0;
            $pelakuUsaha->keanggotaan_koperasi = $request->keanggotaan_koperasi;
            $pelakuUsaha->status_keaktifan = $request->status_keaktifan;
            $pelakuUsaha->npwp = $request->npwp;
            $pelakuUsaha->nib = $request->nib;
            $pelakuUsaha->iumk = $request->iumk;
            $pelakuUsaha->pirt = $request->pirt;
            $pelakuUsaha->bpom = $request->bpom;
            $pelakuUsaha->sertifikat_merk = $request->sertifikat_merk;
            $pelakuUsaha->sertifikat_halal = $request->sertifikat_halal;
            $pelakuUsaha->haki = $request->haki;
            $pelakuUsaha->csr = $request->csr ? 1 : null;
            $pelakuUsaha->bpum = $request->bpum ? 1 : null;
            $pelakuUsaha->kur = $request->kur ? 1 : null;
            $pelakuUsaha->pameran = $request->pameran ? 1 : null;
            $pelakuUsaha->pasar = $request->pasar ? 1 : null;
            $pelakuUsaha->padat_karya = $request->padat_karya ? 1 : null;
            $pelakuUsaha->puspita = $request->puspita ? 1 : null;
            $pelakuUsaha->swk = $request->swk ? 1 : null;
            $pelakuUsaha->industri_rumahan = $request->industri_rumahan ? 1 : null;
            $pelakuUsaha->skg = $request->skg ? 1 : null;
            $pelakuUsaha->peken_tokel = $request->peken_tokel ?? null;
            $pelakuUsaha->rumah_kreatif = $request->rumah_kreatif ? 1 : null;
            $pelakuUsaha->pelatihan = $request->pelatihan ? 1 : null;
            $pelakuUsaha->oss = $request->oss ? 1 : null;
            $pelakuUsaha->omset_bulanan = $request->omset_awal;
            $pelakuUsaha->keterangan = $request->keterangan;
            $pelakuUsaha->tgl_verifikasi_kec = $tgl_verifikasi_kec;
            $pelakuUsaha->created_by = Auth::user()['id'];
            /* Melakukan operasi simpan pada data table */
            $pelakuUsaha->save();

            /* Menyimpan hasil operasi */
            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Data berhasil disimpan!'
            ]);
            /* Inisiasi proses transaksi pada database */
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function edit($id, TPelakuUsahaDisdag131022Model $pelakuUsaha)
    {
        // $id = Crypt::decryptString($id);
        $data['pelaku_usaha'] = $pelakuUsaha->find(Crypt::decryptString($id));

        $provinsiKu = $data['pelaku_usaha']['provinsi_pemilik'];
        $kabupatenKu = $data['pelaku_usaha']['kabupaten_pemilik'];
        $kecamatanKu = $data['pelaku_usaha']['kecamatan_pemilik'];


        // Menyiapkan $data_wilayah yang berisi data array dari provinsi, kabupaten, dan kecamatan
        $data['provinsi_non_sby'] = MSetupPropModel::orderBy('nm_prop', 'asc')->get();
        $data['provinsi'] = MSetupPropModel::select('id_m_setup_prop')
            ->where('nm_prop', 'JAWA TIMUR')->first();
        $data['provinsiKu'] = MSetupPropModel::select('id_m_setup_prop', 'nm_prop')
            ->where('id_m_setup_prop', $provinsiKu)->first();
        $data['kabupatenKu'] = MSetupKabModel::select('id_m_setup_kab', 'nm_kab')
            ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
            ->where('id_m_setup_kab', $kabupatenKu)->get();
        $data['kabupaten'] = MSetupKabModel::select('id_m_setup_kab')
            ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
            ->where('nm_kab', 'KOTA SURABAYA')->get();
        $data['kecamatan'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')
            ->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)
            ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)->orderBy('nm_kec', 'ASC')->get();
        $data['kecamatanKu'] = MSetupKecModel::select('id_m_setup_kec', 'nm_kec')
            ->where('id_m_setup_kab', $kabupatenKu)
            ->where('id_m_setup_prop', $provinsiKu)->orderBy('nm_kec', 'ASC')->get();




        /* Melakukan pengecekan terhadap variable $data['pelaku_usaha'] khususnya pada object kecamatan_usaha dan kelurahan_usaha */
        if ($data['pelaku_usaha']->kecamatan_usaha != null && $data['pelaku_usaha']->kelurahan_usaha != null) {
            /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_usaha, kelurahan_usaha */
            $data['kelurahan_usaha'] = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')
                ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
                ->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)
                ->where('id_m_setup_kec', $data['pelaku_usaha']->kecamatan_usaha)
                ->where('id_m_setup_kel', $data['pelaku_usaha']->kelurahan_usaha)->orderBy('nm_kel', 'ASC')->get();
        } else {
            /* Mengisi variable $data['kelurahan_usaha'] dengan value null atau 0 */
            $data['kelurahan_usaha'] = 'kosong';
        }

        if ($data['pelaku_usaha']->provinsi_pemilik != null && $data['pelaku_usaha']->kabupaten_pemilik != null) {
            /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_pemilik, kelurahan_pemilik */

            $data['kabupaten_pemilik'] = MSetupKabModel::select('id_m_setup_kab', 'nm_kab')
                ->where('id_m_setup_prop', $provinsiKu)
                ->where('id_m_setup_kab', $kabupatenKu)->orderBy('nm_kab', 'ASC')->get();
        } else {
            /* Mengisi variable $data['kelurahan_pemilik'] dengan value null atau 0 */
            $data['kabupaten_pemilik'] = "kosong";
        }



        if ($data['pelaku_usaha']->kecamatan_pemilik != null && $data['pelaku_usaha']->kelurahan_pemilik != null) {
            /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_pemilik, kelurahan_pemilik */

            $data['kelurahan_pemilik'] = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')
                ->where('id_m_setup_prop', $provinsiKu)
                ->where('id_m_setup_kab', $kabupatenKu)
                ->where('id_m_setup_kec', $kecamatanKu)
                ->where('id_m_setup_kel', $data['pelaku_usaha']->kelurahan_pemilik)->orderBy('nm_kel', 'ASC')->get();
        } else {
            /* Mengisi variable $data['kelurahan_pemilik'] dengan value null atau 0 */
            $data['kelurahan_pemilik'] = "kosong";
        }


        if ($data['pelaku_usaha']->kecamatan_domisili != null && $data['pelaku_usaha']->kelurahan_domisili != null) {
            /* Mengambil data kelurahan berdasarkan  $data['provinsi'], $data['kabupaten'], kecamatan_domisili, kelurahan_domisili */
            $data['kelurahan_domisili'] = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')
                ->where('id_m_setup_prop', $data['provinsi']->id_m_setup_prop)
                ->where('id_m_setup_kab', $data['kabupaten'][0]->id_m_setup_kab)
                ->where('id_m_setup_kec', $data['pelaku_usaha']->kecamatan_domisili)
                ->where('id_m_setup_kel', $data['pelaku_usaha']->kelurahan_domisili)->orderBy('nm_kel', 'ASC')->get();
        } else {
            /* Mengisi variable $data['kelurahan_usaha'] dengan value null atau 0 */
            $data['kelurahan_domisili'] = 'kosong';
        }

        $data['kategori_usaha'] = [
            'MAKANAN_DAN_MINUMAN' => 'Makanan Dan Minuman',
            'FASHION' => 'Fashion',
            'JASA' => 'Jasa',
            'KERAJINAN' => 'Kerajinan',
            'PEMBUDIDAYA_SEKTOR_PERTANIAN' => 'Pembudidaya Sektor Pertanian',
            'TOKO' => 'Toko',
        ];

        $data['skala_pemasaran'] = [
            'LOKAL' => 'Lokal',
            'REGIONAL' => 'Regional',
            'NASIONAL' => 'Nasional',
            'INTERNASIONAL' => 'Internasional',
        ];

        $data['status_bangunan'] = [
            'MILIK_SENDIRI' => 'Milik Sendiri',
            'SEWA' => 'Sewa',
        ];

        $data['sumber_permodalan'] = [
            'SENDIRI' => 'Sendiri',
            'PINJAMAN_BANK' => 'Pinjaman Bank',
            'PINJAMAN_NON_BANK' => 'Pinjaman Non Bank',
            'HIBAH' => 'Hibah',
        ];


        $data['keanggotaan_koperasi'] = [
            'YA' => 'Ya',
            'TIDAK' => 'Tidak',
        ];

        $data['status_keaktifan'] = [
            'AKTIF' => 'Aktif',
            'TIDAK_AKTIF' => 'Tidak Aktif',
            'LIBUR' => 'Libur',
        ];

        return view('pelaku_usaha.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        /* Membuat validasi sesusai dengan $request */
        $decrypted_id = Crypt::decryptString($id);


        $validator = Validator::make($request->all(), [
            'nik' => 'unique:t_pelaku_usaha_disdag_181022,nik,' . $decrypted_id . ',id_t_pelaku_usaha_disdag_181022,deleted_at,NULL',
            /* "no_telp_usaha" => "required",
            "nama_usaha" => "required",
            "kategori_kbli" => "required",
            "produk_usaha" => "required",
            "jenis_produk" => "required",
            "skala_pemasaran" => "required",
            "status_bangunan" => "required",
            "tahun_binaan" => "required",
            "jumlah_permodalan" => "required",
            "kapasitas_produksi" => "required",
            "biaya_produksi" => "required",
            "jumlah_tenaga_kerja" => "required",
            "no_npwp_pemilik" => "required",
            "no_nib" => "required",
            "no_iumk" => "required",
            "no_pirt" => "required",
            "no_bpom" => "required",
            "sertifikat_merk" => "required",
            "haki" => "required", */
        ]);

        /* Inisiasi nama atribut */
        $attrNames = array(
            /* "telp_usaha" => "Telp Usaha",
            "nama_usaha" => "Nama Usaha",
            "kategori_usaha" => "Kategori Usaha",
            "produk_usaha" => "Produk Usaha",
            "skala_pemasaran" => "Skala Pemasaran",
            "status_bangunan" => "Status Bangunan",
            "tahun_binaan" => "Tahun Binaan",
            "biaya_produksi" => "Biaya Produksi",
            "jumlah_tenaga_kerja" => "Jumlah Tenaga Kerja",
            "satus_anggota_koperasi" => "Status Anggota Koperasi",
            "sumber_keaktifan" => "Sumber Keaktifan",
            "no_npwp_pemilik" => "Nomor NPWP Pemilik",
            "no_nib" => "Nomor NIB",
            "no_iumk" => "Nomor IUMK",
            "no_pirt" => "Nomor PIRT",
            "no_bpom" => "Nomor BPOM",
            "sertifikat_merk" => "Sertifikat Merk",
            "haki" => "HAKI", */);

        /* setAttributesName dengan parameter $attrNames ke dalam $validator */
        $validator->setAttributeNames($attrNames);

        DB::beginTransaction();
        $pelaku_usaha = TPelakuUsahaDisdag131022Model::find(Crypt::decryptString($id));
        /* Mengupdate data pelaku_usaha berdasarkan $id */

        if ($validator->passes()) {
            $roles = auth()->user()->load('roles')->toArray();
            $roles = $roles['roles'][0]['name'];
            if ($roles == "superadmin") {
                # code...
                $tgl_verifikasi_kec = $pelaku_usaha->tgl_verifikasi_kec ?? null;
            } else {
                $tgl_verifikasi_kec = Carbon::now()->toDateTimeString();
                # code...
            }

            $pelaku_usaha->update([
                'is_surabaya' => $request->status,
                'nik' => $request->nik,
                'nama_pelaku_usaha' => $request->nama_pelaku_usaha,
                'alamat_ktp' => $request->alamat_ktp,
                'provinsi_pemilik' => $request->provinsi ? $request->provinsi : $request->id_m_setup_prop,
                'kabupaten_pemilik' => $request->kabupaten_kota ? $request->kabupaten_kota : $request->id_m_setup_kab,
                'kelurahan_pemilik' => $request->kelurahan,
                'kecamatan_pemilik' => $request->kecamatan,
                'kecamatan_usaha' => $request->kecamatan_usaha,
                'kelurahan_usaha' => $request->kelurahan_usaha,
                'rt_ktp' => $request->rt_ktp,
                'rw_ktp' => $request->rw_ktp,
                'tanggal_lahir' => $request->tanggal_lahir,
                'no_telp' => $request->no_telp,
                'nama_usaha' => $request->nama_usaha,
                'alamat_usaha' => $request->alamat_usaha,
                'kelurahan_usaha' => $request->kelurahan_usaha,
                'kecamatan_usaha' => $request->kecamatan_usaha,
                'alamat_domisili' => $request->alamat_domisili,

                'kelurahan_domisili' => $request->kelurahan_domisili ? $request->kelurahan_domisili : $request->kelurahan_domisili_ktp,
                'kecamatan_domisili' => $request->kecamatan_domisili ? $request->kecamatan_domisili : $request->kecamatan_domisili_ktp,

                'kategori_jenis_produk' => $request->kategori_jenis_produk,
                'jenis_produk' => $request->jenis_produk,
                'skala_pemasaran' => $request->skala_pemasaran,
                'status_bangunan' => $request->status_bangunan,
                'tahun_binaan' => $request->tahun_binaan,
                'sumber_permodalan' => $request->sumber_permodalan,
                'jumlah_permodalan' => $request->jumlah_permodalan,
                'kapasitas_produksi' => $request->kapasitas_produksi,
                'biaya_produksi' => $request->biaya_produksi,
                'jumlah_tenaga_kerja' => $request->jumlah_tenaga_kerja,
                // 'beras' => ($request->beras =>=> 1) ? 1 : 0,
                'ayam_bebek' => $request->ayam_bebek ? 1 : 0,
                'beras' => $request->beras ? 1 : 0,
                'buah' => $request->buah ? 1 : 0,
                'cabai' => $request->cabai ? 1 : 0,
                'daging' => $request->daging ? 1 : 0,
                'gula' => $request->gula ? 1 : 0,
                'kain' => $request->kain ? 1 : 0,
                'kayu' => $request->kayu ? 1 : 0,
                'kedelai' => $request->kedelai ? 1 : 0,
                'minyak' => $request->minyak ? 1 : 0,
                'sayur' => $request->sayur ? 1 : 0,
                'tepung' => $request->tepung ? 1 : 0,
                'telur' => $request->telur ? 1 : 0,
                'keanggotaan_koperasi' => $request->keanggotaan_koperasi,
                'status_keaktifan' => $request->status_keaktifan,
                'npwp' => $request->npwp,
                'nib' => $request->nib,
                'iumk' => $request->iumk,
                'pirt' => $request->pirt,
                'bpom' => $request->bpom,
                'sertifikat_merk' => $request->sertifikat_merk,
                'sertifikat_halal' => $request->sertifikat_halal,
                'haki' => $request->haki,
                'csr' => $request->csr ? 1 : null,
                'bpum' => $request->bpum ? 1 : null,
                'kur' => $request->kur ? 1 : null,
                'pameran' => $request->pameran ? 1 : null,
                'pasar' => $request->pasar ? 1 : null,
                'padat_karya' => $request->padat_karya ? 1 : null,
                'puspita' => $request->puspita ? 1 : null,
                'swk' => $request->swk ? 1 : null,
                'skg' => $request->skg ? 1 : null,
                'peken_tokel' => $request->peken_tokel ?? null,
                'peken_tokel' => $request->peken_ukm ?? null,
                'rumah_kreatif' => $request->rumah_kreatif ? 1 : null,
                'industri_rumahan' => $request->industri_rumahan ? 1 : null,
                'pelatihan' => $request->pelatihan ? 1 : null,
                'oss' => $request->oss ? 1 : null,
                'omset_bulanan' => $request->omset_awal,
                'keterangan' => $request->keterangan,
                'updated_by' => Auth::user()['id'],
            ]);
            DB::commit();

            return response()->json([
                "status" => true,
                "message" => "Data berhasil diupdate",
            ]);

            /* Inisiasi data untuk diinput ke dalam database */

            try {
                /* Mengambil data $pelaku_usaha berdasarkan $id */
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function update_keckel(Request $request, $id)
    {
        /* Membuat validasi sesusai dengan $request */
        $validator = Validator::make($request->all(), [
            "no_telp_usaha" => "required",
            "nama_usaha" => "required",
            "kategori_kbli" => "required",
            "produk_usaha" => "required",
            "jenis_produk" => "required",
            "skala_pemasaran" => "required",
            "status_bangunan" => "required",
            "tahun_binaan" => "required",
            "jumlah_permodalan" => "required",
            "kapasitas_produksi" => "required",
            "biaya_produksi" => "required",
            "jumlah_tenaga_kerja" => "required",
            "no_npwp_pemilik" => "required",
            "no_nib" => "required",
            "no_iumk" => "required",
            "no_pirt" => "required",
            "no_bpom" => "required",
            "sertifikat_merk" => "required",
            "haki" => "required",
        ]);

        /* Inisiasi nama atribut */
        $attrNames = array(
            "telp_usaha" => "Telp Usaha",
            "nama_usaha" => "Nama Usaha",
            "kategori_usaha" => "Kategori Usaha",
            "produk_usaha" => "Produk Usaha",
            "skala_pemasaran" => "Skala Pemasaran",
            "status_bangunan" => "Status Bangunan",
            "tahun_binaan" => "Tahun Binaan",
            "biaya_produksi" => "Biaya Produksi",
            "jumlah_tenaga_kerja" => "Jumlah Tenaga Kerja",
            "satus_anggota_koperasi" => "Status Anggota Koperasi",
            "sumber_keaktifan" => "Sumber Keaktifan",
            "no_npwp_pemilik" => "Nomor NPWP Pemilik",
            "no_nib" => "Nomor NIB",
            "no_iumk" => "Nomor IUMK",
            "no_pirt" => "Nomor PIRT",
            "no_bpom" => "Nomor BPOM",
            "sertifikat_merk" => "Sertifikat Merk",
            "haki" => "HAKI",
        );

        /* setAttributesName dengan parameter $attrNames ke dalam $validator */
        $validator->setAttributeNames($attrNames);

        DB::beginTransaction();
        $pelaku_usaha = TPelakuUsahaDisdag131022Model::find(Crypt::decryptString($id));
        /* Mengupdate data pelaku_usaha berdasarkan $id */
        $tgl_verifikasi_kec = Carbon::now();
        $pelaku_usaha->update([
            'is_surabaya' => $request->status,
            'nik' => $request->nik,
            'nama_pelaku_usaha' => $request->nama_pelaku_usaha,
            'alamat_ktp' => $request->alamat_ktp,
            'provinsi_pemilik' => $request->provinsi ? $request->provinsi : $request->id_m_setup_prop,
            'kabupaten_pemilik' => $request->kabupaten_kota ? $request->kabupaten_kota : $request->id_m_setup_kab,
            'kelurahan_pemilik' => $request->kelurahan,
            'kecamatan_pemilik' => $request->kecamatan,
            'no_telp' => $request->no_telp,
            'kecamatan_usaha' => $request->kecamatan_usaha,
            'kelurahan_usaha' => $request->kelurahan_usaha,
            'nama_usaha' => $request->nama_usaha,
            'alamat_usaha' => $request->alamat_usaha,
            'kelurahan_usaha' => $request->kelurahan_usaha,
            'kecamatan_usaha' => $request->kecamatan_usaha,
            'alamat_domisili' => $request->alamat_domisili,

            'kelurahan_domisili' => $request->kelurahan_domisili ? $request->kelurahan_domisili : $request->kelurahan_domisili_ktp,
            'kecamatan_domisili' => $request->kecamatan_domisili ? $request->kecamatan_domisili : $request->kecamatan_domisili_ktp,
            'status_keaktifan' => $request->status_keaktifan,

            'kategori_jenis_produk' => $request->kategori_jenis_produk,
            'jenis_produk' => $request->jenis_produk,
        ]);
        DB::commit();

        return response()->json([
            "status" => true,
            "message" => "Data berhasil diupdate",
        ]);
        if ($validator->passes()) {

            /* Inisiasi data untuk diinput ke dalam database */

            try {
                /* Mengambil data $pelaku_usaha berdasarkan $id */
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else {
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();
        $id = Crypt::decryptString($request->id);

        $cek_omset = TOmsetUsahaModel::where('id_t_pelaku_usaha', $id)->first();
        $pelaku_usaha = TPelakuUsahaDisdag131022Model::find($id);

        //NOTE CEK APAKAH ADA DATA OMSET PADA PELAKU USAHA
        if ($cek_omset) {
            return response()->json([
                'status' => 'error',
                'pesan'  => 'Maaf, Data Gagal Terhapus!',
                'desc'  => 'Terdapat data omset pada pelaku usaha!',
            ]);
        } else {
            if ($pelaku_usaha->delete()) {
                $pelaku_usaha->deleted_by = Auth::user()['id'];
                $pelaku_usaha->save();
                DB::commit();
                return response()->json([
                    'status' => 'success',
                    'pesan'  => 'Data Terhapus!',
                ]);
            }
        }
    }

    public function getSubKategoriUsaha(Request $request)
    {
        $m_kategori_usaha_id = $request->m_kategori_usaha_id;

        /* Memanggil relasi sub_kategori_usaha dari model MKategoriUsahaModel */
        $data = MKategoriUsahaModel::find($m_kategori_usaha_id)->sub_kategori_usaha;

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function getJenisTempatUsaha(Request $request)
    {
        $m_jenis_tempat_usaha_id = $request->m_jenis_tempat_usaha_id;

        /* Memanggil relasi tempat_usaha dari model MJenisTempatUsahaModel */
        $data = MJenisTempatUsahaModel::find($m_jenis_tempat_usaha_id)->tempat_usaha;

        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function cek_nik(Request $request)
    {
        if (!is_numeric($request->nik)) {
            return \Response::json(['error', 'format NIK tidak di izinkan !']);
        }

        $curl = curl_init();
        // $url = 'https://api.surabaya.go.id/integrasi/api/cek-nik';
        // $url = 'https://api.surabaya.go.id/integrasi/api/data-penduduk/cek-nik';
        $url = 'https://api.surabaya.go.id/integrasi/api/cekin/cek-nik';

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => $url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_SSL_VERIFYPEER => false,
        //     CURLOPT_SSL_VERIFYHOST => false,
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "POST",
        //     CURLOPT_POSTFIELDS => array('nik' => $request->nik),
        //     CURLOPT_HTTPHEADER => array(
        //         'user: kependudukan@2022',
        //         'password: 887trw'
        //     ),
        // ));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('nik' => $request->nik),
            CURLOPT_HTTPHEADER => array(
                'user: cekin',
                'password: cbG93Tba'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $data = "cURL Error #:" . $err;
        } else {
            $data = json_decode($response);
        }

        $response = $data;
        $req_nik = $request->nik;
        $data_nik = DB::table('t_pelaku_usaha_disdag_181022')
            ->select('nik')
            ->where('nik', $req_nik)
            ->whereNull('deleted_at')
            ->first();

        if ($data_nik && $request->status != 'update') {
            return \Response::json([
                'error' => 'Nomor Identitas Sudah Terdaftar !',
                'nik' => $request->nik
            ]);
        } else {
            if ($response->status == "2") {
                $anggota     = $response->details;
                foreach ($anggota as $angg) {
                    $nik_anggota = $angg->nik;
                    if ($nik_anggota == $request->nik) {
                        #Convert status kawin
                        if ($angg->status_kawin) {
                            switch ($angg->status_kawin) {
                                case 'BELUM KAWIN   ':
                                    $status = 1;
                                    break;
                                case 'KAWIN':
                                    $status = 2;
                                    break;
                                case 'CERAI HIDUP':
                                    $status = 3;
                                    break;
                                case 'CERAI MATI':
                                    $status = 4;
                                    break;
                                default:
                                    $status = null;
                                    # code...
                                    break;
                            }
                            // dd($status);
                        } else {
                            $status = null;
                        }


                        #CONVERT TANGGAL LAHIR
                        $tgl_lahir         = $angg->tgl_lahir;
                        $tgl_lahir_cvrt = date("Y-m-d", strtotime($tgl_lahir));

                        if (substr($response->kode_wilayah, 0, 4) == '3578') {
                            $provinsi_sby = 35;
                            $kabupaten_sby = 78;
                            $kecamatan_sby = intval(substr($response->kode_wilayah, 4, 2));
                            $kelurahan_sby = intval(substr($response->kode_wilayah, 6, 4));
                            $search = \DB::table('m_setup_prop')
                                ->select('m_setup_kel.*', 'm_setup_kec.nm_kec', 'm_setup_kel.nm_kel', 'm_setup_kab.nm_kab', 'm_setup_prop.nm_prop')
                                ->join('m_setup_kab', function ($join) {
                                    $join->on('m_setup_kab.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                                })
                                ->join('m_setup_kec', function ($join) {
                                    $join->on('m_setup_kec.id_m_setup_kab', '=', 'm_setup_kab.id_m_setup_kab')
                                        ->on('m_setup_kec.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                                })
                                ->join('m_setup_kel', function ($join) {
                                    $join->on('m_setup_kel.id_m_setup_kec', '=', 'm_setup_kec.id_m_setup_kec')
                                        ->on('m_setup_kel.id_m_setup_kab', '=', 'm_setup_kab.id_m_setup_kab')
                                        ->on('m_setup_kel.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                                })
                                ->where('m_setup_prop.id_m_setup_prop', $provinsi_sby)
                                ->where('m_setup_kab.id_m_setup_kab', $kabupaten_sby)
                                ->where('m_setup_kec.id_m_setup_kec', $response->id_kecamatan)
                                ->where('m_setup_kel.id_m_setup_kel', $response->id_kelurahan)
                                // ->where('m_setup_kec.nm_kec', $response->kecamatan)
                                // ->where('m_setup_kel.nm_kel', $response->kelurahan)
                                ->first();
                            // dd($search);
                            if ($search) {
                                $kecamatan_sby = $search->id_m_setup_kec;
                                $kelurahan_sby = $search->id_m_setup_kel;
                            }
                        } else {
                            $provinsi_sby  = null;
                            $kabupaten_sby = null;
                            $kecamatan_sby = null;
                            $kelurahan_sby = null;
                        }

                        $jadi = array(
                            'message'       => "OK",
                            'kode_wilayah'  => $response->kode_wilayah,
                            'no_kk'         => $response->no_kk,
                            // 'tgl_kk'         => $response->tanggal_kk,
                            'nik'           => $nik_anggota,
                            'nama_lengkap'  => $angg->nama_lengkap,
                            'nama_ayah'     => $angg->nama_ayah,
                            'pekerjaan'     => $angg->pekerjaan,
                            // 'provinsi'      => $response->provinsi,
                            // 'kota'          => $response->kabupaten,
                            // 'kecamatan'     => $response->kecamatan,
                            // 'kelurahan'     => $response->kelurahan,
                            'provinsi'      => $search->nm_prop ?? '',
                            'id_provinsi'   => $response->id_provinsi,
                            'kota'          => $search->nm_kab ?? '',
                            'id_kabupaten'  => $response->id_kabupaten,
                            'kecamatan'     => $search->nm_kec ?? '',
                            'id_kecamatan'  => $response->id_kecamatan,
                            'kelurahan'     => $search->nm_kel ?? '',
                            'id_kelurahan'  => $response->id_kelurahan,
                            'agama'         => $angg->agama,
                            'tempat_lahir'  => $angg->tempat_lahir,
                            'tgl_lahir'     => $tgl_lahir_cvrt,
                            'alamat'        => $response->alamat,
                            'rw'            => (int) $response->no_rw,
                            'rt'            => (int) $response->no_rt,
                            'jenis_kelamin' => $angg->jenis_kelamin,
                            'status_kawin'  => $angg->status_kawin,
                            'status_kawin2'  => $status,
                            'kewarganegaraan' => 'WNI',
                            'hub_keluarga'    => $angg->hub_keluarga,
                            'value_provinsi'  => $provinsi_sby,
                            'value_kabupaten' => $kabupaten_sby,
                            'value_kecamatan' => $kecamatan_sby,
                            'value_kelurahan' => $kelurahan_sby,
                        );
                        // dd($jadi);
                        return \Response::json($jadi);
                    }
                }
            } else {

                return \Response::json([
                    'error' => 'Nomor Identitas tidak ditemukan !',
                    'nik' => $request->nik
                ]);
            }
        }
    }

    public function cek_nik_update(Request $request)
    {
        // dd($request->nik);
        if (!is_numeric($request->nik)) {
            return \Response::json(['error', 'format NIK tidak di izinkan !']);
        }

        $curl = curl_init();
        // $url = 'https://api.surabaya.go.id/integrasi/api/cek-nik';
        // $url = 'https://api.surabaya.go.id/integrasi/api/data-penduduk/cek-nik';
        $url = 'https://api.surabaya.go.id/integrasi/api/cekin/cek-nik';

        // curl_setopt_array($curl, array(
        //     CURLOPT_URL => $url,
        //     CURLOPT_RETURNTRANSFER => true,
        //     CURLOPT_ENCODING => "",
        //     CURLOPT_SSL_VERIFYPEER => false,
        //     CURLOPT_SSL_VERIFYHOST => false,
        //     CURLOPT_MAXREDIRS => 10,
        //     CURLOPT_TIMEOUT => 30,
        //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        //     CURLOPT_CUSTOMREQUEST => "POST",
        //     CURLOPT_POSTFIELDS => array('nik' => $request->nik),
        //     CURLOPT_HTTPHEADER => array(
        //         'user: kependudukan@2022',
        //         'password: 887trw'
        //     ),
        // ));

        curl_setopt_array($curl, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => array('nik' => $request->nik),
            CURLOPT_HTTPHEADER => array(
                'user: cekin',
                'password: cbG93Tba'
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $data = "cURL Error #:" . $err;
            // $data['jenjang'] = $kategori;
        } else {
            $data = json_decode($response);
            // $data['jenjang'] = $kategori;
        }

        $response = $data;
        // dd($response);
        // dd($response->status);
        $req_nik = $request->nik;
        $data_nik = DB::table('t_pelaku_usaha_disdag_181022')
            ->select('nik')
            ->where('nik', $req_nik)
            ->first();

        // dd($array_nik);
        // $collect_nik = new Collection($array_nik);
        // dd($collect_nik);
        // for ($i = 0; $i < count($array_nik); $i++) {
        //     $data = $array_nik[$i]->nik;
        // }
        // dd($data);
        if ($response->status == "2") {
            $anggota     = $response->details;
            foreach ($anggota as $angg) {
                // dd($angg);
                $nik_anggota = $angg->nik;
                if ($nik_anggota == $request->nik) {
                    #Convert status kawin
                    // dd($angg);
                    if ($angg->status_kawin) {
                        switch ($angg->status_kawin) {
                            case 'BELUM KAWIN   ':
                                $status = 1;
                                break;
                            case 'KAWIN':
                                $status = 2;
                                break;
                            case 'CERAI HIDUP':
                                $status = 3;
                                break;
                            case 'CERAI MATI':
                                $status = 4;
                                break;
                            default:
                                $status = null;
                                # code...
                                break;
                        }
                        // dd($status);
                    } else {
                        $status = null;
                    }


                    #CONVERT TANGGAL LAHIR
                    $tgl_lahir         = $angg->tgl_lahir;
                    $tgl_lahir_cvrt = date("Y-m-d", strtotime($tgl_lahir));

                    if (substr($response->kode_wilayah, 0, 4) == '3578') {
                        $provinsi_sby = 35;
                        $kabupaten_sby = 78;
                        $kecamatan_sby = intval(substr($response->kode_wilayah, 4, 2));
                        $kelurahan_sby = intval(substr($response->kode_wilayah, 6, 4));
                        $search = \DB::table('m_setup_prop')
                            ->select('m_setup_kel.*', 'm_setup_kec.nm_kec', 'm_setup_kel.nm_kel', 'm_setup_kab.nm_kab', 'm_setup_prop.nm_prop')
                            ->join('m_setup_kab', function ($join) {
                                $join->on('m_setup_kab.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                            })
                            ->join('m_setup_kec', function ($join) {
                                $join->on('m_setup_kec.id_m_setup_kab', '=', 'm_setup_kab.id_m_setup_kab')
                                    ->on('m_setup_kec.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                            })
                            ->join('m_setup_kel', function ($join) {
                                $join->on('m_setup_kel.id_m_setup_kec', '=', 'm_setup_kec.id_m_setup_kec')
                                    ->on('m_setup_kel.id_m_setup_kab', '=', 'm_setup_kab.id_m_setup_kab')
                                    ->on('m_setup_kel.id_m_setup_prop', '=', 'm_setup_prop.id_m_setup_prop');
                            })
                            ->where('m_setup_prop.id_m_setup_prop', $provinsi_sby)
                            ->where('m_setup_kab.id_m_setup_kab', $kabupaten_sby)
                            ->where('m_setup_kec.id_m_setup_kec', $response->id_kecamatan)
                            ->where('m_setup_kel.id_m_setup_kel', $response->id_kelurahan)
                            // ->where('m_setup_kec.nm_kec', $response->kecamatan)
                            // ->where('m_setup_kel.nm_kel', $response->kelurahan)
                            ->first();
                        // dd($search);
                        if ($search) {
                            $kecamatan_sby = $search->id_m_setup_kec;
                            $kelurahan_sby = $search->id_m_setup_kel;
                        }
                    } else {
                        $provinsi_sby  = null;
                        $kabupaten_sby = null;
                        $kecamatan_sby = null;
                        $kelurahan_sby = null;
                    }

                    $jadi = array(
                        'message'       => "OK",
                        'kode_wilayah'  => $response->kode_wilayah,
                        'no_kk'         => $response->no_kk,
                        // 'tgl_kk'         => $response->tanggal_kk,
                        'nik'           => $nik_anggota,
                        'nama_lengkap'  => $angg->nama_lengkap,
                        'nama_ayah'     => $angg->nama_ayah,
                        'pekerjaan'     => $angg->pekerjaan,
                        // 'provinsi'      => $response->provinsi,
                        // 'kota'          => $response->kabupaten,
                        // 'kecamatan'     => $response->kecamatan,
                        // 'kelurahan'     => $response->kelurahan,
                        'provinsi'      => $search->nm_prop ?? '',
                        'id_provinsi'   => $response->id_provinsi,
                        'kota'          => $search->nm_kab ?? '',
                        'id_kabupaten'  => $response->id_kabupaten,
                        'kecamatan'     => $search->nm_kec ?? '',
                        'id_kecamatan'  => $response->id_kecamatan,
                        'kelurahan'     => $search->nm_kel ?? '',
                        'id_kelurahan'  => $response->id_kelurahan,
                        'agama'         => $angg->agama,
                        'tempat_lahir'  => $angg->tempat_lahir,
                        'tgl_lahir'     => $tgl_lahir_cvrt,
                        'alamat'        => $response->alamat,
                        'rw'            => (int) $response->no_rw,
                        'rt'            => (int) $response->no_rt,
                        'jenis_kelamin' => $angg->jenis_kelamin,
                        'status_kawin'  => $angg->status_kawin,
                        'status_kawin2'  => $status,
                        'kewarganegaraan' => 'WNI',
                        'hub_keluarga'    => $angg->hub_keluarga,
                        'value_provinsi'  => $provinsi_sby,
                        'value_kabupaten' => $kabupaten_sby,
                        'value_kecamatan' => $kecamatan_sby,
                        'value_kelurahan' => $kelurahan_sby,
                    );
                    // dd($jadi);
                    return \Response::json($jadi);
                }
            }
        } else {

            return \Response::json([
                'error' => 'Nomor Identitas tidak ditemukan !',
                'nik' => $request->nik
            ]);
        }
    }

    public function excel_omset(Request $request)
    {
        $data = TOmsetUsahaModel::select('t_pelaku_usaha_disdag_181022.nik', 't_pelaku_usaha_disdag_181022.nama_pelaku_usaha', 't_pelaku_usaha_disdag_181022.nama_usaha', 't_pelaku_usaha_disdag_181022.alamat_ktp',  't_pelaku_usaha_disdag_181022.kecamatan', 't_pelaku_usaha_disdag_181022.kelurahan', 't_pelaku_usaha_disdag_181022.rw_ktp', 't_pelaku_usaha_disdag_181022.rt_ktp', DB::raw("TO_CHAR(t_omset_usaha.bulan_laporan, 'MM/YYYY') AS bulan_laporan"), 't_omset_usaha.jumlah_omset',)
            ->leftJoin('t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022')
            ->orderBy('t_pelaku_usaha_disdag_181022.nik')
            ->get();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->nik = "'" . $data[$i]->nik;
        }

        $excel = Excel::download(new ExportAllOmset($data), 'omset_umkm_per' . '_' . date('Y-m-d') . '.xlsx', \Maatwebsite\Excel\Excel::XLSX);
        return $excel;
    }
}
