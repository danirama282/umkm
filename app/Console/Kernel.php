<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Commands\getPadatKaryaDaily::class,
        Commands\getPekenDaily::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        /* testing */
        // $schedule->command('daily:peken')->everyMinute()->timezone('Asia/Jakarta')->withoutOverlapping();
        // $schedule->command('daily:padat_karya')->everyMinute()->timezone('Asia/Jakarta')->withoutOverlapping();

        /* original */
        // $schedule->command('inspire')->hourly();

        /* add schedule command daily:padat_karya setTimeZone Asia/Jakarta */
        $schedule->command('daily:padat_karya')->dailyAt('05:00')->timezone('Asia/Jakarta')->withoutOverlapping();
        $schedule->command('daily:peken')->dailyAt('05:00')->timezone('Asia/Jakarta')->withoutOverlapping();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
