<?php

namespace Database\Seeders;

use App\Models\MJenisUsahaModel;
use Illuminate\Database\Seeder;

class MJenisUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /* Mengcreate m jenis usaha */
        $m_jenis_usaha = [
            [
                'nama_jenis_usaha' => 'SWK',
            ],
            [
                'nama_jenis_usaha' => 'SKG',
            ],
            [
                'nama_jenis_usaha' => 'Rumah Kreatif',
            ],
            [
                'nama_jenis_usaha' => 'UMKM',
            ],
            [
                'nama_jenis_usaha' => 'Tokel',
            ],
            [
                'nama_jenis_usaha' => 'Industri Rumahan',
            ],
            [
                'nama_jenis_usaha' => 'Pasar',
            ],
            [
                'nama_jenis_usaha' => 'UMKM Jahit',
            ],
        ];

        foreach ($m_jenis_usaha as $key => $value) {
            MJenisUsahaModel::create($value);
        }
    }
}
