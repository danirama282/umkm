<x-templates.default>
    <x-slot name="title">Master Jenis Usaha</x-slot>

    <!-- Konten baris keempat -->
    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="text-bold mt-2">Master Jenis Usaha</h5>
                    </div>
                    <div>
                        <a href="{{ route('jenis_usaha.create') }}" class="btn btn-primary">Tambah Jenis Usaha</a>
                    </div>
                </div>

                <div class="card-body ">
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Jenis Usaha</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

</x-templates.default>