<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TPelakuUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'x_t_pelaku_usaha';
    protected $fillable = [
        'nama_usaha',
        'm_jenis_usaha_det_id',
        'alamat_usaha',
        'kecamatan_usaha',
        'kelurahan_usaha',
        'rt_usaha',
        'rw_usaha',
        'telp_usaha',
        'tgl_berdiri_usaha',
        'm_status_bangunan_id',
        'nik_pemilik',
        'nama_pemilik',
        'alamat_pemilik',
        'kecamatan_pemilik',
        'kelurahan_pemilik',
        'rt_pemilik',
        'rw_pemilik',
        'telp_pemilik',
        'tgl_lahir_pemilik',
        'jenis_kelamin_pemilik',
        'status_mbr_pemilik',
        'akun_tokopedia',
        'akun_shopee',
        'no_npwp_pemilik',
        'no_npwp_usaha',
        'no_nop',
        'no_nib',
        'status_binaan_pemilik',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Relasi ke MKategoriUsahaModel */
    public function kategori_usaha()
    {
        return $this->belongsTo(MKategoriUsahaModel::class, 'm_kategori_usaha_id', 'id');
    }

    /* Relasi ke MJenisTempatUsahaModel */
    public function jenis_tempat_usaha()
    {
        return $this->belongsTo(MJenisTempatUsahaModel::class, 'm_jenis_tempat_usaha_id', 'id');
    }

    /* Relasi ke MSkalaUsahaModel */
    public function skala_usaha()
    {
        return $this->belongsTo(MSkalaUsahaModel::class, 'm_skala_usaha_id', 'id');
    }

    /* Relasi ke MSkalaPemasaranModel */
    public function skala_pemasaran()
    {
        return $this->belongsTo(MSkalaPemasaranModel::class, 'm_skala_pemasaran_id', 'id');
    }

    /* Mengambil getNmKecamatan Atribut dari MSetupKecModel dengan left join raw query */
    public function getNmKecamatanAttribute($data)
    {
        /* Where id_m_setup_prop == 35 dan id_ms_setup_kab == 78 */
        $data = \DB::select("SELECT * FROM m_setup_kec WHERE id_m_setup_prop = 35 AND id_m_setup_kab = 78 AND id_m_setup_kec = ?", [$this->kecamatan_usaha]);
        return $data[0]->nm_kec;
    }

    public function getNmKelurahanAttribute($data)
    {
        $nama_kelurahan = TPelakuUsahaModel::select('m_setup_kel.nm_kel')
            ->leftJoin(
                'm_setup_kec',
                'm_setup_kec.id_m_setup_kec',
                't_pelaku_usaha.kecamatan_usaha'
            )
            ->leftJoin(
                'm_setup_kel',
                function ($join) {
                    $join->on('m_setup_kel.id_m_setup_kec', '=', 'm_setup_kec.id_m_setup_kec');
                    $join->on('m_setup_kel.id_m_setup_kel', '=', 't_pelaku_usaha.kelurahan_usaha');
                }
            )
            ->where('m_setup_kec.id_m_setup_prop', '=', '35')
            ->where('m_setup_kec.id_m_setup_kab', '=', '78')
            ->where('m_setup_kel.id_m_setup_prop', '=', '35')
            ->where('m_setup_kel.id_m_setup_kab', '=', '78')
            ->where('m_setup_kel.id_m_setup_kec', '=', $data->kecamatan_usaha)
            ->where('m_setup_kel.id_m_setup_kel', '=', $data->kelurahan_usaha)
            ->first();

        return $nama_kelurahan->nm_kel;
    }

    /* Fungsi berisi raw query raw untuk mengcount jumlah data TPelakuUsahaModel */
    // public function scopeCountData()
    // {
    //     $data = \DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha_disdag_131022");
    //     return $data[0]->total;
    // }

    /* Fungsi berisi raw query untuk mengcount jumlah data TPelakuUsahaModel bersadarkan status_mbr_pemilik == MBR */
    public function scopeCountDataMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha WHERE status_mbr_pemilik = 'MBR' and deleted_at is null");
        return $data[0]->total;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data TPelakuUsahaModel bersadarkan status_mbr_pemilik == NON MBR */
    public function scopeCountDataNonMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS total FROM t_pelaku_usaha WHERE status_mbr_pemilik = 'NON MBR' and deleted_at is null");
        return $data[0]->total;
    }

    // fungsi berisi raw querry untuk mengcount jumlah data
    public function scopeCountJumlahProduksi($query)
    {
        $data = \DB::select("SELECT count(*) AS jum_produksi FROM t_pelaku_usaha WHERE m_kategori_usaha_id='1' and deleted_at is null");
        return $data[0]->jum_produksi;
    }

    public function scopeCountJumlahJasa($query)
    {
        $data = \DB::select("SELECT count(*) as jum_jasa from t_pelaku_usaha where m_kategori_usaha_id='2' and deleted_at is null");
        return $data[0]->jum_jasa;
    }

    public function scopeCountJumlahIndustri($query)
    {
        $data = \DB::select("SELECT count(*) as jum_industri from t_pelaku_usaha where m_kategori_usaha_id='3' and deleted_at is null");
        return $data[0]->jum_industri;
    }

    public function scopeCountJumlahTokel($query)
    {
        $data = \DB::select("SELECT count(*) as jum_tokel from t_pelaku_usaha where m_kategori_usaha_id='1' and m_sub_kategori_usaha_id='1' and deleted_at is null");
        return $data[0]->jum_tokel;
    }

    public function scopeCountJumlahTokelMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_tokel_mbr from t_pelaku_usaha where m_kategori_usaha_id='1' and m_sub_kategori_usaha_id='1' and status_mbr_pemilik='MBR' and deleted_at is null");
        return $data[0]->jum_tokel_mbr;
    }

    public function scopeCountJumlahTokenNonMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_tokel_non_mbr from t_pelaku_usaha where m_kategori_usaha_id='1' and m_sub_kategori_usaha_id='1' and status_mbr_pemilik='NON MBR' and deleted_at is null");
        return $data[0]->jum_tokel_non_mbr;
    }

    public function scopeCountJumlahJahit($query)
    {
        $data = \DB::select("SELECT count(*) as jum_jahit from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='2' and deleted_at is null");
        return $data[0]->jum_jahit;
    }

    public function scopeCountJumlahJahitMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_jahit_mbr from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='2' and status_mbr_pemilik='MBR' and deleted_at is null");
        return $data[0]->jum_jahit_mbr;
    }

    public function scopeCountJumlahJahitNonMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_jahit_non_mbr from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='2' and status_mbr_pemilik='NON MBR' and deleted_at is null");
        return $data[0]->jum_jahit_non_mbr;
    }

    public function scopeCountJumlahCuciSepatu($query)
    {
        $data = \DB::select("SELECT count(*) as jum_cuci_sepatu from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='3' and deleted_at is null");
        return $data[0]->jum_cuci_sepatu;
    }

    public function scopeCountJumlahCuciSepatuMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_cuci_sepatu_mbr from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='3' and status_mbr_pemilik='MBR' and deleted_at is null");
        return $data[0]->jum_cuci_sepatu_mbr;
    }

    public function scopeCountJumlahCuciSepatuNonMbr($query)
    {
        $data = \DB::select("SELECT count(*) as jum_cuci_sepatu_non_mbr from t_pelaku_usaha where m_kategori_usaha_id='2' and m_sub_kategori_usaha_id='3' and status_mbr_pemilik='NON MBR' and deleted_at is null");
        return $data[0]->jum_cuci_sepatu_non_mbr;
    }

    //checkpooint

    /* Fungsi berisi raw query untuk mengcount jumlah data CUCI HELM */
    public function scopeCountDataCuciHelm($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_cuci_helm FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '4' AND deleted_at IS NULL");
        return $data[0]->jum_cuci_helm;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data CUCI HELM MBR */
    public function scopeCountDataCuciHelmMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_cuci_helm_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '4' AND status_mbr_pemilik = 'MBR' AND deleted_at IS NULL");
        return $data[0]->jum_cuci_helm_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data CUCI HELM NON MBR */
    public function scopeCountDataCuciHelmNonMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_cuci_helm_non_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '4' AND status_mbr_pemilik = 'NON MBR' AND deleted_at IS NULL");
        return $data[0]->jum_cuci_helm_non_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data LAUNDRY */
    public function scopeCountDataJumlahLaundry($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_laundry FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '5' AND deleted_at IS NULL");
        return $data[0]->jum_laundry;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data LAUNDRY MBR */
    public function scopeCountDataJumlahLaundryMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_laundry_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '5' AND status_mbr_pemilik = 'MBR' AND deleted_at IS NULL");
        return $data[0]->jum_laundry_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data LAUNDRY NON MBR */
    public function scopeCountDataJumlahLaundryNonMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_laundry_non_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '2' AND m_sub_kategori_usaha_id = '5' AND status_mbr_pemilik = 'NON MBR' AND deleted_at IS NULL");
        return $data[0]->jum_laundry_non_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data SWK */
    public function scopeCountDataJumlahSwk($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_swk FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '6' AND deleted_at IS NULL");
        return $data[0]->jum_swk;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data SWK MBR */
    public function scopeCountDataJumlahSwkMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_swk_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '6' AND status_mbr_pemilik = 'MBR' AND deleted_at IS NULL");
        return $data[0]->jum_swk_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data SWK NON MBR */
    public function scopeCountDataJumlahSwkNonMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_laundry_non_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '6' AND status_mbr_pemilik = 'NON MBR' AND deleted_at IS NULL");
        return $data[0]->jum_laundry_non_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data UMKM */
    public function scopeCountDataJumlahUmkm($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_umkm FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '7' AND deleted_at IS NULL");
        return $data[0]->jum_umkm;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data UMKM MBR */
    public function scopeCountDataJumlahUmkmMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_umkm_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '7' AND status_mbr_pemilik = 'MBR' AND deleted_at IS NULL");
        return $data[0]->jum_umkm_mbr;
    }

    /* Fungsi berisi raw query untuk mengcount jumlah data UMKM NON MBR */
    public function scopeCountDataJumlahUmkmNonMbr($query)
    {
        $data = \DB::select("SELECT COUNT(*) AS jum_umkm_non_mbr FROM t_pelaku_usaha WHERE m_kategori_usaha_id = '3' AND m_sub_kategori_usaha_id = '7' AND status_mbr_pemilik = 'NON MBR' AND deleted_at IS NULL");
        return $data[0]->jum_umkm_non_mbr;
    }

    public function scopeCountSebaranUmkm($query)
    {
        $data = \DB::select(
            "SELECT x.nm_kec,
            swk_mbr.swk_mbr,
            swk_non_mbr.swk_non_mbr,
            skg_mbr.skg_mbr,
            skg_non_mbr.skg_non_mbr,
            rk_mbr.rk_mbr,
            rk_non_mbr.rk_non_mbr,
            peken_mbr.peken_mbr,
            peken_non_mbr.peken_non_mbr,
            ir_mbr.ir_mbr,
            ir_non_mbr.ir_non_mbr,
            pasar_mbr.pasar_mbr,
            pasar_non_mbr.pasar_non_mbr,
            umkm_jahit_mbr.umkm_jahit_mbr,
            umkm_jahit_non_mbr.umkm_jahit_non_mbr
        FROM
            m_setup_kec x
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS swk_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '1'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS swk_mbr ON swk_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS swk_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '1'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS swk_non_mbr ON swk_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS skg_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '2'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS skg_mbr ON skg_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS skg_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '2'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS skg_non_mbr ON skg_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS rk_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '3'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS rk_mbr ON rk_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS rk_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '3'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS rk_non_mbr ON rk_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS peken_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '4'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS peken_mbr ON peken_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS peken_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '4'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS peken_non_mbr ON peken_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS ir_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '5'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS ir_mbr ON ir_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS ir_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '5'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS ir_non_mbr ON ir_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS pasar_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '6'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS pasar_mbr ON pasar_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS pasar_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '6'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS pasar_non_mbr ON pasar_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS umkm_jahit_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '7'
                AND A.status_mbr_pemilik = 'MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS umkm_jahit_mbr ON umkm_jahit_mbr.kecamatan_pemilik = x.id_m_setup_kec
            LEFT JOIN (
            SELECT A
                .kecamatan_pemilik,
                COUNT ( A.* ) AS umkm_jahit_non_mbr
            FROM
                t_pelaku_usaha A
            WHERE
                A.m_jenis_tempat_usaha_id = '7'
                AND A.status_mbr_pemilik = 'NON MBR'
                AND A.deleted_at IS NULL
            GROUP BY
                A.kecamatan_pemilik
            ) AS umkm_jahit_non_mbr ON umkm_jahit_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
        WHERE
            x.id_m_setup_prop = '35'
            AND x.id_m_setup_kab = '78'
        GROUP BY
            x.nm_kec,
            swk_mbr.swk_mbr,
            swk_non_mbr.swk_non_mbr,
            skg_mbr.skg_mbr,
            skg_non_mbr.skg_non_mbr,
            rk_mbr.rk_mbr,
            rk_non_mbr.rk_non_mbr,
            peken_mbr.peken_mbr,
            peken_non_mbr.peken_non_mbr,
            ir_mbr.ir_mbr,
            ir_non_mbr.ir_non_mbr,
            pasar_mbr.pasar_mbr,
            pasar_non_mbr.pasar_non_mbr,
            umkm_jahit_mbr.umkm_jahit_mbr,
            umkm_jahit_non_mbr.umkm_jahit_non_mbr"
        );
        // dd($data);
    }

    public function scopeCountPenerimaCsr($query)
    {
        $data = \DB::select(
            "SELECT
                x.nama_perusahaan,
                jum_pelaku_usaha.jum_pelaku_usaha
            FROM
                m_perusahaan x
            LEFT JOIN
                (SELECT
                            b.m_perusahaan_id,
                            count(a.*) jum_pelaku_usaha
                    FROM
                            t_pelaku_usaha a
                            join t_bantuan_usaha b on a.id=b.t_pelaku_usaha_id
                    WHERE
                            a.deleted_at is null
                            and b.deleted_at is null
                            and m_perusahaan_id is null
                    GROUP BY
                            m_perusahaan_id) as jum_pelaku_usaha on jum_pelaku_usaha.m_perusahaan_id=x.id
            WHERE
                x.deleted_at is null
            GROUP BY
                x.nama_perusahaan,
                jum_pelaku_usaha.jum_pelaku_usaha"
        );

        return $data;
    }

    public function scopeCountWilayahSebaranUkm($query)
    {
        $data = \DB::select(
            "SELECT
                x.nm_kec,
                tokel_mbr.tokel_mbr,
                tokel_non_mbr.tokel_non_mbr,
                jahit_mbr.jahit_mbr,
                jahit_non_mbr.jahit_non_mbr,
                cuci_sepatu_mbr.cuci_sepatu_mbr,
                cuci_sepatu_non_mbr.cuci_sepatu_non_mbr,
                cuci_helm_mbr.cuci_helm_mbr,
                cuci_helm_non_mbr.cuci_helm_non_mbr,
                laundry_mbr.laundry_mbr,
                laundry_non_mbr.laundry_non_mbr,
                swk_mbr.swk_mbr,
                swk_non_mbr.swk_non_mbr,
                umkm_mbr.umkm_mbr,
                umkm_non_mbr.umkm_non_mbr
            FROM
                m_setup_kec x
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS tokel_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '1'
                    AND A.m_sub_kategori_usaha_id = '1'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS tokel_mbr ON tokel_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS tokel_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '1'
                    AND A.m_sub_kategori_usaha_id = '1'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS tokel_non_mbr ON tokel_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS jahit_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '2'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS jahit_mbr ON jahit_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS jahit_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '2'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS jahit_non_mbr ON jahit_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS cuci_sepatu_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '3'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS cuci_sepatu_mbr ON cuci_sepatu_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS cuci_sepatu_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '3'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS cuci_sepatu_non_mbr ON cuci_sepatu_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS cuci_helm_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '4'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS cuci_helm_mbr ON cuci_helm_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS cuci_helm_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '4'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS cuci_helm_non_mbr ON cuci_helm_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS laundry_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '5'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS laundry_mbr ON laundry_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS laundry_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '2'
                    AND A.m_sub_kategori_usaha_id = '5'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS laundry_non_mbr ON laundry_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS swk_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '3'
                    AND A.m_sub_kategori_usaha_id = '6'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS swk_mbr ON swk_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS swk_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '3'
                    AND A.m_sub_kategori_usaha_id = '6'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS swk_non_mbr ON swk_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS umkm_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '3'
                    AND A.m_sub_kategori_usaha_id = '7'
                    AND A.status_mbr_pemilik = 'MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS umkm_mbr ON umkm_mbr.kecamatan_pemilik = x.id_m_setup_kec
                LEFT JOIN (
                SELECT A
                    .kecamatan_pemilik,
                    COUNT ( A.* ) AS umkm_non_mbr
                FROM
                    t_pelaku_usaha A
                WHERE
                    A.m_kategori_usaha_id = '3'
                    AND A.m_sub_kategori_usaha_id = '7'
                    AND A.status_mbr_pemilik = 'NON MBR'
                    AND A.deleted_at IS NULL
                GROUP BY
                    A.kecamatan_pemilik
                ) AS umkm_non_mbr ON umkm_non_mbr.kecamatan_pemilik = x.id_m_setup_kec
            WHERE
                x.id_m_setup_prop = '35'
                AND x.id_m_setup_kab = '78'
            GROUP BY
                x.nm_kec,
                tokel_mbr.tokel_mbr,
                tokel_non_mbr.tokel_non_mbr,
                jahit_mbr.jahit_mbr,
                jahit_non_mbr.jahit_non_mbr,
                cuci_sepatu_mbr.cuci_sepatu_mbr,
                cuci_sepatu_non_mbr.cuci_sepatu_non_mbr,
                cuci_helm_mbr.cuci_helm_mbr,
                cuci_helm_non_mbr.cuci_helm_non_mbr,
                laundry_mbr.laundry_mbr,
                laundry_non_mbr.laundry_non_mbr,
                swk_mbr.swk_mbr,
                swk_non_mbr.swk_non_mbr,
                umkm_mbr.umkm_mbr,
                umkm_non_mbr.umkm_non_mbr"
        );

        return $data;
    }

    public function scopeGrafikStatusMBR()
    {
        $data = \DB::select("SELECT kec.nm_kec,
								tpu.jum_pemilik_mbr,
								tpu.jum_pemilik_non_mbr
						FROM m_setup_kec kec
							LEFT JOIN(
								SELECT
						kecamatan_pemilik,
						COUNT (

								CASE
								WHEN
											status_mbr_pemilik :: TEXT = 'MBR' :: TEXT AND deleted_at is NULL THEN id
								END) AS jum_pemilik_mbr,
						COUNT(
								CASE
								WHEN
											status_mbr_pemilik :: TEXT = 'NON MBR' :: TEXT AND deleted_at is NULL THEN id
								END) AS jum_pemilik_non_mbr
							FROM
								t_pelaku_usaha
						GROUP BY
						kecamatan_pemilik
							) tpu on kec.id_m_setup_kec = tpu.kecamatan_pemilik
							WHERE kec.id_m_setup_kab = 78 AND kec.id_m_setup_prop = 35
							ORDER BY kec.nm_kec desc");

        // dd($data);
        return $data;
    }

    public function scopeGrafikJenisUsaha()
    {
        $data = \DB::select("SELECT
							kec.nm_kec,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'SWK' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_swk,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'UMKM' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_umkm,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'LAUNDRY' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_laundry,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'CUCI HELM' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_cuci_helm,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'CUCI SEPATU' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_cuci_sepatu,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'JAHIT' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_jahit,
							COUNT ( CASE WHEN msku.nama_sub_kategori_usaha = 'TOKEL' AND tpu.deleted_at IS NULL THEN tpu.ID END ) AS jum_pemilik_tokel
						FROM
							m_setup_kec kec
							JOIN t_pelaku_usaha tpu ON kec.id_m_setup_kec = tpu.kecamatan_pemilik
							JOIN m_sub_kategori_usaha msku ON tpu.m_sub_kategori_usaha_id = msku.id
						WHERE
							kec.id_m_setup_kab = 78
							AND kec.id_m_setup_prop = 35
						GROUP BY
							kec.nm_kec
						ORDER BY
							kec.nm_kec desc");

        return $data;
    }
}
