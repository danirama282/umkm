<?php

namespace App\Http\Controllers;

use App\Models\MSetupKabModel;
use App\Models\MSetupKecModel;
use App\Models\MSetupKelModel;
use Illuminate\Http\Request;

class MWilayahController extends Controller
{
    public function getKabupaten(Request $request)
    {
        $id_prop = $request->id_prop;
        $kabupaten = MSetupKabModel::where('id_m_setup_prop', $id_prop)->get();
        return response()->json($kabupaten);
    }

    /* Mencari kelurahan berdasarkan $request dari function ajax di halaman pelaku_usaha.create id_m_setup_prop, id_m_setup_kab, id_m_setup_kec */
    public function getKelurahan(Request $request)
    {
        // dd($request->all());
        $id_m_setup_prop = $request->id_m_setup_prop ?  $request->id_m_setup_prop : '35';
        $id_m_setup_kab = $request->id_m_setup_kab ? $request->id_m_setup_kab : '78';
        $id_m_setup_kec = $request->id_m_setup_kec;
        // dd($id_m_setup_prop, $id_m_setup_kab, $id_m_setup_kec);

        $data = MSetupKelModel::select('id_m_setup_kel', 'nm_kel')->where('id_m_setup_prop', $id_m_setup_prop)->where('id_m_setup_kab', $id_m_setup_kab)->where('id_m_setup_kec', $id_m_setup_kec)->orderBy('nm_kel', 'ASC')->get();
        // dd($data->toArray());
        return response()->json([
            'status' => 'success',
            'data' => $data
        ]);
    }

    public function cari_kabupaten(Request $request)
    {
        try {
            $kabupaten_kota = MSetupKabModel::orderBy('nm_kab', 'asc')->where('id_m_setup_prop', $request->id_m_setup_prop)->get();
            return response()->json([
                'status'     => true,
                'data'      => $kabupaten_kota
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'pesan'     => 'Terjadi Kesalahan Teknis! Silahkan Coba Lagi.',
                // 'err'    => $e->getMessage()
            ]);
        }
    }

    public function cari_kecamatan(Request $request)
    {
        try {
            $kecamatan = MSetupKecModel::orderBy('nm_kec', 'asc')->where('id_m_setup_prop', $request->id_m_setup_prop)->where('id_m_setup_kab', $request->id_m_setup_kab)->get();
            return response()->json([
                'status'     => true,
                'data'      => $kecamatan
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'pesan'     => 'Terjadi Kesalahan Teknis! Silahkan Coba Lagi.',
                // 'err'    => $e->getMessage()
            ]);
        }
    }

    public function cari_kelurahan(Request $request)
    {
        // dd($request->all());
        try {
            $kelurahan = MSetupKelModel::orderBy('nm_kel', 'asc')->where('id_m_setup_prop', $request->id_m_setup_prop)->where('id_m_setup_kab', $request->id_m_setup_kab)->where('id_m_setup_kec', $request->id_m_setup_kec)->get();
            // dd($kelurahan);
            return response()->json([
                'status'     => 'success',
                'data'      => $kelurahan
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'pesan'     => 'Terjadi Kesalahan Teknis! Silahkan Coba Lagi.',
                // 'err'    => $e->getMessage()
            ]);
        }
    }
}
