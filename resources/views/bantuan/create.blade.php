<x-templates.default>
    <x-slot name="title">Form Entri Data Bantuan</x-slot>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header">
                    <h3 class="fw-semi-bold">Entri Data Bantuan</h3>
                </div>
                <div class="card-body ">
                    <form class="form fv-plugins-bootstrap5 fv-plugins-framework" id="form_submit_bantuan"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="pelaku_usaha_id" name="pelaku_usaha_id"
                            value="{{ $pelaku_usaha->id }}">
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="recipient-name">Tanggal
                                Menerima Bantuan</label>
                            <div class="col-sm-10">
                                <input class="form-select datetimepicker" id="tanggal_bantuan" name="tanggal_bantuan"
                                    type="date" />
                                <div class="fv-plugins-message-container invalid-feedback"></div>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                Bantuan</label>
                            <div class="col-sm-10">
                                <select class="form-select js-select2" id="jenis_bantuan" name="jenis_bantuan"
                                    data-control="select2" data-placeholder="- Pilih Jenis Bantuan -">
                                    <option></option>
                                    @foreach ($bantuan as $div)
                                        <option value="{{ $div->id }}">{{ $div->nama_jenis_bantuan }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row mb-3">
                            <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Apakah dengan
                                pihak Swasta</label>
                            <div class="col-sm-10">
                                <select class="form-select js-select2" id="pihak_swasta" name="pihak_swasta"
                                    data-control="select2" data-placeholder="- Pilih Jawaban -">
                                    <option></option>
                                    <option value="ya">Ya</option>
                                    <option value="tidak">Tidak</option>
                                </select>
                            </div>
                        </div>
                        <div id="swasta">
                            {{-- form nama perusahaan, akan muncul apabila pihak swasta = Ya --}}
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama
                                    Perusahaan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="nama_perusahaan" name="nama_perusahaan"
                                        data-control="select2" data-placeholder="- Pilih Nama Perusahaan -">
                                        <option></option>
                                        @foreach ($perusahaan as $div)
                                            <option value="{{ $div->id }}">{{ $div->nama_perusahaan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div id="binaan">
                            {{-- form binaan, akan muncul ketika jenis bantuan = PEMBINAAN --}}

                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Pembinaan
                                    </h6>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                    Binaan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="jenis_binaan" name="jenis_binaan"
                                        data-control="select2" data-placeholder="- Pilih Jenis Binaan -">
                                        <option></option>
                                        @foreach ($binaan as $div)
                                            <option value="{{ $div->id }}">{{ $div->nama_jenis_binaan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama
                                    Binaan</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Nama Binaan" name="nama_binaan" id="nama_binaan">
                                </div>
                            </div>
                        </div>
                        <div id="permodal">
                            {{-- form permodalan, akan muncul ketika jenis bantuan = PERMODALAN --}}

                            <div class="row mb-3">
                                <div class="col-sm-12">
                                    <h6 class="text-decoration-underline fs-0 fw-semi-bold text-left">Data Permodalan
                                    </h6>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jenis
                                    Permodalan</label>
                                <div class="col-sm-10">
                                    <select class="form-select js-select2" id="jenis_permodalan" name="jenis_permodalan"
                                        data-control="select2" data-placeholder="- Pilih Jenis Permodalan -">
                                        <option></option>
                                        @foreach ($permodalan as $div)
                                            <option value="{{ $div->id }}">{{ $div->nama_jenis_permodalan }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Nama Barang
                                    /
                                    Alat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Nama Barang" name="nama_barang" id="nama_barang">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm" for="message-text">Jumlah
                                    Barang /
                                    Alat</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        placeholder="Isi Jumlah Barang" name="jumlah_barang" id="jumlah_barang">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-sm-2 col-form-label col-form-label-sm"
                                    for="message-text">Nominal</label>
                                <div class="col-sm-10">
                                    <input type="text" class="form-control form-control-solid"
                                        onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))"
                                        data-inputmask="'alias': 'numeric','digits': 2,'groupSeparator': ',','removeMaskOnSubmit': true,'autoUnmask':true"
                                        placeholder="Isi Nominal" name="nominal" id="nominal">
                                </div>
                            </div>
                        </div>
                        <!--begin::Actions-->
                        <div class="d-flex justify-content-end">
                            <a href="{{ route('bantuan.index', Crypt::encryptString($pelaku_usaha->id)) }}"
                                class="btn btn-danger me-1 mb-1">
                                <span class="fas fa-backward" data-fa-transform="shrink-3"></span> Kembali
                            </a>
                            <button class="btn btn-primary me-1 mb-1" type="submit">
                                <span class="fas fa-save me-2" data-fa-transform="shrink-3"></span> Simpan
                            </button>
                        </div>
                        <!--end::Actions-->
                    </form>
                </div>
            </div>
        </div>
    </div>

    @push('scripts')
        {{-- script currency separator --}}
        <script>
            $(document).ready(function() {
                $('#nominal').inputmask();
            })
        </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.inputmask/5.0.7/jquery.inputmask.min.js"></script>

        <!-- Usage select2 -->
        <script>
            $(document).ready(function() {
                $(".js-select2").select2({
                    /* Tema untuk select2 bs 5 */
                    theme: "bootstrap-5",
                });
            });
        </script>

        <!-- Membuat ajax ketika submit formTambahJenisUsaha -->
        <script>
            $(document).ready(function() {
                $('#swasta').hide();
                $('#binaan').hide();
                $('#permodal').hide();
                // var id = $('#pelaku_usaha_id').val();
                $('#form_submit_bantuan').submit(function(e) {
                    e.preventDefault();
                    $.ajax({
                        url: "{{ route('bantuan.simpan_bantuan') }}",
                        type: "POST",
                        dataType: "json",
                        data: $('#form_submit_bantuan').serialize(),
                        success: function(data) {
                            if (data.status == true) {
                                Swal.fire({
                                    title: 'Berhasil',
                                    html: data.message,
                                    icon: 'success',
                                    confirmButtonText: 'OK'
                                }).then((result) => {
                                    if (result.isConfirmed) {
                                        window.location.href =
                                            "{{ route('bantuan.index', Crypt::encryptString($pelaku_usaha->id)) }}";
                                    }
                                });
                            } else {
                                Swal.fire({
                                    title: 'Gagal',
                                    html: data.message,
                                    icon: 'error',
                                    confirmButtonText: 'OK'
                                })
                            }
                        }
                    });
                });

                $('#pihak_swasta').on('change', function() {
                    //$('#swasta').show

                    s = $(this).val();

                    if (s == "ya") {
                        $('#swasta').show();
                    } else if (s == 'tidak') {
                        $('#swasta').hide();
                    }
                });

                $('#jenis_bantuan').on('change', function() {

                    b = $(this).val();
                    console.log(b);
                    if (b == 1) {
                        $('#binaan').show();
                        $('#permodal').hide();
                    } else if (b == 2) {
                        $('#binaan').hide();
                        $('#permodal').show();
                    }
                })
            });
        </script>
        <script>
            const form_submit_bantuan = document.getElementById('form_submit_bantuan');

            // Validator script untuk form submit divisi
            var validator_form_submit_bantuan = FormValidation.formValidation(
                form_submit_bantuan, {
                    fields: {
                        tanggal_bantuan: {
                            validators: {
                                notEmpty: {
                                    message: 'Tanggal Menerima Bantuan Harus Diisi !'
                                }
                            }
                        },
                        jenis_bantuan: {
                            validators: {
                                notEmpty: {
                                    message: 'Jenis Bantuan Harus Diisi !'
                                }
                            }
                        },
                        pihak_swasta: {
                            validators: {
                                notEmpty: {
                                    message: 'Status Kerja Sama Harus Diisi !'
                                }
                            }
                        },
                    },
                    plugins: {
                        trigger: new FormValidation.plugins.Trigger(),
                        bootstrap: new FormValidation.plugins.Bootstrap5({
                            rowSelector: '.fv-row',
                            eleInvalidClass: '',
                            eleValidClass: ''
                        })
                    }
                }
            );
        </script>
    @endpush
</x-templates.default>
