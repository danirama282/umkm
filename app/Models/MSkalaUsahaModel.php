<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MSkalaUsahaModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_skala_usaha';
    protected $fillable = [
        'nama_skala_usaha',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /* Relasi ke TPelakuUsahaModel */
    public function pelaku_usaha()
    {
        return $this->hasMany(TPelakuUsahaModel::class, 'm_skala_usaha_id', 'id');
    }
}
