<x-templates.default>
    <x-slot name="title">Dashboard</x-slot>

    <!-- Jika $data['user']->name == "walikotasby" maka akan menampilkan element card yang berisi ucapan selamat datang -->
    @if ($data['user']->name == 'walikotasby')
    <div class="row g-0 pt-3">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card h-lg-100">
                <div class="card-body position-relative">
                    <h5 class="text-dark">Selamat Datang, <span class="fw-bold">Yth. Bapak Wali Kota
                            Surabaya</span>!</h5>
                </div>
            </div>
        </div>
    </div>
    @endif

    @hasanyrole('superadmin|admin|user')
    <!-- BARIS KE 1 -->

    @include('home.detail_gamis')

    @include('home.card_um')

    <!-- Katergori Usaha -->
    <div class="row g-0 light flex-fill">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="card text-white bg-danger">
                <div class="card-body d-flex justify-content-center py-1">
                    <div class="card-title mb-0">
                        <h3 class="mt-2 text-white text-uppercase fs-3">KATEGORI USAHA</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Child Kategori Usaha -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-4 mb-2 ">
            <a href="#detailFashionModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailFashionModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/fashion-2.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Fashion</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_fashion }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-2">
            <a href="#detailTokoModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailTokoModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/toko.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Toko</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_toko }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-2 ps-lg-2">
            <a href="#detailKategoriMaminModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailKategoriMaminModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/mamin.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Makanan dan Minuman</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_mamin }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-4">
            <a href="#detailKerajinanModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailKerajinanModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/kerajinan.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Kerajinan</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_kerajinan }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-4 ps-lg-2">
            <a href="#detailBudidayaModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailBudidayaModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/sprout.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Budidaya Pertanian</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_budidaya }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-4 mb-4 ps-lg-2">
            <a href="#detailJasaModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailJasaModal">
                <div class="card border h-100 border-success" style="border: 6px solid #e63757!important;">
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <img src="{{ asset('dist/assets/img/icons/jasa.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Jasa</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kategori_usaha'][0]->jumlah_jasa }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="d-flex justify-content-center">
                <button class="btn btn-md bg-custom1 mb-3 text-white" id="tampil_usaha">Tampilkan</button>
            </div>
        </div>
    </div>

    <div id='usaha'>
        @include('home.kategori')
    </div>

    <!-- Kategori KBLI  -->
    <div class="row g-0 light flex-fill">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="card text-white bg-warning">
                <div class="card-body d-flex justify-content-center py-1">
                    <div class="card-title mb-0">
                        <h3 class="mt-2 text-white text-uppercase fs-3">KBLI</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Child Kategori KBLI -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-6 mb-2 ">
            <a href="#" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailIndustriPengolahanModal">
                <div class="card border h-100 border-warning">
                    <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-2">C</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <!-- <i class="fas fa-industry"></i> -->
                            <img src="{{ asset('dist/assets/img/icons/industri.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Industri Pengolahan</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kbli']['Industri Pengolahan'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-6 mb-2 ps-lg-2">
            <a href="#detailPerdaganganModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailPerdaganganModal">
                <div class="card border h-100 border-warning">
                    <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-2">G</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <!-- <i class="fas fa-warehouse"></i> -->
                            <img src="{{ asset('dist/assets/img/icons/dagang.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Perdagangan</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kbli']['Perdagangan'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-6 mb-4">
            <a href="#detailAkomodasiMaminModal" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailAkomodasiMaminModal">
                <div class="card border h-100 border-warning">
                    <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-2">I</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            <!-- <i class="fas fa-store"></i> -->
                            <img src="{{ asset('dist/assets/img/icons/mamin.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Akomodasi Mamin</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kbli']['Akomodasi Mamin'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-6 col-lg-6 mb-4 ps-lg-2">
            <a href="#detailAktivitasJasaLainnya" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#detailAktivitasJasaLainnya">
                <div class="card border h-100 border-warning">
                    <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-2">S</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right: 20px;">
                            {{-- <i class="fas fa-store"></i> --}}
                            <img src="{{ asset('dist/assets/img/icons/jasa.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Aktivitas Jasa Lainnya</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kbli']['Aktivitas Jasa'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
        <div class="col-sm-12 col-lg-12 mb-4 ps-lg-2">
            <a href="#pertanianKehutananPerikanan" class="text-decoration-none" data-bs-toggle="modal"
                data-bs-target="#pertanianKehutananPerikanan">
                <div class="card border h-100 border-warning">
                    <div class="text-white text-center bg-warning justify-content-center">
                        <div class="card-title mb-0">
                            <h3 class="mt-2 text-white text-uppercase fs-2">A</h3>
                        </div>
                    </div>
                    <div class="card-body d-flex">
                        <div class="d-flex fs-6" style="align-items: center; margin-right:20px;">
                            <img src="{{ asset('dist/assets/img/icons/pertanian.png') }}" alt="" width="70">
                        </div>
                        <div>
                            <div class="card-title mb-0">
                                <h3 class="display-6 mt-2 text-uppercase fs-2">Pertanian Kehutanan Perikanan</h3>
                            </div>
                            <div class="card-title mb-0">
                                <h3 class="fw-medium mt-2 text-uppercase fs-1">
                                    {{ $data['total_kbli']['Pertanian, Kehutanan dan Perikanan'] }}
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="row g-0 light flex-fill">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="d-flex justify-content-center">
                <button class="btn btn-md bg-warning mb-3 text-white" id="tampil_kbli">Tampilkan</button>
            </div>
        </div>
    </div>

    <div id="kbli">
        @include('home.kbli')
    </div>

    <!-- Intervensi -->
    <div class="row g-0 light flex-fill">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="card text-white bg-success">
                <div class="card-body d-flex justify-content-center py-1">
                    <div class="card-title mb-0">
                        <h3 class="mt-2 text-white text-uppercase fs-3">Intervensi</h3>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Child Intervensi-->
    @include('home.master_intervensi')

    <div class="row g-0 light flex-fill">
        <div class="col-sm-6 col-lg-12 mb-2">
            <div class="d-flex justify-content-center">
                <button class="btn btn-md bg-success mb-3 text-white" id="tampil_gamis_intervensi">Tampilkan</button>
            </div>
        </div>
    </div>

    <div id="gamis_intervensi">
        @include('home.gamis_intervensi')
    </div>

    @endrole

    @role('superadmin')
    <!-- BARIS KE 5 -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-12 mb-4">
            <div class="card border h-100 border-info">
                <div class="text-white d-flex bg-info justify-content-center">
                    <div class="card-title mb-0 fs-2">
                        <h3 class="display-6 mt-2 text-white text-center text-uppercase fs-2">Wilayah Sebaran
                            Intervensi UM</h3>
                    </div>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <div class="table table-responsive">
                        <table class="table table-bordered table-bordered font-sans-serif mb-0"
                            id="wilayah_sebaran_umkm">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr class="fs--2">
                                    <th class="text-center">NO.</th>
                                    <th class="text-center">KECAMATAN</th>
                                    <th class="text-center">SWK</th>
                                    <th class="text-center">SKG</th>
                                    <th class="text-center">PASAR</th>
                                    <th class="text-center">PEKEN</th>
                                    <th class="text-center">PELATIHAN</th>
                                    <th class="text-center">PAMERAN</th>
                                    <th class="text-center">BPUM</th>
                                    <!-- <th class="text-center">INDUSTRI RUMAHAN</th> -->
                                    <th class="text-center">RUMAH KREATIF</th>
                                    <th class="text-center">PADAT KARYA</th>
                                    <th class="text-center">KUR</th>
                                    <th class="text-center">CSR</th>
                                    <th class="text-center">PUSPITA</th>
                                    <th class="text-center">OSS</th>
                                    {{-- <th class="text-center">BELUM INTERVENSI</th> --}}
                                </tr>
                            </thead>
                            <tbody class="text-center fs--1">
                                <!-- ISI DATA -->
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-12 mb-4">
                        <div class="d-flex justify-content-center px-2">
                            <div class="card-title mb-0 fs-2 font-sans-serif">Grafik Wilayah Sebaran Intervensi UM
                            </div>
                        </div>
                        <div class="intervensi-um p-2" style="height: 950px;" data-echart-responsive="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BARIS KE 6 -->
    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-12 mb-4">
            <div class="card border h-100 border-danger">
                <div class="text-white d-flex bg-danger justify-content-center">
                    <div class="card-title mb-0 fs-2">
                        <h3 class="display-6 mt-2 text-white text-center text-uppercase fs-2">Wilayah Sebaran
                            Kategori Usaha</h3>
                    </div>
                </div>
                <div class="card-body d-flex justify-content-center">
                    <div class="table table-responsive">
                        <table class="table table-bordered table-bordered font-sans-serif mb-0"
                            id="datatabel_wilayah_sebaran_kategori_usaha">
                            <thead class="bg-200 text-900 border-tabel">
                                <tr class="fs--2">
                                    <th class="text-center">NO.</th>
                                    <th class="text-center">KECAMATAN</th>
                                    <th class="text-center">FASHION</th>
                                    <th class="text-center">TOKO</th>
                                    <th class="text-center">MAKANAN DAN MINUMAN</th>
                                    <th class="text-center">KERJAINAN</th>
                                    <th class="text-center">BUDIDAYA PERTANIAN</th>
                                    <th class="text-center">JASA</th>
                                </tr>
                            </thead>
                            <tbody class="text-center fs--1">
                                <!-- ISI DATA -->
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-lg-12 mb-4">
                        <div class="d-flex justify-content-center px-2">
                            <div class="card-title mb-0 fs-2 font-sans-serif">Grafik Wilayah Sebaran Kategori
                                Usaha
                            </div>
                        </div>
                        <div class="sebaran-kategori-usaha p-2" style="height: 950px;" data-echart-responsive="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endrole

    @include('home.modal');
    @includeIf('home.detail_kategori_usaha')
    @includeIf('home.detail_intervensi_um')
    @includeIf('home.detail_omset');

    <meta name="csrf-token" content="{{ csrf_token() }}" />

    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    <link href="{{ asset('dist/assets/css/custom-style.css') }}" rel="stylesheet" id="user-style-default">
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>


    <script>
        $(document).ready(function() {
                $('#kbli').hide();
                $('#usaha').hide();
                $('#gamis_intervensi').hide();
                $('#tampil_usaha').click(function() {
                    if ($('#usaha').is(':visible')) {
                        $(this).text('Tampilkan');
                    } else {
                        $(this).text('Sembunyikan');
                    }
                });
                $('#tampil_kbli').click(function() {
                    if ($('#kbli').is(':visible')) {
                        $(this).text('Tampilkan');
                    } else {
                        $(this).text('Sembunyikan');
                    }
                });
                $('#tampil_gamis_intervensi').click(function() {
                    if ($('#gamis_intervensi').is(':visible')) {
                        $(this).text('Tampilkan');
                    } else {
                        $(this).text('Sembunyikan');
                    }
                });
                $('#tampil_kbli').click(function(e) {
                    e.preventDefault();
                    $('#kbli').slideToggle('fast');
                });
                $('#tampil_usaha').click(function(e) {
                    e.preventDefault();
                    $('#usaha').slideToggle('fast');
                });
                $('#tampil_gamis_intervensi').click(function(e) {
                    e.preventDefault();
                    $('#gamis_intervensi').slideToggle('fast');
                });
            });
    </script>
    <script>
        $(function() {
                /* grafikstatusMBR(); */
                grafikInterverensi();
                var url = "{!! route('dataTablesWilayahDisdag') !!}";
                $('#wilayah_sebaran_umkm').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nm_kec',
                            name: 'nm_kec'
                        },
                        {
                            data: 'jumlah_swk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_skg_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pasar_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_tp_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pelatihan_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pameran_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_bpum_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        /*  {
                             data: 'jumlah_ir_non_mbr',
                             render: function(data, type, row) {
                                 if (data == null) {
                                     return 0;
                                 } else {
                                     return data;
                                 }

                             }
                         }, */
                        {
                            data: 'jumlah_rk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_padat_karya_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_kur_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_csr_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_puspita_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_oss_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        // {
                        //     data: 'jumlah_belum_intervensi',
                        //     render: function(data, type, row) {
                        //         if (data == null) {
                        //             return 0;
                        //         } else {
                        //             return data;
                        //         }

                        //     }
                        // },

                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
    </script>


    <script>
        $(function() {
                $('#resetUMKM').on('click', function() {
                    // console.log("masuk");
                    $('#jenis_usaha').prop('selectedIndex', 0);
                    $('#tahun_laporan').prop('selectedIndex', 0);
                });

                $('#searchUMKM').on('click', function() {
                    // console.log("masuk");

                    var jenis_usaha = $('#jenis_usaha').val();
                    var tahun_laporan = $('#tahun_laporan').val();

                    // console.log(tahun_laporan);

                    if (jenis_usaha != "" && tahun_laporan != "") {
                        var url = "{!! route('dataTotalOmset') !!}";

                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $('#omset_umkm').DataTable({
                            processing: true,
                            serverSide: true,
                            bDestroy: true,
                            ajax: {
                                url: url,
                                type: "POST",
                                data: {
                                    jenis_umkm: jenis_usaha,
                                    tahun_laporan: tahun_laporan
                                },
                            },
                            columns: [{
                                    data: 'DT_RowIndex',
                                    name: 'DT_RowIndex',
                                    orderable: false,
                                    searchable: false
                                },
                                {
                                    data: 'nama_sub_kategori_usaha',
                                    name: 'nama_sub_kategori_usaha'
                                },
                                {
                                    data: 'jum_omset_januari',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')

                                },
                                {
                                    data: 'jum_omset_februari',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_maret',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_april',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_mei',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_juni',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_juli',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_agustus',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_september',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_oktober',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_november',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },
                                {
                                    data: 'jum_omset_desember',
                                    render: $.fn.dataTable.render.number(',', '.', 0, 'Rp ')
                                },

                            ],
                            "pageLength": 10,
                            "lengthMenu": [
                                [5, 10, 25, 50, -1],
                                [5, 10, 25, 50, "All"]
                            ],
                            "language": {
                                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                                "infoFiltered": "(disaring dari _MAX_ total data)",
                                "lengthMenu": "Menampilkan _MENU_ data",
                                "search": "Cari:",
                                "zeroRecords": "Tidak ada data yang sesuai",
                                /* Kostum pagination dengan element baru */
                                "paginate": {
                                    "previous": "<i class='fas fa-angle-left'></i>",
                                    "next": "<i class='fas fa-angle-right'></i>"
                                }
                            },
                        });

                    }

                });


            });
    </script>

    <script>
        $(function() {
                var url = "{!! route('dataTablesPenerimaCsr') !!}";
                $('#penerima_csr').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nama_perusahaan',
                            name: 'nama_perusahaan'
                        },
                        {
                            data: 'jum_pelaku_usaha',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Menghilangkan penomoran dan hanya menampilkan tombol sebelumn sesudah */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }

                    },
                });
            });
    </script>

    <script>
        function grafikInterverensi() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: "{{ route('grafikInterverensi') }}",
                    dataType: 'json',
                    success: function(data) {
                        // console.log(data);
                        // Swal.close();
                        // jum_data_sw();
                        // data_valid();

                        data_kec = [];
                        data_swk = [];
                        data_skg = [];
                        data_pasar = [];
                        data_tp = [];
                        data_pelatihan = [];
                        data_pameran = [];
                        data_bpum = [];
                        /* data_ir = []; */
                        data_rk = [];
                        data_padat_karya = [];
                        data_kur = [];
                        data_csr = [];
                        data_puspita = [];
                        data_oss = [];
                        // data_belum_intervensi = [];

                        data.forEach(element => {
                            data_kec.push(element.nm_kec);
                            data_swk.push(element.jumlah_swk_non_mbr);
                            data_skg.push(element.jumlah_skg_non_mbr);
                            data_pasar.push(element.jumlah_pasar_non_mbr);
                            data_tp.push(element.jumlah_tp_non_mbr);
                            data_pelatihan.push(element.jumlah_pelatihan_non_mbr);
                            data_pameran.push(element.jumlah_pameran_non_mbr);
                            data_bpum.push(element.jumlah_pameran_non_mbr);
                            /* data_ir.push(element.jumlah_ir_non_mbr); */
                            data_rk.push(element.jumlah_rk_non_mbr);
                            data_padat_karya.push(element.jumlah_padat_karya_non_mbr);
                            data_kur.push(element.jumlah_kur_non_mbr);
                            data_csr.push(element.jumlah_csr_non_mbr);
                            data_puspita.push(element.jumlah_puspita_non_mbr);
                            data_oss.push(element.jumlah_oss_non_mbr);
                            // data_belum_intervensi.push(element.jumlah_belum_intervensi);
                        });
                        // console.log(data_kec);

                        // Grafik Status Jenis Usaha
                        var $horizontalStackChartEl = document.querySelector('.intervensi-um');
                        if ($horizontalStackChartEl) {
                            // Get options from data attribute
                            var userOptions = utils.getData($horizontalStackChartEl, 'options');
                            var chart = window.echarts.init($horizontalStackChartEl);
                            var kec = data_kec;

                            var getDefaultOptions = function getDefaultOptions() {
                                return {
                                    color: ['#1e88e5', '#b71c1c', '#fdd835', '#4caf50', '#f57c00', '#00bcd4',
                                        '#b388ff', '#795548', '#9e9d24', '#00796b','#40E0D0','#ff339f','#6020ff'
                                    ],
                                    tooltip: {
                                        trigger: 'axis',
                                        axisPointer: {
                                            type: 'shadow'
                                        },
                                        padding: [7, 10],
                                        backgroundColor: utils.getGrays()['100'],
                                        borderColor: utils.getGrays()['300'],
                                        textStyle: {
                                            color: utils.getColors().dark
                                        },
                                        borderWidth: 1,
                                        transitionDuration: 0,
                                        formatter: tooltipFormatter
                                    },
                                    toolbox: {
                                        feature: {
                                            magicType: {
                                                type: ['tiled']
                                            }
                                        },
                                        right: 0
                                    },
                                    legend: {
                                        data: ['SWK', 'SKG', 'PASAR', 'PEKEN', 'PELATIHAN', 'PAMERAN', 'BPUM',
                                            'RUMAH KREATIF', 'PADAT_KARYA', 'KUR', 'CSR', 'PUSPITA','OSS'
                                        ],
                                        textStyle: {
                                            color: utils.getGrays()['600']
                                        },
                                        // left: 0
                                    },
                                    xAxis: {
                                        type: 'value',
                                        axisLine: {
                                            show: true,
                                            lineStyle: {
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500']
                                        },
                                        splitLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['200']
                                            }
                                        }
                                    },
                                    yAxis: {
                                        type: 'category',
                                        data: data_kec,
                                        axisLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500'],
                                            // formatter: function formatter(value) {
                                            // return value.substring(0, 3);
                                            // }
                                        }
                                    },
                                    series: [{
                                        name: 'SWK',
                                        type: 'bar',
                                        barMinWidth: '100%',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_swk
                                    }, {
                                        name: 'SKG',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_skg
                                    }, {
                                        name: 'PASAR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pasar
                                    }, {
                                        name: 'PEKEN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_tp
                                    }, {
                                        name: 'PELATIHAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pelatihan
                                    }, {
                                        name: 'PAMERAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pameran
                                    }, {
                                        name: 'BPUM',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_bpum
                                    }, {
                                        name: 'RUMAH KREATIF',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_rk
                                    }, {
                                        name: 'PADAT_KARYA',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_padat_karya
                                    }, {
                                        name: 'KUR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_kur
                                    }, {
                                        name: 'CSR',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_csr
                                    }, {
                                        name: 'PUSPITA',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_puspita
                                    },{
                                        name: 'OSS',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_oss
                                    }, ],
                                    grid: {
                                        right: 7,
                                        left: 7,
                                        // bottom: 5,
                                        // top: '15%',
                                        containLabel: true
                                    }
                                };
                            };

                            echartSetOption(chart, userOptions, getDefaultOptions);
                        };

                    },
                    error: function(data) {
                        // Swal.close();

                        console.log(data);
                    }
                });
            }



            // Grafik Jumlah Omset
            var $lineAreaChartEl = document.querySelector('.jumlah-omset');
            if ($lineAreaChartEl) {
                // Get options from data attribute
                var userOptions = utils.getData($lineAreaChartEl, 'options');
                var chart = window.echarts.init($lineAreaChartEl);
                var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September',
                    'October', 'November', 'December'
                ];
                var data = [1142, 1160, 1179, 946, 1420, 1434, 986, 1247, 1051, 1297, 927, 1282];

                var _tooltipFormatter7 = function _tooltipFormatter7(params) {
                    return "\n      <div>\n          <h6 class=\"fs--1 text-700 mb-0\">\n            <span class=\"fas fa-circle me-1\" style='color:"
                        .concat(params[0].borderColor, "'></span>\n            ").concat(params[0].name, " : ").concat(
                            params[0].value, "\n          </h6>\n      </div>\n      ");
                };

                var getDefaultOptions = function getDefaultOptions() {
                    return {
                        tooltip: {
                            trigger: 'axis',
                            padding: [7, 10],
                            backgroundColor: utils.getGrays()['100'],
                            borderColor: utils.getGrays()['300'],
                            textStyle: {
                                color: utils.getColors().dark
                            },
                            borderWidth: 1,
                            formatter: _tooltipFormatter7,
                            transitionDuration: 0,
                            axisPointer: {
                                type: 'none'
                            }
                        },
                        xAxis: {
                            type: 'category',
                            data: months,
                            boundaryGap: false,
                            axisLine: {
                                lineStyle: {
                                    color: utils.getGrays()['300'],
                                    type: 'solid'
                                }
                            },
                            axisTick: {
                                show: false
                            },
                            axisLabel: {
                                color: utils.getGrays()['400'],
                                formatter: function formatter(value) {
                                    return value.substring(0, 3);
                                },
                                margin: 15
                            },
                            splitLine: {
                                show: false
                            }
                        },
                        yAxis: {
                            type: 'value',
                            splitLine: {
                                lineStyle: {
                                    color: utils.getGrays()['200']
                                }
                            },
                            boundaryGap: false,
                            axisLabel: {
                                show: true,
                                color: utils.getGrays()['400'],
                                margin: 15
                            },
                            axisTick: {
                                show: false
                            },
                            axisLine: {
                                show: false
                            },
                            min: 600
                        },
                        series: [{
                            type: 'line',
                            data: data,
                            itemStyle: {
                                color: utils.getGrays().white,
                                borderColor: utils.getColor('primary'),
                                borderWidth: 2
                            },
                            lineStyle: {
                                color: utils.getColor('primary')
                            },
                            showSymbol: false,
                            symbolSize: 10,
                            symbol: 'circle',
                            smooth: false,
                            hoverAnimation: true,
                            areaStyle: {
                                color: {
                                    type: 'linear',
                                    x: 0,
                                    y: 0,
                                    x2: 0,
                                    y2: 1,
                                    colorStops: [{
                                        offset: 0,
                                        color: utils.rgbaColor(utils.getColors().primary, 0.5)
                                    }, {
                                        offset: 1,
                                        color: utils.rgbaColor(utils.getColors().primary, 0)
                                    }]
                                }
                            }
                        }],
                        grid: {
                            right: '3%',
                            left: '3%',
                            bottom: '10%',
                            top: '5%'
                        }
                    };
                };

                echartSetOption(chart, userOptions, getDefaultOptions);
            }
            // END GRAFIK JUMLAH Omset
    </script>

    <script>
        $(function() {
                var url = "{!! route('dataTablesWilayahSebaranUkm') !!}";
                $('#wilayah_sebaran_ukm').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nm_kec',
                            name: 'nm_kec'
                        },
                        {
                            data: 'tokel_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }
                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);

                                $(td).css('background-color', '#bbdefb');
                            }
                        },
                        {
                            data: 'tokel_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }
                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#bbdefb');
                            }
                        },
                        {
                            data: 'jahit_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }

                        },
                        {
                            data: 'jahit_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_sepatu_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_sepatu_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_helm_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'cuci_helm_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'laundry_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'laundry_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#c8e6c9');
                            }
                        },
                        {
                            data: 'swk_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'swk_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'umkm_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },
                        {
                            data: 'umkm_non_mbr',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            },
                            createdCell: function(td, cellData, rowData, row, col) {
                                //console.log(rowData['target']);
                                // console.log(cellData);


                                $(td).css('background-color', '#ffcc80');
                            }
                        },

                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
    </script>

    {{-- START Datatabel sebaran kategori usaha --}}
    <script>
        $(function() {
                /* grafikstatusMBR(); */
                grafikSebaranKategoriUsaha();
                var url = "{!! route('dataTableSebaranKategoriUsaha') !!}";
                $('#datatabel_wilayah_sebaran_kategori_usaha').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: url,
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        {
                            data: 'nm_kec',
                            name: 'nm_kec'
                        },
                        {
                            data: 'jumlah_fashion',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_toko',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_mamin',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_kerajinan',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_pertanian',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                        {
                            data: 'jumlah_jasa',
                            render: function(data, type, row) {
                                if (data == null) {
                                    return 0;
                                } else {
                                    return data;
                                }

                            }
                        },
                    ],
                    "pageLength": 10,
                    "lengthMenu": [
                        [5, 10, 25, 50, -1],
                        [5, 10, 25, 50, "All"]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        /* Kostum pagination dengan element baru */
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });
            });
    </script>
    {{-- END Datatabel sebaran kategori usaha --}}

    {{-- Start Grafik sebaran kategori usaha --}}
    <script>
        function grafikSebaranKategoriUsaha() {
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: 'post',
                    url: "{{ route('grafikSebaranKategoriUsaha') }}",
                    dataType: 'json',
                    success: function(data) {
                        data_kec = [];
                        data_fashion = [];
                        data_toko = [];
                        data_mamin = [];
                        data_kerajinan = [];
                        data_pertanian = [];
                        data_jasa = [];

                        data.forEach(element => {
                            data_kec.push(element.nm_kec);
                            data_fashion.push(element.jumlah_fashion);
                            data_toko.push(element.jumlah_toko);
                            data_mamin.push(element.jumlah_mamin);
                            data_kerajinan.push(element.jumlah_kerajinan);
                            data_pertanian.push(element.jumlah_pertanian);
                            data_jasa.push(element.jumlah_jasa);
                        });
                        // console.log(data_kec);

                        // Grafik Status Jenis Usaha
                        var $horizontalStackChartEl = document.querySelector('.sebaran-kategori-usaha');
                        if ($horizontalStackChartEl) {
                            // Get options from data attribute
                            var userOptions = utils.getData($horizontalStackChartEl, 'options');
                            var chart = window.echarts.init($horizontalStackChartEl);
                            var kec = data_kec;

                            var getDefaultOptions = function getDefaultOptions() {
                                return {
                                    color: ['#1e88e5', '#f57c00', '#4caf50', '#b71c1c', '#fdd835', '#00bcd4' ],
                                    tooltip: {
                                        trigger: 'axis',
                                        axisPointer: {
                                            type: 'shadow'
                                        },
                                        padding: [7, 10],
                                        backgroundColor: utils.getGrays()['100'],
                                        borderColor: utils.getGrays()['300'],
                                        textStyle: {
                                            color: utils.getColors().dark
                                        },
                                        borderWidth: 1,
                                        transitionDuration: 0,
                                        formatter: tooltipFormatter
                                    },
                                    toolbox: {
                                        feature: {
                                            magicType: {
                                                type: ['tiled']
                                            }
                                        },
                                        right: 0
                                    },
                                    legend: {
                                        data: ['FASHION', 'TOKO', 'MAKANAN DAN MINUMAN', 'KERAJINAN',
                                            'PEMBUDIDAYA SEKTOR PERTANIAN', 'JASA'
                                        ],
                                        textStyle: {
                                            color: utils.getGrays()['600']
                                        },
                                        // left: 0
                                    },
                                    xAxis: {
                                        type: 'value',
                                        axisLine: {
                                            show: true,
                                            lineStyle: {
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500']
                                        },
                                        splitLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['200']
                                            }
                                        }
                                    },
                                    yAxis: {
                                        type: 'category',
                                        data: data_kec,
                                        axisLine: {
                                            lineStyle: {
                                                show: true,
                                                color: utils.getGrays()['300']
                                            }
                                        },
                                        axisTick: {
                                            show: false
                                        },
                                        axisLabel: {
                                            color: utils.getGrays()['500'],
                                        }
                                    },
                                    series: [{
                                        name: 'FASHION',
                                        type: 'bar',
                                        barMinWidth: '100%',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_fashion
                                    }, {
                                        name: 'TOKO',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_toko
                                    }, {
                                        name: 'MAKANAN DAN MINUMAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_mamin
                                    }, {
                                        name: 'KERAJINAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_kerajinan
                                    }, {
                                        name: 'PEMBUDIDAYA SEKTOR PERTANIAN',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_pertanian
                                    }, {
                                        name: 'JASA',
                                        type: 'bar',
                                        stack: 'total',
                                        label: {
                                            show: true,
                                            formatter: function(param) {
                                                return param.data == 0 ? '' : param.data;
                                            },
                                            // textStyle: {
                                            // 	color: '#fff'
                                            // }
                                        },
                                        emphasis: {
                                            focus: 'series'
                                        },
                                        data: data_jasa
                                    }, ],
                                    grid: {
                                        right: 7,
                                        left: 7,
                                        // bottom: 5,
                                        // top: '15%',
                                        containLabel: true
                                    }
                                };
                            };

                            echartSetOption(chart, userOptions, getDefaultOptions);
                        };

                    },
                    error: function(data) {
                        // Swal.close();

                        console.log(data);
                    }
                });
            }
    </script>
    {{-- END Grafik sebaran kategori usaha --}}
    @endpush

</x-templates.default>
