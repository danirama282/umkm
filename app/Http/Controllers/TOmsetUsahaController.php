<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\TOmsetUsahaModel;
use Yajra\DataTables\DataTables;
use App\Models\TPelakuUsahaDisdag131022Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\QueryBuilderExport;

class TOmsetUsahaController extends Controller
{
    public function index(Request $request, $id)
    {
        $id_disdag = Crypt::decryptString($id);
        $detail_pelaku_usaha = TPelakuUsahaDisdag131022Model::getDataOmsePelakuUsaha($id_disdag);
        // dd($detail_pelaku_usaha['data'][0]);
        $data = TOmsetUsahaModel::select('t_omset_usaha.*')
            ->leftJoin('t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022')
            ->where('t_omset_usaha.id_t_pelaku_usaha', $id_disdag)
            ->get();

        if ($request->ajax()) {
            return datatables()
                ->of($data)
                ->addIndexColumn()

                ->editColumn('bulan_laporan', function ($user) {
                    return [
                        'display' => Carbon::parse($user->bulan_laporan)->isoFormat('MMMM Y')
                    ];
                })

                ->addColumn('action', function ($data) {
                    return '
                    <div class="d-flex flex-column bd-highlight mb-3">
                        <a href="' . route('omset_usaha.edit', Crypt::encryptString($data->id)) . '" class="btn btn-sm btn-warning mb-2" title="Edit Data">
                        <span class="fas fa-edit" data-fa-transform="shrink-3"></span>Ubah Data Omset Usaha
                        </a>
                        <button class="btn btn-sm btn-danger" id="delete_data" data-id="' . Crypt::encryptString($data->id) . '" data-nama="" title="Hapus ">
                        <span class="fas fa-trash-alt" data-fa-transform="shrink-3"></span>Hapus
                        </button>
                    </div>
                    ';
                })

                ->rawColumns(['action'])
                ->make(true);
        }

        return view('omset_usaha.index', ['detail_pelaku_usaha' => $detail_pelaku_usaha['data']]);
    }

    public function create($id)
    {
        /* Memanggil data pelaku usaha dari database via Model */
        $id = Crypt::decryptString($id);
        // dd($id);
        $pelaku_usaha = TPelakuUsahaDisdag131022Model::select('t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022')
            ->where('t_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022', $id)
            ->first();
        // dd('ok');
        return view('omset_usaha.create', [
            'pelaku_usaha' => $pelaku_usaha
        ]);
    }

    public function store(Request $request)
    {
        // dd($request->all());
        $valid = $request->validate([
            'bulan_laporan' => 'required',
            'jumlah_omset'  => 'required',
        ]);

        $omset_usaha = new TOmsetUsahaModel();

        $omset_usaha->id_t_pelaku_usaha = $request->pelaku_usaha;
        $omset_usaha->bulan_laporan = $request->tahun . '-' . $request->bulan_laporan . '-01';
        $omset_usaha->jumlah_omset = Str::replace(',', '', $request->jumlah_omset);
        // dd($omset_usaha);
        $omset_usaha->save();

        /* Mengembalikan response json */
        return response()->json([
            'status'    => 'success',
            'message'   => 'Data berhasil disimpan!',
        ]);
    }

    public function edit($id)
    {
        // dd('ok');
        $id = Crypt::decryptString($id);
        $pelaku_usaha = TPelakuUsahaDisdag131022Model::select('t_pelaku_usaha_disdag_181022.* as id_pelaku_usaha', 't_omset_usaha.*')
            ->join('t_omset_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha')
            ->where('t_omset_usaha.id', $id)
            ->first();
        return view('omset_usaha.edit', compact('pelaku_usaha'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());

        $omset_usaha = TOmsetUsahaModel::find(Crypt::decryptString($id));

        $valid = $request->validate([
            'bulan_laporan_omset' => 'required',
            'tahun_laporan' => 'required',
            'jumlah_omset'  => 'required',
        ]);

        if ($valid) {
            try {
                // DB::beginTransaction();
                //Mengambil data bulan dan tahun di blade menjadi data Date
                $bulanlaporan = date('Y-m-d', strtotime($request->tahun_laporan . '-' . $request->bulan_laporan_omset . '-01'));
                /* Proses simpan ke database via model */
                // $id_pelaku_usaha = Crypt::decryptString($request->id_pelaku_usaha);
                // $omset_usaha->id_t_pelaku_usaha_disdag_181022 = $id_pelaku_usaha;
                $omset_usaha->bulan_laporan = $bulanlaporan;
                $omset_usaha->jumlah_omset = Str::replace(',', '', $request->jumlah_omset);

                // dd($omset_usaha);
                $omset_usaha->save();

                // DB::commit();

                /* Mengembalikan response json */
                return response()->json([
                    'status'    => 'success',
                    'message'   => 'Data berhasil disimpan!',
                ]);
            } catch (\Throwable $th) {
                return response()->json([
                    'status'    => 'error',
                    'message'   => 'Data gagal disimpan!',
                ]);
            }
        } else {
            /* return json dengan status check */
            return response()->json([
                'status'    => 'check',
                'message'   => 'Data tidak boleh kosong!',
            ]);
        }
    }

    public function destroy(Request $request)
    {
        DB::beginTransaction();
        $id = Crypt::decryptString($request->id);
        $omset_usaha = TOmsetUsahaModel::find($id);
        if ($omset_usaha->delete()) {
            DB::commit();
            return response()->json([
                'status' => true,
                'pesan'  => 'Data Terhapus!',
            ]);
        } else {
            return response()->json([
                'status' => false,
                'pesan'  => 'Maaf, Data Gagal Terhapus!',
            ]);
        }
    }

    public function excel($id)
    {
        $id_disdag = Crypt::decryptString($id);

        $detail_pelaku_usaha = TPelakuUsahaDisdag131022Model::getDataOmsePelakuUsaha($id_disdag);

        $data = TOmsetUsahaModel::select('t_pelaku_usaha_disdag_181022.nik', 't_pelaku_usaha_disdag_181022.nama_pelaku_usaha', 't_pelaku_usaha_disdag_181022.nama_usaha', 't_pelaku_usaha_disdag_181022.alamat_ktp',  't_pelaku_usaha_disdag_181022.kecamatan', 't_pelaku_usaha_disdag_181022.kelurahan', 't_pelaku_usaha_disdag_181022.rw_ktp', 't_pelaku_usaha_disdag_181022.rt_ktp', DB::raw("TO_CHAR(t_omset_usaha.bulan_laporan, 'MM/YYYY') AS bulan_laporan"), 't_omset_usaha.jumlah_omset',)
            ->leftJoin('t_pelaku_usaha_disdag_181022', 't_omset_usaha.id_t_pelaku_usaha', 't_pelaku_usaha_disdag_181022.id_t_pelaku_usaha_disdag_181022')
            ->where('t_omset_usaha.id_t_pelaku_usaha', $id_disdag)->get();

        for ($i = 0; $i < count($data); $i++) {
            $data[$i]->nik = "'" . $data[$i]->nik;
            // $data[$i]->nik = json_encode($data[$i]->nik);
        }

        // dd($data->toArray());

        return Excel::download(new QueryBuilderExport($data), 'omset_' . $data[0]->nama_pelaku_usaha . '_' . date('Y-m-d') . '.xlsx');
    }
}
