<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJenisBinaanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_jenis_binaan';
    protected $fillable = [
        'nama_jenis_binaan',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function binaan()
    {
        return $this->hasMany(BinaanModel::class, 'm_jenis_binaan_id', 'id');
    }
}
