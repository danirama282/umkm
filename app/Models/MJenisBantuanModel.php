<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJenisBantuanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_jenis_bantuan';
    protected $fillable = [
        'nama_jenis_bantuan',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function bantuan()
    {
        return $this->hasMany(BantuanModel::class, 'm_jenis_bantuan_id', 'id');
    }
}
