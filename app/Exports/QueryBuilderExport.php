<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use PhpOffice\PhpSpreadsheet\Shared\XMLWriter;

class QueryBuilderExport implements FromCollection, WithHeadings, ShouldAutoSize, WithEvents
{
    // use Exportabl
    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    public function collection()
    {
        return collect($this->data);
    }

    public function headings(): array
    {
        $prefix_col = $this->data[0];
        // dd($prefix_col->toArray());

        /* get index name from prefix_col */
        $index_name = array_keys($prefix_col->toArray());

        /* replace string and capitalize text */
        for ($i = 0; $i < count($index_name); $i++) {
            $index_name[$i] = strtoupper(str_replace('_', ' ', $index_name[$i]));

            // if ($index_name[$i] == 'nik') {
            //     /* textuppercase it */
            //     $index_name[$i] = strtoupper($index_name[$i]);
            // }

            // if ($index_name[$i] != 'nik') {
            //     $index_name[$i] = ucwords($index_name[$i]);
            // }
        }

        return $index_name;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->getDelegate()->getStyle('A1:J1')->applyFromArray([
                    'font' => [
                        'bold' => true,
                        'size' => 12,
                        'text-transform' => 'uppercase',
                    ],
                    'fill' => [
                        'fillType' => Fill::FILL_SOLID,
                        'startColor' => [
                            // 'rgb' => 'F2F2F2',
                            /* add rgb yellow */
                            'rgb' => 'FFFF00',
                        ],
                    ],
                    'borders' => [
                        'outline' => [
                            'borderStyle' => Border::BORDER_THIN,
                            'color' => ['rgb' => '000000'],
                        ],
                    ],
                    'alignment' => [
                        'horizontal' => Alignment::HORIZONTAL_CENTER,
                    ],
                ]);
            },
        ];
    }
}
