<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MStatusBangunanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat data untuk MStatusBangunanModel
        \DB::table('m_status_bangunan')->insert([
            'nama_status_bangunan' => 'SEWA',
            'created_at' => now(),
            'updated_at' => now(),
        ]);

        \DB::table('m_status_bangunan')->insert([
            'nama_status_bangunan' => 'MILIK SENDIRI',
            'created_at' => now(),
            'updated_at' => now(),
        ]);
    }
}
