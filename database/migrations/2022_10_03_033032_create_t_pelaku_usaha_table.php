<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTPelakuUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pelaku_usaha', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_usaha', 255);
            $table->string('alamat_usaha', 255);
            $table->unsignedInteger('kecamatan_usaha')->nullable()->default(null);
            $table->unsignedInteger('kelurahan_usaha')->nullable()->default(null);
            $table->string('rt', 255);
            $table->string('rw', 255);
            $table->string('telp_usaha', 255);
            $table->date('tgl_berdiri_usaha');
            $table->string('produk_usaha', 255);
            $table->string('nama_pemilik', 255);
            $table->string('nik_pemilik', 255);
            $table->string('alamat_pemilik', 255);
            $table->unsignedBigInteger('provinsi_pemilik')->nullable()->default(null);
            $table->unsignedBigInteger('kabupaten_pemilik')->nullable()->default(null);
            $table->unsignedBigInteger('kecamatan_pemilik')->nullable()->default(null);
            $table->unsignedBigInteger('kelurahan_pemilik')->nullable()->default(null);
            $table->string('rt_pemilik', 255);
            $table->string('rw_pemilik', 255);
            $table->string('telp_pemilik', 255);
            $table->date('tgl_lahir_pemilik');
            $table->boolean('jenis_kelamin_pemilik');
            $table->boolean('status_mbr_pemilik');
            $table->string('no_npwp_pemilik', 255);
            $table->string('no_npwp_usaha', 255);
            $table->string('no_nop', 255);
            $table->string('no_nib', 255);
            $table->string('akun_tokopedia', 255);
            $table->string('akun_shopee', 255);
            $table->string('status_binaan_pemilik')->nullable()->default(null);
            /* Membuat foreignid m_jenis_usaha_det_id dari tabel m_jenis_usaha_det */
            $table->unsignedBigInteger('m_jenis_usaha_det_id');
            $table->unsignedBigInteger('m_kategori_usaha_id')->nullable()->default(null);
            $table->unsignedBigInteger('m_sub_kategori_usaha_id')->nullable()->default(null);
            $table->unsignedBigInteger('m_jenis_tempat_usaha_id')->nullable()->default(null);
            $table->unsignedBigInteger('m_tempat_usaha_id')->nullable()->default(null);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pelaku_usaha');
    }
}
