<?php

namespace App\Http\Controllers;

use App\Models\Bantuan;
use App\Models\MJenisBantuanModel;
use App\Models\MJenisBinaanModel;
use App\Models\MJenisPermodalanModel;
use App\Models\MPerusahaanModel;
use App\Models\TBantuanUsahaModel;
use App\Models\TPelakuUsahaModel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;


class BantuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, $id, TBantuanUsahaModel $bantuanUsaha)
    {
        $id = Crypt::decryptString($id);

        // $data['anggota'] = T_anggota_divisi::find(Crypt::decryptString($id));
        // dd($id);
        // $data['id_divisi_kedua'] = M_divisi_det::where('id', $data['anggota']->id_divisi_det_kedua)->first();
        $pelaku_usaha = TPelakuUsahaModel::select(
            't_pelaku_usaha.*',
            'm_kategori_usaha.nama_kategori_usaha as nama_kategori_usaha',
            'm_sub_kategori_usaha.nama_sub_kategori_usaha',
            'm_jenis_tempat_usaha.nama_jenis_tempat_usaha as nama_jenis_tempat_usaha',
            'm_tempat_usaha.nama_jenis_tempat_usaha as nama_tempat_usaha',
            'm_setup_kec.nm_kec',
            'm_setup_kel.nm_kel'
        )
            ->leftJoin('m_kategori_usaha', 't_pelaku_usaha.m_kategori_usaha_id', 'm_kategori_usaha.id')
            ->leftjoin('m_sub_kategori_usaha', 't_pelaku_usaha.m_sub_kategori_usaha_id', 'm_sub_kategori_usaha.id')
            ->leftJoin('m_jenis_tempat_usaha', 't_pelaku_usaha.m_jenis_tempat_usaha_id', 'm_jenis_tempat_usaha.id')
            ->leftJoin('m_tempat_usaha', 't_pelaku_usaha.m_tempat_usaha_id', 'm_tempat_usaha.id')
            ->leftJoin('m_setup_kel', function ($join) {
                $join->on('m_setup_kel.id_m_setup_kel', 't_pelaku_usaha.kelurahan_pemilik');
                $join->on('m_setup_kel.id_m_setup_kec', 't_pelaku_usaha.kecamatan_pemilik');
                // $join->on('m_setup_kel.id_m_setup_kab','=', 78);
                // $join->on('m_setup_kel.id_m_setup_prop','=', 35);
            })
            ->leftJoin('m_setup_kec', function ($join) {
                $join->on('m_setup_kec.id_m_setup_kec', 't_pelaku_usaha.kecamatan_pemilik');
                // $join->on('m_setup_kec.id_m_setup_kab','=', 78);
                // $join->on('m_setup_kec.id_m_setup_prop','=', 35);
            })
            ->where('t_pelaku_usaha.id', $id)
            ->where([
                ['m_setup_kel.id_m_setup_kab', '=', '78'],
                ['m_setup_kel.id_m_setup_prop', '=', '35'],
            ])->where([
                ['m_setup_kec.id_m_setup_kab', '=', '78'],
                ['m_setup_kec.id_m_setup_prop', '=', '35'],
            ])->first();
        // dd($pelaku_usaha);


        if ($request->ajax()) {
            $bantuan = TBantuanUsahaModel::select('t_bantuan_usaha.id as idban', 't_bantuan_usaha.tanggal_menerima_bantuan as tgl_menerima_bantuan', 't_bantuan_usaha.m_jenis_bantuan_id as jenis_bantuan_id', 't_bantuan_usaha.m_jenis_permodalan_id as jenis_permodalan_id', 't_bantuan_usaha.m_jenis_binaan_id as jenis_binaan_id', 'm_jenis_bantuan.nama_jenis_bantuan as jenis_bantuan', 'm_perusahaan.nama_perusahaan as nama_perusahaan')
                ->leftJoin('m_jenis_bantuan', 'm_jenis_bantuan.id', 't_bantuan_usaha.m_jenis_bantuan_id')
                ->leftJoin('m_perusahaan', 'm_perusahaan.id', 't_bantuan_usaha.m_perusahaan_id')
                ->leftJoin('t_pelaku_usaha', 't_bantuan_usaha.t_pelaku_usaha_id', 't_pelaku_usaha.id')
                ->where('t_pelaku_usaha.id', $id)
                ->get();

            dd($bantuan);


            $datatable = [];

            foreach ($bantuan as $key => $value) {
                $datatable[$key]['id_bantuan'] = $value->idban;
                $datatable[$key]['tgl_menerima_bantuan'] = $value->tgl_menerima_bantuan;
                $datatable[$key]['jenis_bantuan'] = $value->jenis_bantuan;
                $datatable[$key]['nama_perusahaan'] = $value->nama_perusahaan;

                $select = "";
                $leftjoin = "";

                $where = "";

                // dd($value->jenis_bantuan_id);

                if ($value->jenis_bantuan_id == 1) {
                    $select = "SELECT m_jenis_binaan.nama_jenis_binaan AS jenis_binaan,
                    t_bantuan_usaha.nama_binaan AS nama_binaan";

                    $leftjoin = "LEFT JOIN m_jenis_binaan ON t_bantuan_usaha.m_jenis_binaan_id = m_jenis_binaan.id";

                    $where = " where m_jenis_binaan.id = " . $value->jenis_binaan_id . " AND t_bantuan_usaha.id = " . $value->idban;
                    // $where = " where m_jenis_binaan.id = ".$value->jenis_binaan_id." AND t_bantuan_usaha.id = 13";


                } else if ($value->jenis_bantuan_id == 2) {

                    // dd('masuk sini');

                    $select = "SELECT m_jenis_permodalan.nama_jenis_permodalan AS jenis_permodalan,
                    t_bantuan_usaha.nama_barang AS nama_barang,
                    t_bantuan_usaha.jumlah_barang AS jumlah_barang,
                    t_bantuan_usaha.nominal_binaan AS nominal";

                    $leftjoin = "LEFT JOIN m_jenis_permodalan ON t_bantuan_usaha.m_jenis_permodalan_id = m_jenis_permodalan.id";

                    $where = " where m_jenis_permodalan.id = " . $value->jenis_permodalan_id . " AND t_bantuan_usaha.id = " . $value->idban;

                    // $where = " where m_jenis_permodalan.id = ".$value->jenis_permodalan_id." AND t_bantuan_usaha.id = ".$value->idban;


                }


                $sql = $select . " FROM t_bantuan_usaha " . $leftjoin . " " . $where;

                // dd($sql);


                $data_bantuan = DB::select($sql);


                // $datatable[$key]['data_bantuan']= $data_bantuan;

                $ban = -1;

                // dd($data_bantuan);

                foreach ($data_bantuan as $val) {
                    foreach ($val as $i => $b) {
                        $ban++;

                        if ($i == "nama_barang") {
                            $b = "Nama Barang: " . $b;
                        } else if ($i == "jumlah_barang") {
                            $b = "Jumlah Barang: " . $b;
                        } else if ($i == "nama_binaan") {
                            $b = "Nama Binaan: " . $b;
                        } else if ($i == "jenis_binaan") {
                            $b = "Jenis Binaan: " . $b;
                        } else if ($i == "jenis_permodalan") {
                            $b = "Jenis Permodalan: " . $b;
                        } else if ($i == "nominal") {
                            $b = 'Rp. ' . number_format($b, 0, ',', '.');
                        }

                        $datatable[$key]['data_bantuan'][$ban] = $b;
                    }
                }





                // dd($data_bantuan);

                // $datatable[$key]['data_bantuan'] = '<ul class="list-group">
                //                                         <li class="list-group-item">'.News Feed.'</li>
                //                                         <li class="list-group-item">Messages</li>
                //                                         <li class="list-group-item">Events</li>
                //                                         <li class="list-group-item">Groups</li>
                //                                         <li class="list-group-item">Pages</li>
                //                                     </ul>';


            }

            // dd($datatable);

            $datatable = json_encode($datatable);

            $datatable = json_decode($datatable);


            // dd($datatable);



            return DataTables::of($datatable)
                ->addIndexColumn()
                ->editColumn('tgl_menerima_bantuan', function ($datatable) {
                    return $datatable->tgl_menerima_bantuan ? with(new Carbon($datatable->tgl_menerima_bantuan))->format('d/m/Y') : '';
                })
                ->addColumn('aksi', function ($row) {
                    return '
                    <div class="d-flex flex-column bd-highlight mb-3">
                        <a href="' . route('bantuan.edit_bantuan', ['id' => Crypt::encryptString($row->id_bantuan)]) . '" class="btn btn-sm btn-warning mb-2" title="Edit Data">
                        <span class="fas fa-edit" data-fa-transform="shrink-3"></span>Ubah Data Bantuan
                        </a>
                        <button class="btn btn-sm btn-danger" id="delete_data" data-id="' . Crypt::encryptString($row->id_bantuan) . '" data-nama="" title="Hapus ">
                        <span class="fas fa-trash-alt" data-fa-transform="shrink-3"></span>Hapus
                        </button>
                    </div>
                    ';
                })
                ->rawColumns(['aksi'])
                ->make(true);
        }

        // dd($pelaku_usaha->id);

        return view('bantuan.index', ['pelaku_usaha' => $pelaku_usaha]);
    }

    public function edit_bantuan($id)
    {
        $id = Crypt::decryptString($id);

        $jenisbantuan = MJenisBantuanModel::orderBy('id', 'desc')->get();
        $perusahaan = MPerusahaanModel::orderBy('id', 'desc')->get();
        $binaan = MJenisBinaanModel::orderBy('id', 'desc')->get();
        $permodalan = MJenisPermodalanModel::orderBy('id', 'desc')->get();

        $bantuan = TBantuanUsahaModel::where('id', $id)->first();

        // dd($bantuan);

        return view('bantuan.edit', [
            'bantuan' => $bantuan,
            'perusahaan' => $perusahaan,
            'binaan' => $binaan,
            'jenisbantuan' => $jenisbantuan,
            'permodalan' => $permodalan
        ]);
    }

    public function update_bantuan(Request $request, $id)
    {
        // dd($id);
        $id = Crypt::decryptString($id);
        // $id = Crypt::decryptString($request->id);

        // dd('masuk update');

        $validator = Validator::make($request->all(), [
            'tanggal_bantuan'   => 'required',
            'jenis_bantuan'     => 'required',
            'pihak_swasta'      => 'required',
        ]);

        $attrNames = array(
            'tanggal_bantuan'   => 'Tanggal Bantuan Usaha Diterima',
            'jenis_bantuan'     => 'Jenis Bantuan',
            'pihak_swasta'      => 'Status Kerjsama Dengan Pihak Usaha',
        );

        $validator->setAttributeNames($attrNames);

        if ($validator->passes())
        {
            DB::beginTransaction();

            try {
                $bantuan = TBantuanUsahaModel::find($id);
                // $pelaku_usaha_id = Crypt::decryptString($request->pelaku_usaha_id);
                $pelaku_usaha_id = Crypt::decryptString($request->pelaku_usaha_id);
                $bantuan->t_pelaku_usaha_id = $pelaku_usaha_id;

                // dd($bantuan);
                // dd($omset_usaha);
                /* Proses simpan ke database via model */
                // $bantuan->t_pelaku_usaha_id = $request->pelaku_usaha_id;
                $bantuan->m_jenis_bantuan_id = $request->jenis_bantuan;
                $bantuan->m_jenis_binaan_id = $request->jenis_binaan;
                $bantuan->m_jenis_permodalan_id = $request->jenis_permodalan;
                $bantuan->m_perusahaan_id = $request->nama_perusahaan;
                $bantuan->tanggal_menerima_bantuan = $request->tanggal_bantuan;
                $bantuan->status_kerjasama = $request->pihak_swasta;
                $bantuan->nama_binaan = $request->nama_binaan;
                if ($request->nominal != null) {
                    $request->nominal = str_replace(',', '', $request->nominal);
                }
                $bantuan->nominal_binaan = $request->nominal;
                $bantuan->nama_barang = $request->nama_barang;
                $bantuan->jumlah_barang = $request->jumlah_barang;
                $bantuan->save();

                DB::commit();

                /* Mengembalikan response json */
                return response()->json([
                    'status'    => true,
                    'message'   => 'Data berhasil dirubah!',
                ]);
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else{
            $custom_error = "";
            foreach ($validator->errors()->all() as $data) {
                /* Memasukkan element html tag <br> kedalam $custom_error */
                $custom_error .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_error
            ]);
        }
    }

    public function hapus_bantuan(Request $request)
    {
        DB::beginTransaction();
        $bantuan = TBantuanUsahaModel::find(Crypt::decryptString($request->id));
        if ($bantuan->delete()) {
            DB::commit();
            return response()->json([
                'status' => false,
                'pesan'  => "Data Terhapus",
            ]);
        } else {
            return response()->json([
                'status' => true,
                'pesan'  => 'Maaf, Data Gagal Terhapus'
            ]);
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function tambah_bantuan(Request $request)
    {
        $bantuan = MJenisBantuanModel::orderBy('id', 'desc')->get();
        $perusahaan = MPerusahaanModel::orderBy('id', 'desc')->get();
        $binaan = MJenisBinaanModel::orderBy('id', 'desc')->get();
        $permodalan = MJenisPermodalanModel::orderBy('id', 'desc')->get();
        // $pelaku_usaha = TPelakuUsahaModel::where('id',$request)->first();
        $pelaku_usaha = TPelakuUsahaModel::find(Crypt::decryptString($request->id));
        // dd($pelaku_usaha);
        return view('bantuan.create', ['pelaku_usaha' => $pelaku_usaha, 'bantuan' => $bantuan, 'perusahaan' => $perusahaan, 'binaan' => $binaan, 'permodalan' => $permodalan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function simpan_bantuan(Request $request)
    {
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'tanggal_bantuan'   => 'required',
            'jenis_bantuan'     => 'required',
            'pihak_swasta'      => 'required',
            // 'nama_perusahaan'   => 'required',
            // 'jenis_binaan'      => 'required',
            // 'nama_binaan'       => 'required',
            // 'jenis_permodalan'  => 'required',
            // 'nama_barang'       => 'required',
            // 'jumlah_barang'     => 'required',
            // 'nominal'           => 'required',
        ]);
        $attrNames = array(
            'tanggal_bantuan'   => 'Tanggal Bantuan',
            'jenis_bantuan'     => 'Jenis Bantuan',
            'pihak_swasta'      => 'Pihak swasta',
        );

        $validator->setAttributeNames($attrNames);

        if ($validator->passes()) {

            DB::beginTransaction();

            try {
                $bantuan = new TBantuanUsahaModel();

                $bantuan->t_pelaku_usaha_id = $request->pelaku_usaha_id;
                $bantuan->m_jenis_bantuan_id = $request->jenis_bantuan;
                $bantuan->m_jenis_binaan_id = $request->jenis_binaan;
                $bantuan->m_jenis_permodalan_id = $request->jenis_permodalan;
                $bantuan->m_perusahaan_id = $request->nama_perusahaan;
                $bantuan->tanggal_menerima_bantuan = $request->tanggal_bantuan;
                $bantuan->status_kerjasama = $request->pihak_swasta;
                $bantuan->nama_binaan = $request->nama_binaan;
                if ($request->nominal != null) {
                    $request->nominal = str_replace(',', '', $request->nominal);
                }
                $bantuan->nominal_binaan = $request->nominal;
                $bantuan->nama_barang = $request->nama_barang;
                $bantuan->jumlah_barang = $request->jumlah_barang;
                // dd($request->all());
                $bantuan->save();

                DB::commit();

                // return redirect('/admin/instansi/index');

                return response()->json(['status' => true, 'pesan'  => 'Data Bantuan Berhasil Disimpan',]);
                // $id = Crypt::decrypt($id);
            } catch (\Throwable $th) {
                DB::rollBack();

                return response()->json([
                    'status' => false,
                    'message' => 'Data gagal disimpan!',
                    'error' => $th->getMessage()
                ]);
            }
        } else {
            $custom_eror = "";
            foreach ($validator->errors()->all() as $data) {
                $custom_eror .= '<h6 class="font-sans-serif fs--1">' . $data . '</h6>';
            }

            return response()->json([
                'status' => false,
                'message' => $custom_eror
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Bantuan  $bantuan
     * @return \Illuminate\Http\Response
     */
    // public function data()
    // {
    //     // $bantuan = TBantuanUsahaModel::orderBy('created_at','desc')->get();
    //     $bantuan = TBantuanUsahaModel::leftJoin('m_jenis_bantuan','m_jenis_bantuan.id', 't_bantuan_usaha.m_jenis_bantuan_id')
    //         ->leftJoin('m_jenis_binaan', 'm_jenis_binaan.id', 't_bantuan_usaha.m_jenis_binaan_id')
    //         ->leftJoin('m_perusahaan', 'm_perusahaan.id', 't_bantuan_usaha.m_perusahaan_id')
    //         ->select('t_bantuan_usaha.*', 'm_jenis_bantuan.nama_jenis_bantuan', 'm_jenis_binaan.nama_jenis_binaan', 'm_perusahaan.nama_perusahaan')
    //         ->get();

    //     // return $bantuan;

    //     return datatables($bantuan)
    //         ->addIndexColumn()
    //         ->addColumn('aksi', function ($bantuan) {
    //             '<div class="btn-group">
    //                     <a href="' . route('bantuan.edit_bantuan.', ['id' => ($bantuan->id)]) . '" class="btn btn-sm btn-primary" title="Edit Bantuan ' . $bantuan->id . '">
    //                         <i class="fas fa-edit"></i>
    //                     </a>
    //                     <button class="btn btn-sm btn-danger" id="delete_data" data-id="' .($bantuan->id) . '" data-id="' . $bantuan->id . '" title="Hapus ' . $bantuan->id . '">
    //                         <i class="fas fa-trash-alt text-right"></i>
    //                     </button>
    //                 </div>';
    //         })
    //         ->rawColumns(['aksi'])
    //         ->make(true);

    //     }

}
