<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class MSubKategoriUsahaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create data di MSubKategoriUsahaModel
        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'TOKEL',
            'm_kategori_usaha_id' => 1,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'JAHIT',
            'm_kategori_usaha_id' => 2,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'CUCI SEPATU',
            'm_kategori_usaha_id' => 2,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'CUCI HELM',
            'm_kategori_usaha_id' => 2,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'LAUNDRY',
            'm_kategori_usaha_id' => 2,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'SWK',
            'm_kategori_usaha_id' => 3,
        ]);

        \App\Models\MSubKategoriUsahaModel::create([
            'nama_sub_kategori_usaha' => 'UMKM',
            'm_kategori_usaha_id' => 3,
        ]);
    }
}
