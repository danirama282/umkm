@foreach ($data['gamis_kbli'] as $key => $value)
<div class="card bg-warning mb-3 px-2 pb-2 h-100">
    <div class="row g-0">
        <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
            data-bs-target="#detailIntervensi"
            onclick="funDetailKbli('jiwa_kumulatif',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Kumulatif')">
            <h3 class="pt-3 text-white text-uppercase fs-3 text-center">{{ $value->nm_kategori }}</h3>
            <div class="d-flex align-items-center justify-content-center flex-column pb-3">
                <!-- tag aktivitas jasa -->
                @if (
                $value->jiwa_kumulatif_industri_pengolahan == 0 &&
                $value->kk_kumulatif_industri_pengolahan == 0 &&
                $value->jiwa_kumulatif_perdagangan == 0 &&
                $value->kk_kumulatif_perdagangan == 0 &&
                $value->jiwa_kumulatif_akomodasi_mamin == 0 &&
                $value->kk_kumulatif_akomodasi_mamin == 0 &&
                $value->jiwa_kumulatif_pertanian_kehutanan_perikanan == 0 &&
                $value->kk_kumulatif_pertanian_kehutanan_perikanan == 0)
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->jiwa_kumulatif_aktivitas_jasa }} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->kk_kumulatif_aktivitas_jasa }} KK
                </h3>
                <!-- tag perdagangan -->
                @elseif (
                $value->jiwa_kumulatif_industri_pengolahan == 0 &&
                $value->kk_kumulatif_industri_pengolahan == 0 &&
                $value->jiwa_kumulatif_akomodasi_mamin == 0 &&
                $value->kk_kumulatif_akomodasi_mamin == 0 &&
                $value->jiwa_kumulatif_aktivitas_jasa == 0 &&
                $value->kk_kumulatif_aktivitas_jasa == 0 &&
                $value->jiwa_kumulatif_pertanian_kehutanan_perikanan == 0 &&
                $value->kk_kumulatif_pertanian_kehutanan_perikanan == 0)
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->jiwa_kumulatif_perdagangan }} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->kk_kumulatif_perdagangan }} KK
                </h3>
                <!-- tag akomodasi mamin -->
                @elseif (
                $value->jiwa_kumulatif_industri_pengolahan == 0 &&
                $value->kk_kumulatif_industri_pengolahan == 0 &&
                $value->jiwa_kumulatif_perdagangan == 0 &&
                $value->kk_kumulatif_perdagangan == 0 &&
                $value->jiwa_kumulatif_aktivitas_jasa == 0 &&
                $value->kk_kumulatif_aktivitas_jasa == 0 &&
                $value->jiwa_kumulatif_pertanian_kehutanan_perikanan == 0 &&
                $value->kk_kumulatif_pertanian_kehutanan_perikanan == 0)
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->jiwa_kumulatif_akomodasi_mamin }} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->kk_kumulatif_akomodasi_mamin }} KK
                </h3>
                <!-- tag industri pengolahan -->
                @elseif (
                $value->jiwa_kumulatif_perdagangan == 0 &&
                $value->kk_kumulatif_perdagangan == 0 &&
                $value->jiwa_kumulatif_akomodasi_mamin == 0 &&
                $value->kk_kumulatif_akomodasi_mamin == 0 &&
                $value->jiwa_kumulatif_aktivitas_jasa == 0 &&
                $value->kk_kumulatif_aktivitas_jasa == 0 &&
                $value->jiwa_kumulatif_pertanian_kehutanan_perikanan == 0 &&
                $value->kk_kumulatif_pertanian_kehutanan_perikanan == 0)
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->jiwa_kumulatif_industri_pengolahan }} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->kk_kumulatif_industri_pengolahan }} KK
                </h3>
                <!-- tag pertanian, kehutanan, dan perikanan -->
                @elseif (
                $value->jiwa_kumulatif_industri_pengolahan == 0 &&
                $value->kk_kumulatif_industri_pengolahan == 0 &&
                $value->jiwa_kumulatif_perdagangan == 0 &&
                $value->kk_kumulatif_perdagangan == 0 &&
                $value->jiwa_kumulatif_akomodasi_mamin == 0 &&
                $value->kk_kumulatif_akomodasi_mamin == 0 &&
                $value->jiwa_kumulatif_aktivitas_jasa == 0 &&
                $value->kk_kumulatif_aktivitas_jasa == 0)
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->jiwa_kumulatif_pertanian_kehutanan_perikanan }} Jiwa
                </h3>
                <h3 class="text-white text-uppercase fs-1 text-center">
                    {{ $value->kk_kumulatif_pertanian_kehutanan_perikanan }} KK
                </h3>
                @endif
            </div>
        </a>
    </div>

    <div class="row g-0 light">
        <div class="col-sm-6 col-lg-6 mb-lg-0 mb-2">

            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#detailIntervensi"
                        onclick="funDetailKbli('jiwa_kumulatif_warkin',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Miskin Kumulatif')">
                        <div class="card-title mb-0">
                            <h6 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Miskin
                            </h6>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                            <h6 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->jiwa_kumulatif_warkin }} Jiwa
                            </h6>
                            <h6 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->kk_kumulatif_warkin }} KK
                            </h6>
                        </div>
                    </a>
                    <div class="d-flex justify-content-between align-items-center" style="gap: 16px;">
                        <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailIntervensi"
                            onclick="funDetailKbli('jiwa_misek_warkin',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Miskin Esktrim')">
                            <div class="card border h-100 border-warning w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h6 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Miskin
                                            Ekstrim
                                        </h6>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h6 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->jiwa_misek_warkin }} Jiwa
                                        </h6>
                                        <h6 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->kk_misek_warkin }} KK
                                        </h6>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailIntervensi"
                            onclick="funDetailKbli('jiwa_sub_warkin',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Miskin Non Esktrim')">
                            <div class="card border h-100 border-warning w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Miskin Non Ekstrim
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->jiwa_sub_warkin }} Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->kk_sub_warkin }} KK
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3 px-lg-2 px-0 mb-lg-0 mb-2">
            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#detailIntervensi"
                        onclick="funDetailKbli('jiwa_kumulatif_prakin',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Pra Miskin Kumulatif')">
                        <div class="card-title mb-0">
                            <h6 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Pra Miskin
                            </h6>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->jiwa_kumulatif_prakin }} Jiwa
                            </h3>
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->kk_kumulatif_prakin }} KK
                            </h3>
                        </div>
                    </a>
                    <div class="d-flex justify-content-between align-items-center">
                        <!-- <div class="card text-black bg-secondary">
                                                            <div class="card-body text-center" style="width: 300px;">
                                                                <div class="card-title mb-0">
                                                                    <h3 class="display-6 mt-2 text-black text-uppercase fs-1">
                                                                        Warga
                                                                        Miskin
                                                                        Ekstrim
                                                                    </h3>
                                                                </div>
                                                                <div class="card-title mb-0">
                                                                    <h3 class="fw-medium mt-2 text-black text-uppercase fs-1">
                                                                        XX </h3>
                                                                </div>
                                                            </div>
                                                        </div> -->
                        <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailIntervensi"
                            onclick="funDetailKbli('jiwa_sub_prakin',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Pra Miskin')">
                            <div class="card border h-100 border-warning w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Pra
                                            Miskin
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->jiwa_sub_prakin }} Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->kk_sub_prakin }} KK
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-lg-3">
            <div class="card bg-light">
                <div class="card-body text-center h-100">
                    <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                        data-bs-target="#detailIntervensi"
                        onclick="funDetailKbli('jiwa_kumulatif_lainnya',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Lainnya Kumulatif')">
                        <div class="card-title mb-0">
                            <h3 class="display-6 mt-2  text-uppercase fs-1">Warga
                                Lainnya
                            </h3>
                        </div>
                        <div class="card-title mb-0 pb-3">
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->jiwa_kumulatif_lainnya }} Jiwa
                            </h3>
                            <h3 class="mt-2 text-uppercase fs--1 text-center">
                                {{ $value->kk_kumulatif_lainnya }} KK
                            </h3>
                        </div>
                    </a>
                    <!-- <div style="height: 80px;">
                                                    </div> -->
                    <div class="d-flex justify-content-between align-items-center">
                        <a href="#detailIntervensi" class="text-decoration-none w-100" data-bs-toggle="modal"
                            data-bs-target="#detailIntervensi"
                            onclick="funDetailKbli('jiwa_kumulatif_lainnya',{{$value->id_m_kategori_kbli}},'{{$value->nm_kategori}}','Warga Lainnya')">
                            <div class="card border h-100 border-warning w-100">
                                <div class="card-body">
                                    <div class="card-title mb-0">
                                        <h3 class="display-6 mt-2 text-uppercase fs--1">
                                            Warga
                                            Lainnya
                                        </h3>
                                    </div>
                                    <div class="card-title mb-0">
                                        <!-- <h3 class="fw-medium mt-2 text-black text-uppercase fs--2"></h3> -->
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->jiwa_kumulatif_lainnya }} Jiwa
                                        </h3>
                                        <h3 class="mt-2 text-uppercase fs--2 text-center">
                                            {{ $value->kk_kumulatif_lainnya }} KK
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- <div class="col-sm-6 col-lg-12 mb-2 d-flex">
                                </div> -->
</div>
@endforeach

{{-- MODAL DETAIL INTERVENSI --}}
<div class="modal fade" id="detailIntervensi" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1" id="judul_modal_gamis_kbli"></h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="tabel_detailintervensi" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th width=5%>No.</th>
                                        <th width=10%>NIK</th>
                                        <th width=10%>Nama Pelaku Usaha</th>
                                        <th width=10%>Nama Usaha</th>
                                        <th width=10%>Alamat</th>
                                        <th width=10%>Kecamatan</th>
                                        <th width=10%>Kelurahan</th>
                                        <th width=2%>RT</th>
                                        <th width=2%>RW</th>
                                        <th width=5%>NIB</th>
                                        <th class="text-center" width=5%>#</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    function funDetailKbli(gamis,kbli,nm_kategori,jenis_gamis) {
        console.log(gamis,kbli,nm_kategori);
        $('#judul_modal_gamis_kbli').html('Detail Jiwa '+ jenis_gamis + ' '+ nm_kategori.toLowerCase().replace(/\b[a-z]/g, function(letter) {
            return letter.toUpperCase();
        }));

        $('#tabel_detailintervensi').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                "url": "{!! route('detailGamisKbli') !!}",
                "type": "POST",
                "data": {
                    gamis: gamis,
                    kbli: kbli
                }
            },
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex',
                    orderable: false,
                    searchable: false
                },
                {
                    data: 'nik',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_pelaku_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nama_usaha',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'alamat',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kecamatan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'kelurahan',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rw',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'rt',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'nib',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },
                {
                    data: 'aksi',
                    render: function(data, type, row) {
                        if (data == null) {
                            return "Tidak Ada";
                        } else {
                            return data;
                        }
                    }
                },

            ],
            "pageLength": 5,
            "lengthMenu": [
                [5, 10, 25, 50, -1],
                [5, 10, 25, 50, "All"]
            ],
            "language": {
                "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                "infoFiltered": "(disaring dari _MAX_ total data)",
                "lengthMenu": "Menampilkan _MENU_ data",
                "search": "Cari:",
                "zeroRecords": "Tidak ada data yang sesuai",
                /* Kostum pagination dengan element baru */
                "paginate": {
                    "previous": "<i class='fas fa-angle-left'></i>",
                    "next": "<i class='fas fa-angle-right'></i>"
                }
            },
            "bDestroy": true
        });

    }

    /* function funDetailKategori(request,kategori) {


        $.ajax({
            type: "POST",
            url: "{{ route('detailKategori') }}",
            data: {
                gamis: request,
                kategori: kategori
            },
            dataType: "JSON",
            success: function (response) {

            }
        });
    } */
</script>
@endpush