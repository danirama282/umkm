<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MSetupKecModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = "m_setup_kec";
    protected $primaryKey = "id_m_setup_kec";

    public $timestamps = true;
}
