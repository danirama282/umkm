<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MJenisPermodalanModel extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'm_jenis_permodalan';
    protected $fillable = [
        'nama_jenis_permodalan',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function permodalan()
    {
        return $this->hasMany(PermodalanModel::class, 'm_jenis_permodalan_id', 'id');
    }
}
