<?php

namespace Database\Seeders;

use App\Models\MSetupKecModel;
use App\Models\MSetupKelModel;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Membuat user bapenda dengan role superadmin
        User::create([
            'name' => 'bapenda',
            'email' => 'bapenda@test.com',
            'password' => bcrypt('!*2022bapenda'),
            'nama_user' => 'Bapenda',
        ])->assignRole('superadmin');

        // Membuat user bpsda dengan role superadmin
        User::create([
            'name' => 'bpsda',
            'email' => 'bpsda@test.com',
            'password' => bcrypt('!*2022bpsda'),
            'nama_user' => 'Bpsda',
        ])->assignRole('superadmin');

        /* get data_wilayah from MSetupKecModel innerJOin MSetupKelModel where id_m_setup_kab == 78 id_m_setup_prop == 35 */
        // $data_wilayah = MSetupKecModel::select('m_setup_kel.id_m_setup_kel', 'm_setup_kel.nm_kel')
        //     ->join('m_setup_kel', 'm_setup_kel.id_m_setup_kec', '=', 'm_setup_kec.id_m_setup_kec')
        //     ->where('m_setup_kel.id_m_setup_kab', 78)
        //     ->where('m_setup_kel.id_m_setup_prop', 35)
        //     ->distinct()
        //     ->get();

        /* add user berdasarkan $data_wilayah */
        // foreach ($data_wilayah as $key => $value) {
        //    $user = User::create([
        //         /* jika ada $value->nm_kel yang memiliki spasi, maka spasi akan dihilangkan */
        //         'name' => 'kel_' . Str::lower(str_replace(' ', '', $value->nm_kel)),
        //         'email' => 'kel_' . Str::lower(str_replace(' ', '', $value->nm_kel)) .'@test.com',
        //         'password' => bcrypt(Str::lower(str_replace(' ', '', $value->nm_kel)).'2022'),
        //         'id_m_setup_kel' => $value->id_m_setup_kel,
        //         'id_m_setup_kec' => $value->id_m_setup_kec,
        //         'nama_user' => Str::lower(str_replace(' ', '', $value->nm_kel)),
        //    ]);  

        //    $user->assignRole('user');
        // }
    }   
}
