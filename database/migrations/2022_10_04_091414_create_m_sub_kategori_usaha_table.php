<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMSubKategoriUsahaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_sub_kategori_usaha', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('m_kategori_usaha_id');
            $table->foreign('m_kategori_usaha_id')->references('id')->on('m_kategori_usaha');
            $table->string('nama_sub_kategori_usaha');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_sub_kategori_usaha');
    }
}
