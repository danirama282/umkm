<x-templates.default>
    <x-slot name="title">Data Monitor Pelaku Usaha</x-slot>
    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Monitor Pelaku Usaha</h5>
                    </div>
                </div>
                <div class="card-body position-relative">
                    <form id="formMonitorPelakuUsaha">
                        @csrf
                        <input type="hidden" name="id_m_setup_prop" value="{{ $data['provinsi']->id_m_setup_prop }}">
                        <input type="hidden" name="id_m_setup_kab" value="{{ $data['kabupaten'][0]->id_m_setup_kab }}">
                        <div class="row">
                            <div class="row mb-3">
                                <label class="col-12 col-md-2 col-lg-2 col-form-label col-form-label-sm"
                                    for="kecamatan">Kecamatan</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select name="kecamatan" id="kecamatan" class="form-select js-select2"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Pilih Kecamatan</option>
                                        @foreach ($data['kecamatan'] as $kecamatan)
                                        <option value="{{ $kecamatan->id_m_setup_kec }}">{{ $kecamatan->nm_kec }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label class="col-12 col-md-2 col-lg-2 col-form-label col-form-label-sm"
                                    for="kelurahan">Kelurahan</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select name="kelurahan" id="kelurahan" class="form-select js-select2"
                                        aria-label="Default select example">
                                        <option value="" selected disabled>Pilih Kelurahan</option>
                                    </select>
                                </div>
                            </div>
                            {{-- <div class="row mb-3">
                                <label for="" class="col-2 col-form-label col-form-label">Kategori KBLI</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="" id="">
                                        <option value=""></option>
                                    </select>
                                </div>
                            </div> --}}
                            <div class="row mb-3">
                                <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Kategori Usaha</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="kategori_usaha" id="kategori_usaha">
                                        <option value="" selected disabled>Pilih Kategori Usaha</option>
                                        <option value="FASHION">Fashion</option>
                                        <option value="JASA">Jasa</option>
                                        <option value="KERAJINAN">Kerajinan</option>
                                        <option value="MAKANAN DAN MINUMAN">Makanan Dan Minuman</option>
                                        <option value="PEMBUDIDAYA SEKTOR PERTANIAN">Pembudidaya Sektor Pertanian
                                        </option>
                                        <option value="TOKO">Toko</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Intervensi</label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="intervensi" id="intervensi">
                                        <option value="" selected disabled>Pilih Intervensi Usaha</option>
                                        <option value="csr">CSR</option>
                                        <option value="industri_rumahan">Industri Rumahan</option>
                                        <option value="kur">KUR</option>
                                        <option value="pameran">Pameran</option>
                                        <option value="pasar">Pasar</option>
                                        <option value="puspita">Puspita</option>
                                        <option value="padat_karya">Padat Karya</option>
                                        <option value="pelatihan">Pelatihan</option>
                                        <option value="peken_tokel">Peken</option>
                                        <option value="rumah_kreatif">Rumah Kreatif</option>
                                        <option value="swk">SWK</option>
                                        <option value="skg">SKG</option>
                                    </select>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <label for="" class="col-12 col-md-2 col-lg-2 col-form-label col-form-label">Bahan Baku Pokok
                                </label>
                                <div class="col-12 col-md-10 col-lg-10">
                                    <select class="form-select js-select2" name="bahan_baku" id="bahan_baku">
                                        <option value="" selected disabled>Pilih Bahan Baku Pokok Usaha</option>
                                        <option value="ayam_bebek">Ayam / Bebek</option>
                                        <option value="beras">Beras</option>
                                        <option value="buah">Buah</option>
                                        <option value="cabai">Cabai</option>
                                        <option value="daging">Daging</option>
                                        <option value="gula">Gula</option>
                                        <option value="kain">Kain</option>
                                        <option value="kayu">Kayu</option>
                                        <option value="kedelai">Kedelai</option>
                                        <option value="sayur">Sayur</option>
                                        <option value="telur">Telur</option>
                                        <option value="tepung">Tepung</option>
                                        <option value="minyak">Minyak</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="d-flex justify-content-end">
                            <a {{-- href="" --}} class="btn btn-secondary me-1 mb-1 reset">
                                <span data-fa-transform="shrink-3"></span> Reset
                            </a>
                            <a href="javascript:void(0)" class="btn btn-info me-1 mb-1 filter" type="button">
                                <span data-fa-transform="shrink-3"></span> Filter
                            </a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="row g-0">
        <div class="col-lg-12 col-xl-12 col-xxl-12 mb-3 ps-lg-2">
            <div class="card">
                <div class="card-header d-flex">
                    <div class="me-auto">
                        <h5 class="fw-bold fs-1 mt-2">Tabel Monitor Pelaku Usaha</h5>
                    </div>
                </div>

                <div class="card-body">
                    <div class="table table-responsive">
                        <table class="table table-bordered" id="dataTable">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    {{-- <th>NIK</th> --}}
                                    <th>Nama Pelaku Usaha</th>
                                    <th>Nama Usaha</th>
                                    <th>Kecamatan</th>
                                    <th>Kelurahan</th>
                                    <th>Kategori Usaha</th>
                                    {{-- <th>KBLI</th> --}}
                                    <th>Intervensi</th>
                                    <th>Bahan Baku</th>
                                    {{-- <th>Aksi</th> --}}
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @push('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.css" />
    @endpush

    @push('scripts')
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs5/dt-1.12.1/datatables.min.js"></script>

    <script>
        $('.filter').click(function() {
                $('#dataTable').DataTable().ajax.reload();

            });

            $('.reset').on("click", function() {
                /* $('#kecamatan').prop('selected', function() {
                    return this.defaultSelected;
                });
                $('#kelurahan').prop('selected', function() {
                    return this.defaultSelected;
                });
                $('#intervensi').prop('selected', function() {
                    return this.defaultSelected;
                });
                $('#bahan_baku').prop('selected', function() {
                    return this.defaultSelected;
                }); */

            $('#kecamatan').val('');
            $('#kecamatan').trigger('change');
            $('#kecamatan').val('');
            $('#intervensi').trigger('change');
            $('#intervensi').val('');
            $('#bahan_baku').trigger('change');
            $('#bahan_baku').val('');
            $('#kategori_usaha').trigger('change');
            $('#kategori_usaha').val('');
            $('#kelurahan').prop('disabled', true);
            $('#kelurahan').html('<option value="" selected disabled>Pilih Kelurahan</option>');
        });
    </script>


    <script>
        $('select[name="kelurahan"]').prop('disabled', true);

            $('[name="kecamatan"]').on('change', function() {
                var id_m_setup_kec = $(this).val();
                if (id_m_setup_kec) {
                    $.ajax({
                        url: "{{ route('getKelurahan') }}",
                        type: "POST",
                        dataType: "json",
                        data: {
                            id_m_setup_prop: $('[name="id_m_setup_prop"]').val(),
                            id_m_setup_kab: $('[name="id_m_setup_kab"]').val(),
                            id_m_setup_kec: id_m_setup_kec,
                            _token: "{{ csrf_token() }}",
                        },
                        success: function(data) {
                            console.log(data);
                            // melakukan foreach data dari kelurahan ke tampilan option
                            var render =
                                '<option value="" disabled>Pilih :</option>';

                            for (var i = 0; i < data.data.length; i++) {
                                render += '<option value="' + data.data[i].id_m_setup_kel + '">' + data
                                    .data[i].nm_kel + '</option>';
                            }
                            console.log(render);
                            $('[name="kelurahan"]').html(render);

                            // Menghilangkan atribut disabled pada select2 kelurahan
                            $('select[name="kelurahan"]').prop('disabled', false);
                        }
                    });
                } else {
                    $('select[name="kelurahan"]').empty();
                }
            });
    </script>

    <script>
        $(function() {
                var table = null;

                // var id = $('#pelaku_usaha_id').val();

                // console.log(id);

                var url = "{{ route('monitor_pu.datatable') }}";

                table = $('#dataTable').DataTable({
                    processing: true,
                    serverSide: true,
                    // ajax: url,
                    // type: "POST",
                    ajax: {
                        url,
                        type: 'POST',
                        // header: {
                        //     'X-CSRF-TOKEN':
                        // },
                        data: function(d) {
                            d.kecamatan = $('#kecamatan').val();
                            d.kelurahan = $('#kelurahan').val();
                            d.bahan_baku = $('#bahan_baku').val();
                            d.intervensi = $('#intervensi').val();
                            d.kategori_usaha = $('#kategori_usaha').val();
                            d._token = "{{ csrf_token() }}";
                            // d.custom = $('#myInput').val();
                            // etc
                        },
                    },
                    // data: function () {
                    //     tes = 11;
                    // },
                    columns: [{
                            data: 'DT_RowIndex',
                            name: 'DT_RowIndex',
                            orderable: false,
                            searchable: false
                        },
                        // {
                        //     data: 'nik',
                        //     name: 'nik'
                        // },
                        {
                            data: 'nama_pelaku_usaha',
                            name: 'nama_pelaku_usaha',
                        },
                        {
                            data: 'nama_usaha',
                            name: 'nama_usaha',
                        },
                        {
                            data: 'kecamatan',
                            name: 'kecamatan',
                        },
                        {
                            data: 'kelurahan',
                            name: 'kelurahan',
                        },
                        {
                            data: 'kategori_usaha',
                            name: 'kategori_usaha',

                        },
                        {
                            data: 'intervensi',
                            name: 'intervensi',
                            orderable: false,
                            searchable: false

                        },
                        {
                            data: 'bahan_baku',
                            name: 'bahan_baku',
                            orderable: false,
                            searchable: false

                        },
                        // {
                        //     data: 'aksi',
                        //     name: 'aksi',
                        //     orderable: false,
                        //     searchable: false
                        // },
                    ],
                    "pageLength": 5,
                    "lengthMenu": [
                        [5, 10, 25, 50],
                        [5, 10, 25, 50]
                    ],
                    "language": {
                        "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                        "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                        "infoFiltered": "(disaring dari _MAX_ total data)",
                        "lengthMenu": "Menampilkan _MENU_ data",
                        "search": "Cari:",
                        searchPlaceholder: "Nama Pelaku Usaha/Usaha",
                        "zeroRecords": "Tidak ada data yang sesuai",
                        "paginate": {
                            "previous": "<i class='fas fa-angle-left'></i>",
                            "next": "<i class='fas fa-angle-right'></i>"
                        }
                    },
                });


            });

            $('.filter').click(function() {
                $('#dataTable').DataTable().ajax.reload();

            });
    </script>
    @endpush
</x-templates.default>
