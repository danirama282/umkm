<div class="modal fade" id="detailOmsetModal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl" role="document">
        <div class="modal-content position-relative">
            <div class="position-absolute top-0 end-0 mt-2 me-2 z-index-1">
                <button class="btn-close btn btn-sm btn-circle d-flex flex-center transition-base"
                    data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body p-0">
                <div class="rounded-top-lg py-3 ps-4 pe-6 bg-light">
                    <!-- <h4 class="mb-1" id="modalExampleDemoLabel">Add a new illustration </h4> -->
                    <h5 class="fw-bold fs-1" id="judul_modal_detailOmset"></h5>
                </div>
                <div class="p-4 pb-2">
                    <div class="card-body position-relative bg-info text-black rounded">
                        <input type="hidden" name="id_omset" id="id_omset" {{--
                            value="{{ Crypt::encryptString(strval($detail_pelaku_usaha[0]->id)) }}" --}}>
                        <div class="row">
                            <div class="col-md-12 mb-0">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="col-form-label" for="nik">NIK Pemilik</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-form-label">:</label>
                                    </div>
                                    <div class="col-sm-3"><input class="form-control-plaintext outline-none" id="nik"
                                            name="nik" type="text" readonly="" {{--
                                            value="{{ $detail_pelaku_usaha[0]->nik }}" --}} />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 mb-0">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="col-form-label" for="namaPemilik">Nama Pemilik</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-form-label">:</label>
                                    </div>
                                    <div class="col-sm-3"><input class="form-control-plaintext outline-none"
                                            id="namaPemilik" name="namaPemilik" type="text" readonly="" {{--
                                            value="{{ $detail_pelaku_usaha[0]->nama_pelaku_usaha }}" --}} />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <label class="col-form-label" for="alamatPemilik">Alamat Pemilik</label>
                                    </div>
                                    <div class="col-sm-1">
                                        <label class="col-form-label">:</label>
                                    </div>
                                    <div class="col-sm-9"><input class="form-control-plaintext outline-none"
                                            id="alamatPemilik" name="alamatPemilik" type="text" readonly="" {{--
                                            value="{{ $detail_pelaku_usaha[0]->alamat_pemilik }}, RT {{ $detail_pelaku_usaha[0]->rt_pemilik }}, RW {{ $detail_pelaku_usaha[0]->rw_pemilik }}, Kelurahan  {{ $detail_pelaku_usaha[0]->kelurahan }}, Kecamatan  {{ $detail_pelaku_usaha[0]->kecamatan }}"
                                            --}} />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="p-4 pb-2">
                    <canvas id="detail-omset"></canvas>
                </div>
                <div class="p-4 pb-2">
                    <div class="table table-responsive">
                        <div class="table table-responsive">
                            <table class="table table-bordered font-sans-serif table-bordered fs--2 mb-0"
                                id="tabel_detailomset" style="width: 100%">
                                <thead class="bg-200 text-900 border-tabel">
                                    <tr>
                                        <th width=5%>No.</th>
                                        <th width=10%>Bulan Laporan Omset</th>
                                        <th width=10%>Jumlah Omset</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button class="btn btn-secondary" type="button" data-bs-dismiss="modal">Close</button> -->
                <button class="btn btn-danger me-1 mb-1" type="button" data-bs-dismiss="modal">
                    <span class="fas fa-window-close me-2" data-fa-transform="shrink-3"></span> Tutup
                </button>
                <!-- <button class="btn btn-primary" type="button"> </button> -->

            </div>
        </div>
    </div>
</div>

@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
<script>
    let modal = '#detailOmsetModal';

        // var $lineChartEl = document.querySelector('.echart-line-chart-example');
        // console.log('masuk');
        function detailNyo(nik, nama, alamat) {
            // console.log(params);
            $('#judul_modal_detailOmset').html('Detail Omset '+ nama.toLowerCase().replace(/\b[a-z]/g, function(letter) {
                return letter.toUpperCase();
            }));
            grafikDetailOmset(nik,nama);
            $(modal).modal('show');
            // console.log(nik, nama, alamat);
            $('#detailOmsetModal [name=nik]').val(nik)
            $('#detailOmsetModal [name=nik]').addClass('text-black')
            $('#detailOmsetModal [name=namaPemilik]').val(nama)
            $('#detailOmsetModal [name=namaPemilik]').addClass('text-black')
            $('#detailOmsetModal [name=alamatPemilik]').val(alamat)
            $('#detailOmsetModal [name=alamatPemilik]').addClass('text-black')
            // $('#detailFashionModal').modal('hide');

            $('#tabel_detailomset').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    "url": "{!! route('detailOmset') !!}",
                    "type": "POST",
                    "data": {
                        nik: nik,
                    }
                },
                columns: [{
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false
                    },
                    {
                        data: 'bulan_laporan',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                return data;
                            }
                        }
                    },
                    {
                        data: 'jumlah_omset',
                        render: function(data, type, row) {
                            if (data == null) {
                                return "Tidak Ada";
                            } else {
                                // return data;

                                /* convert format currency rp */
                                var reverse = data.toString().split('').reverse().join(''),
                                    ribuan = reverse.match(/\d{1,3}/g);
                                ribuan = ribuan.join('.').split('').reverse().join('');
                                return ribuan;
                            }
                        }
                    },
                ],
                "pageLength": 5,
                "lengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
                "language": {
                    "info": "Menampilkan _START_ sampai _END_ dari _TOTAL_ data",
                    "infoEmpty": "Menampilkan 0 sampai 0 dari 0 data",
                    "infoFiltered": "(disaring dari _MAX_ total data)",
                    "lengthMenu": "Menampilkan _MENU_ data",
                    "search": "Cari:",
                    "zeroRecords": "Tidak ada data yang sesuai",
                    /* Kostum pagination dengan element baru */
                    "paginate": {
                        "previous": "<i class='fas fa-angle-left'></i>",
                        "next": "<i class='fas fa-angle-right'></i>"
                    }
                },
                "bDestroy": true
            })
        }

        function grafikDetailOmset(nik,nama) {
            // console.log(nik,'ini nik');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                type: "post",
                url: "{!! route('detailOmsetGrafik') !!}",
                dataType: "json",
                "data": {
                        nik: nik,
                    },
                success: function (response) {
                    // console.log(response,'cek data detail omset');
                    const ctx = document.getElementById('detail-omset');

                    var bulan_laporan = [];
                    var jumlah_omset = [];
                    $.each(response, function (index, value) {
                        // console.log(bulan_laporan.includes(value.bulan_laporan));
                        if (bulan_laporan.includes(value.bulan_laporan) == true) {
                           /* jika ada bulan laporan yang sama, maka total_penjumlahan dari kedua bulan_laporan tersebut */
                            var index = bulan_laporan.indexOf(value.bulan_laporan);
                            jumlah_omset[index] = parseInt(jumlah_omset[index]) + parseInt(value.jumlah_omset);
                            return;
                        }
                        // console.log(bulan_laporan.filter(value.bulan_laporan));
                        bulan_laporan.push(value.bulan_laporan);
                        jumlah_omset.push(value.jumlah_omset);
                    });
                    // console.log(bulan_laporan,jumlah_omset);
                    // return;

                    let chartStatus = Chart.getChart("detail-omset");
                    if (chartStatus != undefined) {
                        chartStatus.destroy();
                    }

                    /* covert bulan_laporan format to month name */
                    bulan_laporan = bulan_laporan.map(function (item) {
                        var month = new Date(item).getMonth();
                        var year = new Date(item).getFullYear();
                        var monthName = new Date(item).toLocaleString('default', { month: 'long' });
                        var yearName = new Date(item).toLocaleString('default', { year: 'numeric' });
                        return monthName + ' ' + yearName;
                    });

                    new Chart(ctx, {
                        type: 'line',
                        data: {
                            labels: bulan_laporan,
                            datasets: [{
                                label: 'OMSET '+nama,
                                data: jumlah_omset,
                                borderWidth: 1
                            }]
                        },
                        options: {
                            scales: {
                                y: {
                                    beginAtZero: true
                                }
                            }
                        }
                    });
                }
            });
        }
</script>
@endpush